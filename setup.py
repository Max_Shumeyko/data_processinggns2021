from setuptools import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from setuptools import find_packages

extensions = [Extension("./tools/*", ["./tools/*.pyx"], 
                libraries=["m"])] # Unix-like specific]

classifiers=[
    'Operating System :: Linux',
    'Programming Language :: Python2.7',
]

config = {
    'name' : 'data_processingGNS2021',
    'description': 'data processing package for GNS setup',
    'author': 'Maxim Shumeyko',
    'url': 'https://gitlab.com/Max_Shumeyko/data_processingGNS2021.git',
    'download_url': 'Where to download it.',
    'author_email': 'eastwoodknight@yandex.ru',
    'version': '0.1',
    'install_requirments': ['nose'],
    'packages': find_packages(), 
    'scripts': [],
    'classifiers' : classifiers,

    'ext_modules' : cythonize(extensions),
}

setup(**config)

