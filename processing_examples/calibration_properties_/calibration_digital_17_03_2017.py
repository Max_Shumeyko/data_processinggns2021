# -*- coding: utf-8 -*-
"""
Created on Fri Mar 17 16:34:57 2017

@author: eastwood
"""
hist = spectr_front.data # first run read_data.py to get this distribution
xmin, xmax = 250, 600
def get_calibration_properties():#set calibration parameters

    calibration_properties = record()
    calibration_properties.visualize= True
    calibration_properties.weighted_average_sigma = False
    calibration_properties.dlt = 10 # параметр для определения области соответствия расчетного положения пиков (соотв. пропорции калибровочных энергий) и реальных найденных пиков (метод spectrum_tools.search_peaks), если реальные пики не находятся вблизи расчетных 
                                    # в области dlt, то они заменяются на расчетные, в противном случае выбирается пик наиболее близкий к расчетному. see calibration.calibrate_area
    #calibration_properties.energies = [6143,6264,6899.2,7137,7922,8699,9261]
    calibration_properties.energies = [6040,6264,7137,6899.2,8699,9261]#[7137,7922,8699,9261]#[6040,6143,6264,8699,9261]# 

    search_properties = record() #for noisy spectrum
    search_properties.widths=np.arange(1,5)
    search_properties.wavelet= 'blackman'
    search_properties.min_length=1.
    search_properties.min_snr=0.0
    search_properties.noise_perc=0.10 
    
    filter_properties = record()
    filter_properties.window_smooth=5
    filter_properties.smooth_wavelet='blackman'
    filter_properties.background_options='BACK1_ORDER8,BACK1_INCLUDE_COMPTON' 
    filter_properties.threshold= 4
    return calibration_properties, search_properties, filter_properties

#back
hist = spectr_back.data # first run read_data.py to get this distribution
xmin, xmax = 1000, 2500
def get_calibration_properties():#set calibration parameters

    calibration_properties = record()
    calibration_properties.visualize= True
    calibration_properties.weighted_average_sigma = False
    calibration_properties.dlt = 6 # параметр для определения области соответствия расчетного положения пиков (соотв. пропорции калибровочных энергий) и реальных найденных пиков (метод spectrum_tools.search_peaks), если реальные пики не находятся вблизи расчетных 
                                    # в области dlt, то они заменяются на расчетные, в противном случае выбирается пик наиболее близкий к расчетному. see calibration.calibrate_area
    #calibration_properties.energies = [6040,6143,6264,6899.2,7137,7922,8699,9261]
    calibration_properties.energies = [6040,6143,6264,6899.2,7137,7922,8699,9261]#[7137,7922,8699,9261]#[6040,6143,6264,8699,9261]# 

    search_properties = record() #for noisy spectrum
    search_properties.widths=np.arange(1,5)
    search_properties.wavelet= 'blackman'
    search_properties.min_length=1.
    search_properties.min_snr=0.0
    search_properties.noise_perc=0.10 
    
    filter_properties = record()
    filter_properties.window_smooth=7
    filter_properties.smooth_wavelet='blackman'
    filter_properties.background_options='BACK1_ORDER8,BACK1_INCLUDE_COMPTON' 
    filter_properties.threshold= 5
    return calibration_properties, search_properties, filter_properties
    
calibration_properties, search_properties, filter_properties = get_calibration_properties()