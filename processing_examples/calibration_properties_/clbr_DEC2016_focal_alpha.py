# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 19:12:38 2018

@author: eastwood
"""

class record: pass
    
def get_calibration_properties():#set calibration parameters
    
    calibration_properties = record()
    calibration_properties.visualize = True
    calibration_properties.weighted_average_sigma = False
    
    # параметр для определения области соответствия расчетного положения пиков 
    # (соотв. пропорции калибровочных энергий) и реальных найденных пиков 
    # (метод spectrum_tools.search_peaks), если реальные пики не находятся вблизи расчетных 
    # в области dlt, то они заменяются на расчетные, в противном случае выбирается пик
    # наиболее близкий к расчетному. see calibration.calibrate_area
    calibration_properties.dlt = 25 
    
    # SPECIFY calibration energies
    # EXAMPLE: calibration_properties.energies = [6143,6264,6899.2,7137,7922,8699,9261]#[7137,7922,8699,9261]#[6040,6143,6264,8699,9261]# 
    calibration_properties.energies = [6040, 6264,6899.2,7137,7922,8699,9261] #[6030, 6143, 6258.8, 6625, 6732,6899.2, 7922, 9261]
            
    search_properties = record() #for noisy spectrum
    search_properties.widths = np.arange(1, 5)
    search_properties.wavelet = 'blackman'
    search_properties.min_length = 1.
    search_properties.min_snr = 0.1
    search_properties.noise_perc = 0.10 
    
    filter_properties = record()
    filter_properties.window_smooth = 15
    filter_properties.smooth_wavelet = 'blackman'
    filter_properties.background_options = 'BACK1_ORDER8' #,BACK1_INCLUDE_COMPTON' 
    filter_properties.threshold = 3
    return calibration_properties, search_properties, filter_properties