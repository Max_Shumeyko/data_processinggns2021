# -*- coding: utf-8 -*-
"""
Created on Sat May 25 23:16:58 2019

@author: eastwood

Simulate experimental data for GNS beam irradiation serie
Simulatation includes random uniform noise, Yt(nat) calibration serie,
simulation of decay chain of the 116Lv290 element.

Each Simulator produces randrom uniform noize with GEN_CHAIN_PROBABILITY
probability of appearance of decay chain. To modify Simulator class - 
make a subclass of Simulator_Lv290chain and set for it GEN_CHAIN_PROBABILITY
and set for it simulate_chain_like_event.
  
MAIN FUNCTIONS:
timer_generator(filename = 'probe.bin', simulator=Simulator_Lv290chain,\
                dt=2 * 10**8) 
    - is used to setup simulator class generate and start generating data 
      each dt time interval, convert in to KAMAK analogue format using
      Converter class and write out into filename file.
      
MAIN CLASSES:
Simulator_Lv290chain:  
    GEN_CHAIN_PROBABILITY = 0.0001    
    def __init__(self, procs_map): ...
    The class is used to simulate Am + 48Ca irradiation experiment for 
    producing 290Lv superheavy nuclei at the GFRS detector box setup. 
    Simulates box detector position distribution, times and energies of
    decays and noize. See timer_generator function for the example.
    See simulate_Lv290chain function for the simulation details.

class Simulator_YT_calibration(Simulator_Lv290chain):    
    GEN_CHAIN_PROBABILITY = 0.21 ...
    The class is used to simulate Ytnat + 48Ca irradiation experiment for 
    producing calibration reaction for bunch of isotopes at the GFRS 
    detector box setup. 
    Simulates box detector position distribution, times and energies of
    decays and noize. See timer_generator function for the example.
    See simulate_YT_calibration for the simulation details.
    
class Simulator_Fission(Simulator):
    GEN_CHAIN_PROBABILITY = 0.21 ...
    The class is used to simulate simple flow of fission-like events with 
    gauss distributed energies > 50 MeV. Focal positions only.
    See simulate_uniform_fission for details.
    
Notes:
    The smallest simulation time period is 10ns count - according to time
    resolution of measurment electronics. All simulated time values are 
    converted to counts (10ns).
    
DESCRIPTION:
Output binary format (Yuri KAMAK analogue electronics output:
block.[w*]  word_number  DType       Description
w0          1            np.uint16   ID:1 - 3 - front detectors by 3x16, 
                                     4 - 6 - side detectors 6x8
w1          2            np.uint16   Alpha or ER energy (channel)
w2          3            np.uint16   Fission-scale energy, front strip [4096*Number_strip]
w3          4            np.uint16   Syncronization time (to find focal-side events)
w4          5            np.uint16   TOF time-of-flight, logic mark deltaE1 or deltaE2
w5          6            np.uint16   deltaE1, first TOF-counter
w6          7            np.uint16   deltaE2, second TOF-counter
w7          8            np.uint16   Veto
w8          9            np.uint16   Registr status
w9          10           np.uint8    strip number, main back strip, [0 .. 127], should add +1
w10         11           np.uint8    strip number, second back strip (if events is detected by two back strips) [0 .. 127], should add +1
w11         12           np.uint32   time in seconds, at the beginning of each file = 0
w12         13           np.uint32   time in microseconds, at the beginning of each file = 0
w13         14           np.uint16   Alpha energy channel, main back detector
w14         15           np.uint16   Alpha energy channel, second back detector     

"""

import random 
import collections
import queue
import time
import argparse

from abc import ABCMeta, abstractmethod
from math import pi, tan, atan, acos, sqrt, log

import numpy as np

from output_converter import Converter
#### TODO
# work with data

#### CONSTANTS & FORMATS
AVERAGE_RECOIL_INTENSITY = 300
AVERAGE_RECOIL_TIME = int(10 ** 8 / AVERAGE_RECOIL_INTENSITY) # seconds per event * 10**8 counts (10ns)   
BEAM_X = 24 # front strip number
BEAM_Y = 64 # back strip number
DLT_X = 24
DLT_Y = 64

FOCAL_X_SIZE = 52. # focal detector size horizontal (X, front), mm
FOCAL_Y_SIZE = 132. # focal detector size vertical (X, back), mm
SIDE_X_SIZE = 65. # side detector size horizontal, mm 
SIDE_Y_SIZE = 124. # side detector size vertical (X, back), mm

FRONT_STRIP_NUM = 48
BACK_STRIP_NUM = 128
SIDE_STRIP_NUM = 8

GENERATORS = dict() # collections of generator classes 

Event = collections.namedtuple('Event', 'time proc energy strip_x strip_y beam tof')
dtype_output = np.dtype([('w0', '<u2'), ('w1', '<u2'), ('w2', '<u2'), ('w3', '<u2'), \
       ('w4', '<u2'), ('w5', '<u2'), ('w6', '<u2'), ('w7', '<u2'), \
       ('w8', '<u2'), ('w9', 'u1'), ('w10', 'u1'), ('w11', '<u4'), \
       ('w12', '<u4'), ('w13', '<u2'), ('w14', '<u2')])

def add_generator(generator_cls):
    global GENERATORS
    GENERATORS[generator_cls.__name__] = generator_cls
    return generator_cls

def write_binary(block, filename):
    
    if hasattr(block, '__iter__'):
        with open(filename, 'ab') as f:
            for line in block:
                f.write(line)
    elif type(block) is Event:
        with open(filename, 'ab') as f:
            f.write(block)
    else:
        raise Exception('write binary: unknown input')
        

#### ADDITION SIMULATION FUNCTIONS      
def vec(point0, point1):
        return (point1[0] - point0[0], point1[1] - point0[1])
    
def vec_len(vec):
    return sqrt(vec[0] ** 2 + vec[1] ** 2)

def mlt(vec0, vec1):
    return vec0[0] * vec1[0] + vec0[1] * vec1[1]

def calculate_side_detector_angles_():
    """
    Вычисляет угловое распределение соответствующее угловому положению
    детекторов относительно точки с заданными координатами.
    
    Return func(x, y) # calculates side detector angles distribution.
    
    """
    A0 = (0., 0.)
    A1 = (0., FOCAL_Y_SIZE / 2)
    A2 = (0, FOCAL_Y_SIZE)
    A3 = (FOCAL_X_SIZE, FOCAL_Y_SIZE)
    A4 = (FOCAL_X_SIZE, FOCAL_Y_SIZE / 2)
    A5 = (FOCAL_X_SIZE, 0.)

    # coroutine
    def calculate(x, y):       
        angles = list()
        phi = 0.
        P = (x, y)
        
        v0 = vec(P, A0)
        v1 = vec(P, A1)
        phi += acos(mlt(v0, v1) / vec_len(v0) / vec_len(v1))
        angles.append(phi)
        
        v2 = vec(P, A2)
        phi += acos(mlt(v1, v2) / vec_len(v1) / vec_len(v2))
        angles.append(phi)
        
        v3 = vec(P, A3)
        phi += acos(mlt(v2, v3) / vec_len(v2) / vec_len(v3))
        angles.append(phi)
        
        v4 = vec(P, A4)
        phi += acos(mlt(v3, v4) / vec_len(v3) / vec_len(v4)) 
        angles.append(phi)
        
        v5 = vec(P, A5)
        phi += acos(mlt(v4, v5) / vec_len(v4) / vec_len(v5))
        angles.append(phi)
        
        phi += acos(mlt(v5, v0) / vec_len(v5) / vec_len(v0))        
        angles.append(phi)     
        
        return angles
    
    return calculate

calculate_side_detector_angles = calculate_side_detector_angles_()

def calculate_departure_angle(x, y, phi):
    
    # calc line parameters
    k = tan(phi)
    if k == 0:
        k += (random.random() - 0.5) / 1000. 
    b = y - k * x
    
    # calculate wall collision coordinates
    x_ = - b / k
    
    if x_ < 0:
        if phi < pi:
            x_ = FOCAL_X_SIZE
            y_ = k * x_ + b
        else:
            x_ = 0
            y_ = b
    elif x_ > FOCAL_X_SIZE:
        if phi < pi:
            x_ = 0
            y_ = b
        else:
            x_ = FOCAL_X_SIZE
            y_ = k * x_ + b
    else: # x_collision inside [0, FOCAL_X_SIZE]
        if phi < pi:
            y_ = FOCAL_Y_SIZE
            x_ = (y_ - b) / k
        else:
            x_ = - b / k
            y_ = 0.
            
    vector_collision = vec((x, y), (x_, y_))
    L = vec_len(vector_collision)
    
    strip_angles = []
    h = SIDE_Y_SIZE/ SIDE_STRIP_NUM
    for i in range(1, SIDE_STRIP_NUM + 1):
        strip_angles.append(pi / 2 - atan(h * i / L))
    theta_departure = strip_angles[-1]
    return theta_departure, strip_angles
    
def select_side_detector(phi, angles):
    """
    Select strip from 1 to SIDE_STRIP_NUM according to phi fitting in
    angles ranges.
    
    Return strip number.
    
    """
    detector = 0
    for alpha in angles:
        detector += 1
        if alpha > phi:
            return detector
    raise Exception('Wrong phi angle')
    
def select_side_strip(theta, angles):
    """
    Calculate side strip number according to theta angle less than 
    departure angle. Strips are considered from the bottom up.
    
    Return strip_number (from 1 to SIDE_STRIP_NUM).
    """ 
    detector = 0
    for alpha in angles:
        detector += 1
        if alpha < theta:
            return detector
    raise Exception('Wrong phi angle')

#### BASE SIMULATION FUNCIONS
class GenExc(Exception):
    pass
    
def gen_energy(gen_type, energy=9300, dlt=30):
    if gen_type == 'uniform':
        return random.randrange(700, 14000)
    elif gen_type == 'recoil':
        return random.randrange(5000, 14000)
    elif gen_type == 'gauss':
        return random.gauss(energy, dlt)
    elif gen_type == 'fission':
        return random.randrange(140000, 210000)
    else:
        raise GenExc('No such generation type!')
    
def gen_time(gen_type, t=1):
    """
    Generate time according to <gen_type> distribution and t half-life of the
    given isotope. t [counts].
    
    The smallest simulation time period is 10ns count - according to time
    resolution of measurment electronics. All simulated time values are 
    converted to counts (10ns).
    
    """
    
    if gen_type == 'uniform':
        return random.randrange(0, 2 * t)
    elif gen_type == 'exponential': # the func gets T1/2 half-lifes as t
        return int(random.expovariate(log(2.) / t))
    elif gen_type == 'recoil':
        return gen_time('exponential', AVERAGE_RECOIL_TIME)
    else:
        raise GenExc('No such generation type!')

def gen_focal(x=BEAM_X, y=BEAM_Y, dltX=DLT_X, dltY=DLT_Y):
    """
    Generate 2D Gaussian position distribuion on focal detector.
    Return front_strip, back_strip, x, y #focal strip numbers and coordinates
    
    """
    X_ = x * FOCAL_X_SIZE / FRONT_STRIP_NUM
    Y_ = y * FOCAL_Y_SIZE / BACK_STRIP_NUM 
    
    # generate focal gauss 2D distribution
    pos_x, pos_y =  random.gauss(X_, dltX), \
                    random.gauss(Y_, dltY)
                    
    front_strip = int(pos_x * FRONT_STRIP_NUM / FOCAL_X_SIZE) + 1
    back_strip = int(pos_y * BACK_STRIP_NUM / FOCAL_Y_SIZE) + 1
    
    # check_borders
    if (front_strip < 1):
        front_strip = 0
    elif front_strip > FRONT_STRIP_NUM:
        front_strip = 0
        
    if (back_strip < 1) or (front_strip == 0):
        back_strip = 0
    elif back_strip > BACK_STRIP_NUM:
        back_strip = 0
    
    return front_strip, back_strip, x, y  
        
def gen_side(x, y):
    """
    Generate side position distribution.
    Return side_strip or 'DEPARTURE' (if the particle left the detectors box)
           or 'FOCAL' (if particle remains in focal detector)
    
    """    
    #                                                                          ^ -->
    # generate random                                                          |    
    # spherical uniform distribution; angle starts from the top of the sphere  *
    theta = acos(1 - 2 * random.random()) # [0, Pi] from top to bottom
    phi = 2 * pi * np.random.random() # [0, 2*Pi]
    
    theta_departure, strip_angles = calculate_departure_angle(x, y, phi)
    if theta > pi/2: # the particle remained in the detector
        return 'FOCAL'     
               
    elif theta <= theta_departure:
        return 'DEPARTURE'
    
    else:
        side_detector = select_side_detector(phi, \
                                        calculate_side_detector_angles(x, y))
        side_vertical_strip = select_side_strip(theta, strip_angles)
        side_strip = (side_detector - 1) * 8 + side_vertical_strip
        return side_strip
        
def simulate_uniform_noize(start_time=0):
    time = start_time
    while True:
        energy = gen_energy('gauss', 2500, 300)
        time += gen_time('recoil') # time in 10's nanoseconds
        strip_x, strip_y, x, y = gen_focal()
        if strip_x == 0 or strip_y == 0:
            continue
        beam = False
        tof = random.random() < 0.2
        yield Event(time, 'noize', energy, strip_x, strip_y, beam, tof)

def simulate_focal_side_events(time, proc_name, energy, x, y, \
                               strip_x, strip_y, beam, tof):
    side_strip = gen_side(x, y)
    if side_strip == 'FOCAL':
        yield Event(time, proc_name, energy, strip_x, strip_y, beam, tof)
    elif side_strip == 'DEPARTURE':
        return
    else:
        split_coef = random.random()
        energy1 = split_coef * energy
        energy2 = energy - energy1        
        yield Event(time, proc_name, energy1, strip_x, strip_y, beam, tof)
        time += random.randrange(20) # up to 200 ns
        yield Event(time, proc_name, energy2, -side_strip, 0, beam, tof)

#### SIMULATOR BASE ABSTRACT CLASS
class Simulator(metaclass=ABCMeta):
    GEN_CHAIN_PROBABILITY = 0.0001
    
    @abstractmethod
    def get_procs(self):
        """
        Simulate simple event flow which simulates recoil base events.
        
        """
        procs = {'noize': simulate_uniform_noize(start_time=0)}
        return procs
    
    def __init__(self):        
        self.events = queue.PriorityQueue()
        self.procs = self.get_procs() #dict(procs_map)
        assert len(self.procs) > 0, 'Input procs_map is empty'
        self.container = collections.deque()
        self.time = 0
        self.chains_num = 0
        self.events_num = 0
    
    @abstractmethod    
    def simulate_chain_like_event(self, *arg, **argv):
        """
        Simulates second generation events which follow base recoils and
        are generated with GEN_CHAIN_PROBABILITY probability. 
        
        """
        pass
        
    def run(self, end_time):
        """Schedule and display events until time is up"""
        # schedule the first event 
        if self.time == 0:
            for _, proc in sorted(self.procs.items()):
                first_event = next(proc)
                self.events.put(first_event)
        
        assert self.time < end_time, \
'''
Input end_time={0} less than current: {1}
Check if input end_time({0}) is nearby or less than 
AVERAGE_RECOIL_TIME({2})
'''.format(str(end_time), str(self.time), AVERAGE_RECOIL_TIME)
            

        # main loop of the simulation
        while self.time < end_time:
            # get an event from PriorityQueue
            current_event = self.events.get()
            self.container.append(current_event)
            #print 'Current event: ', current_event
            
            time, proc_name, energy, strip_x, strip_y, \
            beam, tof = current_event
            self.time = time
            
            # generate decay events
            if proc_name == 'noize':
                
                # generate chain-like initial event if noize or chain next
                if tof & (random.random() < self.GEN_CHAIN_PROBABILITY):
                    proc_name = 'chain' + str(self.chains_num)
                    
                    chain_gen = self.simulate_chain_like_event(proc_name,\
                                        start_time=time)   
                    
                    next_event = next(chain_gen)     
                    self.events.put(next_event)
                    self.procs[proc_name] = chain_gen # start chain process
                    self.chains_num += 1
                    self.events_num += 1
                    
                # generate next background event
                next_event = next(self.procs['noize'])                    
                self.events.put(next_event)
                self.events_num += 1
                
            # select the chain process and generate next event 
            elif 'chain' in proc_name:                
                active_proc = self.procs[proc_name]
                
                try:
                    #next_event = active_proc.send(proc_name)
                    next_event = next(active_proc)
                except StopIteration:         # if the chain is exhausted 
                    del self.procs[proc_name] # delete chain process
                else:
                    self.events.put(next_event)
                    self.events_num += 1
                
        else:
            msg = '*** end of simulation time: {} events left in PQueue ***'
            print(msg.format(self.events.qsize()))
            print('Events simulated: ', self.events_num)

#### SIMULATE FISSION SCALE EVENTS  
            
def simulate_uniform_fission(start_time=0):
    time = start_time
    while True:
        energy = gen_energy('gauss', 180000, 800)
        time += gen_time('recoil') # time in 10's nanoseconds
        strip_x, strip_y = 0, 0
        while strip_x == 0 or strip_y == 0:
            strip_x, strip_y, x, y = gen_focal()
        beam = False
        tof = True
        yield Event(time, 'noize', energy, strip_x, strip_y, beam, tof)
        
@add_generator
class Simulator_Fission(Simulator):    
    GEN_CHAIN_PROBABILITY = 0 # no decay chains
    
    def get_procs(self):
        return {'noize': simulate_uniform_fission(start_time=0)}
    
    def simulate_chain_like_event(self, *arg, **argv):
        pass       
    
#### SIMULATE LV290 CHAIN        
def simulate_Lv290chain(proc_name, start_time=0):
    
    strip_x, strip_y, x, y = gen_focal()
    
    if strip_x == 0 or strip_y == 0:
        return # the particle missed the detector blocks
    
    # recoil
    energy = gen_energy('recoil')
    time = start_time + gen_time('recoil') # 
    beam = False
    tof = True
    #proc_name = yield Event(time, proc_name, energy, strip_x, strip_y, beam, tof)    
    yield Event(time, proc_name, energy, strip_x, strip_y, beam, tof)
    
    # recoil Lv290
    energy = gen_energy('gauss', 11000, 30)
    time = start_time + gen_time('exponential', 710000) # 0.71ms, time in x * 10 nanoseconds
    beam = False
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
    
    # second alpha 286Fl
    energy = gen_energy('gauss', 9800, 30)
    time += gen_time('exponential', 13000000) # 0.13s, time in x * 10 nanoseconds
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
    
    # third alpha 282Cn
    energy = gen_energy('gauss', 9000, 30)
    time += gen_time('exponential', 50000000) # 0.5s, time in x * 10 nanoseconds
    beam = True
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
    
    # fission
    energy = gen_energy('fission')
    time += gen_time('exponential', 10000000) #*1000 100s, time in x * 10 nanoseconds
    beam = True
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event


@add_generator
class Simulator_Lv290chain(Simulator):    
    GEN_CHAIN_PROBABILITY = 0.0001 
    
    def get_procs(self):
        procs = {'noize': simulate_uniform_noize(start_time=0)}
        return procs
    
    def simulate_chain_like_event(self, *arg, **argv):
        return simulate_Lv290chain(*arg, **argv)
        
#### SIMULATE YTerbium CALIBRATION RUN 
def simulate_YT_calibration(proc_name, start_time=0):     
    strip_x, strip_y, x, y = gen_focal() 
    
    while strip_x == 0 or strip_y == 0:
        strip_x, strip_y, x, y = gen_focal()
    # recoil
    energy = gen_energy('recoil')
    time = start_time + gen_time('recoil') # 
    beam = False
    tof = True  
    yield Event(time, proc_name, energy, strip_x, strip_y, beam, tof)
    
    # choose the isotope of Th
    num = random.random()
    if num < 0.55:
        for event in gen_Th216(proc_name, time, strip_x, strip_y, x, y):
            yield event
    elif num < 0.85:
        for event in gen_Th217(proc_name, time, strip_x, strip_y, x, y):
            yield event
    elif num < 0.95:
        for event in gen_Th218(proc_name, time, strip_x, strip_y, x, y):
            yield event
    elif num < 1.:
        for event in gen_Th219(proc_name, time, strip_x, strip_y, x, y):
            yield event

def gen_Th216(proc_name, time, strip_x, strip_y, x, y):
     
    # firsht alpha -> Ra212
    energy = gen_energy('gauss', 7922, 30)
    time = time + gen_time('exponential', 2600000) # 26 ms, time in x * 10 nanoseconds
    beam = False
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
        
    # second alpha -> Rn208
    energy = gen_energy('gauss', 6899, 20)
    time += gen_time('exponential', 1300000000) # !13 s, time in x * 10 nanoseconds  
    beam = False
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
    
    # third alpha -> Po204
    energy = gen_energy('gauss', 6140, 20)
    time += gen_time('exponential', 146100000000) # !24.35 min, 1461 s, time in x * 10 nanoseconds  
    beam = False
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
                    
def gen_Th217(proc_name, time, strip_x, strip_y, x, y):
    #print 'Th217 gen, ', proc_name
    
    # alpha -> Ra213
    energy = gen_energy('gauss', 9250, 30)
    time = time + gen_time('exponential', 25200) # 0.252ms, time in x * 10 nanoseconds
    beam = False
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
    
    
    # second alpha -> Rn209
    num = random.random()
    if num < 0.8:
        num = random.random()
        if num < 0.45:
            energy = gen_energy('gauss', 6731, 20)
        elif num < 0.94:
            energy = gen_energy('gauss', 6624, 20)
        else:
            energy = gen_energy('gauss', 6522, 20)
        time += gen_time('exponential', 16440000000) # 2.7 min, 164.4s, time in x * 10 nanoseconds
        
    elif num < 0.81:
        num = random.random()
        if num < 0.69:
            energy = gen_energy('gauss', 8466, 30)
        elif num < 0.97:
            energy = gen_energy('gauss', 8357, 30)
        time += gen_time('exponential', 210000) # 2.1 ms, time in x * 10 nanoseconds
        
    else:
        return   
        
    beam = False
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
    
    # third decay -> alpha Po205 | -> fission
    num = random.random()
    if num < 0.17:
        energy = gen_energy('gauss', 6039, 20)
        time += gen_time('exponential', 172800000000) # 28.8 min, 1728 s, time in x * 10 nanoseconds
        beam = True
        tof = False
        for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
            yield event
        
        
def gen_Th218(proc_name, time, strip_x, strip_y, x, y):
    
    # first alpha Ra214 missed according to decay before reach the detector
    
    # second alpha -> Rn210
    energy = gen_energy('gauss', 7137, 30)
    time = time + gen_time('exponential', 246000000) # 2.46 s, time in x * 10 nanoseconds
    beam = False
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
    
    # third alpha -> Po206
    energy = gen_energy('gauss', 6041, 20)
    time += gen_time('recoil') # !real decayT = 2.4h, time in x * 10 nanoseconds  
                                # but I cut it down
    beam = False
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
    
def gen_Th219(proc_name, time, strip_x, strip_y, x, y):
     
    # firsht alpha -missed
     
    # second alpha Ra215 -> Rn211
    energy = gen_energy('gauss', 8700, 30)
    time = time + gen_time('exponential', 15500) # 1.55 ms, time in x * 10 nanoseconds
    beam = False
    tof = False
    for event in simulate_focal_side_events(time, proc_name, energy, x, y, \
                                            strip_x, strip_y, beam, tof):
        yield event
    
@add_generator
class Simulator_YT_calibration(Simulator):    
    GEN_CHAIN_PROBABILITY = 0.91
    
    def get_procs(self):
        return {'noize': simulate_uniform_noize(start_time=0)}
    
    def simulate_chain_like_event(self, *arg, **argv):
        return simulate_YT_calibration(*arg, **argv)
    
#### TIME based generator
def timer_generator(filename='probe.bin', \
                    #Simulator=Simulator_Lv290chain,\
                    Simulator=Simulator_YT_calibration,
                    dt=2*10**8):
    
    end_time = 0
    #cart = {'noize': simulate_uniform(start_time=0)}
    sim = Simulator()
        
    converter = Converter()
    converter.write_coefs_to_file() # write generated coefficients    
    f_convert = lambda event: converter.convert_to_output_structure(event)
    
    while True:
        sim.container.clear()
        end_time += dt
        sim.run(end_time)
        write_binary((f_convert(event) for event in sim.container), \
                     filename)
        time.sleep(1)      
          
def main(seed=None):
    """Initialize random generator, build procs and run simulation"""
    end_time = 60 * 10**8 # 60 s, 1 count = 10ns
    if seed is not None:
        random.seed(seed) # get reproducible results
        cart = {'noize': simulate_uniform_noize(start_time=0)}
        sim = Simulator_Lv290chain(cart)
        sim.run(end_time)
                
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
Discrete event simulation DGFRS. Use generator classes to simulate 
experiment-like chains of irradiation events and decays for DGFRS setup
and get output into binary file (see output_converter.py and timer_generator()
for details)''')
    parser.add_argument('-fname', '--filename', 
                        type=str, default='probe.bin', 
                        help='Output filename; default="probe.bin"')
    parser.add_argument('-flname', '--folder', 
                        type=str, default='probe/', 
                        help='Output folder; default="probe/"')
    parser.add_argument('-dt', '--time_interval', type=int, 
                        default=20,
                        help='time interval for one iteration in seconds(!); \
                        default=20 seconds (20 * 10**8 counts of 10 ns each)')
    parser.add_argument('-sc', '--simulation_class', type=str,
                        default='Simulator_YT_calibration',
                        help="""Select a generator class for
specific type of simulation. See GENERATORS (dict), 
which includes next classes: {gen_classes} 

Generates data to <folder>/, data is:
<filename>.bin datafile
coefficients files front_alpha.txt, front_fission, ... etc.
(6 coefs files, see output_converter.py

See generator classes, and decarator @add_generator to 
include new generator classed. """.format(gen_classes=str(list(GENERATORS.keys())))
                        )

    args = parser.parse_args()
    timer_generator(args.filename, dt=args.time_interval * 10**8,
                    Simulator=GENERATORS[args.simulation_class])
#    timer_generator(filename, dt = 20 * 10**10, # time in 10ns counts; 10**8 = 1 second
#                    Simulator=Simulator_YT_calibration)
    #timer_generator(dt = 20 * 10**5, simulator=Simulator_Fission, \
    #                simulate_uniform=simulate_uniform_fission)


