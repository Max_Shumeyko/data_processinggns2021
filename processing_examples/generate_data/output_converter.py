# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 19:59:55 2019

@author: eastwood

Convert simulated data to output analogue format (see below)
using random generated front, back and side coefficients. Also splits signal 
generated in focal detector to double focal-side signals according to
geometry efficiency of the detectors setup.

Input:
Event = collections.namedtuple('Event', 'time proc energy strip_x strip_y beam tof')    
    
Output binary format:
block.[w*]  word_number  DType       Description
w0          1            np.uint16   ID:1 - 3 - front detectors by 3x16, 
                                     4 - 6 - side detectors 3x16 strips
                                     or 6 detectors x 8 strips
w1          2            np.uint16   Alpha or ER energy (channel)
w2          3            np.uint16   Fission-scale energy, front strip [4096*Number_strip]
w3          4            np.uint16   Syncronization time (to find focal-side events)
w4          5            np.uint16   TOF time-of-flight, logic mark deltaE1 or deltaE2
w5          6            np.uint16   deltaE1, first TOF-counter
w6          7            np.uint16   deltaE2, second TOF-counter
w7          8            np.uint16   Veto
w8          9            np.uint16   Registr status
w9          10           np.uint8    strip number, main back strip, [0 .. 127], should add +1
w10         11           np.uint8    strip number, second back strip (if events is detected by two back strips) [0 .. 127], should add +1
w11         12           np.uint32   time in seconds, at the beginning of each file = 0
w12         13           np.uint32   time in microseconds, at the beginning of each file = 0
w13         14           np.uint16   Alpha energy channel, main back detector
w14         15           np.uint16   Alpha energy channel, second back detector     

"""

import os
import random

from numpy import dtype, empty 
from numpy.random import uniform

import data_processingGNS2021.tools.spectrum as sp
#### FORMATS AND STRUCTURES
CLBR_LINE = '\n{:4d}    A = {:2.5f} ; B = {:2.1f}\n'
DIFFER_LINE = '{:4.1f}  {:4.1f}    {:-5.1f}    {:-4d} \n'
ENERGIES = [6039, 6624, 6731, 6899.2, 7137, 7922, 8699, 9261] # calibration peaks
FISSION_THRESHOLD = 30000
dtype_output = dtype([('w0', '<u2'), ('w1', '<u2'), ('w2', '<u2'), ('w3', '<u2'), \
       ('w4', '<u2'), ('w5', '<u2'), ('w6', '<u2'), ('w7', '<u2'), \
       ('w8', '<u2'), ('w9', 'u1'), ('w10', 'u1'), ('w11', '<u4'), \
       ('w12', '<u4'), ('w13', '<u2'), ('w14', '<u2')])
 
      
#### CONVERTER CLASS
class Converter:
    
    def __init__(self, coefs_folder=None):
        if coefs_folder is None:
            self.coefs = self.generate_coefs()
        elif os.path.isdir(coefs_folder):
            self.coefs = self.read_coefs(coefs_folder)
        else:
            raise IOError('Wrong input coefs_folder: {}.'.\
                          format(coefs_folder))

    def read_coefs(self, coefs_folder):
        coefs[scale]['front'] = front_coefs
        coefs = self.generate_coefs()

        initial_path = os.getcwd()
        path = os.join(initial_path, coefs_folder)

        get_stip_num = lambda detector: sp.event_type_dict[detector][1]

        for detector in ('front', 'back', 'side'):
            for scale in ('alpha', 'fission'):
                filename = '{0}_{1}.txt'.format(detector, scale)
                filename = os.path.join(path, filename)
                coefs = sp.get_calibration_coefficient(filename)
                strip_num = get_strip_num(detector)

                for strip in range(1, strip_num+1):
                    coefs[scale][detector][strip]['a'] = coefs[0][strip]
                    coefs[scale][detector][strip]['b'] = coefs[1][strip]

        return self.coefs
        
    def generate_coefs(self):
        
        coefs = dict()

        gen = lambda x_min, x_max: uniform(x_min, x_max)
        
        for scale in ('alpha', 'fission'):

            k = 1. if scale == 'alpha' else 30.

            front_coefs = dict()
            for i in range(1, 49):
                a, b = k * gen(2.3, 2.7), k * gen(0., 20.)
                front_coefs[i] = {'a': a, 'b': b}
            
            back_coefs = dict()
            for i in range(1, 129):
                a, b = k * gen(1.8, 2.2), k * gen(-10., 10.)
                back_coefs[i] = {'a': a, 'b': b}
                
            side_coefs = dict()
            for i in range(1, 49):
                a, b = k * gen(2.8, 3.2), k * gen(-30., 30.)
                side_coefs[i] = {'a': a, 'b': b}
            
            coefs[scale] = dict()
            coefs[scale]['front'] = front_coefs
            coefs[scale]['back'] = back_coefs
            coefs[scale]['side'] = side_coefs
        
        return coefs
    
    def convert_to_output_structure(self, event):
        """
        Convert input generated event (see Event namedtuple) to Block.
        See Block decription.
        
        """
        
        if event.strip_x > 0 and event.strip_y > 0: # focal event
            return self.convert_to_focal(event)
            
        elif event.strip_x < 0: # side event
            return self.convert_to_side(event)
                        
    def convert_to_side(self, event):
        block = empty(1, dtype=dtype_output)[0] 
        
        strip_x = -event.strip_x # conversion for side strips

        side_a, side_b = self.coefs['alpha']['side'][strip_x]['a'], \
                           self.coefs['alpha']['side'][strip_x]['b']

        side_fa, side_fb = self.coefs['fission']['side'][strip_x]['a'], \
                           self.coefs['fission']['side'][strip_x]['b']
        

        
        block[0] = (strip_x - 1) // 16 + 4 # side id = 4 .. 6
        if event.energy < FISSION_THRESHOLD:
            block[1] = int((event.energy - side_b) / side_a)# + 8192 * (event.strip_x - 1)# coef coefs = (3., 0)
        else:
            block[1] = 4095
        block[2] = int((event.energy - side_fb) / side_fa)# + 8192 * (event.strip_x - 1)# coef coefs = (3., 0)
        block[3] = (event.time // 100) # uint16 0 .. 65536 mks
        block[4] = event.tof 
        block[5] = int(event.tof) * random.randrange(1, 40000)
        block[6] = int(block[5] * 0.8)
        block[7] = 0 # veto = false
        block[8] = 0 # status ??
        block[9] = 0
        block[10] = 0
        block[11] = event.time // 10**8 # 10ns * 10**8 = 1s
        block[12] = event.time // 100 # 10ns * 100 = 1mks
        block[13] = 0 # back_chan = 0
        block[14] = 0 # second back chan = 0
        
        return block
        
    def convert_to_focal(self, event):
        block = empty(1, dtype=dtype_output)[0] 
        
        front_a, front_b = self.coefs['alpha']['front'][event.strip_x]['a'], \
                           self.coefs['alpha']['front'][event.strip_x]['b']
                           
        front_fa, front_fb = self.coefs['fission']['front'][event.strip_x]['a'], \
                           self.coefs['fission']['front'][event.strip_x]['b']

        back_a, back_b = self.coefs['alpha']['back'][event.strip_y]['a'], \
                           self.coefs['alpha']['back'][event.strip_y]['b']

#        back_fa, back_fb = self.coefs['fission']['back'][event.strip_y]['a'], \
#                           self.coefs['fission']['back'][event.strip_y]['b']

        # convert 
        block[0] = (event.strip_x - 1) // 16 + 1
        if event.energy < FISSION_THRESHOLD:
            block[1] = int((event.energy - front_b) / front_a)
        else:
            block[1] = 4095
        block[2] = int((event.energy - front_fb) / front_fa)
        block[3] = (event.time // 100) # uint16 0 .. 65536 mks
        block[4] = event.tof 
        block[5] = int(event.tof) * random.randrange(1, 1000)
        block[6] = block[5]
        block[7] = 0 # veto = false
        block[8] = 0 # status ??
        block[9] = event.strip_y
        block[10] = 0
        block[11] = event.time // 10**8 # 10ns * 10**8 = 1s
        block[12] = event.time // 100 # 10ns * 100 = 1mks
        block[13] = int((event.energy - back_b) / back_a) # back_chan = 0
        block[14] = 0 # second back chan = 0
        
        return block
    
    def write_block(self, file_obj, scale, event_type):
        for strip, coef in list(self.coefs[scale][event_type].items()):
            file_obj.write(CLBR_LINE.format(strip, coef['a'], coef['b']))
            file_obj.write(' eexp   ecal      differ   channel\n')
            for energy in ENERGIES:
                channel = int((energy - coef['b']) / coef['a'])
                energy_calculated = channel * coef['a'] + coef['b']
                differ = energy_calculated - energy
                line = DIFFER_LINE.format(energy, energy_calculated, \
                                          differ, channel)
                file_obj.write(line)
        
    def write_coefs_to_file(self, folder='./'):
        """
        Write down coefficients and diff tables to next files:
            front_alpha.txt, front_fission.txt, back_alpha.txt, 
            back_fission.txt, side_alpha.txt, side_fission.txt
            
        Table example:
          47    A = 94.97442 ; B = 308.8
         eexp   ecal      differ   channel
        6039.0  6007.2    -31.8      60 
        6624.0  6577.1    -46.9      66 
        6731.0  6672.1    -58.9      67 
        6899.2  6862.0    -37.2      69 
        7137.0  7052.0    -85.0      71 
        7922.0  7906.7    -15.3      80 
        8699.0  8666.5    -32.5      88 
        9261.0  9236.4    -24.6      94 
        
        , where 
            eexp - experimental energies; 
               calibration peaks: Th217 (9.261 МэВ), Ra215 (8.701 МэВ), 
               Th216 (7.920 МэВ), Ra214(7.137 МэВ), Rn212(6.264 МэВ),
               Rn208 (6.140 МэВ), Rn210 (6.040 МэВ), 
               see Ytnat+48Ca reactions
            
            ecal - calculated energy according to recognized peaks
                   and regression model
            differ = eexp - ecal
            channel - amplitude channel, which were used for calibration.
                
        """
        
        if folder[-1] != '/':
            folder += '/'
        
        print('* generate and write coefficients to files')
        assert os.path.isdir(folder), \
            'error write_to_file(folder) : no such folder!'
            
        for scale in ('alpha', 'fission'):
            with open(folder + 'front_' + scale + '.txt', 'w') as f:
                self.write_block(f, scale, 'front')
                    
            with open(folder + 'back_' + scale + '.txt', 'w') as f:
                self.write_block(f, scale, 'back')
                    
            with open(folder + 'side_' + scale + '.txt', 'w') as f:
                self.write_block(f, scale, 'side')
        
        
