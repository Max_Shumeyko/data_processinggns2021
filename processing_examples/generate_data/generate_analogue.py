# -*- coding: utf-8 -*-
"""
Created on Sat May 25 21:56:28 2019

@author: eastwood
"""

import os
import random

import numpy as np

dtype = np.dtype([('w0', np.uint16), ('w1', np.uint16), ('w2', np.uint16), ('w3', np.uint16),\
                  ('w4', np.uint16), ('w5', np.uint16), ('w6', np.uint16), ('w7', np.uint16),\
                  ('w8', np.uint16), ('w9', np.uint8),('w10', np.uint8), ('w11', np.uint32),\
                  ('w12', np.uint32), ('w13', np.uint16), ('w14', np.uint16)])
                  
d = np.zeros(10, dtype=dtype)

for i in d:
    i['w0'] = random.randrange(1, 7) 
    i['w1'] = random.randrange(1, 8193)
    i['w2'] = random.randrange(1, 4097)
    i['w3'] = random.randrange(1, 65536)
    i['w4'] = random.randrange(0, 2)
    i['w5'] = random.randrange(0, 2)
    i['w6'] = random.randrange(0, 2)
    i['w7'] = random.randrange(0, 2)
    i['w8'] = random.randrange(0, 65535)
    i['w9'] = random.randrange(0, 64)
    i['w10'] = random.randrange(65, 129)
    i['w11'] = random.randrange(1, 10000000)
    i['w12'] = random.randrange(1, 10000000)
    i['w13'] = random.randrange(1, 4095)
    i['w14'] = random.randrange(1, 4095)
    print(i)
    
os.chdir('/home/eastwood/gitlab_projects/data/analogue/2019')

with open('an_simulated.bin', 'wb') as f:
    f.write(d)
#d.astype('int16').tofile('an_simulated.bin')
    