# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 14:17:13 2018
Perform automatic calibration of focal-side channel spectr for
selected STRIPS group in alpha scale.

Prepare data frame and COEFS_FOLDER to start script.
@author: eastwood

"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import differential_evolution

import data_processingGNS2021.tools.spectrum as sp
from data_processingGNS2021.processing_examples.search.search_focal_side \
    import search_focal_side
    
class record: 
    pass

REPORT_FMT = '\n%d    A = %2.3f ; B = %2.f\n'

def apply_side_coefs(coefs, events_frame):
    a, b = coefs
    channels = events_frame['channel']
    energies = channels * a + b
    return np.where(energies > 0, energies, np.zeros(len(energies)))
    

def calc_energy_scale(result, front_energies, side_events, strip):
    side_energies = apply_side_coefs(result, side_events)
    total_energies = front_energies + side_energies
    en_spectr, xsample = np.histogram(
        total_energies,                                     
        bins=np.arange(5000, 10000, 10)
    )
    xsample = xsample[1:]
    return xsample, en_spectr
 
def evaluation_function(xsample, en_spectr, energies, 
                        energies_weights=None, 
                        dlt_en=30):
     result = 0
     energies = sorted(energies)     
     sum_counts = np.sum(en_spectr[(xsample >= energies[0]) &\
                                   (xsample <= energies[1])]
                        )
     
     if not energies_weights:
         energies_weights = np.ones(energies.shape)
         
     for energy, weight in zip(energies, energies_weights):
         ind = (xsample >= energy - dlt_en) & (xsample <= energy + dlt_en)
         s_ = sum(en_spectr[ind]) 
         ind_over = (xsample > energies[-1] + 2 * dlt_en)
         val_over = sum(en_spectr[ind_over]) * 1.5
         result -= val_over
         
         threshold = sum_counts / float(len(energies))
         result = result + s_ * weight if s_ > threshold / 8 \
                  else result - threshold
     return result

def opt_func(result, front_energies, side_events,
             strip=None, weights=None):
    xsample, en_spectr = calc_energy_scale(result, front_energies, 
                                           side_events,strip) 
    
    res = -evaluation_function(xsample, en_spectr, ENERGIES,
                                energies_weights=weights)
    return res

def optimize(parameters_bounds, strip, 
             front_energies, side_events, weights):    
    result = differential_evolution(
               lambda p: opt_func(p, front_energies, side_events,\
                                  strip=strip, weights=weights),
               parameters_bounds,
             )
    p = result.x
    report = REPORT_FMT % (strip, p[0], p[1])
    print('Result: \n', report, result)
    return p

def check_energy_scale(result, strip, energies):
    energies = sorted(energies)
    e_min, e_max = energies[0], energies[-1]
    # prepare coefficients
    side_coefs_ = side_coefs
    side_coefs_[0, strip], side_coefs_[1, strip] = result
    coefs = record()
    coefs.front_coefs = front_coefs
    coefs.side_coefs = side_coefs_
    
    # build spectrum
    en_spectrs, xsample, _ = \
        sp.get_combined_spectrum(
            frame,
            event_type='front-side', scale='alpha', tof=False, 
            energy_scale=True, coefs_folder='.', coefs=coefs, 
            bins=np.arange(5000, 10000, 15)
        )
        
    # plot a figure
    en_spectrs = en_spectrs[strip - 1] #according to en_spectrs has shape [0 .. 47][..]
    xsample = xsample[1:]
    fig, ax = plt.subplots()
    ax.set_title("Focal-side calibration energy spectrum, strip %s" 
                 % (strip,))
    ax.set_xlabel("Energy, KeV")
    ax.set_ylabel("Counts")
    ax.plot(xsample, en_spectrs, linestyle='steps')
    ax.set_xlim(e_min, e_max)
    ax.set_xticks(
        np.arange(round(e_min - 250, -2), round(e_max + 250, -2), 
                  100)
    )
    ax.grid()
    plt.show()
    return xsample, en_spectrs

def write_coefs_to_file(coefs_dict, strips_in_detector=8):
     with open('side_alpha.txt', 'w') as f:
         for strip in sorted(coefs_dict.keys()):
             a, b = coefs_dict[strip]
             for strip_cur in range(strip, strip + strips_in_detector):
                 f.write(REPORT_FMT % (strip_cur, a, b))
    
if __name__ == '__main__':


    STRIPS = [1, 9, 17, 25, 33, 41]
#    ENERGIES = [6899, 7137, 7922, 8699, 9261] # YtCa
#    WEIGHTS = [1., 0.3, 1., 1., 6.]
    ENERGIES = [5304., 7200., 7862., 8404.] # PbCa
    WEIGHTS = [1., 1., 1., 1.]
        
    #### MANUAL CALIBRATION OF FOCAL-SIDE ENERGY SPECTRS IN APLHA SCALE ########
    side_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + '/' + 'side_alpha.txt')
    front_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + '/' + 'front_alpha.txt')
    energies = sp.apply_all_calibration_coefficients(frame, COEFS_FOLDER)
    frame = frame[(energies >= 1100) & (energies <= 7000)]
    res = search_focal_side(frame, energies, 'alpha', 'front', 
                            tof=False, dlt_t=50)
    (front_events, front_energies), (side_events, side_energies) = res
            
    parameters_bounds = [(1.5, 2.5), (-600, 600)]
    results = {}
    for strip in STRIPS: 
        ind = side_events['strip'] == strip
        side_events_ = side_events[ind]
        front_energies_ = front_energies[ind]
        result = optimize(parameters_bounds, strip, 
                          front_energies_, side_events_, WEIGHTS)
        results[strip] = result
        check_energy_scale(result, strip, ENERGIES)
        
    print("Show results: ", results)
    write_coefs_to_file(results)