# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 18:33:54 2018

Perform automatic calibration of side channel spectr in fission scale.

Prepare data frame and alpha calibration coefficients to start using scripts from 
<project_folder>/data_processing/calibration. 
! note: check if alpha scale is already calibrated so the coefficients are 
required to perform calibration in fission scale.

Instruction:
    1. Get data frame from <project_folder>/data_processing/read_data<format>
    2. Specify parameters for calibration:
        * FILEPATH - adress of a file to save calibration coefficients
        * scattered_ions_energy: energy of scattered ions peak 
        * start, stop: specify strips (number) of spectrs to calibrate: start, stop.
           Calibration will be performed for strips from start to stop.
           
        * border:  specify border where there are no more peaks except scattered ions after the border    
        * points_numb: amount of points of alpha-fission coincidences for calibration 
        * visualize : choose if to plot each calibrated detector's strip 
    3. Run this script to perform calibration
@author: eastwood
"""

import os
import matplotlib.pyplot as plt

import data_processingGNS2021.tools.spectrum as sp
import data_processingGNS2021.tools.calibration as clbr

from data_processingGNS2021.tools.fit_lma import fit_lma, fit_lma_robust, fit_func

class record:
    pass

# inditificators
event_type_ind = {'side': 3}
scale_ind = {'alpha': 0, 'fission': 1}

### SPECIFY PARAMETERS FOR CALIBRATION
start, stop = 1, 6
FILEPATH = os.path.join(coefs_folder, 'side_fission_probe.txt')    
points_numb = 800 # amount of points of alpha-fission coincidences for calibration 
visualize = True # choose if to plot each calibrated detector's strip   

# specify energy sample: nonzero energies from focal strip
# !note: change from 'front' to 'back' to perform calibration of back detectors
energies = sp.apply_calibration_coefficients(frame, coefs_folder, 'side', 'alpha')
ind = (frame['event_type'] == event_type_ind['side']) & (frame['strip'] > 0) &\
      (~ frame['tof'])

energies = energies[ind]
frame_ = frame[ind]
    
def make_report(solution, strip, filename):
    report = '\n%d    A = %2.5f ; B = %2.1f\n' % (strip, solution[0], solution[1])
    print(report)
    f = open(filename, 'a')
    f.write(report)
    f.close()
        
def calibrate_strip(strip, visualize=False):
          
    # get pairs of fission channels and (alpha) energies for set \
    # of events in Yb calibration area (6.3-9.3 MeV)
    ind = (frame_['strip'] == strip) & (frame_['scale'] == scale_ind['alpha']) &\
          (energies > 6300) & (energies < 9300)
    fission_ind = np.roll(ind, 1) # according to the specificity of frame data structure
                                  # alpha-events goes right before fission events
                                  # so you need to roll forward to extract fission channels
    ypeaks, xpeaks = energies[ind][:points_numb], frame_[fission_ind]['channel'][:points_numb]
    
    # calibrate fission channels by their alpha energies
    solution = clbr.calibrate_line(xpeaks, ypeaks)
    
    if visualize:
        f = lambda P, x: P[0] * x + P[1]
        fig, ax = plt.subplots()
        ax.plot(xpeaks, ypeaks, 'bo')
        ax.plot(xpeaks, f(solution.beta, xpeaks), 'r')
        plt.show()  
        
    return solution.beta

# main calibration loop
k = 0 #number of well calibrated strips
bad_strips = []
for strip in range(start, stop + 1): 
    try:
        solution = calibrate_strip(strip, visualize=visualize)
        make_report(solution, strip, FILEPATH)
        k += 1  
        print(strip,'was calibrated successfully')
    except Exception as e:
        bad_strips.append(strip)
        print()
        print('Error occured:', e)
        print(strip, "wasn't calibrated")
        
print('Number of succesfully calibrated strips:', k)
print("Strips that weren't calibrated:", bad_strips)

