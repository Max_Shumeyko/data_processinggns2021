#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 14:05:31 2020

@author: eastwood
"""

def get_line(num, a, b):
    return '\n%d    A = %2.5f ; B = %2.1f\n' % (num, a, b)

def write_coefs(filename_output, coefs):
    """Write coefs [[A_coef, B_coef]] to filename_output.txt"""
    with open(filename_output + '.txt', 'w') as f:
        for line_num, (a, b) in enumerate(coefs):
            f.write(get_line(line_num, a, b))