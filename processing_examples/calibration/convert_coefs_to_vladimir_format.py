#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 16:22:06 2020
Read calibration coefficients from given COEFS_FOLDER,
convert to Vladimir's format and write down to coefficients.txt

@author: eastwood
"""
import os
import data_processingGNS2021.tools.spectrum as sp
# hint coefs array structure: [event_type 1-front, 2-back, 3-side]
#                     [scale 0-alpha, 1-fission][coef 0-a, 1-b][strip 0..129]

if __name__ == '__main__':
    #COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/YtCa_Feb2020_digit'
    #COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/PbCa_Feb2020_after_23jan_digit'
    _, tail = os.path.split(COEFS_FOLDER)
    coefs = sp.get_all_calibration_coefficients(coefs_folder=COEFS_FOLDER)
    
    evtypes = {'FRONT': (1, 48), 'BACK': (2, 128), 'SIDE': (3, 48)}
    scales = {'ALPHA': 0, 'FISSION': 1}
    coef = {'a': 0, 'b': 1}
    
    with open('coefficients.txt', 'w') as f:
        f.write(tail + '\n\n')
        f.write('CALIBRATION FUNCTION: F = A * CHANNEL + B;\n')
        for type_name, (type_ind, strip_num) in list(evtypes.items()):
            
            f.write('\n' + '*'*60 + '\n### ' + type_name.upper() + '\n')
            for scale_name, scale_ind in list(scales.items()):
                f.write('\n\n## ' + scale_name.upper())
                for coef_name, coef_ind in list(coef.items()):
                    f.write('\n# ' + coef_name.upper() + '\n')
                    for strip in range(1, strip_num + 1):
                        f.write(
                            str(coefs[type_ind][scale_ind][coef_ind][strip]) + ', '
                        )
        
    
    