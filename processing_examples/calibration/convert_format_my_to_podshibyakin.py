#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  6 16:18:44 2019
Convert my format coefs to Podshibyakin viewer format.
@author: eastwood
"""
import os

import numpy as np

import data_processingGNS2021.tools.spectrum as sp
  
def get_digitizer_channel(strip, e_type='front'):
    chan = strip % 16 
    chan = chan if chan != 0 else 16
    if e_type == 'front':        
        dig = strip // 16 + 1
    elif e_type == 'back':
        dig = strip // 16 + 4
    elif e_type == 'side':
        dig = strip // 16 + 12
        
    if chan == 16:
        dig -= 1
    return dig, chan
          
CLBR_LINE = '{dig:d}\t{ch:d}\t{k:1.6f}\t{b:4.6f}\n'

def get_line(k, b, strip, e_type='front'):        
    dig, ch = get_digitizer_channel(strip, e_type)
    
    line_d = {'dig': dig, 'ch': ch, 'k': k, 'b': b}
    return CLBR_LINE.format(**line_d)
    
if __name__ == '__main__':
    # calculate calibrations coefficients for all strip and detectors
    # front, back, side and write them down to the file.
    os.chdir(COEFS_FOLDER)
    front_coefs = sp.get_calibration_coefficients(
        COEFS_FOLDER + '/front_alpha.txt')
    back_coefs = sp.get_calibration_coefficients(
        COEFS_FOLDER + '/back_alpha.txt')
    side_coefs = sp.get_calibration_coefficients(
        COEFS_FOLDER + '/side_alpha.txt')
    with open('Calibration.txt', 'a') as f:
        # front 
        for strip in range(1, 49):
            a, b = front_coefs[0][strip], front_coefs[1][strip]
            f.write(get_line(a, b, strip, 'front'))
            
        # back 
        for strip in range(1, 129):
            a, b = back_coefs[0][strip], back_coefs[1][strip]
            f.write(get_line(a, b, strip, 'back'))
            
        # side 
        for strip in range(1, 49):
            a, b = side_coefs[0][strip], side_coefs[1][strip]
            f.write(get_line(a, b, strip, 'side'))
            
        for strip in range(1, 17):
            line = CLBR_LINE.format(
                        **{'dig': 15, 'ch': strip, 'k': 1.0, 'b': 0.}
                        )
            f.write(line)
            
        for strip in range(1, 17):
            line = CLBR_LINE.format(
                        **{'dig': 16, 'ch': strip, 'k': 1.0, 'b': 0.}
                        ) 
            f.write(line)