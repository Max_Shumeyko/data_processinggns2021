# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 18:59:31 2018

Perform automatic calibration of front (or back) channel spectr in fission scale.

Prepare data frame and alpha calibration coefficients to start using scripts from 
<project_folder>/data_processing/read_data. 
! note: check if alpha scale is already calibrated so the coefficients are 
required to perform calibration in fission scale.

Instruction:
    1. Get data frame from <project_folder>/data_processing/read_data<format>
    2. Specify parameters for calibration:
        * FILEPATH - adress of a file to save calibration coefficients
        * type_ - specify type of detectors to calibrate ('front'/'back')
        * scattered_ions_energy: energy of scattered ions peak 
        * start, stop: specify strips (number) of spectrs to calibrate: start, stop.
           Calibration will be performed for strips from start to stop.
           
        * border:  specify border where there are no more peaks except scattered ions after the border    
        * points_numb: amount of points of alpha-fission coincidences for calibration 
        * visualize : choose if to plot each calibrated detector's strip 
    3. Run this script to perform calibration
@author: eastwood

"""
import os
import matplotlib.pyplot as plt

import data_processingGNS2021.tools.spectrum as sp
import data_processingGNS2021.tools.calibration as clbr

from data_processingGNS2021.tools.fit_lma import fit_lma, fit_lma_robust, fit_func

class record:
    pass

# inditificators
event_type_ind = {'front': 1, 'back': 2}
scale_ind = {'alpha': 0, 'fission': 1}

### SPECIFY PARAMETERS FOR CALIBRATION
start, stop = 1, 8
type_ = 'front'
FILEPATH = os.path.join(coefs_folder, 'front_fission_probe.txt')
scattered_ions_energy = 187396.0 # energy of the peak of scattered ions
border = 1500 # specify border where there are no more peaks except scattered ions after the border    
points_numb = 800 # amount of points of alpha-fission coincidences for calibration 
visualize = True # choose if to plot each calibrated detector's strip   

    
filter_properties = record()
filter_properties.window_smooth = 5
filter_properties.smooth_wavelet = 'blackman'
filter_properties.background_options = 'BACK1_ORDER8,BACK1_INCLUDE_COMPTON' 
filter_properties.threshold = 3
   
# specify energy sample: nonzero energies from focal strip
# !note: change from 'front' to 'back' to perform calibration of back detectors
energies = sp.apply_calibration_coefficients(frame, coefs_folder, type_, 'alpha')
ind = (frame['event_type'] == event_type_ind[type_]) & (frame['strip'] > 0) &\
      (~ frame['tof'])

energies = energies[ind]
frame_ = frame[ind]

# build front fission spectr
energy_scale = False # True or False
tof = False # True False 'all'
scale = 'fission' # 'alpha'|'fission'
threshold = 0.002
bins = None
options = dict(list(zip(['coefs_folder', 'energy_scale', 'tof', 'scale', 'bins'],\
            [coefs_folder, energy_scale, tof, scale, bins])))           
spectr_front_fission = sp.Spectrum(frame_, event_type=type_, **options)

def get_data(strip):
    convert = lambda sample: np.asarray(sample, dtype=np.double)
    return convert(spectr_front_fission.bins[border+1:]),\
        convert(spectr_front_fission[strip][border:])
    
def make_report(solution, strip, filename):
    report = '\n%d    A = %2.5f ; B = %2.1f\n' % (strip, solution[0], solution[1])
    print(report)
    f = open(filename, 'a')
    f.write(report)
    f.close()
        
def calibrate_strip(strip, visualize=False):
    # get the peak of the scattered ions (the area is inside 180 - 200 MeV interval)
    x, y = get_data(strip) 
    filter_properties.threshold = max(y) * 0.2 # get the peak of the scattered ions
    x, y = clbr.filter_spectr(x[0], x[-1], x, y, filter_properties) #apply filter to supress noise
    
    #remove zero bins from filtered spectrum (noise is supressed!)
    x_ind = np.nonzero(y)[0]
    x, y = x[x_ind[0]: x_ind[-1] + 1], y[x_ind[0]: x_ind[-1] + 1] 
    #fit the scattered ions peak
    solution = fit_lma_robust(x, y, accuracy=0.0001, print_report=False)
    x_sc, y_sc = solution[0], scattered_ions_energy #centre of the peak
       
    if visualize:  #visulize the peak and the fit      
        fig, ax = plt.subplots()
        ax.plot(x, y, linestyle='steps')
        ax.plot(x, fit_func(solution, x), 'r--')
        plt.show()
    
    # get pairs of fission channels and (alpha) energies for set \
    # of events in Yb calibration area (6.3-9.3 MeV)
    ind = (frame_['strip'] == strip) & (frame_['scale'] == scale_ind['alpha']) & \
          (energies > 6300) & (energies < 9300) 
    fission_ind = np.roll(ind, 1) # according to the specificity of frame data structure
                                  # alpha-events goes right before fission events
                                  # so you need to roll forward to extract fission channels
    ypeaks, xpeaks = energies[ind][:points_numb], frame_[fission_ind]['channel'][:points_numb]
    # glue together data sets of Th calibration area and peak of scattered ions
    ypeaks, xpeaks = np.r_[ypeaks, y_sc], np.r_[xpeaks, x_sc] 
    
    # set weights for optimization algorithm
    weights = np.ones(len(xpeaks)) 
    weights[:-1] = 0.5 / (len(weights) - 1)
    weights[-1] = 0.5
    
    solution = clbr.calibrate_line(xpeaks, ypeaks, weights)
    
    if visualize:
        f = lambda P, x: P[0] * x + P[1]
        fig, ax = plt.subplots()
        ax.plot(xpeaks, ypeaks, 'bo')
        ax.plot(xpeaks, f(solution.beta, xpeaks), 'r')
        plt.show()  
        
    return solution.beta

# main calibration loop
k = 0 #number of well calibrated strips
bad_strips = []
for strip in range(start, stop + 1): 
    try:
        solution = calibrate_strip(strip, visualize=visualize)
        make_report(solution, strip, FILEPATH)
        k += 1  
        print(strip,'was calibrated successfully')
    except Exception as e:
        bad_strips.append(strip)
        print()
        print('Error occured:', e)
        print(strip, "wasn't calibrated")
        
print('Number of succesfully calibrated strips:', k)
print("Strips that weren't calibrated:", bad_strips)