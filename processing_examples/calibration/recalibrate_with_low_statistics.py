"""
Recalculate calibration coefficients on alpha front and back strips
according to low statistics. Measure new "energies" values on old coefficients
and use calibration template energies array to recalculate calibration 
coefficients. Use only if calibration lines on spectrum remain staight! 
(see using get_distributions.py)

Prepare alpha calibration coefficients to start 
using scripts from <project_folder>/data_processing/calibration. 
! note: check if alpha scale is already calibrated so the coefficients are 
required to perform calibration in fission scale.

15.01.2020 eastwood
"""
import os

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

import data_processingGNS2021.tools.spectrum as sp

"""Instruction:
1. upload alpha coefficients
2. choose new "energies" using old calibration
3. calc coefs
"""      
#### fitting functions 
def line(x, *p):
    a, b =  p
    return a * x + b

def get_fitted(channels, energies):
    """Fit data by line 
    input:
        channels - fission channels to calibrate
        energies - alpha events energies
    return coefs, coefficients_deviations, fit line
    """
    x, y = channels, energies
    p0 = [1., 0.]
    coef, corr = curve_fit(line, x, y, p0=p0)
    
    std_deviations = np.sqrt(np.diag(corr))
    curve = line(x, *coef)
    return coef, std_deviations, curve

# fit data by line and calculate fission coefficients
def write_report(output_filename, strip, coefs):
    report = '\n%d    A = %2.5f ; B = %2.1f\n' % (strip, coefs[0], coefs[1])
    with open(output_filename, 'a') as f:
        f.write(report)
    
if __name__ == '__main__':
    COEFS_FOLDER_OLD = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Jan_2020_digit'
    COEFS_FOLDER_NEW = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Feb_2020_digit'
    
    NEW_ENERGIES_FRONT = np.asarray([2593., 2693., 3664., 3996., 4268.])
    NEW_ENERGIES_BACK = np.asarray([2592., 2688., 3658., 3991., 4264,])
    CALIBRATION_ENERGIES = np.asarray([5115.2, 5304.3, 7200.3, 7862., 8404.3]) # 5223.3, 
    
    new_coefs_front, new_coefs_back = [], []
    
    
    front_coefs = sp.get_calibration_coefficients(COEFS_FOLDER_OLD + 
                                               '/front_alpha.txt')
    back_coefs = sp.get_calibration_coefficients(COEFS_FOLDER_OLD + 
                                               '/back_alpha.txt')
    # processing
    # FRONT
    (new_a, new_b), _, _ = get_fitted(NEW_ENERGIES_FRONT, CALIBRATION_ENERGIES)
    for front_strip in range(1, 49):
        old_a = front_coefs[0][front_strip]
        old_b = front_coefs[1][front_strip]
        a, b = old_a * new_a, old_b * new_a + new_b
        write_report(COEFS_FOLDER_NEW + '/front_alpha.txt', front_strip,
                     (a, b))
        
    # BACK
    (new_a, new_b), _, _ = get_fitted(NEW_ENERGIES_BACK, CALIBRATION_ENERGIES)
    for back_strip in range(1, 129):
        old_a, old_b = back_coefs[0][back_strip], back_coefs[1][back_strip]
        a, b = old_a * new_a, old_b * new_a + new_b
        write_report(COEFS_FOLDER_NEW + '/back_alpha.txt', back_strip,
                     (a, b))
        
    
        