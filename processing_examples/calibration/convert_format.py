#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 11:31:15 2020
Convert coefficients file to Vladimir's format and write them to FILENAME.txt

OUTPUT FORMAT:
---
# ALPHA FRONT
# a
0.0, ... {8 digits in line; 6 lines}
# b
...

# ALPHA BACK  
# a  
0.0, ... {8 digits in line; 16 lines}
# b
...

# FISSION FRONT
# a
0.0, ... {8 digits in line; 6 lines}
# b
..

# FISSION BACK
# a
0.0, ... {8 digits in line; 16 lines}
# b
...
---
@author: eastwood
"""

import os 

from data_processingGNS2021.tools.spectrum import \
  get_all_calibration_coefficients as get_coefs
# hint coefs array structure: [event_type 1-front, 2-back, 3-side]
#                     [scale 0-alpha, 1-fission][coef 0-a, 1-b][strip 0..129]

FMT_a = "".join("{%d:<2.5f}, " % (i, ) for i in range(8)) 
FMT_b = "".join("{%d:<2.2f}, " % (i, ) for i in range(8)) 
FMT_a += "\n"# line of 8 comma-separated floats
FMT_b += "\n"# line of 8 comma-separated floats

def write_coefs_to_stream(fobject, coefs, strip_num):
    """ Write a, b coefs to file line by line, 8 numbers at line.
    INPUT:
        fobject - file object
        coefs - np.ndarray floats [coef 0-a, 1-b][strip 0..129]
        strip_num - number of strips, each coefs line (a, b) corresponds to one strip.
        
    """
    f.write("# a" + "\n") # ind 0
    for line in range(strip_num // 8):
        num = line * 8 + 1
        f.write(FMT_a.format(*coefs[0][num: num+8]))    
        
    f.write("# b" + "\n") # ind 1
    for line in range(strip_num // 8):
        num = line * 8 + 1
        f.write(FMT_b.format(*coefs[1][num: num+8]))  

if __name__ == '__main__':
    
    # specify calibration and data folders
    COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Jan_2020_digit'
    FILENAME = 'digital_coefs.txt'
    FILENAME = os.path.join(COEFS_FOLDER, FILENAME)
    coefs = get_coefs(COEFS_FOLDER)
    
    event_types = {'front': 1, 'back': 2}
    types_strips_num = {'front': 48, 'back': 128}
    scales = {'alpha': 0, 'fission': 1}
    
    with open(FILENAME, 'w') as f:
        for scale, scale_num in list(scales.items()):
            for event_type, type_num in list(event_types.items()):
                f.write("# " + scale.upper() + " " + event_type.upper() + "\n")
                
                coefs_ = coefs[type_num][scale_num]
                strip_num = types_strips_num[event_type]
                write_coefs_to_stream(f, coefs_, strip_num)
                
                f.write("\n")
                
                    
            
            