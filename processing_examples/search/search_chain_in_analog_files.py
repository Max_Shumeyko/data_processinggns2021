# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 20:24:35 2018

Search for full chains (Recoil-alpha-alpha-...-fission fragment) correlations
in specified group of files.

Prepare data table and alpha calibration coefficients to start using scripts from 
<project_folder>/data_processing/read_data_table<forma>.py 

Instruction:
    1. Get data table from <project_folder>/data_processing/read_data_table<forma>.py
    2. Specify parameters for search:
        * time_dlt: time interval between consequent events in microseconds (1000 mcs = 1 ms
        * chain_length_min, chain_length_max: min and max number of 
            time-correlated events in the chain 
        * recoil_energy_min, recoil_energy_max: energy diaposon of Recoil for search in KeV
        * energy_min, energy_max: energy diaposon of alpha for search in KeV 
        * recoil_first: if recoil can be included as the first event in each chain.  bool
        * fission_flag: if fission events could be included as the member of each chain.  bool
        * only_fission: if search only for fission fragments. bool
        * limit: number of total events to search. If -1: search the whole data_table. bool
    3. Run this script to perform search
@author: eastwood

"""
import os
import gc

import data_processingGNS2021.tools.quick_methods as qm

import data_processingGNS2021.processing_examples.read_data.read_data_table_DEC2016

#### SEARCH CHAINS IN GROUP OF FILES
# digital key function - transform filename to int number for sorting.
# Filename pattern: '10Feb_16_14.bin', '10Feb_16_14-1.bin','10Feb_16_15.bin',...
# --> 1400, 1401, 1500, ...
#f_1 = lambda x: int(x.split('.bin')[0].split('_')[2]) * 100 # filename
#f_2 = lambda x: f_1(x) if '-' not in x else f_1(x.split('-')[0] + '.bin') + \
#    int(x.split('-')[1].split('.bin')[0])
#index_key_func = lambda x: f_2(x) if '.bin' in x else -1

# analogue key function. Filename pattern: 'tsn.400', 'tsn.401', 'tsn.402', ... 
# --> 400, 401, 402, ...
index_key_func = lambda x: int(x.split('tsn.')[1]) if 'tsn.' in x else -1
# example: print sorted(filenames, cmp=cmp_func)
cmp_func = lambda file1, file2: index_key_func(file1) > index_key_func(file2) 

    
def write_chains_to_file(chains, data_table, file_, postfix=None):
    if postfix is not None:
        filename = str(index_key_func(file_)) + '_chains' + postfix + '.txt' 
    else:
        filename = str(index_key_func(file_))+'_chains.txt'
        
    f = open(filename,'a')
    for ind in chains:
        ind_ = (data_table['energy'] >= 5000) & \
        (data_table['time_micro'] >= data_table[ind[0]]['time_micro']) & \
        (data_table['time_micro'] <= data_table[ind[-1]]['time_micro']) & \
        (data_table['cell_x'] == data_table[ind[0]]['cell_x']) & \
        (data_table['cell_y'] == data_table[ind[0]]['cell_y'])
        
        f.write(pprint.pformat(data_table[ind_])+'\n')
        f.write('\n')
        
    f.close()
                        
# Specify search parameters
search_properties.time_dlt = 4000000
search_properties.chain_length_min = 3
search_properties.chain_length_max = 10
search_properties.recoil_energy_min, search_properties.recoil_energy_max = 6500, 19000
search_properties.energy_min, search_properties.energy_max = 8000, 12000
search_properties.recoil_first = True
search_properties.fission_flag = True
search_properties.only_fission = False
search_properties.fission_time_dlt = 8000 # seconds
search_properties.limit =  -1

# Specify file folder and read files 
from data_processingGNS2021.tools.read_data_table_2016DEC import get_data_table as f
from data_processingGNS2021.processing_examples.read_data.read_functions import read_file_

read_file = lambda filename, **argv: read_file_(filename, f,\
                strip_convert=True, coefs_folder=coefs_folder)[0] # return frame

# specify calibration and data folders
coefs_folder = '../../../data/calibration_coefficients/Dec_2016' # or None
os.chdir('../../../data/analogue/Dec_2016')
         
files = [x for x in os.listdir('.') if 'tsn' in x]
#chain_rate_func = lambda x: int(x.split('chain')[0]) if 'chain' in x else -1
#last_processed_file = max(files,key=chain_rate_func)
#files = filter(lambda x: cmp_func(x, '10Dec_16_05.bin'), files)
files = sorted(files, key=index_key_func)
#files = ['10Feb_17_54.bin']#'26Dec_16_21.bin']

chains = []
for file_ in files:
    print(file_)
### TODO    frame = rf.read_dg_files(file_)
    #gc.collect()
    #data_table = sp.get_data_table_dg(frame, coefs_folder=coefs_folder)
    data_table = read_file(file_)
    gc.collect()
    for strip_x in range(1, 49):
        data_table1 = data_table[data_table['cell_x'] == strip_x]
        for strip_y in range(1, 129):
            data_table2 = data_table1[data_table1['cell_y'] == strip_y]
            if len(data_table2) > 0:
                chains1 = qm.find_chains(data_table2, search_properties)
                write_chains_to_file(chains1, data_table2, file_)
    