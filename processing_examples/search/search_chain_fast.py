# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 20:07:20 2018

Search for full chains (Recoil-alpha-alpha-...-fission fragment) correlations.

Prepare data table and alpha calibration coefficients to start using scripts from 
<project_folder>/data_processing/read_data_table<forma>.py 

Instruction:
    1. Get data table as dt_generator from 
       <project_folder>/data_processing/read_data/generator_<analogue|digital>_2019.py 
    2. Specify parameters for search:
        * time_dlt : <int> time in microseconds between two consecutive events;
        * chain_length_min : <int> minimal length of chains;
        * chain_length_max : <int> maximal length of chains; 
                           If has default value -1 -> the upper limit will set
                           to 50;
        * recoil_first : <c_bool> check if first event in chain is recoil;
        * search_fission : <c_bool> check if search includes fission decay 
                         events (energies > 40000.)
    3. Run this script to perform search
    
Output:
    dict with lists of chains:
    dict: [(cell_x, cell_y)] -> [np.ndarray[DATA_EVENT]]
@author: eastwood

"""
import numpy as np

import matplotlib.pyplot as plt
from data_processingGNS2021.tools.correlations_functions import find_chains

class record:
    pass

search_properties = record()

#### FIND R-a (recoil-alpha pairs)
search_properties.time_dlt = 26000000
search_properties.chain_length_min = 2
search_properties.chain_length_max = 2
search_properties.recoil_first = True
search_properties.search_fission = False

energy_bundle = [(6620, 6740, 'R'), (9200, 9340, 'R'), ]

if __name__ == "__main__":
    dt = next(dt_generator)
    chains = find_chains(dt, energy_bundle, search_properties)
    
    # get Recoil-alphas energies
    R_energies = []
    a_energies = []
    for key in list(chains.keys()):
        R_energies.extend([events[0]['energy_front'] for events in chains[key]])
        a_energies.extend([events[1]['energy_front'] for events in chains[key]])
        
    # show
    fig, ax = plt.subplots()
    ax.set_title('Recoil-alpha correlations')
    ax.set_xlabel('Energies of recoil particles, KeV', fontsize='large')
    ax.set_ylabel('Energies of alpha particles, KeV', fontsize='large')
    ax.scatter(R_energies, a_energies, alpha=0.3)
    plt.show()
