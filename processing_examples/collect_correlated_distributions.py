#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 13:24:46 2020

@author: eastwood

FOR DATA TABLE GENERATORS! Requires dt_generator from generator_digital_2019.py
Search for correlated events and build experimental data distributions using 
different parameters and visualisation options from several files 
using data generator! 

Group all collected distributions in one handy DataCollection class - 
see collect_distributions.py

(see <prj>/processing_examples/read_data/generator_digital_2019.py)

Main search parameters: 
* energy_bundle - list of energy limits in KeV, like  [(6620, 6740, 'a'), (9200, 9340, 'a'), ]
       energy_min, energy_max - energy limits in KeV
       event_type - recoil or alpha event type: 'a': alpha particle, 
       'R': recoil nuclei, 'F': fission fragment.
* search_properties struct:
    *  time_dlt : <int> time in microseconds between two consecutive events;
    *  chain_length_min : <int> minimal length of chains;
    *  chain_length_max : <int> maximal length of chains; 
                         If has default value -1 -> the upper limit will set
                         to 50;
    *  recoil_first : <c_bool> check if first event in chain is recoil;
    *  search_fission : <c_bool> check if search includes fission decay 
       events (energies > 40000.)     
                     
Search example:
### FIND R-a (recoil-alpha pairs)
search_properties.time_dlt = 26000000
search_properties.chain_length_min = 2
search_properties.chain_length_max = 2
search_properties.recoil_first = True
search_properties.search_fission = False

energy_bundle = [(6620, 6740, 'a'), (9200, 9340, 'a'), ]
    
dt = next(dt_generator)
chains = find_chains(dt, energy_bundle, search_properties)

Main distribution parameters (options):
*    energy_scale: specify if the spectrum is in energy scale (True) or raw channel. 
        Values: True / False
*    tof: specify if events for spectrum have time-of-flight mark.
        Values: True/False/'all' (spectrum contains all events)
*    scale: specify if spectrum contains events in alpha scale (< 25 MeV) or
        in fission scale ( 50 - 250 MeV). Values: 'alpha'/'fission'.
*    threshold: visualisation option. A parameter for summary 2D spectrum figure
        which specify the threshold as a multiplier for the maximum value of the
        spectrum.
        Values: float 0.0 - 1.0
*    bins: specify bins as np.ndarray for the distribution histogram. 
        Values: None or np.ndarray
*    coefs_folder: specify a folder with calibration coefficients to build the
        distribution in energy scale. Usually it's already specified in 
        <project_folder>/processing_examples/read_data/read_<format>.py scripts
        and ready-to-use after the frame was read
        
        
See <project_folder>/tools/spectrum.py for details. 

"""

from data_processingGNS2021.processing_examples.collect_distributions import go, dialog
from data_processingGNS2021.tools.correlations_functions import find_chains, \
  convert_search_output_to_frame

class record:
    pass

def ask_int(question='Input int value: ', convertion_func=int, 
            exc_message="Can't convert to int. Try again."):
    while True:
        x = input(question)
        try:
            convertion_func(x)
        except ValueError:
            print(exc_message)
            continue
        return convertion_func(x)
    
def ask_bool():
    return ask_int(question="Input bool value: ",
       convertion_func=bool,
       exc_message="Can't convert to bool. Try again."
    )
    
def ask_energy_bundle():
    ask_float =  lambda: ask_int(question="Input float value: ",
       convertion_func=float,
       exc_message="Can't convert to float. Try again."
    )   
    ask_type = lambda: dialog("""Choose type of event: 'a' - alpha, 'F' - fission, 
      'R' - recoil.""", answers=list('aFR'))
    return (ask_float(), ask_float(), ask_type())

def search_dialog():  
    default = {'time_dlt': 26000, # mks
               'chain_length_min': 2,
               'chain_length_max': 2,
               'recoil_first': True,
               'search_fission': False
               }
    
    print('Default search options: ', default)
    # create and fill default search parameters
    search_properties = record()
    for key, value in list(default.items()):
        setattr(search_properties, key, value)
    
    # start dialog to choose search-parameters to change
    decision_dict = dict(enumerate(default.keys()))
    options_to_change = dialog("Choose options to change: ",
        answers=list(decision_dict.keys()),
        presentation=list(decision_dict.items())
    )
    
    if options_to_change:
        for option_index in options_to_change:
            try:
                option = decision_dict[int(option_index)]
            except KeyError:
                continue
            else:
                print("\n>Specify value for option %s ->" % option)
            
            if option in ('time_dlt', 'chain_length_min', 'chain_length_max'):
                value = ask_int()
            elif option in ('recoil_first', 'search_fission'):
                value = ask_bool()
        
        setattr(search_properties, option, value)
        
    # specify energy windows and particle types for searching
    energy_bundles = []
    intervals = 0
    while intervals < search_properties.chain_length_min:
        print("""specify energy window for searching (energy_min_float, 
        energy_max: float, event type: str: """)    
        bundle = ask_energy_bundle()
        energy_bundles.append(bundle)
        print("GOT BUNDLE: ", bundle)
        intervals += 1
        if intervals >= search_properties.chain_length_min:
            answer = input('One more? [y/n] > ')
            if answer.lower() == 'n' or answer.lower() == 'no' \
              or not bool(answer):
                break
        
    return search_properties, energy_bundles

def get_frame_generator(dt_generator, coefs, 
      search_properties, energy_bundles):
    """Get data table generator, apply search properties and energy bundles
    to search in each table, convert to frame for spectrum building 
    and yields the results."""
    for dt in dt_generator:
        chains = find_chains(dt, energy_bundles, search_properties)
        frame = convert_search_output_to_frame(chains, coefs)
        yield frame
        
def check_dt_generator():
    try:
        import types
        assert isinstance(dt_generator, types.GeneratorType, \
            "dt_generator should be generator")
    except NameError:
        print("""Run digital_generator<>.py script to get dt_generator file
        reader (see <project>/processing_examples/read_show""")
        
def check_coefs():
    try:
        coefs
    except NameError:
        print("""Get calibration coefs. See digital_generator_2019.py, 
        tools/spectrum.py""")
           
if __name__ == '__main__':       
    search_properties, energy_bundles = search_dialog()
    frame_generator = get_frame_generator(dt_generator, coefs, 
      search_properties, energy_bundles)   
    collection = go(frame_generator, set_options=True)  
    collection.show()             
