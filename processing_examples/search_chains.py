# -*- coding: utf-8 -*-
"""
Created on Tue Apr 25 15:46:19 2017

@author: eastwood

"""
import os
import gc
import pprint

import data_processingGNS2021.tools.spectrum as sp
import data_processingGNS2021.tools.quick_methods as qm

#from data_processingGNS2021.processing_examples import read_files as rf

class record:
    pass

search_properties = record()

#### FIND R-a (recoil-alpha pairs)
search_properties.time_dlt = 1000
search_properties.chain_length_min = 2
search_properties.chain_length_max = 2
search_properties.recoil_energy_min = 3000
search_properties.recoil_energy_max = 19000
search_properties.energy_min = 6200
search_properties.energy_max = 11000
search_properties.recoil_first = True
search_properties.fission_flag = False
search_properties.only_fission = False
search_properties.limit = -1

chains = qm.find_chains(data_table, search_properties)
for i in chains[: 5]:
    print(data_table[i])
    
    
#### ANALISYS for Yuri
#dlt_t = [data_table['time_macro'][ind[1]] - data_table['time_macro'][ind[0]] for ind in chains]
#strip_x = []
#strip_y = []
#energies_R = []
#energies_a = []
#for ind in chains:
#     strip_x.append(data_table['cell_x'][ind[0]])
#     strip_x.append(data_table['cell_x'][ind[1]])
#     strip_y.append(data_table['cell_y'][ind[0]])
#     strip_y.append(data_table['cell_y'][ind[1]])
#     energies_R.append(data_table['energy'][ind[0]])
#     energies_a.append(data_table['energy'][ind[1]])
#
#import matplotlib.pyplot as plt    
#fig,ax = plt.subplots()
#ax.hist(dlt_t, bins=100)
#fig,ax1 = plt.subplots(2, 2)
#ax1[0,0].hist(strip_x,bins=np.arange(1, 49))
#ax1[0,1].hist(strip_y,bins=np.arange(1, 129))
#ax1[1,0].hist(energies_R, bins=100)
#ax1[1,1].hist(energies_a, bins=100)
    
    
#### FIND full chains (recoil-alpha-...-fission pairs)
#search_properties.time_dlt = 4000
#search_properties.chain_length_min = 2
#search_properties.chain_length_max = -1
#search_properties.recoil_energy_min, search_properties.recoil_energy_max = 6500, 19000
#search_properties.energy_min, search_properties.energy_max = 9200, 9300
#search_properties.recoil_first = True
#search_properties.fission_flag = False
#search_properties.only_fission = False
#search_properties.fission_time_dlt = 200 * 10 ** 6 # 200 seconds
#search_properties.limit =  -1
    
#chains = qm.find_chains(data_table, search_properties)
#
#for i in chains[: 5]:
#    print data_table[i]
    

#### SEARCH CHAINS IN GROUP OF FILES
#digital key function - transform filename to int number for sorting. Filename pattern: '10Feb_16_14.bin', '10Feb_16_14-1.bin','10Feb_16_15.bin',... --> 1400, 1401, 1500, ...
#f_1 = lambda x: int(x.split('.bin')[0].split('_')[2]) * 100 # filename
#f_2 = lambda x: f_1(x) if '-' not in x else f_1(x.split('-')[0] + '.bin') + \
#    int(x.split('-')[1].split('.bin')[0])
#index_key_func = lambda x: f_2(x) if '.bin' in x else -1

#analogue key function. Filename pattern: 'tsn.400', 'tsn.401', 'tsn.402', ... --> 400, 401, 402, ...
#index_key_func = lambda x: int(x.split('tsn.')[1]) if 'tsn.' in x else -1
#cmp_func = lambda file1, file2: index_key_func(file1) > index_key_func(file2) #use like: print sorted(filenames,cmp=cmp_func)
 
#def write_chains_to_file(chains, data_table, file_, postfix=None):
#    filename = str(index_key_func(file_)) + '_chains' + postfix + \
#       '.txt' if postfix is not None else str(index_key_func(file_)) + '_chains.txt'
#    f = open(filename, 'a')
#    for ind in chains:
#        ind_ = (data_table['time_micro'] >= data_table[ind[0]]['time_micro']) & \
#           (data_table['time_micro'] <= data_table[ind[-1]]['time_micro']) & \
#           (data_table['cell_x'] == data_table[ind[0]]['cell_x']) & \
#           (data_table['cell_y'] == data_table[ind[0]]['cell_y'])
#        f.write(pprint.pformat(data_table[ind_])+'\n')
#        f.write('\n')
#    f.close()
    
#def write_chains_to_file(chains, data_table, file_, postfix=None):
#    if postfix is not None:
#        filename = str(index_key_func(file_)) + '_chains' + postfix + '.txt' \
#    else:
#        filename = str(index_key_func(file_))+'_chains.txt'
#        
#    f = open(filename,'a')
#    for ind in chains:
#        ind_ = (data_table['energy'] >= 5000) & \
#        (data_table['time_micro'] >= data_table[ind[0]]['time_micro']) & \
#        (data_table['time_micro'] <= data_table[ind[-1]]['time_micro']) & \
#        (data_table['cell_x'] == data_table[ind[0]]['cell_x']) & \
#        (data_table['cell_y'] == data_table[ind[0]]['cell_y'])
#        
#        f.write(pprint.pformat(data_table[ind_])+'\n')
#        f.write('\n')
#        
#    f.close()
#        
#                
##search parameters
#search_properties.time_dlt = 4000000
#search_properties.chain_length_min = 3
#search_properties.chain_length_max = 10
#search_properties.recoil_energy_min, search_properties.recoil_energy_max = 6500, 19000
#search_properties.energy_min, search_properties.energy_max = 8000, 12000
#search_properties.recoil_first = True
#search_properties.fission_flag = True
#search_properties.only_fission = False
#search_properties.fission_time_dlt = 8000 # seconds
#search_properties.limit =  -1
#
#os.chdir('/home/eastwood/codes/Python_Idle/data/digital/Jan_2016')
#coefs_folder = '/home/eastwood/codes/Python_Idle/data/calibration_coefficients/Dec_2016_digital' # The folder should contain next files with calibration coefficients: front_alpha.txt, front_fission.txt, back_alpha.txt, back_fission.txt, side_alpha.txt, side_fission.txt 
#files = filter(lambda x: 'bin' in x, os.listdir('.'))
##chain_rate_func = lambda x: int(x.split('chain')[0]) if 'chain' in x else -1
##last_processed_file = max(files,key=chain_rate_func)
#files = filter(lambda x: cmp_func(x, '10Dec_16_05.bin'), files)
#files = sorted(files, key=index_key_func)
#files = ['10Feb_17_54.bin']#'26Dec_16_21.bin']
#
#chains = []
#for file_ in files:
#    print file_
#### TODO    frame = rf.read_dg_files(file_)
#    gc.collect()
#    data_table = sp.get_data_table_dg(frame, coefs_folder=coefs_folder)
#    gc.collect()
#    for strip_x in range(1, 49):
#        data_table1 = data_table[data_table['cell_x'] == strip_x]
#        for strip_y in range(1, 129):
#            data_table2 = data_table1[data_table1['cell_y'] == strip_y]
#            if len(data_table2) > 0:
#                chains1 = qm.find_chains(data_table2, search_properties)
#                write_chains_to_file(chains1, data_table2, file_)
    

