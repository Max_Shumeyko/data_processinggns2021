# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 17:08:30 2018
@author: eastwood

Build and spectrum of DGFRS' experimental data distributions using different 
parameters and visualisation options.

You can read data frame using scripts in 
<project_folder>/processing_examples/read_data/...

Main parameters (options):
*    energy_scale: specify if the spectrum is in energy scale (True) or raw channel. 
        Values: True / False
*    tof: specify if events for spectrum have time-of-flight mark.
        Values: True/False/'all' (spectrum contains all events)
*    scale: specify if spectrum contains events in alpha scale (< 25 MeV) or
        in fission scale ( 50 - 250 MeV). Values: 'alpha'/'fission'.
*    threshold: visualisation option. A parameter for summary 2D spectrum figure
        which specify the threshold as a multiplier for the maximum value of the
        spectrum.
        Values: float 0.0 - 1.0
*    bins: specify bins as np.ndarray for the distribution histogram. 
        Values: None or np.ndarray
*    coefs_folder: specify a folder with calibration coefficients to build the
        distribution in energy scale. Usually it's already specified in 
        <project_folder>/processing_examples/read_data/read_<format>.py scripts
        and ready-to-use after the frame was read
        
        
See <project_folder>/tools/spectrum.py for details. 

"""

import data_processingGNS2021.tools.spectrum as sp
#### GET DISTRIBUTIONS
# main parameters
energy_scale = False# True or False
tof = False #True False 'all'
scale = 'alpha' # 'alpha'|'fission'
threshold = 0.02
bins = None
# COEFS_FOLDER = coefs_folder
#    COEFS_FOLDER = COEFS_FOLDER
# calibration coefficients
#COEFS_FOLDER = None
#if energy_scale and COEFS_FOLDER:
#    energy = sp.apply_all_calibration_coefficients(frame, coefs_folder=COEFS_FOLDER)

## bin parameters
#set bin size (step) and channel/energy range
#sp.alpha_energy_scale_step = 20
#sp.alpha_energy_scale_range = 10000
#bins = np.arange(0, 12000, 1) #for building alpha spectrum in digital format

options = dict(list(zip(['coefs_folder', 'energy_scale', 'tof', 'scale', 'bins'],\
            [coefs_folder, energy_scale, tof, scale, bins])))

#### channel/energy distributions
spectr_front = sp.Spectrum(frame, event_type='front', **options)
spectr_back =  sp.Spectrum(frame, event_type='back', **options)
spectr_front.show()
spectr_back.show()
#spectr_side = sp.Spectrum(frame, event_type='side', **options)
#spectr_front_side = sp.CombinedSpectrum(frame, event_type='front-side', **options)
#spectr_back_side  = sp.CombinedSpectrum(frame, event_type='back-side', **options)
#
#deltaE1 = sp.TofSpectrum(frame)
#deltaE2 = sp.TofSpectrum(frame, tof_counter='tofD2')
###
#spectr_front_a = sp.Spectrum(frame, event_type='front', **options)
#spectr_front_a.show(threshold=threshold, supertitle = u'Спектр переднего детектора в автоматической калибровке')

#spectr_front.show(threshold=threshold, supertitle = u'Energy distribution for the 48 front focal detector\'s strips')#Спектр переднего детектора в автоматической калибровке')
#spectr_back.show(threshold=threshold, supertitle = u'Спектр заднего детектора')
#spectr_side.show(supertitle=u'Спектр бокового детектора')
#spectr_front_side.show(threshold=threshold, supertitle=u'Спектр корреляций переднего и бокового детекторов')
#spectr_back_side.show(threshold=threshold, supertitle=u'Спектр  корреляций заднего и бокового детекторов')
#
#deltaE1.show()
#deltaE2.show()

### save and load Spectrum
#spectr_front_a.tofile('Dec_2017_Yuri.txt')
#spectr_front_a1 = sp.Spectrum.fromfile('Dec_2017_Yuri.txt')

### position distribution
#pos_spectr = sp.PositionSpectrum(frame, **options)
#pos_spectr.get_back_distribution().show(supertitle=u'Распределение событий по задним стрипам Alpha')
#pos_spectr.get_front_distribution().show(supertitle=u'Распределение событий по передним стрипам Alpha')
#
#pos_spectr.show()

# rotation time distribution
#rot_spectr = sp.RotTimeSpectrum(frame, **options)
#rot_spectr.show()

### prepare spectrum for manual calibration
#COEFS_FOLDER = 'data/calibration_coefficients/Dec_2017_Yuri_analog/manual'
#options = dict(zip(['coefs_folder', 'energy_scale', 'tof', 'scale', 'bins'],\
#                [COEFS_FOLDER, energy_scale, tof, scale, bins]))
#spectr_front_m = sp.Spectrum(frame, event_type='front', **options)
#spectr_front_m.show(threshold=threshold, supertitle=u'Спектр переднего детектора в ручной калибровке')

# addition: search for recoil
#frame = frame[frame['channel']>800]
#l = len(frame)
#num_pre = 0
#hours = int(frame[s][-1]/3600)
##half_hours = int(hours/2)
#for i in range(1,hours):
#    num = i*int(l/10)
#    frame1 = frame[num_pre:num]
#    num_pre = num
#    pos_spectr = sp.PositionSpectrum(frame,**options)
#    pos_spectr.get_back_distribution().show()
