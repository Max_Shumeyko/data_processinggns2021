# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 16:13:02 2018

@author: eastwood
"""
import os
import re
from datetime import datetime

import numpy as np

from functools import partial


def get_np_name(filename, data_type):
    return "{0}_{1}.npy".format(filename, data_type)

def get_log_name(filename, data_type):
    return "{0}_{1}_log.txt".format(filename, data_type)

def tofile(obj, filename, data_type):
    assert type(obj) == tuple, \
        '{} object should be tuple (data, filesize)'.format(obj)
    assert len(obj) == 2, \
        '{} object should be tuple (data, filesize)'.format(obj)
        
    table, size = obj # np.ndarray table array, int size
    assert type(table) == np.ndarray, \
        'table type should be np.ndarray'
    assert type(size) == int, 'table size should be int'
    
    # write numpy table to .npy file
    np_filename = get_np_name(filename, data_type)
    np.save(np_filename, table)
    
    # write additional data to log file
    log_filename = get_log_name(filename, data_type)
    with open(log_filename, 'w') as f:
        f.write('filename: ' + filename + '\n')
        f.write('size: ' + str(size) + '\n')
        f.write('timestamp: ' + str(datetime.now()) + '\n')
        f.write('table type: ' + str(table.dtype) + '\n')
        f.write('len: ' + str(len(table)) + '\n')
    
def fromfile(filename, data_type):
    # read numpy table from .npy file
    np_filename = get_np_name(filename, data_type)
    table = np.load(np_filename)
    
    # read size parameter from log file
    log_filename = get_log_name(filename, data_type)
    with open(log_filename, 'r') as f:
        line = f.readlines()[1]
        line = line.split('size: ')[1]
        size = int(line)
        
    return table, size

def read_file_(filename, read_func, 
               data_type='frame', serialize=True, **argv):
    """ 
    Read file, save serialized copy and log file
    and print the filename. Return None and print a message if
    the file doesn't exists. 

    Log format:
        filename: str 
        size: len(data_frame)
        timestamp: like '2019-09-03 18:55:23.367310'
        data type: data_frame.dtype (event frame or data_table)
    
    """
    print('-> reading', filename, '...')
    
    # check if the serialized file exists
    np_filename = get_np_name(filename, data_type)
    log_filename = get_log_name(filename, data_type)
    if os.path.isfile(np_filename) and \
            os.path.isfile(log_filename):
        print('-> reading serialized objects of', filename, '...')
        table, size = fromfile(filename, data_type)
        print('-> DONE')
        return table # return if the serialized file exists
    
    # check if the file exists
    if not os.path.isfile(filename):
        print('File %s is missed' % (filename,))
        return None
    
    # read and serialize data
    data = read_func(filename, **argv)
    if serialize:
        print('-> writing serialized objects of', filename, '...')
        tofile(data, filename, data_type)
    print('-> DONE')
    table, size = data
    return table

def parse_names(filenames, prefix, postfix):
    # create filenames list 
    if '-' in filenames:
        numb = re.findall('\d+', filenames)
        numb = list(range(int(numb[0]), int(numb[1]) + 1))
        names = [prefix + str(num_) + postfix for num_ in numb]
        
    elif ',' in filenames:
        names = filenames.split(',')  
        names = [s.strip() for s in names]
        
    elif (prefix in filenames): # len(filenames) < 8 and 
        names = [filenames.strip(), ]#.replace(' ', ''), ]  
        
    else:
        raise ValueError('read_files: Incorrect filenames') 
    
    return names
        
def read_files_(filenames, read_func, prefix='tsn.', postfix='', **argv):
    """
    Read DGFRS experimental data in binary format. (tsn.*, gns.* and so on)
    The function parses 'filenames' input, construct file list and sends it
    to read_func reader function and then concatenate the output.
    
    read_files(filenams, prefix='tsn.', **argv)
        filenames - add names of data files. See filenames's patterns.
        prefix - prefix to separate filename and number
        data_type - 'frame'/'data_table' type of the table,
                    'frame' - table of separated signals front/back/side/..
                    'data_table' - table of full physical events, cell_x+cell_y+side ..
        strip_convert [True/False] - apply special arrays to reorder strip numbers. see 'read_file.pyx'
    return np.ndarray data table. See format description.
    
    Valid filename's patterns:
    <prefix.>xxx
    <prefix.>xxx, <prefix.>yyy, <prefix.>zzz, ...
    <prefix.>xxx - <prefix.>yyy
    <prefix.>xxx-<prefix.>yyy and etc.
    , where xxx, yyy, etc. - int numbers of files.
    
    Usual output format description:
        np.ndarray[EVENT] OR np.ndarray[DATA_EVENT] - format of separate 
        events from detectors, or data events which assembles set of events
        according to phisical particle (MWPC, focal, side time coinsidances)
        See data_types.pxd array_types.py  
        
    """
    if read_func is None:
        raise ValueError('Specify reading function f')
        
    if type(filenames) == np.ndarray:
        return filenames
    
    # create filenames list 
    names = parse_names(filenames, prefix, postfix)
    
    # apply read function to each file from names 
    # and glue all together into one table (np.ndarray)
    print('Reading ...')
    read_func = partial(read_func, **argv) #data_type=data_type,
    if len(names) > 1:
        event_tables = [i for i in map(read_func, names) if i is not None]
    elif len(names) == 1:
        return read_func(names[0]) 
    print('END.')
    
    # if len(filenames) > 1 then concatenate them and return 
    try:
        return np.concatenate(event_tables)  
    except ValueError as e:
        print('Check filenames {}'.format(names))
        print('Check the frame: ', event_tables, '\n')
        raise ValueError(e)
    except TypeError as e:
        print('Event tables have different types')
        print('Type check:')
        for table in event_tables:
            print('TYPE: ', table.dtype)
            print('LEN: ', len(table))
            print()
        raise TypeError(e)
        

def mapper_(filenames, read_func, prefix='tsn.', postfix='', **argv):
    """
    The generator yields DGFRS experimental data in binary format. 
    (tsn.*, gns.* and so on)
    The generator function parses 'filenames' input, construct file list and sends it
    to read_func reader function and then concatenate the output.
    
    read_files(filenams, prefix='tsn.', **argv)
        filenames - list of filenames [<prefix>num1, .., <prefix>numN]
        read_func - specify function for reading
        prefix - prefix to separate filename and number
        postfix - postfix to separate filename and nubmer
        
        **argv - specify additional parameters for read_func such as:
            data_type - 'frame'/'data_table' type of the table,
                    'frame' - table of separated signals front/back/side/..
                    'data_table' - table of full physical events, 
                                   cell_x+cell_y+side ..
            strip_convert [True/False] - apply special arrays to reorder 
                    strip numbers. see 'read_file.pyx'
    
    yield np.ndarray data tables. See format description in 
        <project>/tools/data_types.pxd.
      
    """
    if read_func is None:
        raise ValueError('Specify reading function f')
        
#    print 'Check filenames: ', filenames
    if type(filenames) == np.ndarray:
        yield filenames
    
    # create filenames list 
#    names = parse_names(filenames, prefix, postfix)
    names = filenames
    
    # apply read function to each file from names 
    # and glue all together into one table (np.ndarray)
    print('Reading ...')
    read_func = partial(read_func, **argv) #data_type=data_type,
    if len(names) > 1:
        for name in names:
            frame = read_func(name)
            if frame is not None:
                yield frame
            else:
                print('File {0} is empty'.format(name))
                yield None
    elif len(names) == 1:
        yield read_func(names[0]) 
    print('END.')

