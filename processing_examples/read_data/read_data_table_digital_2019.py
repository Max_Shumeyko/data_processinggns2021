# -*- coding: utf-8 -*-
"""


GET DATA_TABLE DIGITAL 2019
Read analog experimental data from raw binary to data table frame and
calibration coefficients to coefs.
The data format is actual for 2019.

Valid filename's patterns:
    <prefix.shdat>
    <prefix.>xxx
    <prefix.>xxx, <prefix.>yyy, <prefix.>zzz, ...
    <prefix.>xxx - <prefix.>yyy
    <prefix.>xxx-<prefix.>yyy and etc.
    
Output format description (np.ndarray):
    event_type: 1 - focal-back, 3 - side
    energy_front: double 0 .. 250000 [KeV]
    energy_back: double 0 .. 250000 [KeV]
    cell_x: x coordinate 0 .. 48
    cell_y: y coordinate 0 .. 128
    time_micro: int microseconds 
    time_macro: int seconds 
    beam_mark: bool beam-on mark
    tof: bool time-of-flight mark   

@author: eastwood
"""
import os

import data_processingGNS2021.tools.spectrum as sp
from .read_functions import read_file_, read_files_

from data_processingGNS2021.tools.read_digital_2019 import read_data_table as f
#read_data_table(filename,strip_convert=True,offset=0,coefs_folder=None)
# read functions  
  
#read_data_table(filename, offset=0, coefs_folder=None)
#    
#main_reading_dt_loop(long count_blocks, FILE* cfile, \
#np.ndarray[DATA_EVENT, cast=True] data_table, \
#double[:,:,:,::1] coefs)
#    
read_file = lambda filename, **argv: read_file_(filename, f,\
                coefs_folder=coefs_folder, **argv) # return frame, filesize
read_files = lambda filenames, **argv: \
                    read_files_(filenames, read_file, 
                                prefix='', data_type='data_table') # return data_table

# specify calibration and data folders
coefs_folder = '/home/eastwood/gitlab_projects/data/digital/Oct_2019/Coefs'
os.chdir('/home/eastwood/gitlab_projects/data/digital/Nov_2019')

# READ
filenames = '5november3.shdat'

coefs = sp.get_all_calibration_coefficients(coefs_folder=coefs_folder)
coefs_folder = os.path.abspath(coefs_folder)

if __name__ == '__main__':
    data_table = read_files(filenames)#, coefs_folder=coefs_folder) 
    #data_table = data_table[(data_table['energy'] > 100) & (data_table['energy'] < 290000)] # cut trash