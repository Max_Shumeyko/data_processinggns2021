# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 16:29:19 2017

@author: eastwood
"""

import os
import new_data_processing.tools.read_files as rf
import new_data_processing.tools.spectrum as sp
import new_data_processing.tools.quick_methods as qm
import calendar
from dateutil.parser import parse
import pandas as pd
import time

date_to_int = lambda date_str: calendar.timegm(parse(date_str).timetuple())

#### GET DATA ANALOGUE may format    
import new_data_processing.tools.read_file_2Nov2017 as read                                                   
os.chdir('../../../data/analogue/Oct_2017_Yuri_probe')
filenames = 'tsn.82'
frame = rf.read_files(filenames,f = lambda name: read.read_file(name,strip_convert=False))
##
coefs_folder = '/home/eastwood/codes/Python_Idle/data/calibration_coefficients/May_2017_Yuri_probe' # The folder should contain next files with calibration coefficients: front_alpha.txt, front_fission.txt, back_alpha.txt, back_fission.txt, side_alpha.txt, side_fission.txt  #'/home/eastwood/codes/Python_Idle/data/calibration_coefficients/Dec_2016'
#data_table = rf_april.get_data_table(frame,coefs_folder)
#yuri_type = np.dtype([('w1',np.uint16),('w2',np.uint16),('w3',np.uint16),('w4',np.uint16),('w5',np.uint16),('w6',np.uint16),('w7',np.uint16),('w8',np.uint16),('w9',np.uint16),('w10',np.uint16),('w11',np.uint32),('w12',np.uint32)])

#### GET DISTRIBUTIONS
#main values
energy_scale = False # True or False
tof = False # True|False|'all'
scale = 'alpha' # 'alpha'|'fission'
threshold = 0.2
bins = None
if energy_scale:
    energy = sp.apply_all_calibration_coefficients(frame,coefs_folder=coefs_folder)
#print 'control energy',time.time()-t0
options = dict(list(zip(['coefs_folder','energy_scale','tof','scale','bins'],[coefs_folder,energy_scale,tof,scale,bins])))

#### channel/energy distributions
spectr_front = sp.Spectrum(frame,event_type='front',**options)
spectr_back =  sp.Spectrum(frame,event_type='back',**options)
#spectr_front_side = sp.CombinedSpectrum(frame,event_type='front-side',**options)
#spectr_back_side  = sp.CombinedSpectrum(frame,event_type='back-side',**options)
###
spectr_front.show(threshold = threshold)
spectr_back.show(threshold = threshold)

#spectr_front_side.show(threshold = threshold)
#spectr_back_side.show(threshold = threshold)

### position distribution
#pos_spectr = sp.PositionSpectrum(frame,**options)
#pos_spectr.show()

##find threshold for Yuri
#ind = frame['event_type']==2
#print 'test:', np.sum(frame[ind]['DAD4']),np.sum(ind)
#ind = (frame['scale']==0)&(frame['event_type']==2)
#frame1=frame[ind]
#energy1=energy[ind]
#en_scale = np.arange(1,5001,100)
#en_pre=1
#counts=[]
#counts_back=[]
#for en_ in en_scale[1:]:
#    ind = (energy1>en_pre)&(energy1<=en_)
#    counts.append(np.sum(ind))
#    counts_back.append(np.sum(frame1[ind]['DAD4']))
#    en_pre=en_
#fig,ax=plt.subplots()
#ax.plot(en_scale[1:],counts)
#ax.plot(en_scale[1:],counts_back)
#plt.show()    
#f = open('pairs_044.txt','w')
#f.write('energy counts back_counts\n')
#for i,j,k in zip(en_scale[1:],counts,counts_back):
#    f.write('{:5d}{:7d}{:7d}\n'.format(i,j,k))
#f.close()

#addition: search for recoil
#frame = pd.DataFrame(frame)
#frame['energy'] = energy
#formatter = lambda x: "{0:16.1f}".format(x)
#frame['synchronization_time']= frame['synchronization_time'].apply(formatter)
#frame[(frame['synchronization_time']>'34146064000000.0')&((frame['strip']==21)|(frame['strip']==63)|(frame['strip']==64))]

### MEASURE TARGET
#time_spec = sp.get_target_time_distribution(frame,output='target_measures.txt',filenames=filenames,coefs_folder=coefs_folder)    
#sp.read_target_time_distr('target_measures.txt')