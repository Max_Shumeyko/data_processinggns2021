# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 13:23:12 2017

@author: eastwood
"""

import os
import new_data_processing.tools.read_files as rf
import new_data_processing.tools.spectrum as sp
import new_data_processing.tools.quick_methods as qm
import calendar
from dateutil.parser import parse
import pandas as pd
import time
import gc

#### GET DIGITAL DATA
t0 = time.time()
os.chdir('../../../data/digital/Oct_2017')
filenames = '31Oct_17_48_alpha_test.bin'
frame = rf.read_dg_files(filenames)
coefs_folder = '/home/eastwood/codes/Python_Idle/data/calibration_coefficients/Dec_2016_digital' # The folder should contain next files with calibration coefficients: front_alpha.txt, front_fission.txt, back_alpha.txt, back_fission.txt, side_alpha.txt, side_fission.txt 
#print 'control read_dg_files',time.time()-t0
#gc.collect()
###                                                     '/home/eastwood/codes/Python_Idle/new_data_processing/data/calibration_coefficients/Dec_2016'
### get cells table
#data_table = sp.get_data_table_dg(frame,coefs_folder=coefs_folder)
#gc.collect()
#print 'control make data_table',time.time() - t0
##apply date-time filter
#t_start,t_stop = '2016-12-10 10:00:00','2016-12-11 00:00:00'
#t_start,t_stop = date_to_int(t_start),date_to_int(t_stop)
#frame = frame[(frame['time']>t_start)&(frame['time']<t_stop)]

#### GET DISTRIBUTIONS
#main values
energy_scale = False # True or False
tof = False # True|False|'all'
scale = 'alpha' # 'alpha'|'fission'
threshold = 0.2
bins = None
##set bin size (step) and channel/energy range
#sp.alpha_energy_scale_step = 20
#sp.alpha_energy_scale_range = 10000
#bins = np.arange(0,12000,1) #for building alpha spectrum in digital format
#if energy_scale:
#    energy = sp.apply_all_calibration_coefficients(frame,coefs_folder=coefs_folder)
#print 'control energy',time.time()-t0
options = dict(list(zip(['coefs_folder','energy_scale','tof','scale','bins'],[coefs_folder,energy_scale,tof,scale,bins])))

#### channel/energy distributions
spectr_front = sp.Spectrum(frame,event_type='front',**options)
spectr_back =  sp.Spectrum(frame,event_type='back',**options)
#spectr_front_side = sp.CombinedSpectrum(frame,event_type='front-side',**options)
#spectr_back_side  = sp.CombinedSpectrum(frame,event_type='back-side',**options)
###
spectr_front.show(threshold = threshold)
spectr_back.show(threshold = threshold)


