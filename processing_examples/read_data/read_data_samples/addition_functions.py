# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 13:13:29 2017

@author: eastwood
"""

def find_peak_naive(x,y):
    n = len(x)
    y_max = y.max()
    for i in range(n):
        if y[i] == y_max:
            break
    return x[i]