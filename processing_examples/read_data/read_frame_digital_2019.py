# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 17:16:04 2018

GET DATA DIGITAL 2019
Read analog experimental data from raw binary to data frame and
calibration coefficients to coefs.
The data format is actual for JULY 2019.

Valid filename's patterns:
    <prefix.>xxx
    <prefix.>xxx, <prefix.>yyy, <prefix.>zzz, ...
    <prefix.>xxx - <prefix.>yyy
    <prefix.>xxx-<prefix.>yyy and etc.
    
Output format description (np.ndarray): 
    event_type: '1'-front, '2'-back, '3'-side, '4'-veto
    strip: front: 1-48 back: 1-128, side: 1-6
    channel: front: 4096 back: 8192, side: 8192
    scale: 0-alpha, 1-fission
    time_macro: int64 seconds
    time_micro: int64 microseconds
    beam_mark: bool
    tof: bool
    tofD1: bool
    tofD2: bool
    synchronization_time: uint16
    rotation_time # optical detector on rotation target
    

@author: eastwood
"""
import os 

import data_processingGNS2021.tools.spectrum as sp
from .read_functions import read_file_, read_files_

from data_processingGNS2021.tools.read_digital_2019 import read_file as f

# read functions    
read_file = lambda filename, **argv: read_file_(filename, f, **argv) # return frame, filesize
read_files = lambda filenames: read_files_(filenames,
                                           read_file, prefix='',
                                           data_type='frame',
                                           serialize=False) # return frame

# specify calibration and data folders
coefs_folder = '/home/eastwood/gitlab_projects/data/digital/Oct_2019/Coefs'
#'/home/eastwood/gitlab_projects/data/calibration_coefficients/Dec_2016' # or None
#os.chdir('/home/eastwood/codes/C++/DAQ_GNS2019/exe/SIMULATION_DATABASE')
os.chdir('/home/eastwood/gitlab_projects/data/digital/Nov_2019')
#os.chdir('/home/eastwood/gitlab_projects/data/digital/Oct_2019')
#         /home/eastwood/gitlab_projects/data/digital/Sep_2019')
# READ
#filenames = 'tsn.3 - tsn.7'
#filenames = 'SimTh.shdat'
filenames = '5november3.shdat' #09oct3.shdat'
# frame, filesize = read_file('tsn.3')
frame = read_files(filenames)

coefs = sp.get_all_calibration_coefficients(coefs_folder=coefs_folder)
coefs_folder = os.path.abspath(coefs_folder) 
