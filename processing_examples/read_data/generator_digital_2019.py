# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 17:16:04 2018

GET GENERATOR DIGITAL 2019
Read digital experimental data from raw binary to data frame and
calibration coefficients to coefs.
The data format is actual for OCT 2019.  
    
Output format description (np.ndarray): 
    event_type: '1'-front, '2'-back, '3'-side, '4'-veto
    strip: front: 1-48 back: 1-128, side: 1-6
    channel: front: 4096 back: 8192, side: 8192
    scale: 0-alpha, 1-fission
    time_macro: int64 seconds
    time_micro: int64 microseconds
    beam_mark: bool
    tof: bool
    tofD1: bool
    tofD2: bool
    synchronization_time: uint16
    

@author: eastwood
"""
import os 

import data_processingGNS2021.tools.spectrum as sp
from .read_functions import read_file_, mapper_

from data_processingGNS2021.tools.read_digital_2019 import read_file as read_digital

# read functions    
read_file = lambda filename, **argv: read_file_(filename, read_digital, 
                                                serialize=True, **argv) # return frame, filesize
mapper = lambda filenames: mapper_(filenames,
                                   read_file, prefix='',
                                   data_type='frame') # return frame

# specify calibration and data folders
COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Dec_2016' # or None
#'/home/eastwood/codes/C++/DAQ_GNS2019/exe/SIMULATION_DATABASE'
DATA_PATH = '/home/eastwood/gitlab_projects/data/digital/Sep_2019'

# READ
#filenames = 'SimTh.shdat'
filenames = ['Alpha_correct.shdata', ]
f = lambda file_: os.path.join(DATA_PATH, file_)
filenames = list(map(f, filenames))

file_generator = mapper(filenames)

coefs = sp.get_all_calibration_coefficients(coefs_folder=COEFS_FOLDER)
coefs_folder = os.path.abspath(COEFS_FOLDER) 
