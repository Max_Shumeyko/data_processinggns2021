#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 15:22:36 2019

Read protocol file containing data of magnet currents, time, beam current.
@author: eastwood
"""
import os

from datetime import datetime

import pandas as pd

def read_protocol(dir_='/home/eastwood/gitlab_projects/data/addition'):
    """str -> pd.DataFrame
    
    Read protocol.txt from given dir_ folder.
    columns: ['time':datetime{"%Y-%m-%d %H:%M"}, 'Q1':int{A}, 'D1':int{A}, 
                'Q2':int{A}, 'Q3':int{A}, 'D2':int{A}, 
                'beam_current':float{mkA}]
    """
    os.chdir(dir_)
    frame = pd.read_csv('protocol.txt', sep='\s+')
    to_datetime = lambda line: datetime.strptime(line, "%d/%m/%Y %H:%M:%S")
    frame['time'] = (frame['DATA'] + ' ' + frame['TIME']).apply(to_datetime)
    frame['beam_current'] = frame['T3FC6']
    frame = frame[['time', 'Q1', 'D1', 'Q2', 'Q3', 'D2', 'beam_current']]
    # convert text beam current to float
    str_to_float = lambda x: float(x.replace(',', '.'))
    frame['beam_current'] = frame['beam_current'].apply(str_to_float)
    # convert text to int for beam currents
    columns = ['Q1', 'D1', 'Q2', 'Q3', 'D2']
    frame[columns] = frame[columns].applymap(lambda x: int(str_to_float(x)))
    
    # correct negative beam currents
    frame.loc[frame['beam_current'] < 0, 'beam_current'] = 0.
    frame.index = frame['time']
    return frame
    