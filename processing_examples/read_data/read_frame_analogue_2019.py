# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 16:31:09 2018

GET DATA ANALOGUE 
Read analog experimental data from raw binary to data frame and
calibration coefficients to coefs.
The data format is actual for Oct 2019.

Valid filename's patterns:
    <prefix.>xxx
    <prefix.>xxx, <prefix.>yyy, <prefix.>zzz, ...
    <prefix.>xxx - <prefix.>yyy
    <prefix.>xxx-<prefix.>yyy and etc.
    
Output format description (np.ndarray):
    event_type: '1'-front, '2'-back, '3'-side, '4'-veto
    strip: front: 1-48 back: 1-128, side: 1-6
    channel: front: 4096 back: 8192, side: 8192
    scale: 0-alpha, 1-fission
    time_macro: int64 seconds
    time_micro: int64 microseconds
    beam_mark: bool
    tof: bool
    tofD1: bool
    tofD2: bool
    synchronization_time: uint16  
    rotation_time # optical detector on rotation target

@author: eastwood
"""
import os

import data_processingGNS2021.tools.spectrum as sp
from read_functions import read_file_, read_files_, mapper_

from data_processingGNS2021.tools.read_file_2019 import read_file as f

# read functions    
read_file = lambda filename, **argv: read_file_(
    bytes(filename, encoding='utf-8'), 
    f, strip_convert=True,
    count_blocks=0, 
    **argv
) # return frame, filesize
read_files = lambda filenames: read_files_(filenames, 
                                           read_file, prefix='gns. ', 
                                           data_type='frame',
                                           serialize=False) # return frame gns.

mapper = lambda filenames: mapper_(filenames, 
                                   read_file, prefix='gns. ', 
                                   data_type='frame') # return frame gns.

# specify calibration and data folders
#coefs_folder =  '/home/eastwood/gitlab_projects/data/calibration_coefficients/Feb_2018_analog' 
#coefs_folder =  '/home/eastwood/gitlab_projects/data/calibration_coefficients/Sep_2019_sim_analog'# or None
coefs_folder = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Oct_2019'
#/home/eastwood/gitlab_projects/data/analogue/Oct_2019/YtAr40/Coef1' # Coefs# Yt40At Oct2019
#coefs_folder = '/home/eastwood/gitlab_projects/data_processingGNS2021/processing_examples/generate_data'
#os.chdir('/home/eastwood/gitlab_projects/data/analogue/2019')
#os.chdir('/home/eastwood/gitlab_projects/data_processingGNS2021/processing_examples/generate_data')
#os.chdir('/home/eastwood/gitlab_projects/data/simulation/analog/Sep_2019') # simulation
os.chdir('/home/eastwood/gitlab_projects/data/analogue/Oct_2019/YtAr40') # YtAr40 clbr
# READ
#filenames = 'probe.bin'#, probe_fission.bin'
filenames = 'gns. 352 - gns. 354'
#frame, filesize = read_file('gns.347')
frame = read_files(filenames)

generator = mapper(filenames)

coefs = sp.get_all_calibration_coefficients(coefs_folder=coefs_folder)
coefs_folder = os.path.abspath(coefs_folder) 
COEFS_FOLDER = coefs_folder
