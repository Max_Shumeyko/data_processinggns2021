cimport numpy as np
ctypedef np.uint16_t c_word
ctypedef np.uint8_t c_bool
    
cdef packed struct EVENT:
    np.int_t event_type # 1 - focal, 2 - back, 3 - side, 4 - veto
    np.int_t strip # 0 .. 128
    np.int_t channel # 1 .. 32000
    np.int_t scale # 0 - alpha, 1 - fission
    np.int64_t time_macro # mcs
    np.int64_t time_micro
    c_bool beam_mark # bool
    c_bool tof # bool
    c_word tofD1
    c_word tofD2
    c_word synchronization_time 
    c_word rotation_time
                 

cdef packed struct DATA_EVENT:
    np.int_t event_type # 1 - focal-back, 3 - side
    double energy_front
    double energy_back
    np.int_t cell_x # 0 .. 48
    np.int_t cell_y # 0 .. 128
    np.int64_t time_macro # seconds 
    np.int64_t time_micro # microseconds 
    c_bool beam_mark # bool
    c_bool tof # bool  
    c_word tofD1
    c_word tofD2                     
    c_word rotation_time
    
cdef packed struct BLOCK:
    c_word w0
    c_word w1
    c_word w2
    c_word w3
    c_word w4
    c_word w5
    c_word w6
    c_word w7
    c_word w8
    np.uint8_t w9
    np.uint8_t w10
    np.uint32_t w11
    np.uint32_t w12
    c_word w13
    c_word w14

cdef packed struct BLOCK_2016:
    c_word w0
    c_word w1
    c_word w2
    c_word w3
    c_word w4
    c_word w5
    c_word w6
    c_word w7
    c_word w8
    c_word w9
    c_word w10
    c_word w11
    c_word w12
    c_word w13 

#dtype_Dec2017 = np.dtype([('w0',np.uint16),('w1',np.uint16),('w2',np.uint16),('w3',np.uint16),('w4',np.uint16),('w5',np.uint16),('w6',np.uint16),('w7',np.uint16),('w8',np.uint16),('w9',np.uint16),('w10',np.uint32),('w11',np.uint32)])  

ctypedef fused c_number_array:
    double[:] 
    int[:]
    long[:]
    c_word [:]
    c_bool[:]

ctypedef fused c_number:
    double
    int
    long
    c_word
    c_bool
 
#np.types   
#event_type = np.dtype([('event_type',int),('strip',int),('channel',int),('scale',int),('time_macro',np.int64),('time_micro',np.int64)\
#        ('beam_mark',np.bool),('tof',np.bool),('tofD1',np.uint16),('tofD2',np.uint16),('synchronization_time',np.uint16)])
 
#energy_event_type = np.dtype([('event_type',int),('strip',int),('energy',float),('scale',int),('time',np.double),\
#    ('beam_mark',np.bool),('tof',np.bool),('synchronization_time',np.uint16),('DAD4',np.uint16),('DAD5',np.uint16),('target_time',np.uint16)])

#final_event_type = np.dtype([('event_type',int),('energy',float),('cell_x',int),('cell_y',int),\
#                    ('time_micro',np.int64),('time_macro',np.int64),('beam_mark',np.bool),('tof',np.bool)])    
