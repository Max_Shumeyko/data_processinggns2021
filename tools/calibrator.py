# -*- coding: utf-8 -*-
"""
    Created on Wed May 14 14:40:14 2014
    This code is for simple calibration of gaussian peaks by hand.
    @author: eastwoodknight
"""
import os
from math import log

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.optimize import leastsq

from .mpl_cmd import fit_commander
from . import spectrum_tools as st #data_processing_2016.tools.python_scripts.
from .fit_lma import fit_lma, fit_lma_robust, init_p

#data_processing_2016.tools.cython_scripts.
#fitting method functions
class CalibrationError(Exception):
    pass

def line(p,x):
    return p[0] * np.ones(len(x)) + p[1] * x 

def exponential(p,x):
    return p[0] * np.ones(len(x)) + p[1] * np.exp(-x / p[2]) 
    
def gaussian(P,x):
    return P[2] * np.exp(-((x - P[0]) / P[1]) ** 2 / 2) + P[3] * (x - P[5]) + P[4]

def filter_spectr(xmin, xmax, xsample, sample, properties): #window_smooth=7,smooth_wavelet='hanning',background_options='BACK1_ORDER4'):
    #sample = np.array(sample)
    x_ind = (xsample >= xmin) & (xsample <= xmax)
    print(type(x_ind), type(sample), len(x_ind), len(sample))
    sample = sample[x_ind]
    xsample = xsample[x_ind] #).tolist() 
    if (sample == 0).sum() == len(sample):
        raise  CalibrationError('No data in given sample')
    if properties.window_smooth: 
        sample = st.smooth(sample, properties.window_smooth, properties.smooth_wavelet)
        sample_background = st.background(sample, parameters=properties. background_options) #,BACK1_INCLUDE_COMPTON
        sample -= sample_background
        sample[sample < properties.threshold] = 0  
    return xsample, sample
    

def fit_method(x, y, func=gaussian, fit_type='default'):
    x, y = np.asarray(x, dtype=np.double), np.asarray(y, dtype=np.double)
    def residuals(p, y, x):
        return y - func(p, x)

    p0 = init_p(x, y)
    good_fitting = True   
    if fit_type == 'default':
        solution = leastsq(residuals, p0, args=(y, x), Dfun=None)[0] #A,x0,sigm      
    elif fit_type == 'fast':
        solution = np.asarray(fit_lma(x, y, accuracy=0.00001, print_report=False))
    elif fit_type == 'robust':
        solution = np.asarray(fit_lma_robust(x, y, accuracy=0.0001, print_report=False))
        
    x0 = solution[0]
        
    if (x0 < min(x)) or (x0 > max(x)):
        print('bad dataset for gaussian fitting')
        x0 = (x * y).sum() / y.sum()
        solution = x0
        good_fitting = False
    return solution, good_fitting   
    

def make_report(xpeaks, solution, ax, calibration_function=line, \
        filename=None, energies=None, ind=1):
    #energies = np.array([6040,6143,6264,6899,7137,7922,8699,9265]) 
    report = '\n Solution:'
    
    if calibration_function == line:
        report = '\n%d    A = %2.5f ; B = %2.1f\n' % (ind, solution[1], solution[0])
    else:
        report = '\n%d    ' %(ind)
        msv1 = ['A', 'B', 'C', 'D', 'E', 'F']
        for i,coef in enumerate(solution):
            report += str(msv1[i]) + ' = ' + str(coef) + ' ; '
        
    report += '%5s  %5s      %4s   %5s \n' % ('Eexp', 'Ecal', 'Differ', 'Channel')
    S = 0
    if calibration_function == line:
        func = lambda p, x: p[0] + p[1] * x
    elif calibration_function == exponential:
        func = lambda p, x: p[0] + p[1] * np.exp( - x / p[2]) 
    else:
        print('Calibration function is unsuitable')
        return
     
    ypeaks = []       
    for i, en in enumerate(energies):
        Ecalc = func(solution, xpeaks[i]) # solution[0] + solution[1] * xpeaks[i]
        report += '%4.1f  %4.1f    %-5.1f    %-4.1f \n' % (en, Ecalc,\
            Ecalc - en, xpeaks[i]) 
        S += (Ecalc - en) ** 2
    
    xpeaks = np.linspace(min(xpeaks), max(xpeaks), 500)
    ax.plot(xpeaks, func(solution, xpeaks), 'g--')
    plt.show()
    if filename:
        f = open(filename, 'a')
        f.write(report)
        f.close()
    report += 'S/n = %3.1f \n' % (S ** 0.5 / len(energies))
    return report#, (S**0.5/len(energies))   
    
def get_filter_properties():
    class record: pass
    filter_properties = record()
    filter_properties.window_smooth = 5
    filter_properties.smooth_wavelet = 'blackman'
    filter_properties.background_options = 'BACK1_ORDER8,BACK1_INCLUDE_COMPTON' 
    filter_properties.threshold = 3
    return filter_properties
    
    
def is_iterable(obj_):
    try:
        iterator = iter(obj_)
        return True
    except TypeError:
        return False    
    
def get_examples():
    print("""
    
    """)
    
#main class     
class calibrator:
    """
    The class is for performing energy calibration on some amplitude spectrum according for some nuclear decay sequece. 
    It allows to fit set of peaks using highly optimized fitting method by gaussian function with with line basement and
    then calibrate it using some energy set and given calibration function. 
    Initialize an object of calibrator to calibrate histograms by hand:
    Clbr = calibrator(self,x = None, y = None,energies=None,calibration_function = line,filter_properties = False)
    
    See calibrate method to check output.
    ------------
    PROPERTIES:
    
    x,y: data set
    
    energies: calibration energy set, for example energies=[6040,6143,6264,6899.2,7137,7922,8699,9261]
    
    calibration function: line, exponential
    
    filter properties - to smooth histogramm, example:
        one can get filter properties simpy using a function
        filter_properties = get_filter_properties() (can be imported from the
        module like this: from calibrator import get_filter_properties)
        #default filter properties
        #    class record: pass
        #    filter_properties.window_smooth=7 - window size, or number of bins to smooth
        #    filter_properties.smooth_wavelet='blackman'
        #    filter_properties.background_options='BACK1_ORDER8,BACK1_INCLUDE_COMPTON' (see CERN ROOT documentation file Spectrum.pdf)
        #    filter_properties.threshold= 6 - cut off all under this level
        
    fit_type - choose fitting method for peak processing.
        There are three base methods:
        #    'default': uses scipy.optimize.leastsq for fitting.
        #    'fast': uses hand-made gradient method with dampening of hessian matrix. It's abouts 3.5 times faster than default.
        #    'robust': based on fast method it also contains weight function 
        #              to supress influence of noisy bins. (see fit_lma.pyx code)
        #              It's about 4 times slower than default, but it give opportunity to make complex fit in case of peaks overlaying.
        
    ------------
    EXAMPLES:
        
        
        # get hist from somewhere
        from calibrator import calibrator, line
        energies = [6030, 6258.8, 6625., 6732., 6899.2, 7922, 9261] # KeV Yb+48Ca reaction products
        Clbr = calibrator(energies=energies, calibration_function=line, fit_type='default')
        ind = 48
        Clbr.read(hist.data.index, hist[ind])
        Clbr.dl(1250, 2200)
        # --> START CALIBRATION. Type in console:
        # >>> Clbr.calibrate()
        
        
    ------------
    
    """
    def __init__(self, x=None, y=None, energies=None, \
            calibration_function=line, filter_properties=None, fit_type='default'):
        
        fig, ax = plt.subplots()
        ax.set_xlabel('channel')
        ax.set_ylabel('counts')
        
        self.__fig, self.__ax = fig, ax
        self._filter_properties = filter_properties
        if is_iterable(x) and is_iterable(y):
            self.read(x, y)
        self.__calibration_x = []
        self.__calibration_y = []
        self.calibration_function = calibration_function
        
        if energies:
            self.__energies = energies
        else:
            self.__energies = []
        self.set_fit_method(fit_type=fit_type)
   
            
            
    def set_fit_method(self, fit_type='default'):
        """
        Choose fitting method for peak processing.
        There are three base methods:
            'default': uses scipy.optimize.leastsq for fitting.
            'fast': uses hand-made gradient method with dampening of hessian matrix. It's abouts 3.5 times faster than default.
            'robust': based on fast method it also contains weight function to supress influence of noisy bins. 
                      It's about 4 times slower than default, but it give opportunity to make complex fit in case of peaks overlaying.
        """
        
        if fit_type not in ['default', 'fast', 'robust']:
            raise ValueError('No such fitting type. Choose one of those: "default", "fast", "robust" ')
        #processing method
        def method(x,y):
            try:
                solution = fit_method(x, y, fit_type=fit_type)
            except Exception as e:
                print(e)
                solution = np.sum(x * y) / np.sum(y)
                good_fitting = False
            else:
                good_fitting = solution[-1]
            solution = solution[0]
            func = gaussian
            #delete previous calibrated peak from figure
            if len(self.__ax.lines) > 1:
                line_ = self.__ax.lines.pop(-1)
                del line_
            #check if calibration was good    
            if good_fitting:
                params = ['x0', 'sigm', 'A', 'k', 'b', 'O']
                solution_ = [round(x, 2) for x in solution]
                print(dict(list(zip(params, solution_))), 'sum: ', np.sum(y))
                x0 = solution[0]
                y0 = func(solution, x0)
                self.__ax.plot(x, func(solution, x), 'ro-')
            else:
                x0 = solution
                print(x0)
                y0 = y[np.argmax(x >= x0)]
                self.__ax.plot(x0, y0, 'ro-')
            
            print('peak center: ', round(x0), round(y0), '\n')
            self.__calibration_x.append(x0)
            self.__calibration_y.append(y0)   
        #connect event processor to the axes (ploting window)
        br = fit_commander(self.__ax, method)
        self.__fig.canvas.mpl_connect('button_press_event', lambda event: br.onclick(event))
    
    
    def set_filter_properties(self, filter_properties):
        self._filter_properties = filter_properties
        
        
    def set_energies(self, energies):
        """
        Set energy list for calibration.
        """
        if not energies:
            print('Empty list')
        else:
            self.__energies = energies
      
      
    def delete_points(self, ind_list=[]): 
        """
        delete points from calibration list
        set indexes of peaks to delete or empty list [] to delete them all
        """
        if ind_list == []:
            ind_list = list(range(len(self.__calibration_x)))
        self.__calibration_x = [x for x in self.__calibration_x if self.__calibration_x.index(x) not in ind_list]
        self.__calibration_y = [x for x in self.__calibration_y if self.__calibration_y.index(x) not in ind_list]
        
    
    def delete_energies(self, ind_list):
        """
        set indexes to delete elements from energies array
        """
        if not ind_list:
            print('no indexes to delete')
            return
        self.__energies = [x for x in self.__energies if self.__energies.index(x) not in ind_list]    


    def show_points(self): #show calibration list
        """
        Show list of selected points to calibrate.
        """
        if len(self.__calibration_x) == 0:
            print("Calibration peaks list: empty")
            return
        i = 0
        sort_ind = np.argsort(self.__calibration_x)
        self.__calibration_x = np.array(self.__calibration_x)[sort_ind]
        self.__calibration_y = np.array(self.__calibration_y)[sort_ind]
        self.__calibration_x = list(self.__calibration_x) 
        self.__calibration_y = list(self.__calibration_y) 
        print("Calibration peaks list:")
        for x, y in zip(self.__calibration_x, self.__calibration_y):
            print('(%d, x: %f, y: %f)' % (i, x, y)) 
            i += 1
            
    def show_energies(self):
        if len(self.__energies) == 0:
            print("Energy list: empty")
            return
        i = 0
        print("Energy list:")
        self.__energies = sorted(self.__energies)
        for x in self.__energies:
            print('( %d, energy: %f )' % (i, x))
            i += 1


    def calibrate(self, p0=False, filename=None, ind=1):
        """
        Make calibration on selected points. Shows a figure with selected points fitted by calibration function.
        Also can write down a report file.
        
        INPUT:
        p0 - initial coefficients
        ind - number of strip to add to report text (don't affect anything)
        filename - name of file to print in a report. If False - there will be no output file.
        
        OUTPUT
        It prints report with energy list and calibration peaks,
        report table with results of calibration and
        
        return p - 1D np.ndarray[float] of calculated coefficients
        
        EXAMPLE OUTPUT
        >>> Clbr.calibrate()
        Energy list:
        ( 0, energy: 6030.000000 )
        ( 1, energy: 6258.800000 )
        ( 2, energy: 6625.000000 )
        ( 3, energy: 6732.000000 )
        ( 4, energy: 6899.200000 )
        ( 5, energy: 7922.000000 )
        ( 6, energy: 9261.000000 )
        Calibration peaks list:
        (0, x: 1389.060345, y: 12.000000)
        (1, x: 1411.577334, y: 17.212479)
        (2, x: 1444.468729, y: 19.459160)
        (3, x: 1495.644757, y: 26.143864)
        (4, x: 1656.961928, y: 16.778101)
        (5, x: 1819.913358, y: 22.779310)
        (6, x: 1937.921660, y: 21.396183)
        
        1    A = 4.97603 ; B = -826.0
         Eexp   Ecal      Differ   Channel 
        6030.0  6086.0    56.0     1389.1 
        6258.8  6198.0    -60.8    1411.6 
        6625.0  6361.7    -263.3    1444.5 
        6732.0  6616.3    -115.7    1495.6 
        6899.2  7419.0    519.8    1657.0 
        7922.0  8229.9    307.9    1819.9 
        9261.0  8817.1    -443.9    1937.9 
        S/n = 115.3 
        
        array([   4.97603037, -826.04945942])
        
        """
        if not p0:
            if self.calibration_function == line:
                p0 = [1, 0]
            elif self.calibration_function == exponential:
                p0 = [0, 1, -log(max(self.__energies))]
                
        self.show_energies()
        self.show_points() 
        
        if len(self.__calibration_x) != len(self.__energies):
            print("Size of calibrate points array doesn't coincide the size of energy array")
            return
        self.__energies = np.asarray(self.__energies, dtype=np.double)
        self.__calibration_x = np.asarray(self.__calibration_x, \
                                          dtype=np.double) 
        
        def residuals(coef, y, x):
            return y - self.calibration_function(coef, self.__calibration_x)   
            
        solution = leastsq(residuals, p0, args=(self.__energies, \
                                                self.__calibration_x))
        solution = solution[0]
        
        fig, ax = plt.subplots()
        ax.plot(self.__calibration_x, self.__energies, 'ro')
        print(make_report(self.__calibration_x, solution, ax, \
                          calibration_function=self.calibration_function, \
                          filename=filename, \
                          energies=self.__energies, ind=ind))
        
        self.__energies = list(self.__energies)
        self.__calibration_x = list(self.__calibration_x)
        return solution[::-1]
                       
    def clean(self):
        """
        Clean calibration point's list.
        """
        lines = self.__ax.lines
        if len(lines) < 1: 
            return
        else:
            for i in range(len(lines)):
                l = lines.pop(0)
                del l
        self.__calibration_x = []
        self.__calibration_y = []
        plt.show()
        
    def read(self, x, y=None, ind=1):
        """
        Read data set for calibration and show a plot.
        Input:
            x - 1D np.ndarray 
            y - 1D np.ndarray
            
        OR
        
            x - Spectrum, PosSpectrum or CombinedSpectrum from tools.Spectrum
            ind - int strip index
            
        """
        self.clean()
        
        if 'sp' in globals() and issubclass(x.__class__, sp.SpectrumBase):
            x, y = x.bins[:-1], x.data[ind]
            
        assert len(x) > 0 or y is not None, \
            "Calibrator read error: input data is empty"
        assert len(x) == len(y), \
            "Calibrator read error: len(x) != len(y)"
            
        try:
#            if (self._filter_properties is not None) and (type(x) is not None):
#                x, y = filter_spectr(min(x), max(x),\
#                    x, y, self._filter_properties)
            self.__ax.plot(x, y, color='b', linestyle = 'steps')   
        except ValueError:
            print('Wrong or empty arguments: x,y')
        plt.show()
                           
    def dl(self, x1, x2):
        """
        Select an area between x1 and x2.
        """
        self.__ax.set_xlim([x1, x2])
        plt.show()
        
#if __name__ == '__main__':

#    Clbr = calibrator()

#### SIMPLE EXAMPLE - fit random gaussian peak 
#    Clbr = calibrator()
#    Clbr.set_fit_method('robust')
#    print 'Try do some clicks on figure to make peak fit'
#    x = np.arange(-6, 12, 0.1)
#    f = lambda x: 7.3 * np.exp(-(x - 5.7) ** 2/ (2 * 1.9))
#    f1 = lambda x: 0.12 * x + 1.7
#    c = 0.6 * np.random.randn(len(x))
#    y = f(x) + f1(x) + c
#    Clbr.read(x, y[1])



#### SIMPLE EXAMPLE - making calibration of alpha-spectr

#    hist = pd.load('hist')
#    hsum = np.fromfile('hsum',sep=' ')
#    energies = [6899.2, 7137, 7922, 8699, 9261] # KeV Yb+48Ca reaction products
#    Clbr = calibrator(energies = energies, calibration_function = line, \
#                      fit_type='default')
#    ind = 1
#    Clbr.read(hist.index, hist[ind])
#    Clbr.dl(1000, 2100)
#    from read_files import read_files, get_front_spectrs
#    import os
#    os.chdir(u'../../data/analogue/Feb_2016')
#    frame = read_files('tsn.453-tsn.456', strip_convert=True, energy_scale=False)
#    hist,hsum  = get_front_spectrs(frame, tof=False, visualize=False) #apply some methods to get spectrums
#
#    print(""" Select only 5 most right alpha-peaks (in area 600-1200) and 
#    then put next command into console panel to calculate calibration coefficients: 
#    
#    Clbr.calibrate()
#    
#    Try to perform calibration on other strips:
#    
#    ind = 2 # up to 47
#    Clbr.read(hist.index,hist[ind]) # more handy to work in interactive shell!

#    Try different fitting methods:
#    Clbr.set_fit_method('robust') #or 'fast'
#    
#    If you have selected some needless excess peaks, you can delete them from the list using next commands: 
#    Clbr.show_points() # the command show a list of sorted selected peaks and their indexes 
#    Clbr.delete_points([5, 6, 7]) #set the indexes of excess peaks to delete them 
#    the same works for energies, just use commands show_energies, delete_energies in the same way.
#     """)

##### AVERAGE EXAMPLE - apply smoothing method and then calibrate peaks
#if __name__ == '__main__':
#    os.chdir('/home/eastwood/codes/Python_Idle/data_processing/Sep_2015_AM')
#    frame = pd.read_table('digi_348_alpha.txt', header=None, sep='  ')
#    frame1 = pd.read_table('analog_348_alpha.txt', header=None,sep='  ')
#    frame2 = pd.read_table('yuri_spectr_18Jan.txt', header=None,sep='  ')
#    class record: pass
#    filter_properties = record()    
#    filter_properties.window_smooth = 5 
#    filter_properties.smooth_wavelet = 'blackman'
#    filter_properties.background_options = 'BACK1_ORDER8, BACK1_INCLUDE_COMPTON'
#    filter_properties.threshold= 0 
#
#    sample = frame[1]
#    xsample = frame[0]
#    x_ind = (xsample > 5000) & (xsample < 7300)
#    xsample = xsample[x_ind]
#    sample = sample[x_ind]
#    
#    energies = [6899.2, 7137, 7922, 8699, 9261] # KeV Yb+48Ca reaction products
#    Clbr = calibrator(energies=energies, filter_properties=None)
#    Clbr.read(xsample, sample)
#    Clbr.dl(5000, 7300)
#    
#    Clbr1 = calibrator(energies=energies, filter_properties=filter_properties)
#    Clbr1.read(xsample, sample)
#    Clbr1.dl(5000, 7300)
    
#### AVERAGE EXAMPLE - perform calibration with exponential function
## on spectrum including alpha- and scattered-ions subspectrums using smoothing method
#    from read_files import read_files, get_front_spectrs
#    import os   
##    #read data
#    os.chdir(r'./Files88_91')
#    frame = read_files('tsn.88-tsn.91', strip_convert=True)
#    
#    #apply some methods to get spectrums
#    hist,hsum  = get_front_spectrs(frame, tof=False, visualize=False)
#    hist1,hsum1 = get_front_spectrs(frame, tof=True, type_scale='fission_channel', visualize=False)
#    
#    #select a spectrum including alpha-peaks from events without time-of-flight mark (tof=False, first 100 channels)
#    #and a peak of scattered ions which lies in area > 100 channel
#    ind = 8
#    def get_hist(i):
#        hist_probe = hist[i]
#        hist_probe[100:] = 0
#        hist_probe += hist1[i]
#        return hist_probe
#        
#    hist_probe = get_hist(ind)
#    
#    #create calibrator object to make calibrations by hand
#    Clbr = calibrator(calibration_function = exponential)
#    Clbr.set_energies([7922, 8699, 9261, 196591]) # also contain the energy of the scattered beam right-most peak
#    Clbr.read(hist_probe.index, hist_probe)
#    Clbr.dl(25,500) #select an area from 25 to 500 channel       
#    
#    print(""" Select 3 most right alpha-peaks (in area <100 channel) and high peak of scattered ions (nearby 450 channel) 
#and then put next commands into console panel to calculate calibration coefficients: 
#    Clbr.set_energies([7922, 8699, 9261, 196591]) #energies in MeV of the peaks 
#    Clbr.calibrate(,filename='..//fission_clbr.txt',ind=ind)) 
#    #one could set initial coefficients for iterational calculating process and a filename for ouput report 
#if you have selected some needless excess peaks, you can delete them from the list using next commands:
#    Clbr.show_points() # the command show a list of sorted selected peaks and their indexes 
#    Clbr.delete_points([5, 6, 7]) #set the indexes of excess peaks to delete them 
## the same works for energies, just use commands show_energies, delete_energies in the same way.
#        """)