# cython: boundscheck=False
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  2 16:53:31 2016

@author: eastwood
"""


import numpy as np
cimport numpy as np
from array_types import event_type, final_event_type
from data_types cimport c_bool, c_word, BLOCK, EVENT, c_number, \
                        c_number_array, DATA_EVENT
from libc.math cimport fabs
from cython.parallel cimport prange

#### addition quick functions
cpdef np.ndarray find_sync_pairs(\
        np.ndarray[EVENT, cast=True] event_table, \
        int scale, int event_type, \
        c_bool tof=0, int dlt_t=20):
    '''
    Find syncronized pairs of events like front-side or back-side in 
    different energy scales.
    Input:
        event_table
        scale: int energy scale, 0 - 'alpha', '1' - fission
        event_type: int type of event, 1 - front, 2 - back
        tof: time-of-flight flag (True, False)
        dlt_t: time window between two consequent events, microseconds
    Return:
        np.ndarray[[indexes: int][type: int]] - array of pairs indexes,
            where type = (0 - focal, 1 - side)
    Example:
        indexes = find_sync_pairs(event_table, 0, 1) # front-side pairs in alpha scale.
        front_events = event_table[indexes[:, 0]]
        side_events = event_table[indexes[:, 1]]
        
    '''
#                    (fabs(event_table[i].synchronization_time - 
#                          event_table[j].synchronization_time) < dlt_t) &
    cdef:
        long i, j, k
        long[:] side_nums
        long[:, ::1] pairs_array 
   
    side_nums = np.argwhere(event_table['event_type'] == 3)[:, 0]
    pairs_array = np.zeros((side_nums.size, 2), dtype=long)
    
    for i in range(side_nums.shape[0]):
        pairs_array[i, 1] = side_nums[i]
    
    k=0
    for i in side_nums:
        for j in range(i - 1, i - 20, -1):
            if (j > 0) & \
                    (fabs(event_table[i].time_micro - \
                          event_table[j].time_micro) < dlt_t) & \
                    (event_table[j].event_type == event_type) & \
                    (event_table[i].scale == event_table[j].scale == scale) & \
                    (event_table[i].tof == event_table[j].tof == tof): # change type condition
                pairs_array[k, 0] = j
                break
        k += 1
        
    pairs = np.asarray(pairs_array) 
    return pairs[pairs[:, 0] != 0]


cpdef np.ndarray find_R_alphas(np.ndarray[EVENT, cast=True] event_table, \
                               double[:, :, :, ::1] coefficients, \
                               int dlt_t=20000,  \
                               double energy_R_min=4000.,
                               double energy_R_max=14000.,
                               double energy_a_min=5000.,
                               double energy_a_max=11000.):
    '''
    Find syncronized pairs of Recoil-alpha events on focal detector.
    
    Cython function.
    np.ndarray find_R_alphas(np.ndarray[EVENT, cast=True] event_table, \
           double[:, :, :, ::1] coefficients, \
           int dlt_t=20000,  \
           double energy_R_min=4000.,
           double energy_R_max=14000.,
           double energy_a_min=5000.,
           double energy_a_max=11000.)
    Input:
        event_table
        coefficients
        dlt_t: time window between two consequent events, microseconds
        
        double energy_R_min=4000.,
        double energy_R_max=14000.,
        double energy_a_min=5000.,
        double energy_a_max=11000.
        # Recoil energy range: [4000 .. 14000] KeV
        # Alpha energy range: [5000 .. 11000] KeV
        
    Return:
        np.ndarray[len_, dtype=EVENT] - array of pairs events,
            where msv[::2] - recoils, msv[1::2] - alphas
    Example:
        import data_processing_GNS2021.tools.spectrum as sp
        ...
        coefs = sp.get_all_calibration_coefficients(coefs_folder=coefs_folder)
        Ra_events = find_R_alphas(event_table, 20000, coefs) # front-side pairs in alpha scale.
        
    '''
    cdef:
        long i,j,k, size, buffer_size
        long index = 0
        int strip_X_R, strip_Y_R, strip_X_a, strip_Y_a
        double[:] energies
        double[:] times
        EVENT R_event, a_event
        np.ndarray[EVENT, cast=True] output_table
#    event_type # 1 - focal, 2 - back, 3 - side, 4 - veto
#    strip # 0 .. 128; front:1-48 back: 1-128, side: 1-6
#    channel # front:4096 back:8192 side: 8192
#    scale # 0 - alpha, 1 - fission
#    time_macro # sec int64
#    time_micro # microseconds int64
#    beam_mark # bool
#    tof # bool
#    tofD1 # 1st tof wire counter
#    tofD2 # 2nd tof wire counter
#    synchronization_time #  0..65536 
        
    size = len(event_table)
    
    assert size > 0, "Empty data table"
    assert dlt_t > 0, "Time delta must be > 0"
    
    buffer_size = size // 20    
    output_table = np.zeros(buffer_size, dtype=event_type)    
    energies = apply_all_calibration_coefficients(event_table, coefficients)
    
    times = np.zeros(size, dtype=np.double)
#    with nogil:
#        for i in prange(size):
#            times[i] = event_table[i].time_macro * 65536 + \
#                       event_table[i].time_micro
    
    for i in range(size - 10):
        if not event_table[i].tof: # check for Recoil event
            continue
        
         # check for event type
        if (event_table[i].event_type == 1) & \
                (energies[i] >= energy_R_min) & \
                (energies[i] <= energy_R_max):    
            R_event = event_table[i]
            strip_X_R = R_event.strip
            strip_Y_R = event_table[i + 2].strip
        else:
            continue
            
        j = 3
        while (i < size - j - 2) & (times[i] - times[i + j] < dlt_t):
            if event_table[i + j].scale != 0:
                j += 1
                continue
            
            if times[i] == times[i + j]: # check if the same block
                j += 1
                continue
            
            if (event_table[i + j].event_type != 1): # check for front event
                j += 1
                continue
                        
            strip_X_a = event_table[i + j].strip  
            strip_Y_a = event_table[i + j + 2].strip # events: frontA-frontF-backA
            
            if (not event_table[i + j].tof) &\
                 (strip_X_R == strip_X_a) & (strip_Y_R == strip_Y_a) &\
                 (energies[i + j] >= energy_a_min) &\
                 (energies[i + j] <= energy_a_max):
                a_event = event_table[i + j]
#                assert index + 1 == len(output_table), \
#                    'output table overflow; ind = ' + str(index)
                output_table[index] = R_event
                output_table[index + 1] = a_event
                index += 2
                
                assert index < buffer_size, "output_table overflow"
                break
            
            j += 1
                     
    return output_table[:index]


cpdef np.ndarray calculate_time(int[:] time_micro, int[:] time_macro):
    """
    Convert data from low microseconds counter and high counter (x * 65536)
    to time sorted array. Also fixes false counts of high or low counters.
    
    Input:
        time_micro - np.ndarray[int], counts of low counter, mks
        time_macro - np.ndarray[int], counts of high counter, 65536 * mks
    Output:
        np.ndarray[long] times, mks  
        
    """
    cdef:
        long i, size, time, time_pre=0
        
    assert len(time_micro) == len(time_macro), \
        "arrays must have the same size"
    size = len(time_micro)
    
    times = np.zeros(size, dtype=np.int64)
    for i in range(size):
        time = time_macro[i] * 65536 + time_micro[i]
        
        if i > 0:
            if time - time_pre > 65536:
                time -= 65536
            elif time - time_pre < 0:
                time += 65536
            
        times[i] = time
        time_pre = time
        
    return np.asarray(times)
            

def where(np.ndarray[c_bool, ndim=1, cast=True] condition, c_number_array A,\
          c_number_array B):
    cdef:
        int i
        long len_data
    assert (condition.dtype == np.bool) | (type(condition[0])== bool) , \
           'Condition type should be bool'
    assert len(condition) == len(A) == len(B), 'Values should have same size' 
    len_data = condition.shape[0]
    with nogil: 
        for i in prange(len_data):
            if not condition[i]:
                A[i] = B[i]            
    return np.asarray(A)
    
def is_iterable(obj_):
    try:
        iterator = iter(obj_)
        return True
    except TypeError:
        return False

cpdef tuple calc_histogram(c_number_array data, c_number[::1] bins):
    '''
    calc_histogram( c_number_array data, c_number[::1] bins)
    return hist, bins
    Calculates histogram from given set of data on given bins.
    All but the last (righthand-most) bin is half-open. In other words, if bins is:
    [1, 2, 3, 4]
    then the first bin is [1, 2) (including 1, but excluding 2) and the second [2, 3). The last bin, however, is [3, 4], which includes 4.
    ### fill 1D spectrum [bin]
    '''
    cdef:
        c_number ds, min_, max_
        int i,j
        long len_data
        long[:] hist = np.zeros(len(bins) - 1, dtype=np.long)
    len_data = data.shape[0]
    ds = bins[1] - bins[0]
    min_ = bins[0] 
    max_ = bins[-1]
    for i in prange(len_data, nogil=True):
        if (data[i] < min_) | (data[i] > max_):
            continue
        elif data[i] == max_:
            j = -1
        else:
            j = <int>((data[i] - min_) / ds)
        hist[j] += 1
    return np.asarray(hist), np.asarray(bins)
    
    
cpdef long[:, ::1] calc_spectrum(np.ndarray[EVENT, cast=True] data, \
        c_number_array sample, c_number[::1] bins, long[:, ::1] hist):
    '''
    calc_spectrum(data, sample, bins, hist)
    return hist ### 2D spectrum [strip, bin]
    
    Calculates table of histograms from given set of data on given bins.
    All but the last (righthand-most) bin is half-open. In other words, if bins is:
    [1, 2, 3, 4]
    then the first bin is [1, 2) (including 1, but excluding 2) and the second [2, 3). The last bin, however, is [3, 4], which includes 4.
    
    data - table of data events (see EVENT format in data_type.pxd).
        It contains strip number of each event.
    sample - array of channels/energy for binning.
    bins - array of bins
    hist - 2Dspectrum [strip, bin] which finnaly contains histogram for each strip
    
    '''
    cdef:
        c_number ds, min_, max_
        int i, j
        long len_data
        
    len_data = data.shape[0]
    ds = bins[1] - bins[0]
    min_ = bins[0] 
    max_ = bins[-1]
    
    for i in prange(len_data, nogil=True):
        if (sample[i] < min_) | (sample[i] > max_):
            continue
        elif sample[i] == max_:
            j = -1
        else:
            j = <int>((sample[i] - min_) / ds)
        if data[i].strip - 1 > hist.shape[0]:
            continue
        hist[data[i].strip - 1][j] += 1
    return hist
    
cpdef long[:, ::1] calc_2d_int_spectrum(np.ndarray[EVENT, cast=True] data,
          long[:, ::1] spectrum):
    """Fill strip distribution array [front_strip,back_strip]: [0 .. 47][0 .. 127]"""
    cdef:
        long i, len_data = len(data)
        int front_strip = 0
        EVENT event
        
    for i in range(len_data):
        event = data[i]
        if event.event_type == 1:
            front_strip = event.strip
        elif event.event_type == 2:
            assert (front_strip >= 1) & (front_strip <= 48),\
                "front strip is out of range (1, 48), event num: %d" % (i,)
            assert (event.strip >= 1) & (event.strip <= 128), \
                "back strip is out of range (1, 128), event num: %d" % (i,)    
            spectrum[front_strip - 1][event.strip - 1] += 1
    return spectrum
    
        
#add nogil
cdef void convert(np.ndarray[EVENT, cast=True] event_table, int[:] convert_array):
    pass

cpdef np.ndarray apply_calibration_coefficients(\
            np.ndarray[EVENT, cast=True] event_table, \
            double[:, ::1] coefficients):
    
    cdef:
        int channel, strip
        long i, N = event_table.shape[0]
        double[:] energy_array = np.zeros(N, dtype=np.double)
    
    for i in prange(N, nogil=True):
        channel = event_table[i].channel
        strip = event_table[i].strip
        energy_array[i] = <double>channel * coefficients[0, strip] + \
                          coefficients[1, strip]
    return np.asarray(energy_array)

# get energier from channel, strip arrays
cpdef np.ndarray calc_energy(int[:] channels, int[:] strips,
                             double[:, ::1] coefficients):
    assert len(channels) == len(strips), \
        'size of channels and strips should be equal'
    cdef:
        int channel, strip
        long i, N = len(channels)
        double[:] energy_array = np.zeros(N, dtype=np.double)
    
    for i in prange(N, nogil=True):
        channel = channels[i]
        strip = strips[i]
        energy_array[i] = <double>channel * coefficients[0, strip] + \
                          coefficients[1, strip]
    return np.asarray(energy_array)
    
cpdef np.ndarray apply_all_calibration_coefficients(\
            np.ndarray[EVENT, cast=True] event_table, \
            double[:, :, :, ::1] coefficients):
    
    """
    Apply all calibration coefficients to all events from event table.
    Coefs array structure: [event_type 1-front, 2-back, 3-side]
        [scale 0-alpha, 1-fission][coef 0-a, 1-b][strip 0..129]
        
    Return energies # np.array, dtype=np.double
    
    """
    
    cdef:
        long i, N = event_table.shape[0]
        int event_type, scale, strip
        double channel, a, b
        double[:] energy_array = np.zeros(N, dtype=np.double)
        
    for i in prange(N, nogil=True):
        event_type = event_table[i].event_type
        scale = event_table[i].scale
        strip = event_table[i].strip
        channel = <double>event_table[i].channel
        a = coefficients[event_type, scale, 0, strip]
        b = coefficients[event_type, scale, 1, strip]
        
        energy_array[i] = channel * a + b # line calibration
        
        if energy_array[i] < 0:
            energy_array[i] = 0
    return np.asarray(energy_array)
    
    
cpdef np.ndarray recalculate_side_channels(np.ndarray[EVENT, cast=True] event_table,
                double[:] energy, double[:, ::1] coefficients):      
    cdef:
        long i, N = event_table.shape[0]
        long[:] energy_array = np.zeros(N, dtype=np.int)
        
    for i in prange(N, nogil=True):
        if coefficients[0, event_table[i].strip] != 0:
            energy_array[i] = <int>((energy[i] - coefficients[1, event_table[i].strip]) / 
            coefficients[0, event_table[i].strip])
    return np.asarray(energy_array)

cdef c_bool alpha_energy_check(double energy):
    return (energy >= 8000) & (energy <= 12000)
    
cdef c_bool fission_energy_check(double energy):
    return (energy >= 50000)
    
cdef void copy_event_data( DATA_EVENT* data_event, EVENT event, long* j, \
                           double energy, np.int_t cell_x, np.int_t cell_y, \
                           np.int_t event_type):
    data_event.energy_front = energy
    data_event.cell_x = cell_x
    data_event.cell_y = cell_y
    data_event.event_type = event_type
    data_event.time_micro = event.time_micro
    data_event.time_macro = event.time_macro
    data_event.tof = event.tof
    data_event.beam_mark = event.beam_mark
    j[0] += 1    
 

def fill_data_table(np.ndarray[EVENT, cast=True] event_table, \
                    double[:] energy, \
                    np.ndarray[DATA_EVENT, cast=True] data_table):
    cdef:
        long i, k, back_ind, event_type
        long j = 0, N = event_table.shape[0] - 1
        long group_start = 0, group_end =  0
        np.int_t back_strip, front_strip, back_strip1
        double energy_ = 0, back_energy_a_ = 0, back_energy_F_ = 0
        c_bool cond_fa, cond_ba, cond_ba1, cond_ba2, cond_ba_sum, cond_F# = False,False,False,False,False,False
     
    #go through groups
    for i in range(1, N):
        try:
            if event_table[i].time_micro != event_table[i - 1].time_micro:
                group_end = i
         
                energy_, front_strip, back_strip, \
                    back_strip1, event_type,back_ind = 0,0,0,0,0,0
                back_energy_a_ = 0
                back_energy_F_ = 0
                cond_F = False
                for k in range(group_start, group_end):

                    #check front strips
                    if (event_table[k].event_type == 1):
                        cond_F = fission_energy_check(energy[k]) 
                        if cond_F | (alpha_energy_check(energy[k]) & \
                                    (event_table[k].scale == 0)):
                            energy_ = energy[k]
                        front_strip = event_table[k].strip
                        event_type = 1
                    #check back strips
                    if (event_table[k].event_type == 2):
                        
                        if back_strip == 0:
                            back_strip = event_table[k].strip
                        elif event_table[k].strip != back_strip:
                            back_strip1 = event_table[k].strip
                            
                        if (event_table[k].scale == 0):
                            back_energy_a_ += energy[k]
                            if alpha_energy_check(back_energy_a_):
                                energy_ = back_energy_a_
                        elif (event_table[k].scale == 1):
                            back_energy_F_ += energy[k] 
                            if fission_energy_check(back_energy_F_):
                                energy_ = back_energy_F_
                            
                    #check side strips
                    if (event_table[k].event_type == 3) &\
                            ((alpha_energy_check(energy[k]) & \
                              (event_table[k].scale == 0)) | \
                            fission_energy_check(energy[k])):
                        energy_ = energy[k] #back strips have better resolution
                        front_strip = event_table[k].strip
                        back_strip = 0
                        event_type = 3
                
                if energy_ > 0:
                    copy_event_data(&data_table[j], event_table[group_start], \
                        &j, energy_, front_strip, back_strip, event_type)
                    if back_strip1 > 0: 
                        copy_event_data(&data_table[j], event_table[group_start], \
                            &j, energy_, front_strip, back_strip1, 1)                                                                                        
                #
                 
                group_start = i   
        except IndexError as e:
            print 'i,j,k,group_start,group_end', i, j, k, group_start, group_end
            raise e
    return data_table[:j]
    

cdef tuple event_check_dg(double energy_, int strip_, double energy_k, int strip_k):
    if energy_ ==0:
        energy_ = energy_k
        strip_ = strip_k
    elif fabs(strip_ - strip_k) <= 1:
        if energy_k > energy_:
            strip_ = strip_k
        energy_ += energy_k
#    elif energy_k > energy_:
#        energy_ = energy_k
#        strip_ = strip_k
    return energy_,strip_
    

cdef void copy_event_data_dg( DATA_EVENT* data_event, EVENT event, long* j,\
        double energy, np.int_t cell_x, np.int_t cell_y, np.int_t event_type):
    data_event.energy_front = energy
    data_event.cell_x = cell_x
    data_event.cell_y = cell_y
    data_event.event_type = event_type
    data_event.time_micro = event.time_micro
    data_event.time_macro = event.time_macro
    data_event.tof = event.tof
    data_event.beam_mark = event.beam_mark
    j[0] += 1  
  
def fill_cells_table(np.ndarray[EVENT, cast=True] event_table, \
                     double[:] energy, \
                     np.ndarray[DATA_EVENT, cast=True] data_table):
    cdef:
        long i, k, event_type, j=0, N=event_table.shape[0] - 1
        long group_start=0, group_end=0
        np.int_t cell_x, cell_y, cell_y2
        double energy_=0, energy_back=0, energy_side=0, energy_2=0
        c_bool cond_fa, cond_ba, cond_ba1, cond_ba2, cond_ba_sum, cond_F
     
    #go through groups
    for i in range(1, N):
        try:
            if event_table[i].time_micro != event_table[i-1].time_micro:
                group_end = i
                
                energy_, energy_back, energy_side, front_strip, back_strip, \
                side_strip = 0, 0, 0, 0, 0, 0
                cell_x, cell_y, cell_y1 = 0, 0, 0
                energy_1 = 0
                event_type = 1
                for k in range(group_start, group_end):
                    #check front strips
#                    if (event_table[k].event_type == 1):
#                        if cell_x == 0:
#                            cell_x = event_table[k].strip
#                            if energy[k]>energy_:
#                                energy_=energy[k]       
#                    elif (event_table[k].event_type == 2):
#                        if cell_y == 0:
#                            energy_,cell_y = energy[k],event_table[k].strip    
#                        elif fabs(cell_y - event_table[k].strip) <= 1:
#                            energy_ += energy[k]
#                            if cell_y != event_table[k].strip:
#                                cell_y1 = event_table[k].strip
#                        else:                    
#                            if energy_1 == 0:
#                                energy_1 = energy[k]
#                                cell_y1 = event_table[k].strip
#                            elif energy[k]>energy_1:
#                                energy_1 = energy[k]
#                                cell_y1 = event_table[k].strip
#                    elif (event_table[k].event_type == 3):
#                        if cell_x != 0:
#                            energy_ += energy[k]    
#                        else:
#                            energy_,cell_x = energy[k],event_table[k].strip 
#                            event_type = 3
                    if (event_table[k].event_type == 1):
                        if energy[k] > energy_:
                            cell_x = event_table[k].strip
                            energy_ = energy[k]
                    elif (event_table[k].event_type == 2):
                        if cell_y == 0:
                            cell_y = event_table[k].strip 
                            if (energy[k] >= 6000) & (energy[k] <= 12000) & \
                                    (energy_ <= 12000):
                                energy_ = energy[k]
                        elif fabs(cell_y - event_table[k].strip) <= 1:
                            energy_ += energy[k]
                            if cell_y != event_table[k].strip:
                                cell_y1 = event_table[k].strip
                
                    elif (event_table[k].event_type == 3):
                        if cell_x != 0:
                            energy_ += energy[k]    
                        else:
                            energy_, cell_x = energy[k], event_table[k].strip 
                            event_type = 3
                        
                if energy_ > 0:
                    copy_event_data_dg(&data_table[j], \
                        event_table[group_start], &j, energy_, \
                        cell_x, cell_y, event_type)
                    
                if cell_y1 > 0:
                    if energy_1 > 0:
                        copy_event_data_dg(&data_table[j], \
                            event_table[group_start], &j, energy_1, cell_x, \
                            cell_y1, event_type)
                    else:
                        copy_event_data_dg(&data_table[j], \
                            event_table[group_start], &j, energy_, cell_x, \
                            cell_y1, event_type)
                
                
                group_start = i   
        except IndexError as e:
            print 'i,j,k,group_start,group_end', i, j, k, \
                    group_start, group_end
            raise e
            
    return data_table[:j]
  
#def fill_cells_table(np.ndarray[EVENT, cast=True] event_table,double[:] energy,np.ndarray[DATA_EVENT, cast=True] data_table):
#    cdef:
#        long i,k,event_type,j=0,N = event_table.shape[0]-1
#        long group_start=0,group_end =  0
#        np.int_t back_strip,front_strip,side_strip
#        double energy_=0,energy_back=0,energy_side=0,
#        #np.int_t back_strip,front_strip,back_strip1
#        #double energy_=0,back_energy_a_=0,back_energy_F_ = 0
#        c_bool cond_fa,cond_ba,cond_ba1,cond_ba2,cond_ba_sum,cond_F# = False,False,False,False,False,False
#     
#    #go through groups
#    for i in range(1,N):
#        try:
#            if event_table[i].synchronization_time != event_table[i-1].synchronization_time:
#                group_end = i
#                
#                energy_,energy_back,energy_side,front_strip,back_strip,side_strip = 0,0,0,0,0,0
#                for k in range(group_start,group_end):
#                    #check front strips
#                    if (event_table[k].event_type == 1):
#                        energy_,front_strip = event_check_dg(energy_,front_strip,energy[k],event_table[k].strip)
#                    elif (event_table[k].event_type == 2):
#                        energy_back,back_strip = event_check_dg(energy_back,back_strip,energy[k],event_table[k].strip)
#                    elif (event_table[k].event_type == 3):
#                        energy_side,side_strip = event_check_dg(energy_side,side_strip,energy[k],event_table[k].strip)
#
#                if (front_strip > 0):
#                    energy_ = energy_back if (energy_back>6000)&(energy_back<14000) else energy_
#                    event_type = 1
#                    if energy_side > 0:
#                        energy_ += energy_side
#                elif back_strip > 0:
#                    energy_ = energy_back
#                    event_type = 1
#                elif side_strip > 0:
#                    energy_ = energy_side
#                    event_type = 3
#                    front_strip = side_strip
#                        
#                if energy_ > 0:
#                    copy_event_data_dg(&data_table[j],event_table[group_start],&j,energy_,front_strip,back_strip,event_type)
#                    
#                group_start = i   
#        except IndexError as e:
#            print 'i,j,k,group_start,group_end',i,j,k,group_start,group_end
#            raise e
#    return data_table[:j]
    
              
#def find_chains(np.ndarray[DATA_EVENT, cast=True] data_table,search_properties):#time_dlt=4000,chain_length=2):
#    '''
#    find_chains(np.ndarray[DATA_EVENT, cast=True] data_table,search_properties)
#    Find chains using search_properties:
#        search_properties.time_dlt : <int> time in microseconds between two consecutive events
#        search_properties.chain_length_min : <int> minimal length of chains
#        search_properties.chain_length_max : <int> maximal length of chains. If has default value -1 -> the upper limit will set to 50.
#        search_properties.energy_min, search_properties.energy_max: <float> set energy diapason of alpha-particles energies to search
#        search_properties.recoil_energy_min, search_properties.recoil_energy_max: <float> set energy diapason of recoil alpha-particles energies to search
#        search_properties.recoil_first : <c_bool> check if first event in chain is recoil
#        search_properties.fission_flag : <c_bool> search fission fragments (events with energies > 70 MeV)
#        search_properties.fission_time_dlt : <int> time in microseconds between two consecutive fission events
#        search_properties.only_fission : <c_bool> search fission fragments (events with energies > 70 MeV)
#        search_properties.limit : <int> number of total events to search. If -1: search the whole data_table.
#    Return: list of lists with chain event numbers.
#    ***    
#    Example:
#    chains = find_chains(data_table,search_properties)
#    for i in chains[:5]:    
#        print data_table[i] #print first 5 chains
#    '''
#    cdef:
#        list a
#        long i,j, N = data_table.shape[0]-1
#        double time_start = data_table['time_micro'][0], time
#        c_bool recoil_flag, energy_flag
#    if search_properties.chain_length_max <= 0:
#        search_properties.chain_length_max = 50
#    chains = []
#    a = <list>[]
#    if search_properties.limit > -1:
#        N = search_properties.limit
#    for i in range(N):
#        
#        #find R-a-a...
#        time = data_table['time_micro'][i]
#        #check for recoil
#        if <c_bool> search_properties.recoil_first:
#            recoil_flag = (data_table['tof'][i]&(data_table['energy'][i] >= <float>search_properties.recoil_energy_min) & (data_table['energy'][i] <= <float>search_properties.recoil_energy_max)) 
#        else: 
#            recoil_flag = True
#            
#        if (recoil_flag)&(data_table['event_type'][i]==1):
#            
#            j = i+1
#            time_start = data_table['time_micro'][i]
#            time = data_table['time_micro'][j]
#            
#            #search for alpha-particles
#            if not <c_bool>search_properties.only_fission:
#                while time - time_start < <int>search_properties.time_dlt:
#                    energy_flag = (data_table['energy'][j] >= <float>search_properties.energy_min) & (data_table['energy'][j] <= <float>search_properties.energy_max)
#                    if (energy_flag)&\
#                        ((data_table['cell_x'][j]==data_table['cell_x'][i])&\
#                        (data_table['cell_y'][j]==data_table['cell_y'][i]))&\
#                        (data_table['tof'][j]==False):
#                        #|((len(a)>1)&(data_table['event_type'][j]==3)
#                        if len(a) == 0:
#                            a = <list>[i,j]
#                        elif len(a) <= search_properties.chain_length_max:
#                            a.append(j)
#                        else:
#                            break
#                        time_start = data_table['time_micro'][j]
#                    j += 1
#                    if j == N+1:
#                        break
#                    time = data_table['time_micro'][j]
#                    
#            
#            #search for fission fragments
#            if <c_bool>search_properties.fission_flag :
#                while time - time_start < <int>search_properties.fission_time_dlt:
#                    energy_flag = (data_table['energy'][j] >= 60000)     
#                    if (energy_flag)&\
#                        ((data_table['cell_x'][j]==data_table['cell_x'][i])|(data_table['cell_y'][j]==data_table['cell_y'][i]))&\
#                        (data_table['tof'][j]==False):
#                        #|((len(a)>1)&(data_table['event_type'][j]==3)
#                        if len(a) == 0:
#                            a = <list>[i,j]
#                        elif len(a) <= search_properties.chain_length_max:
#                            a.append(j)
#                        else:
#                            break
#                        
#                        time_start = data_table['time_micro'][j]
#                    j += 1
#                    if j == N+1:
#                        break
#                    time = data_table['time_micro'][j]
#                
#            if len(a)>= <int>search_properties.chain_length_min:
#                chains.append(a)
#            a = []
#                
#    return chains            

cdef search_alpha(np.ndarray[DATA_EVENT, cast=True] data_table,
                  np.ndarray[long, cast=True] chain, 
                  object search_properties, long i, long *j, 
                  double *time, double *time_start, int *chain_ind,
                  float *energy_pre,
                  long N):
    
    cdef:
        float energy_min = <float>search_properties.energy_min
        float energy_max = <float>search_properties.energy_max
            
    while time[0] - time_start[0] < <long>search_properties.time_dlt:        
        energy_flag = (data_table['energy_front'][j[0]] >= energy_min)&\
                      (data_table['energy_front'][j[0]] <= energy_max)&\
                      (data_table['energy_front'][j[0]] < energy_pre[0])
        if (energy_flag) &\
              (# focal alpha event
                (data_table['cell_x'][j[0]] == data_table['cell_x'][i])&\
                (data_table['cell_y'][j[0]] == data_table['cell_y'][i])&\
                (data_table['tof'][j[0]] == False)\
              ) | \
              (# side alpha event
                (chain_ind[0] > 0) & (data_table['event_type'][j[0]] == 3)\
              ):
            if chain_ind[0] == 0:
                chain[chain_ind[0]] = i
                chain_ind[0] += 1
                chain[chain_ind[0]] = j[0]
                chain_ind[0] += 1
            elif chain_ind[0] <= search_properties.chain_length_max:
                chain[chain_ind[0]] = j[0]
                chain_ind[0] += 1    
            else:
                break
            time_start[0] = data_table['time_micro'][j[0]]
            energy_pre[0] = data_table['energy_front'][j[0]]
        j[0] += 1
        if j[0] == N + 1:
            break
        time[0] = data_table['time_micro'][j[0]]      
        
def find_chains(np.ndarray[DATA_EVENT, cast=True] data_table,search_properties):#time_dlt=4000,chain_length=2):
    '''
    find_chains(np.ndarray[DATA_EVENT, cast=True] data_table, search_properties)
    Find time correlated decay chains using search_properties:
        
    time_dlt : <int> time in microseconds between two consecutive events;
    chain_length_min : <int> minimal length of chains;
    chain_length_max : <int> maximal length of chains; 
                       If has default value -1 -> the upper limit will set
                       to 50;
    energy_min, energy_max: <float> set energy diapason of alpha-particles 
                            energies to search;
    recoil_energy_min, recoil_energy_max: <float> set energy diapason of
                        recoil alpha-particles energies to search;
    recoil_first : <c_bool> check if first event in chain is recoil;
    fission_flag : <c_bool> search fission fragments 
                   (events with energies > 70 MeV);
    fission_time_dlt : <int> time in microseconds between two consecutive 
                       fission events;
    only_fission : <c_bool> search fission fragments 
                   (events with energies > 70 MeV);
    limit : <int> number of total events to search. If -1: search 
            the whole data_table.
                
    Return: list of lists with chain event numbers.
    
    ***    
    Example:
    --------    
    class record:
        pass

    search_properties = record()
    
    #### FIND R-a (recoil-alpha pairs)
    search_properties.time_dlt = 1000
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_energy_min = 3000
    search_properties.recoil_energy_max = 19000
    search_properties.energy_min = 6200
    search_properties.energy_max = 11000
    search_properties.recoil_first = True
    search_properties.fission_flag = False
    search_properties.only_fission = False
    search_properties.limit = -1
    
    chains = find_chains(data_table, search_properties)
    for i in chains[:5]:    
        print data_table[i] #print first 5 chains
        
    '''
    cdef:
        long i, j
        long N = data_table.shape[0]-1
        int chain_ind = 0
        double time_start = data_table['time_micro'][0]
        double time
        float energy_pre
        c_bool recoil_flag, energy_flag
        np.ndarray[long, cast=True] chain
        
    if search_properties.chain_length_max <= 0:
        search_properties.chain_length_max = 10
    chains = <list>[]
    
    if search_properties.limit > -1:
        N = search_properties.limit
    for i in range(N):
        
        ###find R-a-a...
        time = data_table['time_micro'][i]
        #check for recoil parameter
        if <c_bool> search_properties.recoil_first:
            recoil_flag = (data_table['tof'][i] & \
                (data_table['energy_front'][i] >= <float>search_properties.recoil_energy_min) & \
                (data_table['energy_front'][i] <= <float>search_properties.recoil_energy_max)) 
        else: 
            recoil_flag = True
         
        # search for the first recoil in chain
        if (recoil_flag) & (data_table['event_type'][i] == 1):
            
            j = i + 1
            time_start = data_table['time_micro'][i]
            time = data_table['time_micro'][j]
            energy_pre = data_table['energy_front'][i]
            chain_ind = 0
            chain = np.zeros(search_properties.chain_length_max, dtype=long)
            
            #search for alpha-particles
            if not <c_bool>search_properties.only_fission:
                search_alpha(data_table, chain, search_properties, i, &j, 
                             &time, &time_start, &chain_ind, &energy_pre, N)
                               
            #search for fission fragments
            if chain_ind >= search_properties.chain_length_min:
                i = chain[chain_ind - 1]
                if (i >= N):
                    break
                j = i + 1
                time_start = data_table['time_macro'][i] 
                time = data_table['time_macro'][j]
        
                if <c_bool>search_properties.fission_flag :
                    while time - time_start < <int>search_properties.fission_time_dlt:
                        energy_flag = (data_table['energy'][j] >= 25000)                        
                        if (energy_flag)&\
                              ((data_table['cell_x'][j] == data_table['cell_x'][i]) | \
                              (data_table['cell_y'][j] == data_table['cell_y'][i])): #& \
#                              (data_table['tof'][j] == False) 
#                              |((len(a)>1) & (data_table['event_type'][j]==3): 
                            if chain_ind == 0:
                                chain[chain_ind] = i
                                chain_ind += 1
                                chain[chain_ind] = j
                                chain_ind += 1
                            elif chain_ind <= search_properties.chain_length_max:
                                chain[chain_ind] = j
                                chain_ind += 1    
                            else:
                                break
                            
                        j += 1
                        if j == N+1:
                            break
                        time = data_table['time_macro'][j]
                
            if chain_ind >= <int>search_properties.chain_length_min:
                chains.append(chain[:chain_ind])
                
    return chains
    
#def find_fission(np.ndarray[DATA_EVENT, cast=True] data_table,fission_search_properties):#time_dlt=4000,chain_length=2):
#    '''
#    find_fission(np.ndarray[DATA_EVENT, cast=True] data_table,fission_search_properties)
#    Find fission fragment using fission_search_properties:
#        fission_search_properties.time_start : <float> time in seconds of the event to start searching from
#        fission_search_properties.time_dlt_micro : <int> time searching area in microseconds
#        fission_search_properties.time_dlt_macro : <int> time searching area in seconds
#        fission_search_properties.cell_x : <int> x-strip to search
#        fission_search_properties.cell_y : <int> y-strip to search
#    Return list of lists with chain numbers.
#    ***    
#    Example:
#    chains = find_chains(data_table,search_properties)
#    for i in chains[:5]:    
#        print data_table[i]
#    '''
#    cdef:
#        list a
#        long i,j, N = data_table.shape[0]-1
#        double time_start = fission_search_properties.time, time
#        c_bool tof_flag, energy_flag
#    chains = []
#    a = <list>[]
#    for i in range(1,N):        
#def fill_data_table(np.ndarray[EVENT, cast=True] event_table,double[:] energy,np.ndarray[DATA_EVENT, cast=True] data_table):
#    cdef:
#        long i,j,N = event_table.shape[0]
#        np.int_t back_strip,front_strip
#        double energy_
#        c_bool cond_fa,cond_ba,cond_ba1,cond_ba2,cond_ba_sum,cond_F# = False,False,False,False,False,False
#        
#    #проход по группам
#    i,j = 0,0
#    while i < N:
#        try:
#            if event_table[i].event_type == 1: 
#                cond_fa = alpha_energy_check(energy[i])
#                cond_F = fission_energy_check(energy[i+1]) | fission_energy_check(energy[i+3])
#                cond_ba1 =alpha_energy_check(energy[i+2])
#                front_strip = event_table[i].strip
#                
#                if (event_table[i].synchronization_time == event_table[i+5].synchronization_time)&(event_table[i+5].event_type==2):
#                    ##threat as front-back-back a-F   
#                    cond_F = cond_F | fission_energy_check(energy[i+5])
#                    cond_ba2 =alpha_energy_check(energy[i+4])
#                    if not (cond_ba1 | cond_ba2):
#                        cond_ba_sum = alpha_energy_check(energy[i+2]+energy[i+4])
#                    if cond_ba_sum: # energy_back_strip1 + energy_back_strip2 is inside [8,12] MeV
#                        energy_ = energy[i+2]+energy[i+4]
#                        if energy[i+2]>energy[i+4]:
#                            back_strip = event_table[i+2].strip
#                        else:
#                            back_strip = event_table[i+4].strip
#                        copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                    elif cond_ba1: # energy_back_strip1  is inside [8,12] MeV
#                        energy_ = energy[i+2]
#                        back_strip = event_table[i+2].strip
#                        copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                    elif cond_ba2: # energy_back_strip2  is inside [8,12] MeV
#                        energy_ = energy[i+4]
#                        back_strip = event_table[i+4].strip
#                        copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1) 
#                    elif cond_F: # fission energy of any strip is > 50 MeV
#                        if fission_energy_check(energy[i+3]):
#                            energy_ = energy[i+3]
#                            back_strip = event_table[i+3].strip
#                            copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                        elif fission_energy_check(energy[i+5]):
#                            energy_ = energy[i+5]
#                            back_strip = event_table[i+5].strip
#                            copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                        elif fission_energy_check(energy[i+1]):
#                            energy_ = energy[i+1]
#                            back_strip = 0#event_table[i+2].strip
#                            copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                    elif cond_fa: # energy_front_strip  is inside [8,12] MeV
#                        energy_ = energy[i]
#                        if energy[i+2]>energy[i+4]:
#                            back_strip = event_table[i+2].strip
#                        else:
#                            back_strip = event_table[i+4].strip
#                        copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                    i += 6
#                    
#                elif (event_table[i].synchronization_time == event_table[i+3].synchronization_time)&(event_table[i+3].event_type==2):
#                    #threat as front-back a-F
#                    if cond_ba1: # energy_back_strip1  is inside [8,12] MeV
#                        energy_ = energy[i+2]
#                        back_strip = event_table[i+2].strip
#                        copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1) 
#                    elif cond_F: # fission energy of any strip is > 50 MeV
#                        if fission_energy_check(energy[i+3]):
#                            energy_ = energy[i+3]
#                            back_strip = event_table[i+3].strip
#                        elif fission_energy_check(energy[i+1]):
#                            energy_ = energy[i+1]
#                            back_strip = 0#event_table[i+2].strip
#                        copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                    elif cond_fa: # energy_front_strip  is inside [8,12] MeV
#                        energy_ = energy[i]
#                        back_strip = event_table[i+2].strip
#                        copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                    i += 4
#                    
#                elif (event_table[i].synchronization_time == event_table[i+1].synchronization_time)&(event_table[i+1].event_type==1):
#                    #threat as front a-F
#                    back_strip = 0 
#                    if cond_F: # fission energy of any strip is > 50 MeV
#                        energy_ = energy[i+1]
#                        copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                    elif cond_fa: # energy_front_strip  is inside [8,12] MeV
#                        energy_ = energy[i]               
#                        copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,1)
#                    i += 2
#                    
#                else:
#                    i += 1
#                    
#                #fill data table
#            elif event_table[i].event_type == 3:
#                #threat as side a-f
#                cond_fa = alpha_energy_check(energy[i])
#                cond_F = fission_energy_check(energy[i+1])
#                front_strip = 0
#                back_strip = 0
#                if cond_fa:
#                    energy_ = energy[i]
#                    copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,3)
#                elif cond_F:
#                    energy_ = energy[i+1]
#                    copy_event_data(&data_table[j],event_table[i],&j,energy_,front_strip,back_strip,3)
#                i += 2
#            else:
#                i += 1
#            
#            if i % 100000 == 0:
#                print 'i,j',i,j, data_table,'\n\n'
#        except IndexError:
#            print 'i,j:',i,j
#    return data_table[:j]
            
    
