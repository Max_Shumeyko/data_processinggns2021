from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

import numpy

quick_methods = Extension(name="*", sources=["*.pyx"],
          libraries=["m"],
          include_dirs=[numpy.get_include()]
) # Unix-like specific]
setup(ext_modules=cythonize(quick_methods))

