#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 16:18:47 2019

Time converters and tools for time-series.

@author: eastwood
"""
from dateutil.parser import parse
import pandas as pd
import numpy as np

# uint64, datetime.datetime -> datetime.datetime 
mcs_to_datetime = lambda time, time_start: time_start + pd.offsets.Micro(time) 

def str_to_datetime(string, *arg, **argv):
    """str -> datetime.datetime
    
    See dateutil.parser.parse.
    """
    return parse(string, *arg, **argv)

def microseconds_to_timeserie(data, time_start):
    """np.ndarray[uint64], datetime.datetime -> pd.Series
    
    Convert microseconds array to datetime pd.Series starting at time_start.
    Datetime format: "%Y-%m-%d %H:%M" [t0, t1, ...]. 
    """
    timeserie = pd.Series(data)
    timeserie = [mcs_to_datetime(time, time_start) for time in timeserie]
    return timeserie

def intersection_timeseries(serie1, serie2):
    """[datetime.datetime], [datetime.datetime] -> pd.Series['datetime64[ns]']
    
    Select subsample timeserie which is intersection of two timeseries.
    """
    ts1 = np.asarray([i.to_datetime64() for i in serie1])
    ts2 = np.asarray([i.to_datetime64() for i in serie2])
    
    min1, min2 = min(ts1), min(ts2)
    max1, max2 = max(ts1), max(ts2)
    
    if min1 > max2 or min2 > max1:
        print("no intersection")
        return pd.Series([], dtype='datetime64[ns]')
    
    min_ = max(min1, min2)
    max_ = min(max1, max2)
    
    return pd.Series(ts1[(ts1 >= min_) & (ts1 <= max_)])