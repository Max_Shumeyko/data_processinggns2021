# -*- coding: utf-8 -*-
"""
Created on Fri Sep  2 14:26:38 2016

@author: eastwood
"""

import os
import numpy as np
#import read_file
from . import read_digital

def correct_time(event_tables):
    '''
    Because each table's time array starts from 0,
    the function corrects times by adding the last time value from each table
    to other.
    '''
    N = len(event_tables)
    for i in range(N-1):
        final_time = event_tables[i]['time'][-1]
        event_tables[i+1]['time'] += final_time
    return event_tables

def read_files(filenames,f=None,**argv):
    """
    Read DGFRS experimental data in analogue format.
    read_files(filenams,**argv)
        filenames - add names of data files. See filenames's patterns.
        strip_convert [True/False] - apply special arrays to reorder strip numbers. see 'read_file.pyx'
    return np.ndarray data table. See format description.
    
    Valid filename's patterns:
    tsn.xxx
    tsn.xxx, tsn.yyy, tsn.zzz, ...
    tsn.xxx - tsn.yyy
    tsn.xxx-tsn.yyy and etc.
    
    Output format description:
        event_type: '1'-front, '2'-back, '3'-side, '4'-veto
        strip: front:1-48 back: 1-128, side: 1-6
        channel: front:4096 back:8192 side: 8192
        scale: 0-alpha, 1-fission
        time: double microseconds
        beam_mark: bool
        tof: bool
        synchronization_time: 0..65536
        DAD4: 0..65536 low counter microseconds
        DAD5: 0..65536 high counter shows how many times low counter has rotated. Time up to about 1 hour 40 minutes
    """
    if type(filenames) == np.ndarray:
        return filenames
    
    #a bit of carrying 
    #read = lambda x,y=strlength: read_file(x,y)
    #primitive parser
    def func1(s):
        if '-' in s:
            fn = s.split('-')
            if len(fn) > 2:
                raise ValueError('read_files: Incorrect filenames')
            numb = []
            for i in fn:
                i.replace(' ','')
                numb.append( int(i.split('.')[1]) )
            numb = list(range(numb[0],numb[1]+1))
            for i in range(len(numb)):
                numb[i] = 'tsn.'+str(numb[i])
            return numb
        else:
            pass
    
    if '-' in filenames:
        names = func1(filenames)
    elif ',' in filenames:
        names = filenames.split(',')
    elif ('tsn.' in filenames): # len(filenames) < 8 and 
        names = [filenames,]
    else:
        raise ValueError('read_files: Incorrect filenames')
    
#    if f is None:
#        f = lambda x:read_file.read_file(x,**argv)
    
    print(names)
    event_tables = list(map(f,names))
    #print event_tables
    event_tables = correct_time(event_tables)
    return np.concatenate(event_tables)
    
    
def read_dg_files(filenames):
    """
    Read DGFRS experimental data in digital format.
    read_files(filenams,**argv)
        filenames - add names of data files. See filenames's patterns.
        strip_convert [True/False] - apply special arrays to reorder strip numbers. see 'read_file.pyx'
    return np.ndarray data table. See format description.
    
    Valid filename's patterns:
    xxx.bin
    xxx.bin, yyy.bin, zzz.bin, ...
    
    Output format description:
        event_type: '1'-front, '2'-back, '3'-side, '4'-veto
        strip: front:1-48 back: 1-128, side: 1-6
        channel: front:4096 back:8192 side: 8192
        scale: 0-alpha, 1-fission
        time: double microseconds
        beam_mark: bool
        tof: bool
        synchronization_time: 0..65536
        DAD4: 0..65536 low counter microseconds
        DAD5: 0..65536 high counter shows how many times low counter has rotated. Time up to about 1 hour 40 minutes
    """
    f = lambda x:read_digital.read_file(x)
    
    if type(filenames) == np.ndarray:
        return filenames
     
    if ',' in filenames:
        names = filenames.split(',')
    elif len(filenames) < 8 and ('.bin' in filenames):
        names = [filenames,]
    else:
        if '.bin' in filenames:
            return f(filenames)[0]
        else:
            raise ValueError('read_files: Incorrect filenames')
       
    
    
    print(names)
    event_tables = list(map(f,names))
    #print event_tables
    #event_tables = correct_time(event_tables)
    return np.concatenate([ev[0] for ev in event_tables])
    
#    frame = pd.DataFrame([])
#    
#    for name_ in names:
#        try:
#            hdf_file = name_.split('.')[1]+'.h5'
#            if os.path.exists(hdf_file):
#                frame_cur = pd.read_hdf(hdf_file,'tsn')
#                print hdf_file, 'has been red'
#            else:
#                frame_cur = read_file(name_,**argv)
#            frame = pd.concat([frame,frame_cur],ignore_index=True)
#        except IOError,e:
#            print e
#            continue
#          return frame