# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 11:35:04 2018

@author: eastwood

Convert from analog CAMAC format to dataframe 
(np.ndarray[BLOCK], see Description)

MAIN FUNCTIONS:
    read_file(filename, strip_convert=True,  count_blocks=0, offset=0) - read
    raw binary file and return <np.ndarray> event_table (see Description)
    
    Bunch of get_<parameter> functions: (-> line above 483)
        read_parameter(filename, parameter_name="get_rotation_time")
        get_rotation_time(np.ndarray[BLOCK, cast=True] block_table)
        
"""
# cython: boundscheck=False

from libc.stdio cimport FILE, fopen, fwrite, fscanf, fclose, fprintf, fseek,\
                        ftell, SEEK_END, SEEK_SET, rewind, fread
from libc.stdlib cimport malloc, free   

import os
import numpy as np
import spectrum as sp 
import pkgutil                                                   

from cython cimport boundscheck, wraparound
cimport numpy as np
cimport cython
from libc.math cimport fabs

from data_types cimport c_bool, c_word, DATA_EVENT, EVENT, BLOCK
from array_types import event_type, energy_event_type, final_event_type
from array_types import analog_block

from cython.parallel cimport prange
#### Description
'''
Convert this:
cdef packed struct BLOCK:
    c_word w0
    c_word w1
    c_word w2
    c_word w3
    c_word w4
    c_word w5
    c_word w6
    c_word w7
    c_word w8
    np.uint8_t w9
    np.uint8_t w10
    np.uint32_t w11
    np.uint32_t w12
    c_word w13
    c_word w14
    
To this:
    event_type # 1 - focal, 2 - back, 3 - side, 4 - veto
    strip # 0 .. 128; front:1-48 back: 1-128, side: 1-6
    channel # front:4096 back:8192 side: 8192
    scale # 0 - alpha, 1 - fission
    time_macro # sec int64
    time_micro # microseconds int64
    beam_mark # bool
    tof # bool
    tofD1 # 1st tof wire counter
    tofD2 # 2nd tof wire counter
    synchronization_time #  0..65536 
    rotation_time # optical detector on rotation target
   
Description:
block.[w*]  word_number  DType       Description
w0          1            np.uint16   ID,1 - 1st 16 strips, 2 - 2nd 16, 3 - 3rd 16, 4-7 - side or veto
w1          2            np.uint16   Alpha or ER energy (channel)
w2          3            np.uint16   Fission-scale energy, front strip [4096*Number_strip]
w3          4            np.uint16   Synchronization time (target optical sensor) mks
w4          5            np.uint16   TOF time-of-flight, logic mark deltaE1 or deltaE2
w5          6            np.uint16   deltaE1, first TOF-counter
w6          7            np.uint16   deltaE2, second TOF-counter
w7          8            np.uint16   Alpha scale, side energy
w8          9            np.uint16   Fission scale, side energy
w9          10           np.uint8    strip number, main back strip, [0 .. 127], should add +1
w10         11           np.uint8    strip number, second back strip (if events is detected by two back strips) [0 .. 127], should add +1
w11         12           np.uint32   time in seconds, at the beginning of each file = 0
w12         13           np.uint32   time in microseconds, at the beginning of each file = 0
w13         14           np.uint16   Alpha energy channel, main back detector
w14         15           np.uint16   Alpha energy channel, second back detector     
'''
      
#####
## Strip convertion arrays.
front_convert_array = np.zeros(49,dtype=np.uint16)
back_convert_array = np.zeros(129,dtype=np.uint16)
# data = pkgutil.get_data('data_processing_GNS2021', '/config/strip_config.txt')
# data = data.split('\n')
# get_num = lambda line: int(line.split()[1]) 
# front_convert_array[1:] = np.asarray(map(get_num, data[1:49]), dtype=np.uint16)
# back_convert_array[1:] = np.asarray(map(get_num, data[50:178]), dtype=np.uint16)

#print list(enumerate(back_convert_array))
front_convert_array[1:] = [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46,
                          2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47,
                          3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48]
##front_convert_array[1:]= [48,45,42,39,36,32,29,26,23,20,16,13,10,7,4,33,
##                          47,44,41,38,35,31,28,25,22,19,15,12, 9,6,3,17,
##                          46,43,40,37,34,30,27,24,21,18,14,11, 8,5,2,1]
#
##for back strips 
back_convert_array[1:] = [1,  3,  5,  7,  9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31,
                          2,  4,  6,  8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32,
                          33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63,
                          34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64,
                          65, 67, 69, 71, 73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95,
                          66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96,
                          97, 99,101,103,105,107,109,111,113,115,117,119,121,123,125,127,
                          98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128] 

cdef c_word convert_front_strip(c_word strip):
    return 49 - strip

cdef BLOCK* read_block(BLOCK *block, FILE* file_) :  
    fread(<void*>block, sizeof(BLOCK), 1, file_)
    return block   
    
cdef c_word get_index(BLOCK *block) :
    assert (block.w0 < 1) | (block.w0 > 6), "Wrong index (w0) number" 
    return block.w0 

cdef np.uint64_t get_time(BLOCK *block) :
    return block.w12

cdef c_word get_front_strip(BLOCK *block, strip_convert=False, 
                        c_word[:] front_convert_array=front_convert_array):
    cdef:
        c_word id_ = block.w0 % 16
        c_word strip = block.w2 // 4096 + 1 + 16*(id_-1)
    return front_convert_array[strip] if strip_convert else strip    
    
cdef c_word get_side_strip(BLOCK *block) :
    cdef:
        c_word id_ = block.w0 % 16 - 3
        c_word strip = block.w2 // 4096 + 1 + 16*(id_-1)
    return strip #front_convert_array[strip] #strip
    
cdef c_word get_alpha_channel(BLOCK *block) nogil:
    return block.w1 % 8192
    
cdef c_word get_fission_channel(BLOCK *block) nogil:
    return block.w2 % 4096
    
cdef c_word get_back_alpha_channel1(BLOCK *block) nogil:
    return block.w13 % 8192
    
cdef c_word get_back_alpha_channel2(BLOCK *block) nogil:
    return block.w14 % 8192

cdef c_word get_back_strip(BLOCK *block, strip_convert=False):
    cdef c_word strip = block.w9 + 1 # TODO REMAKE delete + 1
    return strip if not strip_convert else back_convert_array[strip]
    
cdef c_word get_back_strip2(BLOCK *block, strip_convert=False):
    cdef c_word strip = block.w10 + 1 # TODO REMAKE delete + 1
    return strip if not strip_convert else back_convert_array[strip]
 
cdef c_bool get_tof(BLOCK *block):
    return (block.w5 > 0)

cdef copy_addition_data(BLOCK *block, EVENT *event):
    event.time_macro = block.w11
    event.beam_mark = False
    event.tofD1 = block.w5
    event.tofD2 = block.w6
    event.tof = get_tof(block) # (block.w5 > 0) or (block.w6 > 0) #or (block.w4 > 0)
    event.synchronization_time = block.w3 
    event.rotation_time = block.w3
        
cdef c_bool is_veto(BLOCK *block) nogil:
    return (block.w7 % 32768 > 0)
    
cdef EVENT add_veto_event(BLOCK *block, np.uint64_t time_micro) : #np.ndarray[EVENT, cast=True] event_table,   
    cdef EVENT event    
    event.event_type = 4
    event.time_micro = time_micro
    event.strip = 1
    event.channel = get_alpha_channel(block)
    event.scale = 0
    copy_addition_data(block,&event)
    return event

cdef c_bool is_side(BLOCK *block) nogil:
    cdef c_word index = block.w0 % 16 #get_index(block)
    return (index > 3)#&is_time(block)
             
cdef EVENT add_side_event_alpha(BLOCK *block, np.uint64_t time_micro)  :
    cdef EVENT event    
    event.event_type = 3
    event.time_micro = time_micro
    event.strip = get_side_strip(block)
    event.channel = get_alpha_channel(block)
    event.scale = 0 # alpha
    copy_addition_data(block, &event)
    return event
    
cdef EVENT add_side_event_fission(BLOCK *block, np.uint64_t time_micro)  :
    cdef EVENT event    
    event.event_type = 3
    event.time_micro = time_micro
    event.strip = get_side_strip(block)
    event.channel = get_fission_channel(block)
    event.scale = 1 # fission
    copy_addition_data(block, &event)
    return event
   
cdef c_bool is_front(BLOCK *block) nogil:
    cdef c_word index = block.w0 % 16 #get_index(block)
    return (index < 4)#&is_time(block) 
    
cdef c_bool is_back(BLOCK *block) nogil:
    return (block.w9 > 0) or (block.w10 > 0)

cdef EVENT add_front_alpha(BLOCK *block, strip_convert, np.uint64_t time_micro) :
    cdef EVENT event 
    event.event_type = 1
    event.time_micro = time_micro
    event.strip = get_front_strip(block, strip_convert)
    event.channel = get_alpha_channel(block)
    event.scale = 0 # alpha
    copy_addition_data(block, &event)
    return event
    
cdef EVENT add_front_fission(BLOCK *block, strip_convert, np.uint64_t time_micro) :
    cdef EVENT event 
    event.event_type = 1
    event.time_micro = time_micro
    event.strip = get_front_strip(block, strip_convert)
    event.channel = get_fission_channel(block)
    event.scale = 1 # fission
    copy_addition_data(block, &event)
    return event  
       
cdef EVENT add_back_alpha1(BLOCK *block, strip_convert, np.uint64_t time_micro) :
    cdef EVENT event 
    event.event_type = 2
    event.time_micro = time_micro
    event.strip = get_back_strip(block, strip_convert)
    event.channel = get_back_alpha_channel1(block)
    event.scale = 0 # alpha
    copy_addition_data(block, &event)
    return event
    
cdef EVENT add_back_alpha2(BLOCK *block, strip_convert, np.uint64_t time_micro) :
    cdef EVENT event 
    event = add_back_alpha1(block, strip_convert, time_micro)
    event.strip = get_back_strip2(block, strip_convert)
    event.channel = get_back_alpha_channel2(block)
    return event

cdef EVENT add_back_fission1(BLOCK *block, strip_convert, np.uint64_t time_micro) :
    cdef EVENT event 
    event.event_type = 2
    event.time_micro = time_micro
    event.strip = get_back_strip(block, strip_convert)
    event.channel = get_fission_channel(block)
    event.scale = 1 # fission
    copy_addition_data(block, &event)
    return event
    
cdef EVENT add_back_fission2(BLOCK *block, strip_convert, np.uint64_t time_micro) :
    cdef EVENT event 
    event = add_back_fission1(block, strip_convert, time_micro)
    event.strip = get_back_strip2(block, strip_convert)
#    event.channel = block.w14#get_back_alpha_channel1(block)
    return event

# main reading loop
cdef long main_reading_loop(long count_blocks, FILE* cfile, \
                            np.ndarray[EVENT, cast=True] event_table,
                            strip_convert=True):
    cdef:
        long i=0, event_num=0
        np.uint64_t time, true_time
        np.uint64_t time_start=0, time_pre=0, time_collected=0
        BLOCK *block = <BLOCK*>malloc(sizeof(BLOCK))
        EVENT check_event
    
    with boundscheck(False),wraparound(False):
        for i in range(count_blocks):
            read_block(block, cfile) #read data to block
            
            time = get_time(block)            
            # time correction (when timeline crosses 0)
            if i == 0:
                time_start = time
            if time_pre > time:
                time_collected += 4294967296 # time counter ovefloat
            true_time = time + time_collected - time_start
            time_pre = time
            
            if is_front(block):
                check_event = add_front_alpha(block, strip_convert, true_time)
                if check_event.channel > 0:
                    event_table[event_num] = check_event
                    event_num += 1
                    
                check_event = add_front_fission(block, strip_convert, true_time)
                if check_event.channel > 0:
                   event_table[event_num] = check_event
                   event_num += 1
                   
            if is_back(block):
                check_event = add_back_alpha1(block, strip_convert, true_time)
                if check_event.channel > 0:
                    event_table[event_num] = check_event
                    event_num += 1
                    
                check_event = add_back_fission1(block, strip_convert, true_time)
                if check_event.channel > 0:
                    event_table[event_num] = check_event
                    event_num += 1
                    
                check_event = add_back_alpha2(block, strip_convert, true_time)
                if check_event.channel > 0:
                    event_table[event_num] = check_event
                    event_num += 1
                    
                check_event = add_back_fission2(block, strip_convert, true_time)
                if get_back_alpha_channel2(block) > 0:
                    event_table[event_num] = check_event
                    event_num += 1
                    
            if is_side(block):
                event_table[event_num] = add_side_event_alpha(block, true_time)
                event_num += 1
                event_table[event_num] = add_side_event_fission(block, true_time)
                event_num += 1
                
            elif is_veto(block):               
                event_table[event_num] = add_veto_event(block, true_time) #event_table,
                event_num += 1
             
    free(block)
    return event_num


def read_file(filename, strip_convert=True, long count_blocks=0, 
              long offset=0):    
    cdef:
        FILE* cfile
        #long count_blocks    
     
    size = os.path.getsize(filename)
    if count_blocks > 0:
        size_ = sizeof(BLOCK) * count_blocks
        size = size if size_ > size else size_
    else:
        count_blocks = (size - offset) // sizeof(BLOCK)   
    #count_blocks = (os.path.getsize(filename) // sizeof(BLOCK)) # each block contains 14 2-byte values (24 bytes for one block)
    cfile = fopen(filename, 'rb')
    fseek(cfile, offset, SEEK_SET)
    event_table = np.empty(int(4 * count_blocks), dtype=event_type)
    
    #main reading loop
    event_num = main_reading_loop(count_blocks, cfile, event_table, strip_convert)            
    fclose(cfile)    
    return event_table[:event_num], size

# hint coefs array structure: [event_type 1-front, 2-back, 3-side]
#                     [scale 0-alpha, 1-fission][coef 0-a, 1-b][strip 0..129]
cdef double calc_front_alpha_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return block.w1*coefs[1,0,0,strip] + coefs[1,0,1,strip]
 
cdef double calc_front_fission_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return get_fission_channel(block)*coefs[1,1,0,strip] + coefs[1,1,1,strip]

cdef double calc_back_alpha_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return get_back_alpha_channel1(block)*coefs[2,0,0,strip] + coefs[2,0,1,strip]

cdef double calc_back_alpha_energy2(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return get_back_alpha_channel2(block)*coefs[2,0,0,strip] + coefs[2,0,1,strip]
 
cdef double calc_back_fission_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return get_fission_channel(block)*coefs[2,1,0,strip] + coefs[1,1,1,strip]

cdef double calc_side_alpha_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return block.w1*coefs[3,0,0,strip] + coefs[3,0,1,strip]
    
cdef double calc_side_fission_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return block.w2*coefs[3,0,0,strip] + coefs[3,0,1,strip]
    
cdef c_bool alpha_energy_check(double energy):
    return (energy>=0)&(energy<=30000)
    
cdef c_bool fission_energy_check(double energy):
    return (energy>= 30000)
    
    
cdef long get_data_table_loop(long count_blocks, FILE* cfile, \
            np.ndarray[DATA_EVENT, cast=True] data_table, \
            double[:,:,:,::1] coefs, strip_convert=True):
    #    
    #    np.int_t event_type # 1 - focal-back, 3 - side
    #    double energy
    #    np.int_t cell_x # 0 .. 48
    #    np.int_t cell_y # 0 .. 128
    #    double time_micro # microseconds 
    #    double time_macro # seconds 
    #    c_bool beam_mark # bool
    #    c_bool tof # bool   
    cdef:
        long i=0, event_num=0
        np.uint64_t time, true_time
        np.uint64_t time_start=0, time_pre=0, time_collected=0
        c_word back_strip2
        int scale=0
        BLOCK *block = <BLOCK*>malloc(sizeof(BLOCK))
        DATA_EVENT d_event
    
    with boundscheck(False), wraparound(False):
        for i in range(count_blocks):
            read_block(block, cfile) #read data to block
                
            if is_veto(block) == 4:
                continue # exclude veto events
            scale = 0
            d_event.energy_front = 0
            d_event.energy_back = 0
            d_event.event_type = 1
#            d_event.time_micro = block.w12
            d_event.time_macro = block.w11
            d_event.beam_mark = 0
            d_event.tof = get_tof(block) #(block.w5 > 0) | (block.w6 > 0) # (block.w4 > 0) | 
            d_event.tofD1 = block.w5
            d_event.tofD2 = block.w6
            d_event.rotation_time = block.w3
            d_event.cell_x = 0
            d_event.cell_y = 0
            
            # time correction - if timeserie crosses 0 because time counter overflow
            time = get_time(block)
            if i == 0:
                time_start = time
            if time_pre > time:
                time_collected += 4294967296
            d_event.time_micro = time + time_collected - time_start
            time_pre = time
            
            if is_front(block):
                d_event.cell_x = get_front_strip(block, \
                                                 strip_convert=strip_convert)
                # alpha energy
                energy_front = calc_front_alpha_energy(block, \
                                                       d_event.cell_x, coefs)
                # fission energy
                energy_front_F = calc_front_fission_energy(block, \
                                                           d_event.cell_x, coefs)
                if energy_front_F > 40000.:
                    energy_front = energy_front_F
                    
                d_event.energy_front = energy_front
            if is_back(block):
                d_event.cell_y = get_back_strip(block, \
                                                strip_convert=strip_convert)
                # alpha energy
                energy_back = calc_back_alpha_energy(block, d_event.cell_y, \
                                                     coefs)
                
                # second back signal                
                if get_back_alpha_channel2(block) > 0:
                    back_strip2 = get_back_strip2(block,\
                                                  strip_convert=strip_convert)    
                    energy_back += calc_back_alpha_energy2(block, back_strip2, \
                                                           coefs)
                    
                # fission energy: note get energy from front fission channel
                energy_back_F = calc_back_fission_energy(block, d_event.cell_y,\
                                                         coefs)
                
                if energy_back_F > 40000.:
                    energy_back = energy_back_F
                
                d_event.energy_back = energy_back
            if is_side(block):
                d_event.cell_x = get_side_strip(block)
                if d_event.energy_front == 0:
                    d_event.event_type = 3
                    d_event.energy_front = calc_side_alpha_energy(block, \
                                                        d_event.cell_x,coefs)
                else:
                    d_event.energy_front += calc_side_alpha_energy(block, \
                                                        d_event.cell_x,coefs)
                    d_event.energy_back += calc_side_alpha_energy(block, \
                                                        d_event.cell_x,coefs)
#            print "CHECK back energy: ", d_event.energy_back
#            print "CHECK flag front, back, side: ",   is_front(block), is_back(block),  is_side(block)                 
            data_table[event_num] = d_event
            event_num+=1
                
             
    free(block)
    return event_num

def get_data_table(filename,strip_convert=True,offset=0,coefs_folder=None): 
    ''' 
    Input: raw binary file.
    Output format [DATA_EVENT]:
    np.int_t event_type # 1 - focal-back, 3 - side
    double energy
    np.int_t cell_x # 0 .. 48
    np.int_t cell_y # 0 .. 128
    double time_micro # microseconds 
    double time_macro # seconds 
    c_bool beam_mark # bool
    c_bool tof # bool 
    '''
    cdef:
        FILE* cfile
        long count_blocks      
    size = os.path.getsize(filename)
    count_blocks = (size - offset) // sizeof(BLOCK)   
    #count_blocks = (os.path.getsize(filename) // sizeof(BLOCK)) # each block contains 14 2-byte values (24 bytes for one block)
    cfile = fopen(filename, 'rb')
    fseek(cfile, offset, SEEK_SET)
    data_table = np.empty(int(count_blocks), dtype=final_event_type)
    
    #prepare calibration coefficients
    coefs = sp.get_all_calibration_coefficients(coefs_folder=coefs_folder)
    
    #main reading loop
    if coefs_folder is None:
        raise ValueError('Specify calibration coefficients folder')
    event_num = get_data_table_loop(count_blocks, cfile, data_table, \
                                    coefs, strip_convert)
                  
    fclose(cfile)    
    return data_table[:event_num], size
    
    
#### Bunch of get_<parameter> functions:
P_DICT = dict()

def parameter_getter(func):
    global P_DICT
    name = func.__name__.split('getp_')[1]
    P_DICT[name] = func  
    return func

@parameter_getter
def getp_rotation_time(np.ndarray[BLOCK, cast=True] block_table):
    cdef:
        long i, size
        np.ndarray[int, cast=True] output
        
    size = len(block_table)
    output = np.empty(size, dtype=np.uint32)
    with boundscheck(False), wraparound(False):
        for i in prange(size, nogil=True):
            output[i] = block_table[i].w3
            
    return np.asarray(output, dtype=np.uint32)

@parameter_getter
def getp_tof(np.ndarray[BLOCK, cast=True] block_table):
    cdef:
        long i, size
        np.ndarray[np.uint8_t, cast=True] output
        
    size = len(block_table)
    output = np.empty(size, dtype=np.uint8)
    with boundscheck(False), wraparound(False):
        for i in prange(size, nogil=True):
            output[i] = <int>((block_table[i].w4 > 0) | 
                              (block_table[i].w5 > 0) |  
                              (block_table[i].w6 > 0))
            
    return np.asarray(output, dtype=np.bool)

@parameter_getter
def getp_event_type(np.ndarray[BLOCK, cast=True] block_table):
    """Extract event type array from the block_table[BLOCK],
    where 1 - focal (front-back), 2 - back (without front signal), 
          3 - side, 0 - veto & garbage.
    """
    cdef:
        long i, size
        c_word id_, strip
        np.ndarray[np.uint16_t, cast=True] output
        
    size = len(block_table)
    output = np.empty(size, dtype=np.uint16)
    with boundscheck(False), wraparound(False):
        for i in prange(size, nogil=True):
            
            if is_front(&block_table[i]):
                output[i] = 1
            elif is_back(&block_table[i]):
                output[i] = 2
            elif is_side(&block_table[i]):
                output[i] = 3
            else:
                output[i] = 0
                        
    return np.asarray(output, dtype=np.uint16)
           
@parameter_getter
def getp_coord(np.ndarray[BLOCK, cast=True] block_table, convert=True):
    cdef:
        long i, size
        c_word id_, strip
        c_bool convert_ = 1 if convert else 0
        np.ndarray[np.uint16_t, cast=True] output_front
        np.ndarray[np.uint16_t, cast=True] output_back
        np.ndarray[np.uint16_t, cast=True] front_convert = front_convert_array[:]
        np.ndarray[np.uint16_t, cast=True] back_convert = back_convert_array[:]
        
    size = len(block_table)
    output_front = np.empty(size, dtype=np.uint16)
    output_back = np.empty(size, dtype=np.uint16)
    with boundscheck(False), wraparound(False):
        for i in prange(size, nogil=True):
            # read front
            if is_front(&block_table[i]):
                id_ = block_table[i].w0 % 16
                strip = block_table[i].w2 // 4096 + 1 + 16*(id_-1)
                if convert_:
                    output_front[i] = front_convert[strip]
                else:
                    output_front[i] = strip
            else:
                output_front[i] = 0
            
            # read_back
            if is_back(&block_table[i]):
                strip = block_table[i].w9 + 1
                if convert_:
                    output_back[i] = back_convert[strip]
                else:
                    output_back[i] = strip
            else:
                output_back[i] = 0    
                        
    return np.asarray(output_front, dtype=np.uint16), \
           np.asarray(output_back, dtype=np.uint16)
           

         
@parameter_getter
def getp_alpha_channel(np.ndarray[BLOCK, cast=True] block_table):
    cdef:
        long i, size
        np.ndarray[np.uint16_t, cast=True] output
        
    size = len(block_table)
    output = np.empty(size, dtype=np.uint16)
    with boundscheck(False), wraparound(False):
        for i in prange(size, nogil=True):
            output[i] = get_alpha_channel(&block_table[i])
            
    return np.asarray(output, dtype=np.uint16)

@parameter_getter
def getp_back_alpha_channel(np.ndarray[BLOCK, cast=True] block_table):
    cdef:
        long i, size
        np.ndarray[np.uint16_t, cast=True] output
        
    size = len(block_table)
    output = np.empty(size, dtype=np.uint16)
    with boundscheck(False), wraparound(False):
        for i in prange(size, nogil=True):
            output[i] = get_back_alpha_channel1(&block_table[i])
            
    return np.asarray(output, dtype=np.uint16)

@parameter_getter
def getp_fission_channel(np.ndarray[BLOCK, cast=True] block_table):
    cdef:
        long i, size
        np.ndarray[np.uint16_t, cast=True] output
        
    size = len(block_table)
    output = np.empty(size, dtype=np.uint16)
    with boundscheck(False), wraparound(False):
        for i in prange(size, nogil=True):
            output[i] = get_fission_channel(&block_table[i])
            
    return np.asarray(output, dtype=np.uint16)

@parameter_getter
def getp_back_fission_channel(np.ndarray[BLOCK, cast=True] block_table):
    cdef:
        long i, size
        np.ndarray[np.uint16_t, cast=True] output
        
    size = len(block_table)
    output = np.empty(size, dtype=np.uint16)
    with boundscheck(False), wraparound(False):
        for i in prange(size, nogil=True):
            output[i] = get_fission_channel(&block_table[i])
            
    return np.asarray(output, dtype=np.uint16)

@parameter_getter
def getp_time_mks(np.ndarray[BLOCK, cast=True] block_table):
    cdef:
        long i, size
        np.uint64_t col, pre, time_start
        np.ndarray[np.uint64_t, cast=True] output, output1
        
    size = len(block_table)
    output = np.empty(size, dtype=np.uint64)
    output1 = np.empty(size, dtype=np.uint64)
    
    with boundscheck(False), wraparound(False):
        for i in prange(size, nogil=True):
            output[i] = block_table[i].w12
         
        # time correction (when timeline crosses 0)
        time_start = output[0]
        col = 0
        pre = 0
        for i in range(size):
            if output[i] < pre:
                col += 4294967296
            output1[i] = output[i] + col - time_start
            pre = output[i]
            
    return np.asarray(output1, dtype=np.uint64)
            
def read_parameter(filename, func="rotation_time", **argv):
    """Calls parameter getter function "getp_"+func with addition argv 
    arguments and return extracted array.
    
    See read_file_2019.P_DICT for list of parameters.
    """
    
    if not os.path.isfile(filename):
        raise IOError("{} doesn't exist".format(filename))
        
    data = np.fromfile(filename, dtype=analog_block)
    
    global P_DICT
    p_getter = P_DICT.get(func)
    if p_getter is None:
        raise ValueError("Parameter getter doesn't exist: {}".format(func))
        
    return p_getter(data, **argv)

def read_parameters(filename, func_list=["rotation_time"], **argv):
    """filename: str, [func_list: function], **argv -> dict(function_name: str -> [obj])
    
    Calls parameter getter functions from func_list with addition argv 
    arguments and return extracted arrays.
    
    See read_file_2019.P_DICT for list of parameters.
    """
    assert type(func_list) is list, 'func_list must be list'
    
    if not os.path.isfile(filename):
        raise IOError("{} doesn't exist".format(filename))
           
    global P_DICT
    wrong_funcs = []
    for func in func_list:
        p_getter = P_DICT.get(func)
        if p_getter is None:
            wrong_funcs.append(func)
    
    if wrong_funcs:
        raise ValueError("Parameter getter doen't exist: {}"
                         .format(wrong_funcs))
        
    data = np.fromfile(filename, dtype=analog_block)
    output_dict = {} 
    for func in func_list:
        p_getter = P_DICT.get(func)           
        output_dict[func] = p_getter(data, **argv)
        
    return output_dict
 

    
        
