# -*- coding: utf-8 -*-
"""
Created on Thu Aug  7 15:08:07 2014

Code for processing automatic line-calibration of alpha-scale in our experiments.
Based on spectrum_tools.py 
 
@author: eastwoodknight
"""

import numpy as np
import matplotlib.pyplot as plt

from scipy.optimize import leastsq
from scipy.odr import Model, Data, ODR

from . import spectrum as sp
from . import spectrum_tools as st #get_front_spectrs
from . import quick_methods as qm

# energies of peaks for calibration of alpha energy scale from reaction 48Ca + natYb
Alpha_energies = [6040, 6143.8, 6264, 6899.2, 7137, 7922, 8699, 9261]

class Calibration_Error(Exception): pass 
class record: pass
 

def calibrate_line(x, y, weights=False):
    """
    The function for fitting data sample of x, y
    by line function f = a * x + b.
    One can specify weights for each point (sum should = 0)
    
    INPUT:
    x, y - data sample, arrays [1D np.ndarray float or double]
    weights - array of weights for each point
    ! len(x) == len(y) == len(weights)
    
    OUTPUT:
        solution - the Output class stores the output of an ODR run.
        See scipy.odr.Output docs.
        Linear fit coefficients could be extracted like this: 
        >>> solution = calibrate_line(x, y)
        >>> (a, b) = solution.beta
        
    """ 
    f = lambda P, x: P[0] * x + P[1]
    linear = Model(f)
    if np.any(weights):
        mydata = Data(x, y, wd=weights)
    else:
        mydata = Data(x, y)
    myodr = ODR(mydata, linear, beta0=[1., 2.])
    return myodr.run()
 
def show_spectrs(xsample0, sample0, xsample, xmin, xmax, xpeaks, ypeaks,\
    sample, energies, title=None, solution=None):
        
    fig,ax = plt.subplots(3,1,sharex=True)
    if title:
        fig.suptitle(title)
    ax[0].set_title('Raw spectrum')
    ax[0].set_ylabel('counts')
    ax[0].plot(xsample0, sample0, linestyle='steps') 
    ax[0].set_ylim([0, max(ypeaks) * 2.15])
    
    ax[1].set_title('Processed spectrum with marked calibration peaks')
    ax[1].set_ylabel('counts')
    ax[1].set_xlim([xmin, xmax])					
    ax[1].set_ylim([0, max(ypeaks) * 1.25])
    ax[1].plot(xsample, sample, linestyle='steps', color='b', linewidth=3) 
    ax[1].plot(xpeaks, ypeaks, 'ro', linewidth=4)
    
    try:
        ax[2].set_xlim([xmin, xmax])
        ax[2].set_title('Calibration function')
        ax[2].set_ylabel('energy, MeV')
        ax[2].set_xlabel('channel')
        ax[2].plot(xpeaks, energies, 'ro', linewidth=4, label='Data points')
        ax[2].plot(xpeaks, solution[0] * np.ones(len(xpeaks)) + solution[1] * xpeaks, linewidth=2, label='Fitting line')
    except:
        pass
    #print solution[0]*np.ones(len(xpeaks))+solution[1]*xpeaks
    ax[2].legend(loc='lower right')
    plt.show()
    
    
    
def filter_spectr(xmin, xmax, xsample, sample, properties): #window_smooth=7,smooth_wavelet='hanning',background_options='BACK1_ORDER4'):
    """
    The function smooth the spectrum and perform noise supression using
    threshold parameter and wavelet specifications.
    
    INPUT:
        xmin, xmax - specify processing area for X-axis of the sample
        xsample - sample for x-axis (channel or energy)
        sample - sample fro y-axis (counts) 
          !note: len(sample) == len(xsample) !
        properties
        EXAMPLE of properties:
        
        filter_properties = record()
        filter_properties.window_smooth=5
        filter_properties.smooth_wavelet='blackman'
        filter_properties.background_options='BACK1_ORDER8,BACK1_INCLUDE_COMPTON' 
        filter_properties.threshold= 3
        ! See CERN ROOT docs 'spectrum.pdf' for details.
        
    OUTPUT:
        xsample, sample - x-axis and y-axis samples of the processed spectr.
    
    """
    assert xsample.shape == sample.shape, \
        'filter_spectr function requires xsample and sample have the same shape!'    
    sample = np.array(sample)
    x_ind = (xsample >= xmin) & (xsample <= xmax)
    sample = sample[x_ind]
    xsample = xsample[x_ind] #).tolist() 
    if (sample == 0).sum() == len(sample):
        raise  Calibration_Error('No data in given sample')
    if properties.window_smooth: 
        sample = st.smooth(sample, properties.window_smooth, properties.smooth_wavelet)
#        sample_background = st.background(sample, parameters=properties.background_options) #,BACK1_INCLUDE_COMPTON
#        sample -= sample_background
        sample[sample < properties.threshold] = 0  
    return xsample, sample
  


def calibrate_area(xsample, sample, xmin, xmax, calibration_properties,\
    filter_properties, search_properties):   
    """ 
    This function process given sample with xsample x-axis,
    recognize peaks automatically inside (xmin, xmax) area and fit them by line. 
    
    Calibration_properies are used to specify energies for calibration and
    some other parameters for choosing peaks from recognized set.
    Search_properties contains properties for searching peaks.
    Filter_properties contains properties to supress noise and smooth the spectrum.
    See CERN ROOT docs 'spectrum.pdf' for details.
    
    INPUT:  
        xsample - sample for x-axis (channel or energy)
        sample - sample fro y-axis (counts) 
        xmin, xmax - specify processing area for X-axis of the sample
          !note: len(sample) == len(xsample) !
        *properties:  
    
        Example of input parameter's classes:
    
        class record: pass
        calibration_properties = record()
        calibration_properties.visualize= False
        calibration_properties.weighted_average_sigma = 15 # параметр для уточнения середины
                # пика методом среднего взвешенного, должен соотв. примерно
                # полуширине пика, может быть None 
        calibration_properties.dlt = 32 # параметр для определения области соответствия
                # расчетного положения пиков (соотв. пропорции калибровочных энергий)
                # и реальных найденных пиков (метод spectrum_tools.search_peaks), если
                # реальные пики не находятся вблизи расчетных в области dlt, 
                # то они заменяются на расчетные, в противном случае выбирается 
                # пик наиболее близкий к расчетному. see calibration.calibrate_area
        calibration_properties.energies = [6040,6143,6264,6899.2,7137,7922,8699,9261]#[7137,7922,8699,9261]#[6040,6143,6264,8699,9261]# 
    
        search_properties = record() #for noisy spectrum
        search_properties.widths = np.arange(1, 5)
        search_properties.wavelet = 'ricker'
        search_properties.min_length = 1.
        search_properties.min_snr = 0.9
        search_properties.noise_perc = 0.3 
        
        filter_properties = record()
        filter_properties.window_smooth=7
        filter_properties.smooth_wavelet='blackman'
        filter_properties.background_options='BACK1_ORDER8,BACK1_INCLUDE_COMPTON' 
        filter_properties.threshold= 3    
    
    OUTPUT:
        xpeaks [1D np.npdarray float64], 
        solution [1D np.npdarray float64] (coefficients of linear fit of the spectr: a, b)
        
    """
    sample, xsample = np.array(sample), np.array(xsample)
    xsample0 = xsample #make copies to visualize it later
    sample0 = sample
    
    #initiatian of properties
    try:
        weighted_average_sigma = calibration_properties.weighted_average_sigma
        dlt = calibration_properties.dlt
        visualize = calibration_properties.visualize
        energies = calibration_properties.energies
    except:
        raise Exception('Wrong calibration_properties object')
        
    try:
        calibration_properties.title
    except:
        calibration_properties.title = None
    
    #filter data - smooth and delete a background
    if filter_properties:
        xsample,sample = filter_spectr(xmin, xmax, xsample, sample, filter_properties)#5,smooth_wavelet='hanning',background_options='BACK1_ORDER2')
    
    #find peaks
    try:
        xpeaks,ypeaks = st.search_peaks(xsample, sample, search_properties)#sigma=sigma,threshold=threshold) 
    except st.Search_peak_error as e:
        raise Calibration_Error(e)
    except Exception as e:
        if str(e) == "No peak was founded":
            raise Calibration_Error(e)
    xpeaks = np.array(xpeaks)
    
    #delete too close peaks, select the highest of close peaks and sort them
    indx = np.concatenate(([True], np.abs(np.diff(xpeaks)) > dlt)) 
    #print indx
    msv = []
    k = -1
    for i, j in enumerate(indx):
        if j > 0: 
            if k > -1:
                s = k + np.argmax(ypeaks[k:i]) if i - k > 1 else k
                msv.append(s)
            k = i
    if k == len(xpeaks) - 1:
        msv.append(k)
    else:
        msv.append(k + np.argmax(ypeaks[k:]))
    
    indx = np.array(msv)  
    xpeaks, ypeaks= xpeaks[indx], ypeaks[indx]
    
    #check if it's enough peaks detected
    if len(xpeaks) < 3:
        print('Error occured! Peaks founded:', xpeaks, ypeaks)
        show_spectrs(xsample0, sample0, xsample, xmin, xmax,\
            xpeaks, ypeaks, sample, energies, solution=None, title='Error report')
        raise Calibration_Error("Some peaks weren't indentificated in calibration procedure, you should change the parameters.")
        
    k = len(energies) * 2
    if len(xpeaks) > k:
        indx = ypeaks.argsort()
        ypeaks = ypeaks[indx][-k:]
        xpeaks = xpeaks[indx][-k:]
        
    ypeaks = ypeaks[xpeaks.argsort()] #sort lists of peaks
    xpeaks = sorted(xpeaks)
    xpeaks, ypeaks = np.array(xpeaks), np.array(ypeaks)
#    print 'afterproc',xpeaks

    #selecting valid peaks
    spectr_peak_dists = np.diff(np.array(energies, dtype=np.float64) ) / (energies[-1] - energies[0])
    spectr_length = (xpeaks[-1] - xpeaks[-2])/spectr_peak_dists[-1] 
    spectr_peak_dists1 = spectr_peak_dists * spectr_length # distances from right edge to points, proportion between them is the same as for energy calibration spectr
    spectr_peak_dists1 = xpeaks[-1] - spectr_peak_dists1[::-1].cumsum()
    x, y = [], []
    x.append(xpeaks[-1])
    y.append(ypeaks[-1])
        
    def find_closest(msv, m0, dlt): #find a point from msv array, which is the closest to m0 inside dlt diapason
        msv=abs(msv - m0)
        i = msv.argmin()
        if msv[i] < dlt:
            return i
        else:
            return False
                
    for i in range(len(spectr_peak_dists1)):
        l =spectr_peak_dists1[i]
#        print 'l calculated peak:',l
        peak_ind = find_closest(xpeaks, l, dlt)
#        print 'pass:',peak_ind
#        if ypeaks[peak_ind] < 0.25*y[-1]:
#            continue
        if not peak_ind or xpeaks[peak_ind] == x[-1]: 
            ind = (xsample > l).argmax()
            x.append(xsample[ind])
            y.append(sample[ind])
            continue
        x.append(xpeaks[peak_ind])  
        y.append(ypeaks[peak_ind]) 
        spectr_length += l - x[-1] 
        spectr_peak_dists1 = spectr_peak_dists * spectr_length
        spectr_peak_dists1 = xpeaks[-1] - spectr_peak_dists1[::-1].cumsum()
               
    x.reverse()
    y.reverse()
    xpeaks,ypeaks= np.array(x, dtype=np.float64),np.array(y, dtype=np.float64) 

    if len(xpeaks) < len(energies):
        print('Not enough peaks')
        print('Peaks founded: ', xpeaks, ypeaks)
        plt.show()
        raise Calibration_Error('Not enough valid peaks') 
         
    #correct xpeaks by calculating a weighted average of x using +-2*sigma window   
    if weighted_average_sigma:
        k = weighted_average_sigma
        #print 'start wa',k
        xpeaks1 = []
        for x in xpeaks:
            #print xsample, x in xsample	
            ind = (xsample >= x).argmax()#xsample.index(x)  
            hist_sum = sample[ind - k: ind + k + 1]
            ind1 = hist_sum > 0
            hist_sum = hist_sum.sum()
            sample1 = sample[ind - k: ind + k + 1]
            xsample1 = xsample[ind - k: ind + k + 1]
            sample1 = sample1[ind1]
            xsample1 = xsample1[ind1]
            xpeaks1.append((sample1 * xsample1).sum() / hist_sum )
        xpeaks = np.array(xpeaks1) 
        
    #fitting by line
    def residuals(coef, y, x):
        return y - coef[0] * np.ones(len(x)) - coef[1] * x
    p0 = (0,2) #init coefficients    
    
    xpeaks,ypeaks= np.array(xpeaks, dtype=np.float64),np.array(ypeaks, dtype=np.float64)
    solution = leastsq(residuals, p0, args = (energies, xpeaks))[0]
    
    #output
    if visualize:
        show_spectrs(xsample0, sample0, xsample, xmin, xmax,\
            xpeaks, ypeaks, sample, energies, \
            solution=solution, title=calibration_properties.title)
    
    return xpeaks, solution    

def make_report(xpeaks, solution, ind, filename=None, energies=Alpha_energies):
    #energies = np.array([6040,6143,6264,6899,7137,7922,8699,9265]) 
    report = '\n%d    A = %2.5f ; B = %2.1f\n'%(ind,solution[1], solution[0])
    report += '%5s  %5s      %4s   %5s \n' % ('Eexp', 'Ecal', 'Differ', 'Channel')
    S = 0
    
    for i, en in enumerate(energies):
        #print type(solution[0]),type(xpeaks),xpeaks
        Ecalc = solution[0] + solution[1] * xpeaks[i]
        report += '%4.1f  %4.1f    %-5.1f    %-4.1f \n' % (en, Ecalc, Ecalc - en, xpeaks[i]) 
        S += (Ecalc-en) ** 2
    
    if filename:
        f = open(filename, 'a')
        f.write(report)
        f.close()
    report += 'S/n = %3.1f \n' % (S ** 0.5 / len(energies))
    return report, (S ** 0.5 / len(energies))     

def calibrate_spectr(start, stop, xmin, xmax, hist, \
        calibration_properties, filter_properties, search_properties,\
        output_filename=None):
    '''
    Automatically calibrates strips for spect hist.
    Apply calibrate_area for each strip inside region [start, stop] of 
    hist 2D spectrum [hist_num 0..129, channel 0..4096].
    See calibrate_area for details.
    '''
    
    good_results = 0  
    bad_results = []
    xpeaks_list = []
    coef_list = []
    
    for i in range(start, stop + 1):
        print('strip: ',i)
        try:
            xpeaks, coef = calibrate_area(hist.index, hist[i], xmin, xmax, \
                calibration_properties, filter_properties, search_properties)
            report, control = make_report(xpeaks, coef, i,\
                energies=calibration_properties.energies, filename = output_filename)
            print(report)
            
            if control <= 10:
                good_results += 1
                xpeaks_list.append(xpeaks)
                coef_list.append(coef)
            else:
                bad_results.append(i)
        except Calibration_Error as e:
            bad_results.append(i)
            print('strip %1d was not calibrated correctly: %s' % (i, e))
            print('Error occured', e)
        except KeyError as e:
            print('Error occured: in hist array \n', e)
            bad_results.append(i)
            
    print('Good results:', good_results, '/', np.abs(stop - start + 1))
    print('Bad results:', bad_results)       
    return xpeaks_list, coef_list, good_results


def get_calibration_properties():
    #set calibration parameters
    class record: pass
    calibration_properties = record()
    calibration_properties.visualize= False
    calibration_properties.weighted_average_sigma = False
    calibration_properties.dlt = 20 # параметр для определения области соответствия расчетного положения пиков (соотв. пропорции калибровочных энергий) и реальных найденных пиков (метод spectrum_tools.search_peaks), если реальные пики не находятся вблизи расчетных 
                                    # в области dlt, то они заменяются на расчетные, в противном случае выбирается пик наиболее близкий к расчетному. see calibration.calibrate_area
    calibration_properties.energies = [6143, 6264, 6899.2, 7137, 7922, 8699, 9261]#[7137,7922,8699,9261]#[6040,6143,6264,8699,9261]# 

    search_properties = record() #for noisy spectrum
    search_properties.widths = np.arange(1, 5)
    search_properties.wavelet = 'ricker'
    search_properties.min_length = 1.
    search_properties.min_snr = 0.0
    search_properties.noise_perc = 0.1 
    
    filter_properties = record()
    filter_properties.window_smooth = 5
    filter_properties.smooth_wavelet = 'blackman'
    filter_properties.background_options = 'BACK1_ORDER8'#,BACK1_INCLUDE_COMPTON' 
    filter_properties.threshold = 15
    return calibration_properties, search_properties, filter_properties
 
def visualize_side_spectr(xsample,spectrs):
    sample1 = st.smooth(spectrs, 7, 'hanning')
    sample1[sample1 < 0] = 0
    sample = sample1 #- st.background(sample1, niter=3, parameters='BACK1_ORDER8,BACK1_INCLUDE_COMPTON')
    sample[sample < 0] = 0
    fig,ax = plt.subplots(3, 1, sharex=True, sharey=True)		
    ax[0].plot(xsample, sample, linestyle='steps')
    ax[1].plot(xsample, sample1, linestyle='steps')
    ax[2].plot(xsample, spectrs, linestyle='steps')
    plt.show()
    #return sample
 
class Bad_result(Exception): pass
         
def calibrate_side_calibration_spectrs(frame, side_coefs, front_coefs, \
        calibration_properties_energy_scale, \
        calibration_properties_channel_scale, 
        filter_properties_energy_scale, filter_properties_channel_scale,\
        search_properties, ind, step=10):
    
    # add coefficients to data structure
    coefs = record()
    coefs.side_coefs = side_coefs
    coefs.front_coefs = front_coefs
    
    # build combined time-correlated front-side events spectrum 
    # in energy scale
    en_spectrs, xsample, _ = sp.get_combined_spectrum(frame,\
        event_type='front-side', scale='alpha', tof=False, energy_scale=True, \
        coefs_folder='.', coefs=coefs, bins=None)
    en_spectrs = en_spectrs[ind - 1] #according to en_spectrs has shape [0 .. 47][..]
    xsample = xsample[1:]
    
    # apply automatic peak recognition and calibrate founded peaks by line
    # in energy-energy plot
    xpeaks, coef = calibrate_area(xsample, en_spectrs, 4000, 11500,\
        calibration_properties_energy_scale, \
        filter_properties_energy_scale, search_properties)
   
    # apply correction to initial coefficients
    a = side_coefs[0, ind] * coef[1] #A coef[1]; clbr_func = (A*x + B)
    b = side_coefs[1, ind] * coef[1] + coef[0] #B coef[0] 
    
    # calculate square summary of deviation for each founded peak 
    # and print a report
    energies = calibration_properties_energy_scale.energies
    control_S2 = sum((xpeaks - np.array(energies)) ** 2) ** 0.5 / len(xpeaks)
    print('Check energy deviations:')
                               
    print('Control energies:', energies) 
    print('Energies (calculated):', ''.join(['{:4.1f} '.format(i) for i in xpeaks]))
    print('S: ', control_S2)
    print()
    if control_S2 > 350:
        raise Bad_result('wrong coefficients or bad calibration_properties')
        
    #calculate calibration boundaries of spectrums
    xmin = int((4500 - b) / a) # a == side_coefs[0][i] 
    xmax = int((9600 - b) / a)  # b == side_coefs[1][i] 
    
    # build combined time-correlated front-side events spectrum 
    # in channel scale
    coefs.side_coefs = side_coefs
    ch_spectrs, xsample, _ = sp.get_combined_spectrum(frame,\
        event_type='front-side', scale='alpha', tof=False, energy_scale=False, \
        coefs_folder='.', coefs=coefs, bins=None)
        
    # apply automatic peak recognition and calibrate founded peaks by line
    # in channel-energy plot
    ch_spectrs = ch_spectrs[ind - 1] #according to en_spectrs has shape [0 .. 47][..]
    xsample = xsample[1:]
    xpeaks, coef = calibrate_area(xsample, ch_spectrs, xmin, xmax,\
        calibration_properties_channel_scale, \
        filter_properties_channel_scale, search_properties) 
        
    return coef, xpeaks, control_S2
 
   
#CONSTRUCT AND SHOW FOCAL-SIDE ENERGY SPECTRS 
    
def make_focal_side_calibration(frame, side_coefs, front_coefs,\
         calibration_properties_energy_scale,\
         calibration_properties_channel_scale, search_properties,\
         filter_properties_energy_scale, filter_properties_channel_scale, \
         ind=1, iterations=3):   
    """
    Function for making autoumatic calibration of focal-side spectrums strip by strip.
    Output: coefs (A,B), list of peaks (which were used in calibration)
    """
    
    control, coefs, peaks = [], [], []    
    coefs.append((side_coefs[0, ind], side_coefs[1, ind]))
    peaks.append(False)
    
    # choose only focal-side time correlated events (for optimization)
    index_paired = qm.find_sync_pairs(frame, 0, 1, False)
    index_paired = np.c_[index_paired[:, 0], index_paired[:, 1]].flatten()
    frame = frame[index_paired]
    
    # main processing cycle
    for i in range(iterations):
        try:
            
            coef, xpeaks, control_S2 = calibrate_side_calibration_spectrs(frame,\
                side_coefs, front_coefs,\
                calibration_properties_energy_scale,\
                calibration_properties_channel_scale,\
                filter_properties_energy_scale, filter_properties_channel_scale, \
                search_properties, ind, step=10)
            
            control.append(control_S2)
            coefs.append(coef)
            peaks.append(xpeaks)
            side_coefs[0, ind] = coef[1] #A coef[1]
            side_coefs[1, ind] = coef[0] #B coef[0] 
                        
        except (Calibration_Error, Bad_result) as e:
            print('CALIBRATION ERROR OCCURED!')
            while len(coefs) > len(control):
                coefs.pop()
        
    control.append(10 ** 3) # last iteration is out of control!
    if len(coefs) == 0:
        raise Calibration_Error('Bad initial coefficients')
        
    ind1 = np.argmin(np.array(control))   
    print('Least S: ', control[ind1])
    return (coefs[ind1][1], coefs[ind1][0]), peaks[ind1]

       
#if __name__ == '__main__':
    
#    import os
#    os.chdir(r'./exp_data1')
#    #read data 
#    filenames = 'tsn.35 - tsn.61'
#    #hist, sum1 = get_front_spectrs(filenames, strip_convert=True, threshold=0.04)
#    hist, sum1 = get_back_spectrs(filenames, strip_convert=True)
#    xmin, xmax = 800, 1600
#
#    #set calibration parameters
#    class record: pass
#    calibration_properties = record()
#    calibration_properties.visualize= True
#    calibration_properties.weighted_average_sigma = 5
#    calibration_properties.dlt = 12 # параметр для определения области соответствия расчетного положения пиков (соотв. пропорции калибровочных энергий) и реальных найденных пиков (метод spectrum_tools.search_peaks), если реальные пики не находятся вблизи расчетных 
#                                    # в области dlt, то они заменяются на расчетные, в противном случае выбирается пик наиболее близкий к расчетному. see calibration.calibrate_area
#    calibration_properties.energies = [6040, 6143, 6264, 6899.2, 7137, 7922, 8699, 9261]#[7137,7922,8699,9261]#[6040,6143,6264,8699,9261]# 
#
#    search_properties = record() #for noisy spectrum
#    search_properties.widths = np.arange(1,5)
#    search_properties.wavelet = 'ricker'
#    search_properties.min_length = 1.
#    search_properties.min_snr = 0.9
#    search_properties.noise_perc = 0.3 
#    
#    filter_properties = record()
#    filter_properties.window_smooth = 7
#    filter_properties.smooth_wavelet = 'blackman'
#    filter_properties.background_options = 'BACK1_ORDER8,BACK1_INCLUDE_COMPTON' 
#    filter_properties.threshold = 3
#    
##    #choose strips and calibrate them
#    start,stop = 65, 128
##    #output_filename = '/home/eastwood/codes/Python_Idle/data_processing/Sep2014_calibrations/alpha_back_clbr.txt'
#    output_filename = None
#    xpeaks, coefs = calibrate_spectr(start, stop, xmin, xmax, hist,\
#       calibration_properties, filter_properties, search_properties, output_filename=output_filename)