# -*- coding: utf-8 -*-
"""
Created on Tue Aug 16 12:35:40 2016

@author: eastwood

FUNCTIONS:
1) Read data to events table (for separate detector/strip distributions)

    read_file(filename, offset=0) 
        read raw binary file and 
        return <np.ndarray> event_table (see Description)
        
    main_reading_loop(long count_blocks, FILE* cfile, \
            np.ndarray[EVENT, cast=True] event_table)
            
2) Read data to data table(search for coord-time correlations)

    read_data_table(filename, offset=0, coefs_folder=None)
    
    main_reading_dt_loop(long count_blocks, FILE* cfile, \
    np.ndarray[DATA_EVENT, cast=True] data_table, \
    double[:,:,:,::1] coefs)
    
"""
import os
import struct
import numpy as np

from libc.stdio cimport FILE, fopen, fwrite, fscanf, fclose,\
     fprintf, fseek, ftell, SEEK_SET, SEEK_END, rewind, fread
from libc.stdlib cimport malloc, free                                                           

from cython cimport boundscheck,wraparound
cimport numpy as np
cimport cython
from libc.math cimport fabs

from data_types cimport c_bool,c_word,EVENT, DATA_EVENT
from array_types import event_type, final_event_type

import data_processing_GNS2021.tools.spectrum as sp
#### DATA description

cdef packed struct BLOCK:
    np.int64_t Time
    np.uint16_t amplitude
    np.int8_t channel
    np.int8_t slot
    np.int8_t beam
    np.int8_t file_version
    np.int32_t pos

#event_type = np.dtype([('event_type', int), ('strip', int), ('channel', int),\
#    ('scale', int), ('time_macro', np.int64), ('time_micro', np.int64),\
#    ('beam_mark', np.bool), ('tof', np.bool), ('tofD1', np.uint16),\
#    ('tofD2', np.uint16), ('synchronization_time', np.uint16)])
#  
#final_event_type = np.dtype([('event_type',int), ('energy_front',float), \
#                    ('energy_back', float), ('cell_x', int), ('cell_y', int),\
#                    ('time_macro', np.int64), ('time_micro', np.int64), \
#                    ('beam_mark', np.bool), ('tof', np.bool), \
#                    ('tofD1', np.uint16), ('tofD2', np.uint16)])  
        
########################
#### READ TO EVENT FRAME
'''
Convert this:
cdef packed struct SHEInfo:
    LARGE_INTEGER   m_LastEventTime; # time from crate inner counter, 1 count = 10 ns
    WORD            m_LastAmplitude; # amplitude 0 .. 65536
    char            m_LastChannel; # channel on each digitazer, 0 .. 15
    char            m_SlotOrdinalNumber; # slot number (see Leo's crate scheme)
    char            m_BeamOff; # beam mark, on - signal to shut down the beam

    char            m_FileVersion;  // number of the file with raw data from DAQ programm
    DWORD           m_Pos;          // number of according signal id in the file

    
To this:
    event_type # 1 - focal, 2 - back, 3 - side, 4 - veto
    strip # 0 .. 128; front:1-48 back: 1-128, side: 1-6
    channel # front:4096 back:8192 side: 8192
    scale # 0 - alpha, 1 - fission
    time_macro # sec int64
    time_micro # microseconds int64
    beam_mark # bool
    tof # bool
    tofD1 # 1st tof wire counter
    tofD2 # 2nd tof wire counter
    synchronization_time #  0..65536 
'''   
           
#####
cdef BLOCK* read_block(BLOCK* block, FILE* file_) :  
    fread(<void*>block, sizeof(BLOCK), 1, file_)
    return block   
     
cdef copy_addition_data(BLOCK *block, EVENT *event, 
                        c_bool tof_mark, c_word tofD1, c_word tofD2,
                        long *index, np.int64_t time):               
    event.channel = block.amplitude
    event.beam_mark = block.beam
    event.tof = tof_mark
    event.tofD1 = tofD1
    event.tofD2 = tofD2
    event.scale = 0
    event.synchronization_time =  0
    event.time_micro = time // 100 # convert 10ns counts to mks
    event.time_macro = event.time_micro//<np.int64_t>1000.


cdef c_bool is_front(BLOCK *block):
    return (block.slot < 3)#&is_time(block) 
    
cdef c_bool is_back(BLOCK *block):
    return (block.slot > 2) & (block.slot < 11)

cdef c_bool is_side(BLOCK *block)  :
    return (block.slot > 10) & (block.slot < 14)    
 
cdef c_bool is_tof(BLOCK *block):
    return (block.slot == 15) & (block.channel < 4)
       
cdef c_bool is_veto(BLOCK *block):
    return (block.slot == 14)
 
cdef np.int_t get_front_strip(BLOCK *block):
    return block.slot * 16 + block.channel + 1
      
cdef EVENT add_front(BLOCK *block, np.int64_t time,
                           long *index, c_bool tof_mark,
                           c_word tofD1, c_word tofD2):
    cdef EVENT event 
    event.event_type = 1
    event.strip = get_front_strip(block)
    copy_addition_data(block, &event, tof_mark, tofD1, tofD2, index, time)
    return event
    
cdef np.int_t get_back_strip(BLOCK *block):
    return (block.slot - 3) * 16 + block.channel + 1
    
cdef EVENT add_back(BLOCK *block, np.int64_t time,
                           long *index, c_bool tof_mark,
                           c_word tofD1, c_word tofD2):
    cdef EVENT event 
    event.event_type = 2
    event.strip = get_back_strip(block)
    copy_addition_data(block, &event, tof_mark, tofD1, tofD2, index, time)
    return event
    
cdef np.int_t get_side_strip(BLOCK *block):
    return (block.slot - 11) * 16 + block.channel + 1
    
cdef EVENT add_side(BLOCK *block, np.int64_t time,
                           long *index, c_bool tof_mark,
                           c_word tofD1, c_word tofD2):
    cdef EVENT event 
    event.event_type = 3
    event.strip = get_side_strip(block)
    copy_addition_data(block, &event, tof_mark, tofD1, tofD2, index, time)
    return event
 
cdef np.int_t get_veto_strip(BLOCK *block):
    return block.channel + 1
    
cdef EVENT add_veto(BLOCK *block, np.int64_t time,
                           long *index, c_bool tof_mark, 
                           c_word tofD1, c_word tofD2):
    cdef EVENT event 
    event.event_type = 4
    event.strip = get_veto_strip(block)
    copy_addition_data(block, &event, tof_mark, tofD1, tofD2, index, time)
    return event   


cdef long main_reading_loop(long count_blocks, FILE* cfile, \
            np.ndarray[EVENT, cast=True] event_table):
    cdef:
        long i=0, event_num=0, event_num_start=0
        np.int64_t time, time_start = 0, time_zero = 0
        np.int64_t time_window = 300 # to form a block with dE1-tofD2-Front-Back ...
        c_bool tof_mark = 0
        c_word tofD1, tofD2
        
        BLOCK *block = <BLOCK*>malloc(sizeof(BLOCK))
        EVENT *event = <EVENT*>malloc(sizeof(EVENT)) 
        EVENT check_event
    
    with boundscheck(False), wraparound(False):
        for i in range(count_blocks):
            read_block(block, cfile) #read data to block
            
            if i == 0:
                time_zero = block.Time
                
            time = block.Time# - time_zero
                
            if time < 0:
                continue
            
            if time > time_start + time_window:
                tof_mark = 0
                tofD1 = 0
                tofD2 = 0
                time_start = time # start recording serie of events like tof-front-back-...
                event_num_start = event_num
                
            if is_tof(block):
                tof_mark = 1
                if block.channel == 2:
                    tofD1 = block.amplitude
                elif block.channel == 3:
                    tofD2 = block.amplitude
                    
                # correct if tof comes after focal detector signals
                for j in range(event_num - 1, event_num_start - 1, -1):
                    event_table[j].tof = 1
                    event_table[j].tofD1 = tofD1
                    event_table[j].tofD2 = tofD2
                
            if is_front(block):
                event_table[event_num] = add_front(block, time, &event_num, \
                    tof_mark, tofD1, tofD2)
                
                # correct if a front signals comes after the back signal
                if (time > time_start) & \
                  (event_table[event_num - 1].event_type == 2):
                    event_table[event_num], event_table[event_num - 1] = \
                      event_table[event_num - 1], event_table[event_num] 
                      
                event_num += 1
                    
            elif is_back(block):
                event_table[event_num] = add_back(block, time, &event_num, \
                    tof_mark, tofD1, tofD2)
                event_num += 1
                     
            elif is_side(block):
                event_table[event_num] = add_side(block, time, &event_num, \
                    tof_mark, tofD1, tofD2)
                event_num += 1
                
            elif is_veto(block):               
                event_table[event_num] = add_veto(block, time, &event_num, \
                    tof_mark, tofD1, tofD2) 
                event_num += 1
                
             
    free(block)
    free(event)
    return event_num


def read_file(filename, offset=0):    
    cdef:
        FILE* cfile
        long count_blocks      
     
    size = os.path.getsize(filename)
    count_blocks = (size - offset) // sizeof(BLOCK)
    cfile = fopen(filename, 'rb')
    fseek(cfile, offset, SEEK_SET)
    event_table = np.empty(count_blocks, dtype=event_type)
    
    #main reading loop
    event_num = main_reading_loop(count_blocks, cfile, event_table)
                  
    fclose(cfile)    
    return event_table[:event_num], size
    
########################
#### READ TO DATA_EVENT
"""
Output format (data_table):
    
cdef packed struct DATA_EVENT:
    np.int_t event_type # 1 - focal-back, 3 - side
    double energy
    np.int_t cell_x # 0 .. 48
    np.int_t cell_y # 0 .. 128
    np.int64_t time_macro # seconds 
    np.int64_t time_micro # microseconds 
    c_bool beam_mark # bool
    c_bool tof # bool
"""
 
# hint coefs array structure: [event_type 1-front, 2-back, 3-side][scale 0-alpha, 1-fission][coef 0-a, 1-b][strip 0..129]
cdef double calc_front_alpha_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return block.amplitude * coefs[1, 0, 0, strip] + coefs[1, 0, 1, strip]
 
cdef double calc_front_fission_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return block.amplitude * coefs[1, 1, 0, strip] + coefs[1, 1, 1, strip]

cdef double calc_back_alpha_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return block.amplitude * coefs[2, 0, 0, strip] + coefs[1, 0, 1, strip]
 
cdef double calc_back_fission_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return block.amplitude * coefs[2, 1, 0, strip] + coefs[1, 1, 1, strip]

cdef double calc_side_alpha_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return block.amplitude * coefs[3, 0, 0, strip] + coefs[3, 0, 1, strip]
    
cdef double calc_side_fission_energy(BLOCK* block, c_word strip, double[:,:,:,::1] coefs):
    return block.amplitude * coefs[3, 0, 0, strip] + coefs[3, 0, 1, strip]
    
cdef c_bool alpha_energy_check(double energy):
    return (energy >=0 ) & (energy <= 30000)
    
cdef c_bool fission_energy_check(double energy):
    return (energy >= 30000)
      
cpdef np.ndarray[DATA_EVENT, cast=True] resize_data_table\
        (np.ndarray[DATA_EVENT, cast=True] data_table):
    size = len(data_table)
    new_size = int(size * 1.4)
    new_data_table = np.empty(new_size, dtype=final_event_type)
    new_data_table[:size] = data_table[:size]
    data_table = np.empty(0, dtype=final_event_type)
    return new_data_table

cdef DATA_EVENT init_data_event():
    cdef DATA_EVENT d_event
    d_event.energy_front = 0
    d_event.energy_back = 0
    d_event.event_type = 0
    d_event.time_micro = 0
    d_event.time_macro = 0
    d_event.beam_mark = 0
    d_event.tof = 0
    d_event.cell_x = 0
    d_event.cell_y = 0    
    return d_event
    
cdef long main_reading_dt_loop(long count_blocks, FILE* cfile, \
            np.ndarray[DATA_EVENT, cast=True] data_table, \
            double[:,:,:,::1] coefs):
  
    cdef:
        long i=0, event_num=0
        np.int64_t time, time_zero = 0
        np.int64_t time_window = 300 # to form a block with tofD1-tofD2-Front-Back ...
        np.int64_t time_start = -time_window - 1 # init for start counting
        c_bool tof_mark = 0
        c_bool double_back = 0
        c_bool beam_mark = 0
        c_bool veto_mark = 0
        
        c_word tofD1, tofD2

        BLOCK *block = <BLOCK*>malloc(sizeof(BLOCK))
        DATA_EVENT d_event = init_data_event()
    
    with boundscheck(False),wraparound(False):
        for i in range(count_blocks):
            read_block(block, cfile) #read data to block
            
            if i == 0:
                time_zero = block.Time
                
            time = block.Time# - time_zero              
            
            # read data event (set of signals corresponding to a particle)
            # inside block of 3mks. Usually contains combinations of 
            # Tof, front, back, side, veto, beam signals
            if (time > time_start + time_window):
#                print
#                print "CHECK: ", i
#                print "d_event: ", d_event
#                print
                if (veto_mark == 0) &\
                  ((d_event.energy_front > 0) | (d_event.energy_back > 0)) :
#                    print "PASS!"
                    d_event.tof = tof_mark
                    d_event.tofD1 = tofD1
                    d_event.tofD2 = tofD2
                    d_event.beam_mark = beam_mark
                    d_event.time_micro = time_start // 100
                    d_event.time_macro = d_event.time_micro//1000
                    data_table[event_num] = d_event
                    event_num += 1
                else: 
                    veto_mark = 0
                    
                time_start = time
                tof_mark = 0
                tofD1 = 0
                tofD2 = 0
                beam_mark = 0
                double_back = 0
                veto_mark = 0
                d_event = init_data_event()
                
                
                
                
            # check tof-of-flight mark (tofD1 or tofD2)
            if is_tof(block):
                tof_mark = 1 
                if block.channel == 2:
                    tofD1 = block.amplitude
                elif block.channel == 3:
                    tofD2 = block.amplitude
            
            if block.beam:
                beam_mark = 1
            
            if is_veto(block):
                veto_mark = 1
                continue # exclude veto events
                       
            
            if is_front(block):
                d_event.event_type = 1
                d_event.cell_x = get_front_strip(block)
                fission_energy = calc_front_fission_energy(block, d_event.cell_x, coefs)
                energy = calc_front_alpha_energy(block, d_event.cell_x, coefs)  
                if fission_energy_check(fission_energy):
                    d_event.energy_front = fission_energy  
                else:
                    d_event.energy_front = energy
#                print "CHECK front: ", i
#                print "d_event: ", d_event
                 
            if is_back(block):
                d_event.event_type = 1
                if double_back == 1:
                    double_back = 2
                    fission_energy = calc_back_fission_energy(block, d_event.cell_y, coefs)
                    energy = calc_back_alpha_energy(block, d_event.cell_y, coefs)
                    if fission_energy_check(fission_energy):
                        d_event.energy_back += fission_energy  
                    else:
                        d_event.energy_back += energy   
                elif (double_back == 0):
                    double_back = 1
                    d_event.cell_y = get_back_strip(block)
                    fission_energy = calc_back_fission_energy(block, d_event.cell_y, coefs)
                    energy = calc_back_alpha_energy(block, d_event.cell_y, coefs)
                    if fission_energy_check(fission_energy):
                        d_event.energy_back = fission_energy  
                    else:
                        d_event.energy_back = energy  
                elif (double_back == 2): # exclude all back splitted signals except first two
                    pass
#                print "CHECK back: ", i
#                print "d_event: ", d_event
                    
            if is_side(block):
                if d_event.event_type == 0:
                    d_event.event_type = 3
                    d_event.cell_x = get_side_strip(block)
                    d_event.cell_y = 0
                    
                energy = calc_side_alpha_energy(block, d_event.cell_x, coefs)
                fission_energy = calc_side_fission_energy(block, d_event.cell_x, coefs)
                energy = fission_energy if fission_energy_check(fission_energy) else energy
                if d_event.energy_front == 0:
                    d_event.energy_front = energy
                else:
                    d_event.energy_front += energy
                    
    free(block)
    return event_num
    
def read_data_table(filename, offset=0, coefs_folder=None):    
    cdef:
        FILE* cfile
        long count_blocks      
     
    size = os.path.getsize(filename)
    count_blocks = (size - offset) // sizeof(BLOCK)
    cfile = fopen(filename, 'rb')
    fseek(cfile, offset, SEEK_SET)
    data_table = np.empty(6 * count_blocks, dtype=final_event_type)
    
    #prepare calibration coefficients
    coefs = sp.get_all_calibration_coefficients(coefs_folder=coefs_folder)
    
    #main reading loop
    event_num = main_reading_dt_loop(count_blocks, cfile, data_table, coefs)
                  
    fclose(cfile)    
    data_table = data_table[:event_num] 

    # put non-zero energy (front or back) to zero-like empty energy cells
    data_table['energy_front'] = np.where(data_table['energy_front'] > 0,
          data_table['energy_front'], data_table['energy_back'])
    
    return data_table, size # according to first event is empty 
    
    
