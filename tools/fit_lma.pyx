# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 10:30:42 2015

@author: eastwood
ФУНКЦИЯ для фитирования пика функций Гаусса и подложкой в виде прямой линии по заданным массивам точек X,Y.

Фитирующая функция:
f(x,P) = A*exp[ - 0.5*sqr((x - x0)/sigm)) ] + B*(x - O) + C
  // X: 0 - x0, 1 - sigm, 2 - A, 3 - B,  4 - C
  // O - точка соответсвующая середине пика

Примечание: очевидно данная функция не является функцией распределения Гаусса в ее каноническом
виде. Но такой вид функции позволяет проводить успешное фитирование в условиях наличия помех
и шумов в гистограмме. В частных случаях может потребоваться провести дополнительный анализ
на соответствие Гауссовому пику.
*********************************************
ИНТЕРФЕЙС:

Фитирование методом Левенберга-Марквардта:
  fit_LMA(X,Y,P: array of double)
  return P: array of double
  
Фитирование робастным методом Левенберга-Марквардта:
  fit_LMA_robust(X,Y,P: array of double)
  return P: array of double
  
Список параметров P:
// P: 0 - x0 (центр пика); 1 - sgm (дисперсия); 2 - A; 3 - B; 4 - C; 5 - Chi^2 (хи-квадрат)
ВАЖНО: длина векторов параметров length(P) = 6. (!!!) Для успешного фитирования необходимо
хотя бы 6 точек. В случае, если пик задан меньшим количеством точек - следует оценивать его 
параметры (математическое ожидание и дисперсия) известными статистическими функциями.

Начальное приближение параметров для фитирования можно вычислить с помощью функции:
Init_P(X,Y: array of double);// назначение в качестве параметров mddl, rms, integral по выборке
return P
*********************************************
ПРИМЕР РАБОТЫ:
    
    P = fit_LMA(X,Y,init_p(X,Y) )
    
"""
from math import exp, pi
from numpy import dot, zeros,asarray,where
from numpy import exp as np_exp
from numpy.linalg import solve
from numpy.linalg import pinv



    
cpdef double func(double[:] P,double x):
    return P[2]*exp(- ((x-P[0])/P[1])**2/2) + P[3]*(x - P[5]) + P[4]

        
def fit_func(P,x):
    """Use to apply fitting fucntion with parameters set P on np.ndarray x.
    function: P[2]*np_exp(- ((x-P[0])/P[1])**2/2) + P[3]*(x - P[5]) + P[4]
    return np.ndarray 
    """
    a = P[2]*np_exp(- ((x-P[0])/P[1])**2/2) + P[3]*(x - P[5]) + P[4]
    return where(a>0,a,zeros(a.size))
    
    
#def func(R,x):
#    a = R[2]*exp(- ((x-R[0])/R[1])**2/2) + R[3]*(x - R[5]) + R[4]
#    return a if a>0 else 0    
    
    
cdef double[:,::1] jacobian(double[:]A, double[:]X):
    cdef:
        int i
    #  // f(x,X) = A*exp[ - 0.5*sqr((x - x0)/sigm)) ] + B*(x - O) + C
    #  // X: 0 - x0, 1 - sigm, 2 - A, 3 - B, 4 - C
    #  //    5 - O not variated
    Jcb = zeros( (5,len(A)) )
    for i in range(len(A)):
        Jcb[2,i] = exp( - 0.5*((A[i]-X[0])/X[1])**2 )    #  dF/dA
        Jcb[0,i] = X[2]*Jcb[2,i]*(A[i]-X[0])/X[1]**2#{ dF/dx0 }
        Jcb[1,i] = Jcb[2,i]*X[2]*( (A[i]-X[0])/X[1] )**2/X[1] # dF/d sigm
        Jcb[3,i]= A[i] - X[5]
        Jcb[4,i] = 1
        #Jcb[5,i] = -X[3]
    return Jcb
    
    
cdef double[:,::1] jacobian_robust(double[:]A, double[:] X,double[:] weights):
    cdef:
        int i
    #  // f(x,X) = A*exp[ - 0.5*sqr((x - x0)/sigm)) ] + B*(x - O) + C
    #  // X: 0 - x0, 1 - sigm, 2 - A, 3 - B, 4 - C
    #  //    5 - O not variated
    Jcb = zeros( (5,len(A)) )
    for i in range(len(A)):
        Jcb[2,i] = exp( - 0.5*((A[i]-X[0])/X[1])**2 )*weights[i]   #  dF/dA
        Jcb[0,i] = X[2]*Jcb[2,i]*(A[i]-X[0])/X[1]**2*weights[i]#{ dF/dx0 }
        Jcb[1,i] = Jcb[2,i]*X[2]*( (A[i]-X[0])/X[1] )**2/X[1]*weights[i] # dF/d sigm
        Jcb[3,i]= (A[i] - X[5])*weights[i]
        Jcb[4,i] = weights[i]
        #Jcb[5,i] = -X[3]*weights[i]
    return Jcb


cpdef double[:] init_p(double[:] x,double[:] y) :
#   f(x,P) = A*exp[ - 0.5*sqr((x - x0)/sigm)) ] + B*(x - O) + C
#   P: 0 - x0, 1 - sigm, 2 - A, 3 - B,  4 - C
#   O - точка соответсвующая середине пика
    
    cdef:
        int i,j,m,i_max
        double sum_=0,mddl=0,rms=0,t=0,O=0
    m = len(x)
    
    for i in range(m):
        sum_ += y[i]
        mddl += y[i]*x[i]
        t += y[i]*x[i]**2
    mddl /= sum_
    rms = (t/sum_ - mddl**2)**0.5
    
    P = zeros(6)
    P[0] = mddl   
    P[1] = rms
    P[2] = max(y)
    P[5] = (x[0]+x[m-1])/2 + (x[1]-x[0])/100
    if y[0]<y[m-1]:
        P[4] = y[0]
        m = m-1
        t = x[0]
    else:
        P[4] = y[m-1]
        m = 0
        t = x[m-1]
    P[3] = ( y[m] - P[4] )/(x[m] - t+0.1)
    return P
    

cpdef double[:] fit_lma(double[:] X, double[:] Y,double accuracy = 0.001,bint print_report=True) except *:
    assert len(X) == len(Y)
    cdef:
        int i,k,control_count,len_P
        double[:,::1] Jcb,Jt,H,Inv,control_matrix
        double O,lamb=0.01,control=1,control_sum=0,sumf_=0,sumf__pre=0
        double[:] P,P_new,N,dlt_N
    
    P = init_p(X,Y)
    P_new = zeros(len(P))
    P_new[5] = P[5] # O не варьируется
    len_P = 5 # количество варьируемых параметров 
    
    k = len(X)
    N = zeros(k) # N - вектор невязки y(i) - f( x(i) )
    
    for i in range(k):
        N[i] =func(P,X[i]) - Y[i] #столбец 8 x 1 вектор невязки
        sumf_ = sumf_ + N[i]**2
    control_sum = sumf_
    try:
        while control > accuracy:
            if control_count >= 5:
                break
            
            # I вычисление градиентной поправки к вектору параметров
            Jcb = jacobian(X,P)
            H = dot(Jcb,Jcb.T)
            for i in range(len_P):
                H[i,i] += lamb #демфирование матрицы
            Inv = pinv(H,rcond=1e-23*accuracy)
            dlt_N = dot(Inv, dot(Jcb,N))
            for i in range(len_P):
                P_new[i] = P[i] - dlt_N[i]
            
            # II оценивание дальнейшей стратегии фитирования
            sumf_pre = sumf_
            sumf_ = 0
            
            #вычисление вектора невязки и нормы по новым параметрам
            for i in range(k):
                N[i] = func(P_new,X[i]) - Y[i]
                sumf_ += N[i]**2
                
            #анализ изменения нормы невязки
            if sumf_ > sumf_pre:
                lamb /= 10
                control_count += 1
                continue
            else:
                for i in range(len_P+1):
                    P[i] = P_new[i]
                control_count=0
                lamb = 10*lamb if lamb >= 10 else lamb
                
            control = (sumf_pre - sumf_)/control_sum
    except:
        print 'Bad fit, lamb = ',lamb 
    P[1] = abs(P[1])

    if print_report:
        control = 0
        print '\nReport:'
        print '%s    %s    %s' %('F(x_i)','Yi','Dlt')
        for i in range(k):
            print '%f    %f    %f' %(func(P,X[i]),Y[i],func(P,X[i]) - Y[i])
            control += abs(func(P,X[i]) - Y[i])
        control /= k 
        print 'Average dlt: %f' %(control)
    return asarray(P)
    

cdef double[:] weight_func(double[:] X,double[:] Y,double[:] P,int k): 
    cdef:
        int i
        double sum_weights=0
        double[:] weights=zeros(k)
    
    for i in range(k):
        weights[i] = abs( 1/(X[i]-P[0])/(Y[i]-func(P,X[i])) )
        if weights[i]>1.0:
            weights[i]=1.0
        elif weights[i]<0.01:
            weights[i]=0.01
        sum_weights += weights[i]
        
    for i in range(k):
        weights[i] *= k/sum_weights   
    return weights


cpdef double[:] fit_lma_robust(double[:] X, double[:] Y,double accuracy = 0.001,bint print_report=True) except *:
    assert len(X) == len(Y)
    cdef:
        int i,k,control_count,len_P
        double[:,::1] Jcb,Jt,H,Inv,control_matrix
        double O,lamb=0.01,control=1,control_sum=0,sumf_=0,sumf__pre=0,sum_weights=0
        double[:] P,P_new,N,dlt_N
    
    #P = init_p(X,Y)
    P = fit_lma(X,Y,accuracy=0.01,print_report=False)
    P_new = zeros(len(P))
    P_new[5] = P[5] # O не варьируется
    len_P = 5 # количество варьируемых параметров 
    
    k = len(X)
    N = zeros(k) # N - вектор невязки y(i) - f( x(i) )
    
    weights = weight_func(X,Y,P,k)
    
    for i in range(k):
        N[i] = (func(P,X[i]) - Y[i]) #столбец 8 x 1 вектор невязки
        sumf_ = sumf_ + N[i]**2*weights[i]
    control_sum = sumf_
    try:
        while control > accuracy:
            if control_count >= 5:
                break
            
            # I вычисление градиентной поправки к вектору параметров
            weights = weight_func(X,Y,P,k)
            Jcb = jacobian_robust(X,P,weights)
            H = dot(Jcb,Jcb.T)
            for i in range(len_P):
                H[i,i] += lamb #демфирование матрицы
            Inv = pinv(H,rcond=1e-23*accuracy)
            dlt_N = dot(Inv, dot(Jcb,N))
    
            for i in range(len_P):
                P_new[i] = P[i] - dlt_N[i]
            
            # II оценивание дальнейшей стратегии фитирования
            sumf_pre = sumf_
            sumf_ = 0
            
            
            #вычисление вектора невязки и нормы по новым параметрам    
            for i in range(k):
                N[i] = (func(P_new,X[i]) - Y[i]) #столбец 8 x 1 вектор невязки
                #N[i] = func(P_new,X[i]) - Y[i]
                sumf_ += N[i]**2*weights[i]
                
            #анализ изменения нормы невязки
            if sumf_ > sumf_pre:
                lamb /= 10
                control_count += 1
                continue
            else:
                for i in range(len_P+1):
                    P[i] = P_new[i]
                control_count=0
                lamb = 10*lamb if lamb >= 10 else lamb
                
            control = (sumf_pre - sumf_)/control_sum
    except:
        print 'Bad fit, lamb = ',lamb 
    P[1] = abs(P[1])
    #calculate chi^2
    control = 0
    if print_report:
        print '\nReport:'
        print '%s    %s    %s' %('F(x_i)','Yi','Dlt')
        for i in range(k):
            print '%f    %f    %f' %(func(P,X[i]),Y[i],func(P,X[i]) - Y[i])
            control += abs(func(P,X[i]) - Y[i])
        control /= k
        print 'Average dlt: %f' %(control)
    return asarray(P)
        
        