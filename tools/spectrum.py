# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 12:25:30 2016

@author: eastwood

The module contains different methods to plot different amplitude spectrums from
front, back and side detectors in alpha and fission scales.

Apply methods to data frame (see data_types.pxd for actual formats.)

See <..>/processing_examples/get_distributions.py for examples.

"""

import os.path
import time
import pickle as pickle

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from . import quick_methods as qm

from .array_types import final_event_type

#### main values
coefs_folder = '../data/calibration_coefficients/Feb_2016'
alpha_energy_scale_range = 20000
alpha_energy_scale_step = 8
fission_energy_scale_range = 250000
fission_energy_scale_step = 200
side_alpha_energy_scale_range = 20000
side_alpha_energy_scale_step = 20
alpha_channel_range = 8192
fission_channel_range = 4096

#### exceptions
class EmptyDatasetError(Exception):
    pass

class NoCalibrationFile(IOError):
    pass

class WrongSpectrum(TypeError):
    pass

a, b, c = [], [], [] #error check
    
#### addition functions


#### Format description of event_table
'''
    event_type: '1'-front, '2'-back, '3'-side, '4'-veto
    strip: front:1-48 back: 1-128, side: 1-6
    channel: front:4096 back:8192 side: 8192
    scale: 0-alpha, 1-fission
    time: double microseconds
    beam_mark: bool
    tof: bool
    synchronization_time: 0..65536
    DAD4: 0..65536 low counter microseconds
    DAD5: 0..65536 high counter shows how many times low counter has rotated. Time up to about 1 hour 40 minutes
    
'''

event_type_dict = {'front': (1, 48), 'back': (2, 128), 
                   'side': (3, 64), 'veto': (4, 1)} # side 6 strip before 2019; 48 before Oct 2020
scale_dict = {'alpha': 0, 'fission': 1, 'side_alpha': 2, 'side_fission': 2}
calibration_coefficients_files = {}

#### additional and calibration functions
def get_calibration_coefficients(filename):
    """
    Return coefficients from file: array 2xN
    coefs[0] - A, coefs[1] - B; F = A * ch + B
    
    """
    
    import numpy as np
    with open(filename,'r') as f:
        s = f.read()[1:]
    s = s.split('\n\n')
    coefs = np.zeros((2,129), dtype=np.double)
    for line in s:
        line = line.split('\n')[0].split()
        if len(line) == 0: 
            break
        ind = int(line[0])
        coefs[0][ind] = float(line[3])
        coefs[1][ind] = float(line[7])    
    return coefs
    
def get_all_calibration_coefficients(coefs_folder=coefs_folder):
    '''
    Read all calibration coefficients from given coefs_folder and 
    return np.ndarray
    
    Shape: (5, 2, 2, 129)
    Structure: [event_type][scale]    [a_coef/b_coef][strip]
                1 front     0 alpha    0      1       0 - 128
                2 back      1 fission
                3 side
                4 veto

    '''
    
    coefs = np.zeros((5, 2, 2, 129)) # [event_type][scale][a_coef][b_coef]
    get_event_type_num = lambda type_name: event_type_dict[type_name][0] 
    for type_name in ['front', 'back', 'side']:
        for scale in ['alpha', 'fission']:
            coef_filename = coefs_folder + '/' + type_name + '_' + scale + '.txt'
            if not os.path.isfile(coef_filename):
                raise NoCalibrationFile('No file {0}'.format(coef_filename))
            coefs[get_event_type_num(type_name)][scale_dict[scale]] = get_calibration_coefficients(coef_filename)
    return coefs


def apply_calibration_coefficients(event_table, coefs_folder, event_type, \
                                   scale, coefs=None):
    if coefs is None:
        coef_filename = coefs_folder + '/' + event_type + '_' + scale + '.txt'    
        if not os.path.isfile(coef_filename):
            raise NoCalibrationFile('No file {0}'.format(coef_filename))
        coefs = get_calibration_coefficients(coef_filename)
    energy = qm.apply_calibration_coefficients(event_table, coefs)
    return energy

def apply_all_calibration_coefficients(event_table,
                                       coefs_folder=coefs_folder,
                                       coefs=None):
    coefs = get_all_calibration_coefficients(coefs_folder=coefs_folder) \
            if coefs is None else coefs
    energy = qm.apply_all_calibration_coefficients(event_table, coefs)
    return energy

def get_bins(scale, energy_scale):
    if energy_scale:
        if scale == 0:
            return np.arange(1, alpha_energy_scale_range, alpha_energy_scale_step)#8
        elif scale == 1:
            return np.arange(1, fission_energy_scale_range, fission_energy_scale_step)
        elif scale == 2:
            return np.arange(1, side_alpha_energy_scale_range, side_alpha_energy_scale_step)
    else:    
        if scale == 0:
            return np.arange(1, alpha_channel_range)
        elif scale == 1:
            return np.arange(1, fission_channel_range)
        elif scale == 2:
            return np.arange(1, side_alpha_energy_scale_range, side_alpha_energy_scale_step)

#### spectrum functions           
def get_spectrum(event_table, event_type='front', scale='alpha', tof=False, \
        energy_scale=False, coefs_folder=coefs_folder, bins=None):
    '''
    The method allow to get energy or channel distribution from the given event_table.
    Input:
        event_table
        event_type : 'front','back','side','veto'
        energy_scale: choose channel (False) or energy (True) scale to make spectrum
        scale: 'alpha'  (up to 20 MeV with 8 KeV step), 'fission' (up to 200 MeV with 200 KeV step)
        tof: True, False or 'all' (no tof condition)
        coefs_folder: add path to folder with calibration coefficients to apply them and get spectrum in energy scale
    Return:
        spectrum (np.ndarray) [0 .. strip_num-1,0 .. channel_num-1],
        bins (np.ndarray),
        configure (dict: 'event_type':event_type,'scale':scale,'tof':tof,'energy_scale':energy_scale,'shape':shape) 
        
    '''
    
    # init
    event_type_, strip_num = event_type_dict[event_type]
    scale_ = scale_dict[scale]
    if bins is None:
        bins = get_bins(scale_, energy_scale)
    shape = (strip_num, len(bins) - 1)
    spectrum = np.zeros(shape, dtype=int)
    
    # get events which fit the conditions
    if tof == 'all':
        tof_condition = np.ones(event_table.shape[0], dtype=bool)
    else:
        tof_condition = (event_table['tof']==tof)
    event_table = event_table[(event_table['event_type'] == event_type_) & \
        (event_table['scale']==scale_) & tof_condition]
    
    # apply calibration coefs
    if energy_scale:
        if coefs_folder:
            sample = apply_calibration_coefficients(event_table, \
                                                    coefs_folder, \
                                                    event_type, scale)
        else:
            raise Exception('Specify a path to a folder with calibration coefficients to get spectrum in energy scale')
    else:
        sample = event_table['channel']
        
    # calc histograms  
    qm.calc_spectrum(event_table, sample, bins, spectrum) # calc histograms for each strip
    configure = {'event_type': event_type, 'scale': scale, 'tof': tof, \
        'energy_scale': energy_scale, 'shape': shape}
        
    return np.asarray(spectrum), bins, configure
   
def get_combined_spectrum(event_table, event_type='front-side', 
                          scale='alpha', tof=False, energy_scale=False, 
                          coefs_folder=coefs_folder, coefs=None, bins=None,
                          dlt_t=20):
    '''
    The method allow to get combined front-side or back-side
    energy or channel distribution from the given event_table.
    Input:
        event_table
        event_type : 'front-side','back-side'
        energy_scale: choose channel (False) or energy (True) scale 
                        to make the spectrum
        scale: 'alpha'  (up to 20 MeV with 8 KeV step), 
               'fission' (up to 200 MeV with 200 KeV step), 
               'side_alpha' ( 0 .. 20 MeV with 30 KeV step), 
               'side_fission' ( 0 .. 20 MeV with 30 KeV step, 
                               used for digital american format)
        tof: True, False or 'all' (no tof condition)
        coefs_folder: add path to folder with calibration coefficients 
                      to apply them and get spectrum in energy scale
        coefs: None (if use coefs_folder with standart calibration filenames)
               or data structure containing two fields front_coefs and 
               side_coefs, each is numpy array with shape (2,129)[float] - 
               standart result of get_calibration_coefficients function
    Return:
        spectrum (np.ndarray)[0 .. strip_num-1, 0 .. channel_num-1],
        bins (np.ndarray),
        configure (dict: 'event_type': event_type, 'scale': scale, 
                   'tof': tof, 'energy_scale': energy_scale, 'shape': shape) 
    See qm.find_sync_pairs for details.
    
    '''
    # init
    assert np.any([w_ == event_type 
                   for w_ in ['front-side', 'back-side']]), \
           'Wrong event type. Should be "front-side" or "back-side"'
        
    first_event_type, second_event_type = event_type.split('-')
    event_type_, strip_num = event_type_dict[first_event_type]
    scale_ = scale_dict[scale]
    if bins is None:
        bins = get_bins(scale_, energy_scale)
    shape = (strip_num, len(bins) - 1)
    spectrum = np.zeros(shape, dtype=int)
    
    # get events which fit the conditions
    if scale == 'side_alpha':
        scale_ = 0 # corresponds to alpha scale
    elif scale == 'side_fission':
        scale_ = 1
    index = qm.find_sync_pairs(event_table, scale_, event_type_, tof,
                               dlt_t=dlt_t)
    first_event_table = event_table[index[:, 0]]
    second_event_table = event_table[index[:, 1]]
    
    # apply calibration coefs
    if not coefs_folder:
        raise Exception('Specify a path to a folder with calibration coefficients to get spectrum in energy scale')  
    if coefs is not None:
        front_coefs = coefs.front_coefs
        side_coefs = coefs.side_coefs
    else:
        front_coefs = None
        side_coefs = None
    
    first_sample = apply_calibration_coefficients(first_event_table, \
        coefs_folder, first_event_type, scale, front_coefs) #front
    
    second_sample = apply_calibration_coefficients(second_event_table, \
        coefs_folder, second_event_type, scale, side_coefs) #side
    sample = first_sample + second_sample
    
    if not energy_scale:
        if coefs is None:
            coef_filename = coefs_folder + '/' + second_event_type + \
                             '_' + scale + '.txt'
            coefs_ = get_calibration_coefficients(coef_filename)
        else:
            coefs_ = side_coefs
            
        # calc new channel according to (sample - b )/a
        sample = qm.recalculate_side_channels(second_event_table, sample, coefs_)
        
    #calc histograms
    spectrum = qm.calc_spectrum(second_event_table, \
        sample, bins, spectrum) #calc histograms for each side strip
    configure = {'event_type': event_type, 'scale': scale, 'tof': tof, \
        'energy_scale': energy_scale, 'shape': shape}
    return np.asarray(spectrum), bins, configure
 
def get_pos_distribution_2d(event_table, scale='alpha', \
                            tof=False, coefs_folder=coefs_folder,
                            energy_alpha_max=25000, energy_fission_min=25000):
    '''
    Get 2d position distribution of detector's strips
    as np.array [front strips 0..47, back strips 0..127].
    Input:
        event_table
        scale: 'alpha'  (up to 20 MeV with 8 KeV step), 'fission' (up to 200 MeV with 200 KeV step), 'side_alpha' ( 0 .. 20 MeV with 30 KeV step), 'side_fission' ( 0 .. 20 MeV with 30 KeV step, used for digital american format)
        tof: True, False or 'all' (no tof condition)
        coefs_folder: add path to folder with calibration coefficients to apply them and get spectrum in energy scale
    Return:
        spectrum (np.ndarray)[1 .. front_strip_num, 1 .. back_strip_num],
        bins (np.ndarray) front_strip_array,        
        configure (dict: 'event_type':event_type,'shape':shape) 
    
    Hint: use then np.sum(result,axis=0) to get 1d back strip distribution
    and np.sum(result,axis=1) to get 1d front strip distribution.
    
    '''
    
    scale_ = scale_dict[scale] # convert to scale id number
    if tof == 'all':
        tof_condition = np.ones(event_table.shape[0], dtype=bool)
    else:
        tof_condition = (event_table['tof'] == tof)
     
    # specify the sample
    ind = (event_table['event_type'] < 3) & \
        (event_table['scale'] == scale_) & tof_condition
    sample = event_table[ind]
    
    # apply coefficients --> to energy scale
    energy = apply_all_calibration_coefficients(sample, coefs_folder)
    
    # filter by energies according to alpha( < 25MeV) or fission ( > 25MeV) region
    if scale == 'alpha':
        energy_filter = (energy < energy_alpha_max)
    elif scale == 'fission':
        energy_filter = (energy > energy_fission_min)
    sample = sample[energy_filter] # [['strip', 'event_type', 'scale', 'tof']]
    
    # separate to 2 subsamples - front-back pairs
#    ind1 = np.argwhere(np.diff(sample['event_type']) == 1) 
#    ind2 = ind1 + 1
#    first_sample, second_sample = sample[ind1]['strip'], sample[ind2]['strip']
#    first_sample, second_sample = first_sample.ravel(), second_sample.ravel() # align
#    assert len(first_sample) == len(second_sample) > 0, \
#        "Sample's lengths should be equal and > 0; length s1, s2: " + \
#        str(len(first_sample)) + ' ' + str(len(second_sample))
    
    # build empty array for position distribution
    # event_type_dict[x][1] returns number of strips for x detector type, i.e. 48 for 'front' etc.
    spectrum = np.zeros((event_type_dict['front'][1],\
                         event_type_dict['back'][1]), dtype=int)
    shape = spectrum.shape
    configure = {'event_type': 'position', 'scale': scale, \
        'tof': tof, 'energy_scale': 'False', 'shape': shape} 
    bins = np.arange(1, shape[1] + 1)
#    
#    if len(first_sample) < 2:
#        return spectrum, bins, configure # lazy cheat fix for empty files
        
#    if sample['event_type'][0] != 1:
#        first_sample,second_sample = second_sample,first_sample
        
    # call quick method to fill the spectrum
    try:
        spectrum = np.asarray(qm.calc_2d_int_spectrum(sample, spectrum)) 
    except IndexError as e:
        print('Error! ', e)
        
    return spectrum, bins, configure
    
def get_tof_spectrum(event_table, tof_counter = 'tofD1'):
    '''
    Get distribution of tof-counter ('tofD1' or 'tofD2')
    Input:
        event_table
        tof_counter : 'tofD1','tofD2'
    Return:
    spectrum (np.ndarray)[1 .. front_strip_num,1 .. back_strip_num],
    configure (dict: 'event_type':event_type, 'scale':scale, 'tof':tof, 'energy_scale':energy_scale, 'shape':shape) 
    
    Return 2d position distribution of detector's strips
    as np.array [front strips 0..47, back strips 0..127].
    Hint: use then np.sum(result, axis=0) to get 1d back strip distribution
    and np.sum(result, axis=1) to get 1d front strip distribution.
    
    '''
    
    sample = event_table[tof_counter]
    sample = sample[sample > 0]
    if np.any(sample):
        bins = np.arange(1, 4096, 20)
        hist,_ = qm.calc_histogram(sample, bins)
        shape = bins.shape
    else:
        hist,bins = [0., 0.],[0., 1., 2.]
        shape = (1, )
        
    configure = {'event_type': tof_counter, 'shape': shape}
    return hist, bins, configure

def get_rot_time_spectrum(event_table):
    '''
    Get rotatian time distribution.
    Input:
        event_table
    Return:
        spectrum (np.ndarray) [1 .. 40'000] mks
    configure (dict: 'event_type':event_type, 'shape':shape) 
    
    '''
    
    sample = event_table['rotation_time']
    if np.any(sample):
        bins = np.arange(1, 40000, 80)
        hist,_ = qm.calc_histogram(sample, bins)
        shape = bins.shape
        
    configure = {'event_type': 'rotation time', 'shape': shape}
    return hist, bins, configure
           
# position distributions
def get_target_time_distribution(event_table, visualize=True, output=False, \
        filenames=False, coefs_folder=coefs_folder):
    """
        Показывает распределение среднего количества отсчетов в секунду по секторам мишени. 
        Записывает отчет в файл <output>, в этот файл включается название файлов, 
        по которым производилось измерение <filenames>.
        filename - таблица с данными получается с помощью модуля read_files, str
        
    """
    
    # choose events with energies inside (1, 6) MeV
    energy = apply_all_calibration_coefficients(event_table, coefs_folder=coefs_folder)
    ind = (energy > 1000) & (energy < 6000)
    
    # get time serie
    serie = event_table['target_time'][ind]
    serie = serie[(event_table['target_time'] > 40) & \
                    (event_table['target_time'] < event_table['target_time'].max() \
                     - 100) & \
                    (event_table['tof'] == True)]
        
    # total time in seconds
    total_time = float(event_table['time'][-1] - event_table['time'][0]) / 1000000
    hist, bins = qm.calc_histogram(serie, bins=13)
    hist = hist / total_time
    print(total_time)
    
    # write report to file
    if output:
        if not os.path.exists(output):
            f = open(output,'w')
            for i,j in enumerate(hist):
                f.write('{:<8s}'.format(str(i + 1)) )
            f.write('{:<10s}'.format('time(sec)'))
            f.write('{:<10s}'.format('filenames'))
            f.write('\n')
            f.close()
            
        f = open(output,'a+')
        for i in hist:
            f.write('{:<9.3g}'.format(i))
        f.write('{:<10g}'.format(int(total_time)))
        f.write(filenames)
        f.write('\n')
        f.close()
        
    # show chart
    if visualize:
        fig,ax = plt.subplots()
        ax.bar(list(range(1, len(hist) + 1)), hist, alpha=0.6, align='center')
        
    return hist, bins, total_time
                
def read_target_time_distr(filename):
    """
        Читает и строит графики (13 шт) распределения среднего количества 
        отсчетов в секунду по секторам мишени.
        filename - название файла отчета, 
        который записывается в файл функцией get_target_time_distribution.
        
    """
    
    # read the file
    frame = pd.read_csv(filename, sep='\s+')    
    
    # make figure and axes with 3 x 4 == 12 cells
    fig, ax = plt.subplots(3, 4, sharex=True)
    x_pos = np.arange(len(frame))
    width = 0.5
    
    # fill the cells
    for i, ax_ in enumerate(ax.flat):               
        y_pos = frame[str(i + 1)]
        #x_pos = range(len(y_pos))
        ax_.bar(x_pos, y_pos, width)
        ax_.set_ylabel('sector ' + str(i + 1))
        ax_.set_xticks(x_pos + width)
        ax_.set_xticklabels(frame['filenames'], rotation='vertical')
        
    # plots final bar chart for different filenames reports
    frame = pd.DataFrame(frame[frame.columns[:-2]].values, \
        index=frame['filenames'], columns=frame.columns[:-2])
    frame.plot(kind='bar')
 
def get_intensivity(data_table):
    """
    Build intensivity timeserie by 1 min step.
    
    """
    t = data_table['time_micro']
    hist, bins = np.histogram(t, bins=np.arange(0, t[-1], 60 * 10**6))
    return bins[1:], hist

    
class SpectrumBase(object):
    """
    Base spectrum class which provides vectorised methods for distributions
    spectrum. 
    
    """
        
    def _typecheck(self,other):
        if (self.configure != other.configure):
            message = "Can't operate with two different spectrums." + \
                      " Check the configure."
            raise WrongSpectrum(message)    
        
    def __init__(self, data, bins, configure):
        self.data, self.bins, self.configure = pd.DataFrame(data).T, bins, configure
        self.shape = self.configure['shape'] 
        self.data[self.data < 0] = 0 # the main spectrum contains only non-zero counts
        if self.data.columns[0] == 0:
            self.data.columns += 1 # strips are inside [1 .. max_strip_num]
        self.data.values[-1, :] = 0 # too many values outside the interval of interest
        
    def __add__(self,other):
        self._typecheck(other)
        self.data += other.data
        return type(self)(data=self.data.T, bins=self.bins, \
                          configure=self.configure)
        
    def __getitem__(self,index):
        return self.data[index]
    
    def __len__(self):
        return self.shape[0]
        
    def __eq__(self,other):
        try:
            self._typecheck(other)
            return np.all(self.data == other.data) & np.all(self.bins == other.bins)
        except WrongSpectrum as e:
            print(e)
            return False
        
    def show(self, strip=None, threshold=1., supertitle = ''):
        '''
        The method show the distribution for the chosen <strip>(int) parameter or the
        whole distribution if the strip is None.
        The method returns ax (matplotlib class) to work with and change the plot.
        
        '''
        
        def get_data_bins(data,bins):
            return np.r_[0, data[0], data, 0], \
                    np.r_[bins[0], bins[0], bins[1:], bins[-1]] 
        
        import matplotlib.pyplot as plt
        
        # plot for a single strip 
        if strip is not None:
            data, bins = get_data_bins(self.data[strip], self.bins)
            
            fig, ax = plt.subplots()
            ax.plot(bins, data, linestyle='steps')
            plt.suptitle(supertitle, fontsize=23)
            plt.show()
            return fig, ax
        
        # plot the whole 2d distribution
        else:
            data = self.data.copy()
            
            # apply threshold
            threshold = data.values.max() * threshold
            data[data > threshold] = threshold
            
            if len(data) == 0:
                raise ValueError('Empty spectrum')
                
            fig = plt.figure()
            ax1 = fig.add_axes([0.125, 0.35, 0.8, 0.6])
            ax2 = fig.add_axes([0.125, 0.05, 0.64, 0.25], sharex=ax1)
            
            # specify horizontal axis
            x = self.bins
            x_min, x_max, dx = min(x), max(x), int(x[1] - x[0])
            x = np.arange(x_min, x_max + dx, dx) # channel or energy scale
            
            # specify vertical axis
            y = data.columns #counts
            y_min, y_max, dy = min(y), max(y), int(y[1] - y[0])
            y = np.arange(y_min, y_max + 2 * dy, dy) # strip scale
            
            # make grid and colormesh
            xgrid, ygrid = np.meshgrid(x, y)
            a = ax1.pcolormesh(xgrid, ygrid, data.to_numpy().T)
            
            # specify ticks
            h_min, h_max = min(data.min()), max(data.max())
            ticks = np.linspace(h_min, h_max, 40)
            
            # make and plot summary spectrum for all strips
            sum_spectr = np.sum(self.data, axis=1) # scale/sum_counts
            sum_spectr, x = get_data_bins(sum_spectr, x)
            ax2.plot(x, sum_spectr, 'k', linestyle='steps')
            
            cb = fig.colorbar(a, ax=ax1, ticks=ticks)
            ax2.axis(xmax=max(x), ymax=max(sum_spectr) )
            plt.suptitle(supertitle, fontsize=23)
            plt.show()
            return fig, cb, ax1, ax2
    
    def tofile(self, filename):
        pickle.dump(self, open(filename, 'wb'))
    
    @staticmethod
    def fromfile(filename):
        return pickle.load(open(filename, 'rb'))        


class SpectrumBase1D(SpectrumBase):
    
    def __init__(self,data,bins,configure):
        self.data, self.bins, self.configure = pd.Series(data), bins, configure
        self.shape = self.configure['shape'] 
        
    def __add__(self,other): # iadd
        self._typecheck(other)
        self.data += other.data
        return type(self)(data=self.data, bins=self.bins, configure=self.configure)
     
    def show(self, strip=None, threshold=1., supertitle = ''):
        '''
        The method show the distribution for the chosen <strip>(int) parameter or the
        whole distribution if the strip is None.
        The method returns ax (matplotlib class) to work with and change the plot.
        '''            
        import matplotlib.pyplot as plt
        
        fig, ax = plt.subplots()
        ax.plot(self.bins, self.data, linestyle='steps')
        ax.set_xlabel('№ стрипа')
        ax.set_ylabel('Число отсчётов')
        
        plt.suptitle(supertitle, fontsize=23)
        plt.show()
        return fig, ax
  
    
class TofSpectrum(SpectrumBase1D):
    '''
    Build 1d position distribution of Tof-counters.
    The main properties are data(pd.DataFrame), bins(contains x-scale),
    configure (dict, contains main information about the spectrum).
    Initialization:
    I. Build from the event table. (default initialization)
        event_table - one can get it from the data file, using read_data.py (in processing_examples)
        tof_counter : 'tofD1','tofD2'
    II. Raw initialization. 
        data: pd.DataFrame
        bins: np.ndarray
        configure: dict
    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type':event_type,'scale':scale,'tof':tof,'energy_scale':energy_scale,'shape':shape) 
    Methods:
        1. Base methods: + - =
        2. show - visualize data. It can be called to show histogram of the distribution like obj.show().
        3. Reading and writing to file: tofile, fromfile. (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data so it works well with pd.DataFrame
        
    '''
    def __init__(self, event_table=None, tof_counter='tofD1', \
            data=None, bins=None, configure=None, **argv):
                
        if (data is None) and (bins is None) and (configure is None):
            data, bins, configure = get_tof_spectrum(event_table, tof_counter)
            
        super(TofSpectrum, self).__init__(data, bins, configure)
        self.tof_counter = tof_counter
        
    def show(self):
        
        def get_data_bins(data,bins):
            return np.r_[0, data[0], data, 0], \
                np.r_[bins[0], bins[0], bins[1:], bins[-1]]
                
        data,bins = get_data_bins(self.data, self.bins)
        
        fig, ax = plt.subplots()
        ax.plot(bins, data, linestyle='steps')
        ax.set_title(self.tof_counter)
        ax.set_xlabel('channel')
        ax.set_ylabel('counts')
        
        plt.show()
        
        return fig, ax
               

class RotTimeSpectrum(SpectrumBase1D):
    '''Build 1d position rotation time distribution.
    The main properties are data(frame), bins(contains x-scale),
    configure (dict, contains main information about the spectrum).
    Initialization:
    I. Build from the event table. (default initialization)
        event_table - one can get it from the data file, using read_data.py (in processing_examples)
    II. Raw initialization. 
        data: pd.DataFrame
        bins: np.ndarray
        configure: dict
    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type':event_type, 'shape':shape) 
    Methods:
        1. Base methods: + - =
        2. show - visualize data. It can be called to show histogram of the distribution like obj.show().
        3. Reading and writing to file: tofile, fromfile. (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data so it works well with pd.DataFrame
        
    '''
    def __init__(self, event_table=None, \
            data=None, bins=None, configure=None, **argv):
                
        if (data is None) and (bins is None) and (configure is None):
            data, bins, configure = get_rot_time_spectrum(event_table)
            
        super(RotTimeSpectrum, self).__init__(data, bins, configure)
        
    def show(self):
        
        def get_data_bins(data,bins):
            return np.r_[0, data[0], data, 0], \
                np.r_[bins[0], bins[0], bins[1:], bins[-1]]
                
        data, bins = get_data_bins(self.data, self.bins)
        
        fig, ax = plt.subplots()
        ax.plot(bins, data, linestyle='steps')
        ax.set_title('Rotation time distribution')
        ax.set_xlabel('mks')
        ax.set_ylabel('counts')
        
        plt.show()
        
        return fig, ax
         

class Spectrum(SpectrumBase):          
    '''
    Build energy or channel distribution spectrum
    and work with them in convinient way. The main properties are data(pd.DataFrame), bins(contains x-scale),
    configure (dict, contains main information about the spectrum).
    Initialization:
    I. Build from the event table. (default initialization)
        event_table - one can get it from the data file, using read_data.py (in processing_examples)
        event_type : 'front','back','side','veto'
        energy_scale: choose channel (False) or energy (True) scale to make spectrum
        scale: 'alpha'  (up to 20 MeV with 8 KeV step), 'fission' (up to 200 MeV with 200 KeV step)
        tof: True, False or 'all' (no tof condition)
        coefs_folder: add path to folder with calibration coefficients to apply them and get spectrum in energy scale
        bins: np.ndarray default = None
        The folder should contain next files with calibration coefficients: front_alpha.txt, front_fission.txt, back_alpha.txt, back_fission.txt, side_alpha.txt, side_fission.txt 
    II. Raw initialization. 
        data: pd.DataFrame
        bins: np.ndarray
        configure: dict
    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type':event_type,'scale':scale,'tof':tof,'energy_scale':energy_scale,'shape':shape) 
    Methods:
        1. Base methods: + - =
        2. show - visualize data. It can be called to show 1-strip distribution like obj.show(strip_num)
            or the whole 2d strip-bins distributions (without args).
        3. Reading and writing to file: tofile, fromfile. (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data so it works well with pd.DataFrame
        
    '''
    def __init__(self, event_table=None, event_type='front', scale='alpha', \
            tof=False, energy_scale=False, coefs_folder=coefs_folder, \
            data=None, bins=None, configure=None, **argv):
                
        if (data is None) and (configure is None):
            data, bins, configure = get_spectrum(event_table, event_type, \
                                                 scale, tof, energy_scale, \
                                                 coefs_folder, bins)
            
        assert bins is not None, 'Set bins value'
        super(Spectrum, self).__init__(data, bins, configure)
        self.title = self.configure['event_type'] + '-' + self.configure['scale']
        
    def show(self, strip=None, threshold=0.15, supertitle='default'):
        
        if supertitle == 'default':
            supertitle = self.title
        # plot for a single strip
        if strip is not None:
            fig, ax = super(Spectrum, self).show(strip, threshold, supertitle)
            # RU
#            ax.set_ylabel(u'Число отсчётов')
#            if energy_scale == False:
#                ax.set_xlabel(u'№ канала')
#            else:
#                ax.set_xlabel(u'Энергия, КэВ')
            # ENG
            ax.set_ylabel('Number of counts')
            if self.configure['energy_scale']:
                ax.set_xlabel('Energy, KeV')
            else:
                ax.set_xlabel('Channel')
                
            return fig, ax
                
        # plot the whole distribution
        else:        
            fig, cb, ax1, ax2 = super(Spectrum, self).show(strip, threshold, \
                                supertitle)
            # RU
#            cb.set_label(u'Число отсчётов')
#            ax1.set_ylabel(u'№ горизонтального стрипа')
#            ax2.set_ylabel(u'Число отсчётов')
#            
#            if self.configure['energy_scale'] == False:
#                ax1.set_xlabel(u'№ канала')
#                ax2.set_xlabel(u'№ канала')
#            else:
#                ax1.set_xlabel(u'Энергия, КэВ')
#                ax2.set_xlabel(u'Энергия, КэВ')
            # ENG
            cb.set_label('Number of counts', fontsize='large')
            ax1.set_ylabel('Strip number', fontsize='large')
            ax2.set_ylabel('Number of counts')
            
            if self.configure['energy_scale']:
                ax1.set_xlabel('Energy, KeV', fontsize='x-large')
                ax2.set_xlabel('Energy, KeV', fontsize='x-large')
            else:
                ax1.set_xlabel('Channel', fontsize='x-large')
                ax2.set_xlabel('Channel', fontsize='x-large')
                
            return fig, ax1, ax2 
  
      
class CombinedSpectrum(SpectrumBase):          
    '''
    Build combined front-side or back-side energy or channel distribution spectrum
    and work with them in convinient way. The main properties are data(pd.DataFrame), bins(contains x-scale),
    configure (dict, contains main information about the spectrum).
    Initialization:
    I. Build from the event table. (default initialization)
        event_table - one can get it from the data file, using read_data.py (in processing_examples)
        event_type : 'front-side','back-side'
        energy_scale: choose channel (False) or energy (True) scale to make spectrum
        scale: 'alpha'  (up to 20 MeV with 8 KeV step), 'fission' (up to 200 MeV with 200 KeV step), 'side_alpha' (up to 20 MeV with 30 KeV step)
        tof: True, False or 'all' (no tof condition)
        coefs_folder: add path to folder with calibration coefficients to apply them and get spectrum in energy scale
        The folder should contain next files with calibration coefficients: front_alpha.txt, front_fission.txt, back_alpha.txt, back_fission.txt, side_alpha.txt, side_fission.txt
    II. Raw initialization. 
        data: pd.DataFrame
        bins: np.ndarray
        configure: dict
    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type':event_type,'scale':scale,'tof':tof,'energy_scale':energy_scale,'shape':shape) 
    Methods:
        1. Base methods: + - =
        2. show - visualize data. It can be called to show 1-strip distribution like obj.show(strip_num)
            or the whole 2d strip-bins distributions (without args).
        3. Reading and writing to file: tofile, fromfile. (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data so it works well with pd.DataFrame
        
    '''    
    def __init__(self, event_table=None, event_type='front-side', 
                 scale='alpha', tof=False, energy_scale=False, 
                 coefs_folder=coefs_folder, coefs=None, data=None, 
                 bins=None, configure=None, **argv):
                
        if (data is None) and (configure is None):
            data, bins, configure = get_combined_spectrum(
                event_table, event_type, scale, tof, energy_scale, 
                coefs_folder, coefs, bins, **argv
            )
            
        assert bins is not None, 'Set bins value'
        super(CombinedSpectrum, self).__init__(data, bins, configure)
        self.title = self.configure['event_type'] + '-' + self.configure['scale']
        
    def show(self, strip=None, threshold=0.55, supertitle='default'):
        if supertitle == 'default':
            supertitle = self.title
        # plot for a single strip
        if strip is not None:
            fig, ax = super(CombinedSpectrum, self).show(
                strip, threshold, supertitle
            )
            ax.set_ylabel('Число отсчётов')
            if self.configure['energy_scale'] == False:
                ax.set_xlabel('№ канала')
            else:
                ax.set_xlabel('Энергия, КэВ')
                
            return fig, ax
         
        # plot the whole distribution
        else:        
            fig, cb, ax1, ax2 = super(CombinedSpectrum, self).show(
                strip, threshold, supertitle
            )
            cb.set_label('Число отсчётов')
            ax1.set_ylabel('№ бокового стрипа')
            ax2.set_ylabel('Число отсчётов')
            
            if self.configure['energy_scale'] == False:
                ax1.set_xlabel('№ канала')
                ax2.set_xlabel('№ канала')
            else:
                ax1.set_xlabel('Энергия, КэВ')
                ax2.set_xlabel('Энергия, КэВ')
                
            return fig, ax1, ax2
        
        
class PositionSpectrum(SpectrumBase):          
    '''
    Build 2d position distribution of front-back detector's strips. 
    The main properties are data(pd.DataFrame), bins(contains x-scale),
    configure (dict, contains main information about the spectrum).
    Initialization:
    I. Build from the event table. (default initialization)
        event_table - one can get it from the data file, using read_data.py (in processing_examples)
        scale: 'alpha'  (up to 20 MeV with 8 KeV step), 'fission' (up to 200 MeV with 200 KeV step)
        tof: True, False or 'all' (no tof condition)
        coefs_folder: add path to folder with calibration coefficients to apply them and get spectrum in energy scale
        The folder should contain next files with calibration coefficients: front_alpha.txt, front_fission.txt, back_alpha.txt, back_fission.txt, side_alpha.txt, side_fission.txt 
    II. Raw initialization. 
        data: pd.DataFrame
        bins: np.ndarray
        configure: dict
    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type':event_type,'scale':scale,'tof':tof,'energy_scale':energy_scale,'shape':shape) 
    Methods:
        1. Base methods: + - =
        2. show - visualize data. It can be called to show 1-strip distribution like show(strip_num)
            or the whole 2d strip-bins distributions (without args).
        3. Reading and writing to file: tofile, fromfile. (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data so it works well with pd.DataFrame
        
    '''    
    def __init__(self, event_table=None, scale='alpha', tof=False, \
            data=None, bins=None, configure=None,\
            coefs_folder=coefs_folder, **argv):
                
        if (data is None) and (bins is None) and (configure is None):
            data, bins, configure = \
                get_pos_distribution_2d(event_table, scale, tof, \
                                        coefs_folder)
            
        super(PositionSpectrum, self).__init__(data, bins, configure)
        self.data.index = np.arange(1, self.data.shape[0] + 1)
        self.title = configure['event_type'] + '-' + configure['scale']
    
    def get_front_distribution(self,axis=0):
        data = self.data.sum(axis)
        bins = data.index
        configure = self.configure
        configure['shape'] = data.shape
        configure['event_type'] = 'position1D'
        return SpectrumBase1D(data, bins, configure)
    
    def get_back_distribution(self):
        return self.get_front_distribution(axis=1)
    
    def show(self, supertitle = "default"): 
        if supertitle == 'default':
            supertitle = self.title
        x, y = np.arange(1, self.data.shape[0] + 1), \
            np.arange(1, self.data.shape[1] + 1)
            
        fig, ax = plt.subplots()    
        xgrid, ygrid = np.meshgrid(x, y)
        a = ax.pcolormesh(xgrid, ygrid, self.data.as_matrix().T) 
        
        h_min, h_max = min(self.data.min()), max(self.data.max())
        ticks = np.linspace(h_min, h_max, 25)
        
        fig.colorbar(a, ax=ax, ticks=ticks)
        ax.set_title('Position front/back strip distribution')
        ax.set_xlabel('Back strip, num')
        ax.set_ylabel('Front strip, num')
        plt.suptitle(supertitle, fontsize=23)
        plt.show()
        return fig, ax
    
#### Format description of data_table
'''
    event_type: '1'-front-back, '3'-side
    cell_x,cell_y: front:1-48 back: 1-128, side: 0,0
    energy: 0.0 .. 200000.0 MeV
    time: double microseconds
    beam_mark: bool
    tof: bool
    DAD4: 0..65536 low counter microseconds
    DAD5: 0..65536 high counter shows how many times low counter has rotated. Time up to about 1 hour 40 minutes
'''                      
#### search chains    
def get_data_table(event_table, coefs_folder=coefs_folder):
    #delete veto
    event_table = event_table[event_table['event_type'] != 4]
    
    #apply energy coefficients 
    energy = apply_all_calibration_coefficients(event_table, coefs_folder)    
    
    #find pairs and summarize front-side and back-side energies
    pairs_front = qm.find_sync_pairs(event_table, 0, 1)
    energy[pairs_front[:,0]] += energy[pairs_front[:, 1]]
    energy[pairs_front[:,1]] = 0
    
    pairs_back = qm.find_sync_pairs(event_table, 0, 2)
    energy[pairs_back[:,0]] += energy[pairs_back[:, 1]]
    energy[pairs_back[:,1]] = 0
    
    #create new data table
    data_table = np.zeros(len(event_table) // 2, dtype=final_event_type)
    data_table = qm.fill_data_table(event_table, energy, data_table)
    return np.array(data_table)
    
def get_data_table_dg(event_table, coefs_folder=coefs_folder):
    #delete veto
    t0 = time.time()
    event_table = event_table[event_table['event_type'] != 4]
    print('check1 get_dt:', time.time() - t0)
    #apply energy coefficients 
    energy = apply_all_calibration_coefficients(event_table, coefs_folder)    
    print('check2 get_dt:', time.time() - t0)
    #create new data table
    data_table = np.zeros(len(event_table), dtype=final_event_type)
    print('check3 get_dt:', time.time() - t0)
    data_table = qm.fill_cells_table(event_table, energy, data_table)
    print('check4 get_dt:', time.time() - t0)
    return data_table
    
    
    
    
    
    
