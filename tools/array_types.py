# -*- coding: utf-8 -*-
"""
Created on Tue May 30 15:00:18 2017

@author: eastwood
"""
import numpy as np   
event_type = np.dtype([('event_type', int), ('strip', int), ('channel', int),\
        ('scale', int), ('time_macro', np.int64), ('time_micro', np.int64), \
        ('beam_mark', np.bool), ('tof', np.bool), \
        ('tofD1', np.uint16), ('tofD2', np.uint16),\
        ('synchronization_time', np.uint16),
        ('rotation_time', np.uint16)])  
  
energy_event_type = np.dtype([('event_type', int), ('strip', int),\
        ('energy', float), ('scale', int), ('time', np.double),\
        ('beam_mark', np.bool), ('tof', np.bool), \
        ('synchronization_time', np.uint16), ('DAD4', np.uint16), \
        ('DAD5', np.uint16), ('target_time', np.uint16),
        ('rotation_time', np.uint16)])  

final_event_type = np.dtype([('event_type',int), ('energy_front',float), \
                    ('energy_back', float), ('cell_x', int), ('cell_y', int),\
                    ('time_macro', np.int64), ('time_micro', np.int64), \
                    ('beam_mark', np.bool), ('tof', np.bool), \
                    ('tofD1', np.uint16), ('tofD2', np.uint16), 
                    ('rotation_time', np.uint16)])  

analog_block = np.dtype([('w0', np.uint16), ('w1', np.uint16), ('w2', np.uint16),
                       ('w3', np.uint16), ('w4', np.uint16), ('w5', np.uint16), 
                       ('w6', np.uint16), ('w7', np.uint16), ('w8', np.uint16),
                       ('w9', np.uint8),  ('w10', np.uint8),  
                       ('w11', np.uint32), ('w12', np.uint32), 
                       ('w13', np.uint16), ('w14', np.uint16)
                      ])

digital_block = np.dtype([('time', np.uint64), ('amplitude', np.uint16), 
            ('channel', np.int8), ('slot', np.int8), ('beam', np.int8),
            ('file_version', np.int8), ('pos', np.int32)])

