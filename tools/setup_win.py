import setuptools
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy

quick_methods = Extension(name="*", sources=["*.pyx"],
                          include_dirs=[numpy.get_include()]
)
          # libraries=["m"]) # Unix-like specific]
setup(ext_modules=cythonize(quick_methods))

