#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  9 18:01:45 2019

The module contains different methods to plot different 
correlated amplitude spectrums from
front, back and side detectors in alpha and fission scales.

See <..>/processing_examples/get_correlated_distributions.py for examples.

@author: eastwood
"""

import numpy as np

import matplotlib.pyplot as plt

from . import data_processingGNS2021.tools.spectrum as sp
from . import data_processingGNS2021.tools.correlations_functions as cf
from . import data_processingGNS2021.tools.quick_methods as qm

#### DESCRIPTION
"""
cdef packed struct DATA_EVENT:
    np.int_t event_type # 1 - focal-back, 3 - side
    double energy_front
    double energy_back
    np.int_t cell_x # 0 .. 48
    np.int_t cell_y # 0 .. 128
    np.int64_t time_macro # seconds 
    np.int64_t time_micro # microseconds 
    c_bool beam_mark # bool
    c_bool tof # bool      
    
-> see data_types.pxd, array_types.py for current data formats.

-------------------------------------------------------------------------
FUNCTIONS:

    Position correlations:
        
    1) get_position_alpha_spectrum(data_table, recoil=True, dlt_t=20):
    Get 2d position distribution for correlated recoil-alpha
    or alpha-alpha events on detector's strips
    as np.array [front strips 0..47, back strips 0..127].
    
    2) get_position_R_SF_spectrum(data_table, dlt_t=20):
    
    Get 2d position distribution for correlated recoil-fission
    or alpha-fission events on detector's strips
    as np.array [front strips 0..47, back strips 0..127].
    
    Energy, time correlations:
    
    1) get_a_a_energies(data_table,  \
                     recoil=False, dlt_t=20):
    
    Get energies for correlated recoil-alpha
    or alpha-alpha events on detector's strips
    as (a1_energies, a2_energies).
    
    2) get_time_distribution(data_table, recoil=False, dlt_t=20,
          energy_first=9260., dlt_e1=60.,
          energy_second=8700., dlt_e2=60.):
    
    Get time distribution for correlated recoil-alpha
    or alpha-alpha events on detector's strips. Extracted data can be used
    to estimate decay time of isotopes with given energies.
      
    correlation_functions.pyx functions:
    1) cf.find_a_a(np.ndarray[DATA_EVENT, cast=True] data_table, \
                        c_bool recoil=False, long dlt_t=20) -  
        Find indexes for Recoil-alphas or alpha-aplhas time correlated events.
    
    2) cf.find_fission(data_table, x, y, time=0) - find all data event which 
        occured after given time in given cell (x, y).
        return data_table
    
    3) cf.find_R_SF(np.ndarray[DATA_EVENT, cast=True] data_table, \
                 long dlt_t=20)
        Find indexes for Recoil-alphas - self-fission correlations
------------------------------------------------------------------------

CLASSES:
    
    1) EESpectrum
    
    The class provide opportunity to build
    2d energy distribution for correlated recoil-alpha
    or alpha-alpha events on detector's strips
    
    Initialization:
    I. Build from the data table. (default initialization)
        data_table: see DESCRIPTION for format
        recoil - True to search R-a correlations, False for a-a
        dlt_t - time window, mks    
    II. Raw initialization.
        data: zip(energies_alpha1, energies_alpha_2)
        configure: see Properties

    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type': event_type, 'scale': scale, 
                   'tof': tof, 'energy_scale': energy_scale, 'shape': shape) 
        
    Methods:
        1. Base methods: + - =
        2. show - visualize data. 
            It can be called to show 1-strip distribution like show(strip_num)
            or the whole 2d strip-bins distributions (without args).
        3. Reading and writing to file: tofile, fromfile. 
           (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data 
           so it works well with pd.DataFrame
    
    2) PositionAASpectrum
            
    The class provide opportunity to build
    2d position distribution for correlated recoil-alpha
    or alpha-alpha events on detector's strips
    
    Initialization:
    I. Build from the data table. (default initialization)
        data_table: see DESCRIPTION for format
        recoil - True to search R-a correlations, False for a-a
        dlt_t - time window, mks    
    II. Raw initialization. 
        data_table
        bins: np.ndarray
        configure: dict
    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type': event_type, 'scale': scale, 
                   'tof': tof, 'energy_scale': energy_scale, 'shape': shape) 
        
    Methods:
        1. Base methods: + - =
        2. show - visualize data. 
            It can be called to show 1-strip distribution like show(strip_num)
            or the whole 2d strip-bins distributions (without args).
        3. Reading and writing to file: tofile, fromfile. 
           (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data 
           so it works well with pd.DataFrame
    
    3) PositionRFSpectrum
    
    The class provide opportunity to build
    2d position distribution for correlated recoil-fission
    or alpha-fission events on detector's strips
    
    Initialization:
    I. Build from the data table. (default initialization)
        data_table: see DESCRIPTION for format
        dlt_t - time window, mks    
    II. Raw initialization. 
        data_table
        bins: np.ndarray
        configure: dict
    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type': event_type, 'scale': scale, 
                   'tof': tof, 'energy_scale': energy_scale, 'shape': shape) 
    Methods:
        1. Base methods: + - =
        2. show - visualize data. 
            It can be called to show 1-strip distribution like show(strip_num)
            or the whole 2d strip-bins distributions (without args).
        3. Reading and writing to file: tofile, fromfile. 
           (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data 
           so it works well with pd.DataFrame

"""
#### CORRELATED SPECTRUM FUNCTIONS
#### spectrum functions           
def get_position_alpha_spectrum(data_table, recoil=True, dlt_t=20):
    '''
    Get 2d position distribution for correlated recoil-alpha
    or alpha-alpha events on detector's strips
    as np.array [front strips 0..47, back strips 0..127].
    Input:
        data_table: see DESCRIPTION for format
        recoil - True to search R-a correlations, False for a-a
        dlt_t - time window, mks
    Return:
        spectrum (np.ndarray)[1 .. front_strip_num, 1 .. back_strip_num],
        bins (np.ndarray) front_strip_array, 
        configure (dict: 'event_type': 'position', 'scale': 'fission', \
                   'tof': 'False', 'energy_scale': 'True', 'shape': shape) 
    
    Hint: use then np.sum(result, axis=0) to get 1d back strip distribution
    and np.sum(result, axis=1) to get 1d front strip distribution.
    
    '''
    # init
    shape = (sp.event_type_dict['front'][1], sp.event_type_dict['back'][1]) # 48 x 128
    bins = np.arange(1, shape[0] + 1)
    spectrum = np.zeros(shape, dtype=int)
    tof = True if recoil else 'all'
    configure = {'event_type': 'position', 'scale': 'alpha', \
                 'tof': tof, 'energy_scale': 'True', 'shape': shape} 
    
    # find correlations
    indexes = cf.find_a_a(data_table, recoil, dlt_t)
    if indexes is not None:
        indexes = indexes[::2]
    else:
        print('No correlations were found')
        return spectrum, bins, configure # lazy cheat fix for empty files   
    
    x_sample = data_table[indexes]['cell_x']
    y_sample = data_table[indexes]['cell_y']
    
    # calculate 2D histogram    
    try:
        spectrum = qm.calc_2d_int_spectrum(x_sample, 
                                           y_sample, spectrum)
        spectrum = np.asarray(spectrum)
    except IndexError as e:
        print('Error! ', e)
        
    return spectrum, bins, configure    

def get_position_R_SF_spectrum(data_table, dlt_t=20):
    '''
    Get 2d position distribution for correlated recoil-fission
    or alpha-fission events on detector's strips
    as np.array [front strips 0..47, back strips 0..127].
    Input:
        data_table: see DESCRIPTION for format
        recoil - True to search R-a correlations, False for a-a
        dlt_t - time window, mks
    Return:
        spectrum (np.ndarray)[1 .. front_strip_num, 1 .. back_strip_num],
        bins (np.ndarray) front_strip_array,        
        configure (dict: 'event_type': 'position', 'scale': 'fission', \
                   'tof': 'False', 'energy_scale': 'True', 'shape': shape) 
    
    Hint: use then np.sum(result, axis=0) to get 1d back strip distribution
    and np.sum(result, axis=1) to get 1d front strip distribution.
    
    '''
    # init
    shape = (sp.event_type_dict['front'][1], sp.event_type_dict['back'][1])
    bins = np.arange(1, shape[0] + 1)
    spectrum = np.zeros(shape, dtype=int)
    configure = {'event_type': 'position', 'scale': 'fission', \
                 'tof': 'False', 'energy_scale': 'True', 'shape': shape} 
    
    # find correlations
    indexes = cf.find_R_SF(data_table, dlt_t)
    if indexes is not None:
        indexes = indexes[::2]
    else:
        print('No correlations were found')
        return spectrum, bins, configure # lazy cheat fix for empty files  
    
    x_sample = data_table[indexes]['cell_x']
    y_sample = data_table[indexes]['cell_y']
    
    try:
        spectrum = qm.calc_2d_int_spectrum(x_sample, 
                                           y_sample, spectrum)
        spectrum = np.asarray(spectrum)
    except IndexError as e:
        print('Error! ', e)
        
    return spectrum, bins, configure 

def get_a_a_energies(data_table,  \
                     recoil=False, dlt_t=20):
    '''
    Get energies for correlated recoil-alpha
    or alpha-alpha events on detector's strips
    as (a1_energies, a2_energies).
    Input:
        data_table: see DESCRIPTION for format
        recoil - True to search R-a correlations, False for a-a
        dlt_t - time window, mks
    Return:
        (a1_energies, a2_energies), # a1_energies np.ndarray double # for scatter plot
        configure
    
    Hint: use then np.sum(result, axis=0) to get 1d back strip distribution
    and np.sum(result, axis=1) to get 1d front strip distribution.
    
    '''
    # init
    shape = 2 
    tof = True if recoil else 'all'
    configure = {'event_type': 'energy2D', 'scale': 'alpha', \
                 'tof': tof, 'energy_scale': 'True', 'shape': shape} 
    
    # find correlations
    indexes = cf.find_a_a(data_table, recoil, dlt_t)
    if indexes is None:
        print('No correlations were found')
        return (None, None), configure # lazy cheat fix for empty files   
    
    x_sample = data_table[indexes[::2]]['energy_front'] # first correlated events, recoils
    y_sample = data_table[indexes[1::2]]['energy_front']# second correlated events, alphas
        
    return (x_sample, y_sample), configure 

def get_time_distribution(data_table, recoil=False, dlt_t=20,
                          energy_first=9260., dlt_e1=60.,
                          energy_second=8700., dlt_e2=60.):
    '''
    Get time distribution for correlated recoil-alpha
    or alpha-alpha events on detector's strips. Extracted data can be used
    to estimate decay time of isotopes with given energies.
    Input:
        data_table: see DESCRIPTION for format
        recoil - True to search R-a correlations, False for a-a
        dlt_t - time window, mks
        energy_first - energy of the first isotope, KeV
        dlt_e1 - KeV, the algorithm will search in (e +- dlt_e) energy window
        energy_second - energy of the descendant isotope, KeV
        dlt_e2 - KeV, the algorithm will search in (e +- dlt_e) energy window
    Return:
        spectrum (np.ndarray)[1 .. 5*dlt_t],
        configure
    
    '''
    # init
    shape = dlt_t * 5 
    if dlt_t > 60:
        bins = np.asarray(np.linspace(1, shape, 60), dtype=int)
    else:
        bins = np.arange(1, shape + 1)
    tof = True if recoil else 'all'
    configure = {'event_type': 'time', 'scale': 'alpha', \
                 'tof': tof, 'energy_scale': 'True', 'shape': shape} 
    
    data_table = data_table[
                    ((data_table['energy_front'] > energy_first - dlt_e1) &
                     (data_table['energy_front'] < energy_first + dlt_e1)) |
                    ((data_table['energy_front'] > energy_second - dlt_e2) &
                     (data_table['energy_front'] < energy_second + dlt_e2)) 
                    ]
    # find correlations
    indexes = cf.find_a_a(data_table, recoil, dlt_t)
    if indexes is None:
        print('No correlations were found')
        return np.zeros(bins.shape, dtype=int), bins, configure # lazy cheat fix for empty files   
    
    times = data_table[indexes[1::2]]['time_micro'] - \
            data_table[indexes[::2]]['time_micro']
            
    # calc histogram 
    spectrum, bins = qm.calc_histogram(times, bins) # calc histograms for each strip
     
    return np.asarray(spectrum), bins, configure

#### CLASSES

class EESpectrum(sp.SpectrumBase):
    """
    The class provide opportunity to build
    2d energy distribution for correlated recoil-alpha
    or alpha-alpha events on detector's strips
    
    Initialization:
    I. Build from the data table. (default initialization)
        data_table: see DESCRIPTION for format
        recoil - True to search R-a correlations, False for a-a
        dlt_t - time window, mks    
    II. Raw initialization.
        data: zip(energies_alpha1, energies_alpha_2)
        configure: see Properties

    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type': event_type, 'scale': scale, 
                   'tof': tof, 'energy_scale': energy_scale, 'shape': shape) 
        
    Methods:
        1. Base methods: + - =
        2. show - visualize data. 
            It can be called to show 1-strip distribution like show(strip_num)
            or the whole 2d strip-bins distributions (without args).
        3. Reading and writing to file: tofile, fromfile. 
           (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data 
           so it works well with pd.DataFrame
   
    """

    def __init__(self, data_table=None, dlt_t=20000, 
                 data=None, configure=None,**argv):
        if (data is None) and (configure is None):
            data, configure = get_a_a_energies(data_table, dlt_t)
        self.data_e1, self.data_e2 = data
        self.configure = configure

    def __add__(self, other):
        self._typecheck(other)
        self.data_e1 += other.data_e1
        self.data_e2 += other.data_e2
        return type(self)(data=list(zip(self.data_e1, self.data_e2)), \
                          configure=self.configure)

    def __getitem__(self, index):
        return (self.data_e1[index], self.data_e2[index])

    def __len__(self):
        return len(self.data_e1)

    def __eq__(self, other):
        try:
            self._typecheck(other)
            return np.all(self.data_e1 == other.data_e1) &\
                   np.all(self.data_e2 == other.data_e2)
        except sp.WrongSpectrum as e:
            print(e)
            return False
        
    def show(self):
        x, y = self.data_e2, self.data_e1
        
        # definitions for the axes
        left, width = 0.1, 0.65
        bottom, height = 0.1, 0.65
        spacing = 0.005
        
        
        rect_scatter = [left, bottom, width, height]
        rect_histx = [left, bottom + height + spacing, width, 0.2]
        rect_histy = [left + width + spacing, bottom, 0.2, height]
        
        # start with a rectangular Figure
        plt.figure(figsize=(8, 8))
        
        ax_scatter = plt.axes(rect_scatter)
        ax_scatter.tick_params(direction='in', top=True, right=True)
        ax_histx = plt.axes(rect_histx)
        ax_histx.tick_params(direction='in', labelbottom=False)
        ax_histy = plt.axes(rect_histy)
        ax_histy.tick_params(direction='in', labelleft=False)
        
        # the scatter plot:
        ax_scatter.scatter(x, y)
        
        # now determine nice limits by hand:
        binwidth = 0.25
        lim = np.ceil(np.abs([x, y]).max() / binwidth) * binwidth
        ax_scatter.set_xlim((-lim, lim))
        ax_scatter.set_ylim((-lim, lim))
        
        bins = np.arange(-lim, lim + binwidth, binwidth)
        ax_histx.hist(x, bins=bins)
        ax_histy.hist(y, bins=bins, orientation='horizontal')
        
        ax_histx.set_xlim(ax_scatter.get_xlim())
        ax_histy.set_ylim(ax_scatter.get_ylim())
        
        # set titles
        ax_scatter.set_xlabel('Energy of second alpha (descendant), KeV')
        ax_scatter.set_ylabel('Energy of first alpha (recoil or ancestor), KeV')
        
        plt.show()
        
class PositionAASpectrum(sp.PositionSpectrum):          
    '''
    The class provide opportunity to build
    2d position distribution for correlated recoil-alpha
    or alpha-alpha events on detector's strips
    
    Initialization:
    I. Build from the data table. (default initialization)
        data_table: see DESCRIPTION for format
        recoil - True to search R-a correlations, False for a-a
        dlt_t - time window, mks    
    II. Raw initialization. 
        data_table
        bins: np.ndarray
        configure: dict
    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type': event_type, 'scale': scale, 
                   'tof': tof, 'energy_scale': energy_scale, 'shape': shape) 
        
    Methods:
        1. Base methods: + - =
        2. show - visualize data. 
            It can be called to show 1-strip distribution like show(strip_num)
            or the whole 2d strip-bins distributions (without args).
        3. Reading and writing to file: tofile, fromfile. 
           (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data 
           so it works well with pd.DataFrame
   
    '''
    
    def __init__(self, data_table=None, recoil=True, dlt_t=20, 
                 data=None, bins=None, configure=None,**argv):
                
        if (data is None) and (bins is None) and (configure is None):
            data, bins, configure = \
                get_position_alpha_spectrum(data_table, recoil, dlt_t)
            
        super(sp.PositionSpectrum, self).__init__(data, bins, configure)
        self.data.index = np.arange(1, self.data.shape[0] + 1)
        
class PositionRFSpectrum(sp.PositionSpectrum):          
    '''
    The class provide opportunity to build
    2d position distribution for correlated recoil-fission
    or alpha-fission events on detector's strips
    
    Initialization:
    I. Build from the data table. (default initialization)
        data_table: see DESCRIPTION for format
        dlt_t - time window, mks    
    II. Raw initialization. 
        data_table
        bins: np.ndarray
        configure: dict
    Properties:
        data (pd.DataFrame)
        bins (np.ndarray),
        configure (dict: 'event_type': event_type, 'scale': scale, 
                   'tof': tof, 'energy_scale': energy_scale, 'shape': shape) 
    Methods:
        1. Base methods: + - =
        2. show - visualize data. 
            It can be called to show 1-strip distribution like show(strip_num)
            or the whole 2d strip-bins distributions (without args).
        3. Reading and writing to file: tofile, fromfile. 
           (use cPickle for saving objects)
        4. One can apply any numpy or scipy function directly to object.data 
           so it works well with pd.DataFrame
        
    '''
    
    def __init__(self, data_table=None, dlt_t=20000, 
                 data=None, bins=None, configure=None,**argv):
                
        if (data is None) and (bins is None) and (configure is None):
            data, bins, configure = \
                get_position_R_SF_spectrum(data_table, dlt_t)
            
        super(sp.PositionSpectrum, self).__init__(data, bins, configure)
        self.data.index = np.arange(1, self.data.shape[0] + 1)
 