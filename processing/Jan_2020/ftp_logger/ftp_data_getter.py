#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 14 12:50:28 2020
FTP-client for uploading analog data files and data log with 
creating dates to use for processing.
@author: eastwood
"""

import os
import re
import datetime
from ftplib import FTP, error_perm

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from data_processingGNS2021.processing_examples.read_data.read_protocol \
import read_protocol
from bisect import bisect

def get_login_password():
    login = input('LOGIN: ')
    password = input('PASSWORD: ')
    return login, password

month_dict = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 
              'May': '05', 'June': '06', 'July': '07', 'Aug': '08', 
              'Sept': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}

def get_timestamp(log_files_frame):
     result = []
     lf = log_files_frame
     now =datetime.datetime.now()
     year = now.year
     for month, day, daytime in zip(lf['Month'], lf['day'], lf['daytime']):
         day = str(day)
         if len(day) == 1:
             day = '0' + day
             
         month = month_dict[month]
         line = "%s %s %s %s" % (year, month, day, daytime)
         result.append(line)
     timestamps = pd.to_datetime(result, format="%Y %m %d %H:%M")
     return timestamps
 
def show_magnet_currents(log_frame):
    fig, ax = plt.subplots()
    lf = log_frame
    columns =  ['D1', 'D2', 'Q1', 'Q2', 'Q3']  
    for name in columns:
        lf[name].plot(ax=ax, label=name)
    plt.title("Magnet currents during time")
    plt.ylabel("Current, A")
    plt.legend(loc='upper left')
    plt.show()
    
def get_log_files_frame():
    with open("DIMA.txt", "r") as f:
        a = f.readlines()
    lines = a[1::2]
    
    filenames = []
    timestamps = []
    for line in lines:
        t0, t1, name = line.split()
        timeline = t0 + ' ' + t1
        # remove microseconds
        pattern = '\d{1,2}:\d{1,2}:\d{1,2}(\.?\d*)'
        microseconds = re.findall(pattern, timeline)
        microseconds = microseconds[0] if microseconds else ''
        k = len(microseconds)
        if k > 0:
            timeline = timeline[:-k]
        timestamp = pd.to_datetime(timeline, format="%Y.%m.%d %H:%M:%S")
        timestamps.append(timestamp)
        filenames.append(name)
    
    log_files_frame = pd.DataFrame({
        'filename': filenames,
        'Time_start': timestamps,
    })
    return log_files_frame
    
if __name__ == '__main__':
    # constants
    DATA_PATH_analog = "data/DIMA/PbCa"
    DATA_PATH_analog_local = '/home/eastwood/gitlab_projects/data/analogue/Jan_2020'
    FILE_PREFIX = 'PbCa'
    
#### LOGIN
    login, password = get_login_password()    
    ftp = FTP("159.93.82.6")
    ftp.login(login, password)    
    
#### GET PROTOCOL FILE
    ftp.cwd("kontrol/protocol")
    try:
        filename = "protocol.txt"
        with open(filename, 'wb') as local_file:
            ftp.retrbinary('RETR ' + filename, local_file.write)
    except error_perm:
        print("Error while downloading %s" % (filename, ))
        
    ftp.cwd("../..")
    
#### GET DATA LOG
    # upload DIMA.txt - log with time_start, time_stop data for each file
    ftp.cwd("data/LEOEXPRESS")
    try:
        filename = "DIMA.txt"
        with open(filename, 'wb') as local_file:
            ftp.retrbinary('RETR ' + filename, local_file.write)
    except error_perm:
        print("Error while downloading %s" % (filename, ))
    ftp.cwd("../..")
    log_files_frame = get_log_files_frame()
    
    # JOIN data from protocol.txt and 'log_dirPbCa.txt' and 
#### CREATE ANALOG LOG FILE 
#    header = 'Filename,Q1,Q2,Time_start,adc_fails'.split(',')
    protocol_frame = read_protocol(".")
    log_frame = pd.DataFrame(
                {"Filename": log_files_frame['filename'],
                 "Time_start": log_files_frame['Time_start'],
                 })
    log_frame['adc_fails'] = np.zeros(len(log_frame), dtype=np.bool)
    log_frame.index = log_frame['Time_start']
    log_frame.sort_index(inplace=True)
    get_time_limits = lambda df: (df['time'].iloc[0], 
                                  df['time'].iloc[-1])
    time_min, time_max = get_time_limits(protocol_frame)  
    check_time_fits_protocol = lambda time, t_min, t_max: \
                               (time >= t_min) & (time <= t_max)
                               
    data_collection = {'D1': [], 'D2': [], \
                       'Q1': [], 'Q2': [], 'Q3': []}
    for item in log_frame.itertuples():
        time = item.Time_start
        assert check_time_fits_protocol(time, time_min, time_max),\
            "Protocol doesn't contain time: (%s) for file: (%s)" % \
            (item.Time_start, item.Filename)
        ind = bisect(protocol_frame['time'], time)
        p_line = protocol_frame.iloc[ind]
        for key in list(data_collection.keys()):
            data_collection[key].append(p_line[key])
    
    for key in list(data_collection.keys()):
        log_frame[key] = data_collection[key]
     
    del log_frame['Time_start']
    log_frame.to_csv('log_analog.txt')
    show_magnet_currents(log_frame)

#### UPLOAD FILES to local data folder
#    uploaded_files = set(os.listdir(DATA_PATH_analog_local))
#    new_files = set(log_files_frame['filename']) - uploaded_files
#    new_files = ['PbCa.90', 'PbCa.91', 'PbCa.92']
#    for filename in new_files:
#        host_file = os.path.join(
#            DATA_PATH_analog_local, filename
#        )
#        
#        try:
#            with open(host_file, 'wb') as local_file:
#                ftp.retrbinary('RETR ' + filename, local_file.write)
#        except error_perm:
#            print "Error while downloading %s" % (filename, )
    
    ftp.close()