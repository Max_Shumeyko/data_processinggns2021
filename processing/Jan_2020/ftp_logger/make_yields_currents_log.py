#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 15:59:17 2019

Составления лога, включающего статистику коррелированных событий:
Recoil-alpha, Recoil-Fission, количество альфа-событий и осколков деления
alphas, Fissions., а также лог токов пучка I7 и токов магнитов в 
отдельном окне.


Входные данные:
    * файлы с данными: PbCa.33, ...
      см. DATA_PATH_analog, DATA_PATH_digital
    * файлы с калибровочными коэффициентам для передних стрипов (см. <>/tools/spectrum.py)
      см. COEFS_FOLDER_analog и COEFS_FOLDER_digital
    '/home/eastwood/gitlab_projects/data/calibration_coefficients/Oct_2019'
    * файлы-таблицы files_Q1_Q2.txt и files_Q1_Q2_digital.txt
      |Filename:str|D1:float(ток магнита)|D2:float (ток магнита)|Time_start(время
      начала записи файла в формате "Year-month-day Hour-minute-second")|
      |adc_fails:bool (если файл был остановлен при вылете adc)
      ~/gitlab_projects/data_processingGNS2021/processing/tables
    * файл-протокол с данными по токам: 23.11.19.xlsx, ...
        "~/gitlab_projects/data_processingGNS2021/processing/Jan_2020/calculations/protocol.txt"
        
Выходные данные:
    * графики распределения удельного выхода particle_yield("Counts/I_beam", time[min]) 
    * функция и график гиперплоскости удельного выхода частиц от токов 
        магнитов: F_o(Mean("Counts/I_beam"), Magnet1[A], Magnet2[A])
        
@author: eastwood

"""
import os


from datetime import datetime as dtt

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.interpolate import interp1d, splrep, splev 
from scipy.integrate import quad

from data_processingGNS2021.processing.Jan_2020.calculations.parabola_fit\
    import robust_parabola_fit, parabola

from data_processingGNS2021.processing_examples.read_data.read_protocol \
    import read_protocol
# read functions data table
from data_processingGNS2021.processing_examples.read_data.read_functions \
    import read_file_, mapper_ 
from data_processingGNS2021.tools.read_file_2019 import get_data_table as read_dt_a
from data_processingGNS2021.tools.read_digital_2019 import read_data_table as read_dt_d
from data_processingGNS2021.tools.correlations_functions import find_chains

class record:
    pass

#### PREPARE DATA    
def get_log_file(filename):
    """filename: str -> pd.DataFrame
    
    Read files_{Magnet_1}_{Magnet_2}.txt and build data frame with columns
     |Filename:str|D1:float(ток магнита)|D2:float(ток магнита)|
     |Time_start(время начала записи файла в формате 
     "Year-month-day Hour-minute-second")|adc_fall:fall (если файл был
     остановлен при вылете adc)
    """
    to_datetime = lambda line: dtt.strptime(line, "%Y-%m-%d %H:%M:%S")
    
    df = pd.read_csv(filename)
    df['Time_start'] = list(map(to_datetime, df['Time_start']))
    return df

def data_generator(filenames, mapper):
    for filename, data_table in zip(filenames, mapper(filenames)):
        yield os.path.split(filename)[1], data_table
    
def data_generator_analog(filenames):
    """[str] -> generator (filename, data_table)
    
    Read data tables from given analog filenames list.
    Yields (filename, data_table)
    """
    read_file_dt = lambda filename, **argv: read_file_(filename, read_dt_a,\
                       coefs_folder=COEFS_FOLDER_analog,                                
                       serialize=False,
                       **argv) # return frame, filesize

    mapper_dt_a = lambda filenames: mapper_(filenames, read_file_dt,\
                                            prefix='gns. ',
                                            data_type='data_table')
    return data_generator(filenames, mapper_dt_a)

def data_generator_digital(filenames):
    """[str], pd.DataFrame -> [(pd.DataFrame, int, int)]
    
    Read data tables from given digital filenames list and calculate time interval
    for each using start times from log_frame (see read_protocol).
    
    Yields (filename, data_table)
    """
    read_file_dt = lambda filename, **argv: read_file_(filename, read_dt_d,\
                coefs_folder=COEFS_FOLDER_digital, 
                serialize=False,
                **argv) # return frame, filesize

    mapper_dt_d = lambda filenames: mapper_(filenames, read_file_dt,\
                                            data_type='data_table')
    return data_generator(filenames, mapper_dt_d)

def get_datagen(data_folder, data_type, 
                  analog_name='PbCa.', digit_name='shdat'):
    """Yields (filename, data_table)"""
    filenames = os.listdir(data_folder)
    name_part = analog_name if data_type == 'analog' else digit_name
    filenames = [fname for fname in filenames if name_part in fname]
    
    get_full_path=lambda filename: os.path.join(data_folder, filename)
    filenames = list(map(get_full_path, filenames))
    
    return data_generator_analog(filenames) if data_type == 'analog'\
            else data_generator_digital(filenames)

def generate_time_borders(data_tables_generator, log_frame):
    """Calculate time interval
    for each using start times from log_frame (see read_protocol).
    
    Yields (data_table, time_start, time_stop)
    """
    for filename, data_table in data_tables_generator:
        try:
            time_start = log_frame\
              .loc[log_frame['Filename'] == filename, 'Time_start']\
              .iloc[0]
        except KeyError as e:
            print("Not such field %s in the log frame" % (filename,))
            print(e)
            continue
        except IndexError as e:
            print("Not such file %s in the log frame" % (filename,))
            print(e)
            continue    
        dt_time = data_table['time_micro'][-1] - data_table['time_micro'][0]
        print("File time length (hours): ", \
            round(dt_time // 10**6 / 3600., 1))
        time_stop = time_start + pd.offsets.Second(dt_time // 10**6)
        yield data_table, time_start, time_stop

#### CALCULATIONS
#def get_beam_integral(protocol_frame, time_start, time_stop):
#    """
#    pd.DataFrame, pd.Timestamp, pd.Timestamp -> float
#    
#    Calculate beam current integral for given time interval.
#    BC_integral = sum(delta_times * beam_currents); delta_times in seconds!
#    
#    """
#    pf = protocol_frame
#    t1, t2 = time_start, time_stop
#    subframe = pf.loc[(pf['time'] >= t1) & (pf['time'] <= t2)]
#    time_pre = subframe['time'].iloc[0]
#    delta_t = time_pre - t1
#    integral = delta_t.total_seconds() * subframe['beam_current'].iloc[0]
#    
#    for _, line in subframe.iloc[1:].iterrows():
#        delta_t = line['time'] - time_pre
#        time_pre = line['time']
#        integral += delta_t.total_seconds() * line['beam_current']
#        
#    delta_t = t2 - line['time']
#    integral += delta_t.total_seconds() * line['beam_current']
#    return integral

def line_function(x0, x1, y0, y1):
    return lambda x: y0 + (y1 - y0) * (x - x0) / (x1 - x0)

def get_bc_array(seconds_array, beam_current_pre, line):
    """Calculates beam current array for given time interval (seconds_array)
    by linear approximation of two points:
        (t_start, beam_current_start), (t_stop, beam_current_stop),
          where N = len(seconds_array)
        
    [int], float, pd.Series -> [float] 
    
    """
    t_start, t_stop = seconds_array[0], seconds_array[-1]
    bc_start, bc_stop = beam_current_pre, line['beam_current']
    
    line = line_function(t_start, t_stop, bc_start, bc_stop)
    return list(map(line, seconds_array))

def get_beam_integral(protocol_frame, time_start, time_stop):
    """
    pd.DataFrame, pd.Timestamp, pd.Timestamp -> float
    
    Calculate beam current integral for given time interval.
    BC_integral = integral(beam_current)dt; 
        delta_times in seconds!
    
    """
    pf = protocol_frame
    t1, t2 = time_start, time_stop
    subframe = pf.loc[(pf['time'] >= t1) & (pf['time'] <= t2)]
    time_pre = subframe['time'].iloc[0]
    beam_current_pre = subframe['beam_current'].iloc[0]
    delta_t = time_pre - t1
    integral = delta_t.total_seconds() * subframe['beam_current'].iloc[0]
    
    for _, line in subframe.iloc[1:].iterrows():
        delta_t = line['time'] - time_pre        
        
        # calculate beam current (bc) integral on time interval using 
        # quadratic intergration of
        # linear interpolation function on given interval bc_start, bc_stop        
        seconds_array = np.arange(int(delta_t.total_seconds())) # 0 .. total_sec, dlt_t = 1 sec
        bc_array = get_bc_array(seconds_array, 
                                beam_current_pre, line)
        interpolation_line = interp1d(seconds_array, bc_array)
        integral += quad(interpolation_line,
                         seconds_array[0], seconds_array[-1])[0]
        time_pre = line['time']
        beam_current_pre = line['beam_current']
        
    delta_t = t2 - line['time']
    integral += delta_t.total_seconds() * line['beam_current']
    return integral
        
def calc_len_chains(chains):
    """Calculate number of founded correlations.
    See search_chains functions.
    
    """
    len_ = 0
    for chain in list(chains.values()):
        len_ += len(chain)
    return len_

def search_Ra(data_table):
    """Count recoil-alpha correlations for No252 isotopes with energies
    in range [8300, 8680] KeV.
    
    """
    search_properties = record()

    #### FIND R-a (recoil-alpha pairs)
    search_properties.time_dlt = 12000000 # microseconds -> 12 seconds
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_first = True
    search_properties.search_fission = False
    
    energy_bundle = [(2000, 25000, 'R'), (8300, 8680, 'a'), ]
    
    chains = find_chains(data_table, energy_bundle, search_properties)
    return calc_len_chains(chains)

def search_RF(data_table):
    """Count recoil-fission correlations for No252 isotopes."""
    search_properties = record()

    search_properties.time_dlt = 12000000 # microseconds -> 12 seconds
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_first = True
    search_properties.search_fission = True
    
    energy_bundle = [(5000, 25000, 'R'), (None, None, 'F'),]
    chains = find_chains(data_table, energy_bundle, search_properties)
    return calc_len_chains(chains)
    
def search_correlations(data_table):
    """
    pd.DataFrame -> int, int, int, int
    
    Calculate number of R-alpha, R-fission, alpha particles and fission 
    decays in given data table.
    
    """
    Ra_count = search_Ra(data_table)
    RF_count = search_RF(data_table)
    
    dt = data_table
    a_count = np.sum((dt['energy_front'] >= 8300) & 
                (dt['energy_front'] <= 8680) & (~dt['tof']))
    F_count = np.sum(dt['energy_front'] > 50000)
    return Ra_count, RF_count, a_count, F_count

def calculate_output(data_generator, protocol_frame):
    """Calculate recoil-alpha, recoil-fission, alpha and fission event 
    numbers provided by No252 decay modes and beam current integral
    (Ra_count, RF_count, a_count, F_count, integral) 
    for each hour and extract magnet currents for it using
    protocol_frame with experimental data. Then shows result.
    
    """   
    output = {'time': [],
              'Yield_Ra': [], 'Yield_RF': [],
              'Yield_a': [], 'Yield_F': [],
              'Q1': [], 'Q2': [], 'Q3': [],
              'D1': [], 'D2': [],
              'integral': [],
              }
    # processing
    for data_table, time_start, time_stop in data_generator:
        # split by time
        time_begin = 0 # data sample, raw mks
        time_end = data_table['time_micro'][-1] # 5 min
        delta_t = 300 * 10**6 # 5 min
        
        delta_tstamp = pd.offsets.Minute(5)
        time_start_interval = time_start # protocol sample, timestamp
        while time_begin < time_end:
            # get samples                
            ind =  (data_table['time_micro'] >= time_begin) &\
                   (data_table['time_micro'] <= time_begin + delta_t)
            data_sample = data_table[ind]
            
            if time_start_interval + delta_tstamp > time_stop:
                time_stop_interval = time_stop
            else:
                time_stop_interval = time_start_interval + delta_tstamp
            ind_pf = (protocol_frame['time'] >= time_start_interval) &\
                     (protocol_frame['time'] <= time_stop_interval)
            protocol_sample = protocol_frame[ind_pf]            
                
            if (len(data_sample) < 10) or (len(protocol_sample) < 3):
                time_begin += delta_t # for data table, raw microseconds
                time_start_interval += delta_tstamp # for protocol frame, timestamp
                continue
            
            # calculations
            integral = get_beam_integral(protocol_sample, 
                                         time_start_interval, 
                                         time_stop_interval)
            try:
                Ra_count, RF_count, a_count, F_count = \
                    search_correlations(data_sample)
            except AssertionError as e:
                print(e.message)
                print(data_sample[:10])
                print(len(data_sample))
                
                time_begin += delta_t # for data table, raw microseconds
                time_start_interval += delta_tstamp # for protocol frame, timestamp
                continue    
            
            if integral > 0:
                yield_Ra = Ra_count / integral
                yield_RF = RF_count / integral
                yield_a = a_count / integral
                yield_F = F_count / integral
            else:
                yield_Ra, yield_RF, yield_a, yield_F = 0., 0., 0., 0.
            
            # roll times
            time_begin += delta_t # for data table, raw microseconds
            time_start_interval += delta_tstamp # for protocol frame, timestamp
            
            # fill output values
            output['time'].append(time_start_interval)
            output['Yield_Ra'].append(yield_Ra)
            output['Yield_RF'].append(yield_RF)
            output['Yield_a'].append(yield_a)
            output['Yield_F'].append(yield_F)
            output['integral'].append(integral)
            output['Q1'].append(protocol_sample['Q1'].mean())
            output['Q2'].append(protocol_sample['Q2'].mean())
            output['Q3'].append(protocol_sample['Q3'].mean())
            output['D1'].append(protocol_sample['D1'].mean())
            output['D2'].append(protocol_sample['D2'].mean())
    
    df = pd.DataFrame(output)
    df.index = df['time']
    df.sort_index(inplace=True)        
    return df 

### REPORT
FMT_COUNTS = \
"""

DATA TYPE: {0:<10s}; 
Integral = integral(I(time))d(time); I - beam current [A] per second
Number recoil-alphas: {1:<6d}; 
Number recoil-fissions: {2:<6d}; 
Number alphas: {3:<6d}; 
Number fissions: {4:<6d};
integral = {5:<8.2f}

"""

FMT_YIELDS = \
"""DATA TYPE: {0:<10s};
YIELD = sum(events) / integral(I(time))d(time); I - beam current [A] per second
Yield recoil-alphas: {1:<6.5f}; 
Yield recoil-fissions: {2:<6.5f}; 
Yield alphas: {3:<6.5f}; 
Yield fissions: {4:<6.5f}

"""

def make_report(data_type, magnets_group, magnets, Ra_count, RF_count, a_count, 
         F_count, integral, output_filename):
    """Make report output, plot, print and write to file."""
    print("MAGNETS GROUP: ", magnets_group)
    print(FMT_COUNTS.format(data_type.upper(), Ra_count, RF_count,
                            a_count, F_count, integral))
    yield_Ra = Ra_count / integral
    yield_RF = RF_count / integral
    yield_a = a_count / integral
    yield_F = F_count / integral
    print(FMT_YIELDS.format(data_type.upper(), yield_Ra, yield_RF, 
                            yield_a, yield_F))
    
    FMT_HEADER = "data_type,{0},{1},yield_Ra,yield_Rf,yield_a,yield_F\n"\
                 .format(*magnets)
                
    first_open = not os.path.isfile(output_filename) 
    with open(output_filename, 'a') as f:
        if first_open:
            f.write(FMT_HEADER)
        magnet_current1, magnet_current2 = magnets_group
        parameters = (data_type, magnet_current1, magnet_current2, 
                      yield_Ra, yield_RF, yield_a, yield_F)
        
        line = ','.join(str(item) for item in parameters)
        f.write(line + '\n')

def read_magnets_report(folder='.', filename='magnets_report.txt'):
     if os.path.isfile(filename):
         frame = pd.read_csv(filename)
         frame = frame.drop_duplicates()
         return frame
     raise IOError("No such file: %s" % filename)     

def plot_magnet_currents(report_frame, magnets, data_type, yield_types):
    """Plots several yield lines (Ra, RF, a, F) for each magnet type.
    Plot: "Magnet current, A" / "Yield, (A*s)^-1"
    
    """   
    for magnet in magnets:
        fig, ax = plt.subplots()
        ax.set_title("Yields on %s magnet;\nData sourse: %s" % 
                     (magnet, data_type.upper())
                     )
        ax.set_xlabel("Magnet current, A")
        ax.set_ylabel("Yield, events/ (A * second)")
        for yield_name in list(yield_types.keys()):                        
            currents = report_frame[magnet]
            yields = report_frame[yield_name]
            ax.plot(currents, yields, 'x', label=yield_types[yield_name])
            

#            # plot spline interpolation
#            #spline = interp1d(currents, yields, kind='cubic')#'quadratic')#splrep(currents, yields)
#            spline = splrep(currents, yields, k=4, s=0)
#            x_array = np.linspace(min(currents), max(currents), 100)
#            #s_yields = spline(x_array)
#            s_yields = splev(x_array, spline)
#            ax.plot(x_array, s_yields, '--', 
#                    alpha=0.6,
#                    label=yield_types[yield_name] + "_spline")
            
            # plot parabola fit
            coef, std_ = robust_parabola_fit(currents, yields)
            x_array = np.linspace(min(currents), max(currents), 100)
            s_yields = parabola(coef, x_array)
            ax.plot(x_array, s_yields, '--', 
                    alpha=0.6,
                    label=yield_types[yield_name] + " robust porabola fit")
            
            # find max
            ind_max = np.argmax(s_yields)
            x_max, y_max = x_array[ind_max], s_yields[ind_max]
            ax.plot([x_max, ], [y_max, ], 'D',)
            
        ax.legend()
 
def plot_3d(report_frame, magnets, yield_types):
    """Show 3d yields - currents distributions."""
    if len(magnets) != 2:
        raise ValueError("wrong magnets dimensions!")
        
    magn1, magn2 = magnets
    for yield_type in list(yield_types.keys()):
     
        ind_a = report_frame['data_type'] == 'analog'
        Z_a = report_frame[ind_a][yield_type].values
        X_a = report_frame[ind_a][magn1].values
        Y_a = report_frame[ind_a][magn2].values
        
        ind_d = report_frame['data_type'] == 'digital'
        Z_d = report_frame[ind_d][yield_type].values
        X_d = report_frame[ind_d][magn1].values
        Y_d = report_frame[ind_d][magn2].values

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_title("Yields on magnets: %s;\nYield type: %s" % 
                         (",".join([str(m) for m in magnets]), 
                          str(yield_type))
                    )
        ax.set_xlabel("Magnet current %s, A" % magn1)
        ax.set_xlabel("Magnet current %s, A" % magn2)
        ax.set_zlabel("Yield, events/ (A * second)")
        ax.scatter(X_a, Y_a, Z_a, label="analog")
        ax.scatter(X_d, Y_d, Z_d, label="digital")
        ax.legend()

def show_output(df):
    fig, (ax, ax1, ax2) = plt.subplots(3, 1, sharex=True)
    df['Yield_a'].plot(ax=ax, label="a")
    df['Yield_Ra'].plot(ax=ax, linestyle='-.',label="Ra")
    df['Yield_RF'].plot(ax=ax, linestyle='-.',label="RF")
    df['Yield_F'].plot(ax=ax, label="F")
    df['Q1'].plot(ax=ax1)
    df['integral'].plot(ax=ax2)
    ax.legend()
    ax1.legend()
    ax2.legend()
    plt.show()
    
def show_results():
    report_frame = read_magnets_report()
    rf = report_frame
    
    magnet_names = set(['D1', 'D2', 'Q1', 'Q2', 'Q3'])
    columns = set(rf.columns)
    
    magnets = list(columns & magnet_names)
    data_types = ['analog', 'digital']
    yield_types = {'yield_Ra': 'Yield recoil-alpha correlations', 
                  'yield_Rf': 'Yield recoil-fission correlations', 
                  'yield_a': 'Yield alpha-particles', 
                  'yield_F': 'Yield fission decays'}
    
    for data_type in data_types:
        frame_a = rf[rf['data_type'] == data_type]
        plot_magnet_currents(frame_a, magnets, data_type, yield_types)
    plot_3d(report_frame, magnets, yield_types)
#### MAIN PROCESSING FUNCTION
def process(protocol_frame, output_filename):
    
    global DATA_PATH_analog, DATA_PATH_digital, log_frame_analog, log_frame_digital
    data_source = {
       'analog': (DATA_PATH_analog, log_frame_analog), 
#                   filenames_analog), 
       'digital': (DATA_PATH_digital, log_frame_digital)}#, 
#                    filenames_digital),}
    
    for data_type, (data_path, log_frame) in list(data_source.items()):
        print('DATA_TYPE: %s' % (data_type,))
        # init  
        # yields (filename, data_table)
        data_gen = get_datagen(data_path, data_type) 
        # yields (data_table, time_start, time_stop)
        data_generator = generate_time_borders(data_gen, log_frame)
        # processing
        output =\
            calculate_output(data_generator, protocol_frame)
            
        show_output(output)
        return output

        # show results
#        make_report(data_type, magnets_group, magnets, 
#             Ra_count, RF_count, a_count, 
#             F_count, integral, output_filename) 
#        
if "__main__" in __name__:   
    # get data filenames     
    DATA_PATH_analog = '/home/eastwood/gitlab_projects/data/analogue/Jan_2020'
    DATA_PATH_digital = '/home/eastwood/gitlab_projects/data/digital/Jan_2020'
#    # get calibration coeficients
    COEFS_FOLDER_analog = \
        '/home/eastwood/gitlab_projects/data/calibration_coefficients/Jan_2020'
    COEFS_FOLDER_digital = \
        '/home/eastwood/gitlab_projects/data/calibration_coefficients/Jan_2020_digit'
    
    # get log filename
    # лог файл с токами, именами файлов и временем старта каждого
    LOG_FILE_analog = "/home/eastwood/gitlab_projects/data_processingGNS2021/" +\
               "processing/Jan_2020/calculations/files_Q1_Q2.txt" 
               
    LOG_FILE_digital = "/home/eastwood/gitlab_projects/data_processingGNS2021/" +\
               "processing/Jan_2020/calculations/files_Q1_Q2_digital.txt"
    OUTPUT_FILENAME = 'magnets_report.txt'
    # read data
    log_frame_analog = get_log_file(LOG_FILE_analog)
    log_frame_digital = get_log_file(LOG_FILE_digital)
    protocol_frame = read_protocol('.')

    # NOTE! data reader functions get calibration coefficients from 
    # processing/Jan_2020/read_show/generator_... functions - 
    # see "import mapper_dt" above
    df = process(protocol_frame, '')
#    process(protocol_frame, log_frame_analog, log_frame_digital, 
#            OUTPUT_FILENAME,
#            magnets=['Q1', 'Q2'])
#    
    # results
    #show_results()
        
    
    