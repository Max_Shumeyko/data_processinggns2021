# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 20:07:20 2018

Search for full chains (Recoil-alpha-alpha-...-fission fragment) correlations.

Prepare data table and fission calibration coefficients to start using scripts from 
<project_folder>/data_processing/read_data_table<forma>.py 

Instruction:
    1. Get data table as dt_generator from 
       <project_folder>/data_processing/read_data/generator_<analogue|digital>_2019.py 
    2. Specify parameters for search:
        * time_dlt : <int> time in microseconds between two consecutive events;
        * chain_length_min : <int> minimal length of chains;
        * chain_length_max : <int> maximal length of chains; 
                           If has default value -1 -> the upper limit will set
                           to 50;
        * recoil_first : <c_bool> check if first event in chain is recoil;
    3. Run this script to perform search
    
Output:
    dict with lists of chains:
    dict: [(cell_x, cell_y)] -> [np.ndarray[DATA_EVENT]]
@author: eastwood

"""
import numpy as np

import matplotlib.pyplot as plt
from data_processingGNS2021.tools.correlations_functions import find_chains

class record:
    pass

search_properties = record()

#### FIND R-a (recoil-fission pairs)
search_properties.time_dlt = 15000000
search_properties.chain_length_min = 2
search_properties.chain_length_max = 2
search_properties.recoil_first = True
search_properties.search_fission = True

energy_bundle = [(5000, 25000, 'R'), (None, None, 'F'),] # (8300, 8500, 'a'), 

'''
find_chains(data_table, energy_bundle, search_properties):
    Search time correlated decay chains using search_properties and
    collect them into cell dict.
    Suppose that energies are already filtered (use filter_by_energy).
    
    np.ndarray[DATA_EVENT], [(float)], obj -> 
    -> defaultdict{[cell_x, cell_y): [np.ndarray[DATA_EVENT]]}
    
    Input: 
    data_table (np.ndarray, dtype=DATA_EVENT)
    energy_bundle - list of energy limits in KeV, like  [(6620, 6740), (9200, 9340), ]
    search_properties - search structure with parameters:
        time_dlt : <int> time in microseconds between two consecutive events;
        chain_length_min : <int> minimal length of chains;
        chain_length_max : <int> maximal length of chains; 
                           If has default value -1 -> the upper limit will set
                           to 50;
        recoil_first : <c_bool> check if first event in chain is recoil;
                
    Return: cell dict with list of chains
    
    ***    
    Example:
    --------    
    class record:
        pass

    search_properties = record()
    
    #### FIND R-F (recoil-fission pairs)
    search_properties.time_dlt = 26000000
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_first = True
    search_properties.search_fission = True
    
    energy_bundle = [(5000, 25000, 'R'), (None, None, 'F'),]
        
    dt = next(dt_generator)
    chains = find_chains(dt, energy_bundle, search_properties)
    
    # get data table indexes from given cell
    cell_x = 24
    cell_y = 48
    print chains[(cell_x, cell_y)]
'''

def calc_len_chains(chains):
    len_ = 0
    for chain in list(chains.values()):
        len_ += len(chain)
    return len_
   
if __name__ == "__main__":
#    dt = next(dt_generator)
#    chains = find_chains(dt, energy_bundle, search_properties)
    
    # get Recoil-fissions energies
    R_energies = []
    a_energies = []
    x_coord = []
    y_coord = []
    correlations_count = 0
    fission_count = 0
    delta_t = []
    
    for dt in dt_generator:
        chains = find_chains(dt, energy_bundle, search_properties)
        
        correlations_count += calc_len_chains(chains)
        F_count = np.sum((dt['energy_front'] >= 50000) & (~dt['tof']))
        fission_count += F_count
        
        for key in list(chains.keys()):
            R_energies.extend([events[0]['energy_front'] for events in chains[key]])
            a_energies.extend([events[1]['energy_front'] for events in chains[key]])
            x_coord.extend([events[0]['cell_x'] for events in chains[key]])
            y_coord.extend([events[0]['cell_y'] for events in chains[key]])
            delta_t.extend(
                    [events[1]['time_micro'] - events[0]['time_micro']
                     for events in chains[key]
                    ])
    
    print("Recoil-fission correlations found: ", correlations_count)
    
    print("Energy interval: > 50000 KeV")
    print("Recoil-Fission correlations found: ", correlations_count)
    print("Fission particles found: ", fission_count)
    print("Recoil-Fission count / fission count: ", \
        float(correlations_count) / fission_count)
    # show
    # R-a distribution
    fig, ax = plt.subplots()
    ax.set_title('Recoil-fission correlations')
    ax.set_xlabel('Energies of recoil particles, KeV', fontsize='large')
    ax.set_ylabel('Energies of fission particles, KeV', fontsize='large')
    ax.scatter(R_energies, a_energies, alpha=0.3)
    plt.show()
    
#    # Position scatter
#    fig, ax = plt.subplots()
#    ax.set_title('Position distribution of R-a correlations')
#    ax.set_xlabel('X, strip num', fontsize='large')
#    ax.set_ylabel('Y, strip num', fontsize='large')
#    ax.scatter(x_coord, y_coord, alpha=0.3)
#    plt.show()
    # R-a time distribution
    fig, ax = plt.subplots()
    ax.set_title('Recoil-fission correlations: time distribution')
    ax.set_xlabel('Time, mks', fontsize='large')
    ax.set_ylabel('Counts', fontsize='large')
    ax.hist(delta_t, bins=20)
    plt.show()
    
    # Position X
    fig, ax = plt.subplots()
    ax.set_title('X Position distribution of R-a correlations')
    ax.set_xlabel('X, strip num', fontsize='large')
    ax.set_ylabel('counts', fontsize='large')
    ax.hist(x_coord, bins=np.arange(1, 49))
    plt.show()
    
    # Position Y
    fig, ax = plt.subplots()
    ax.set_title('Y Position distribution of R-a correlations')
    ax.set_xlabel('Y, strip num', fontsize='large')
    ax.set_ylabel('counts', fontsize='large')
    ax.hist(y_coord, bins=np.arange(1, 129))
    plt.show()
    
    
