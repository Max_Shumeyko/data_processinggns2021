# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 14:17:13 2018
Perform automatic calibration of focal-side channel spectr for
selected STRIPS group in alpha scale.

    Instruction:
    1. Prepare data frame [see <PRJ_F>/tools/data_types.pxd EVENT]
      using generator_analogue_<fmt>.py or in other way.
    2. Specify energies for calibration.
    3. Specify calibration coefficients folder.
    3. Start script.

@author: eastwood

"""

import os
from functools import partial

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import differential_evolution

import data_processingGNS2021.tools.spectrum as sp
from data_processingGNS2021.processing_examples.search.search_focal_side \
    import search_focal_side
from data_processingGNS2021.tools.spectrum_tools import background
    
class record: 
    pass

REPORT_FMT = '\n%d    A = %2.3f ; B = %2.f\n'


def apply_side_coefs(coefs, events_frame):
    a, b = coefs
    channels = events_frame['channel']
    energies = channels * a + b
    return np.where(energies > 0, energies, np.zeros(len(energies)))    

def calc_energy_scale(result, front_energies, side_events, strip):
    side_energies = apply_side_coefs(result, side_events)
    total_energies = front_energies + side_energies
    en_spectr, xsample = np.histogram(
        total_energies,                                     
        bins=np.arange(5500, 7500, 10)
    )
    xsample = xsample[1:]
    return xsample, en_spectr
 
def evaluation_function(xsample, en_spectr, energies, 
                        energies_weights=None, 
                        dlt_en=20):
     result = 0
     energies = sorted(energies)     
     
     # eliminate background (подложку)
     back = background(en_spectr, niter=5, parameters='BACK1_ORDER8')
     en_spectr = en_spectr - back
     en_spectr[en_spectr < 0] = 0.
     
     sum_counts = np.sum(
         en_spectr[(xsample >= energies[0]) &\
                   (xsample <= energies[1])]
     )
     
     threshold = sum_counts / float(len(energies)) / 2
     
     if not energies_weights:
         energies_weights = np.ones(energies.shape)
         
     for energy, weight in zip(energies, energies_weights):
         ind = (xsample >= energy - dlt_en) & (xsample <= energy + dlt_en)
         s_ = sum(en_spectr[ind]) 
         ind_out = (xsample > energies[-1] + dlt_en) & \
                   (xsample < energies[0] - dlt_en)
         val_out = sum(en_spectr[ind_out]) 
         result -= val_out        
         
         result = result + s_ * weight if s_ > threshold \
                  else result - sum_counts 
     return result

def opt_func(result, front_energies, side_events,
             strip=None, weights=None):
    xsample, en_spectr = calc_energy_scale(result, front_energies, 
                                           side_events,strip) 
    
    res = -evaluation_function(xsample, en_spectr, ENERGIES,
                                energies_weights=weights)
    return res

def optimize(parameters_bounds, strip, 
             front_energies, side_events, weights):  
    
    optimization_function = partial(
        opt_func,
        front_energies=front_energies, side_events=side_events,
        strip=strip, weights=weights
    )
    result = differential_evolution(
        optimization_function, parameters_bounds, popsize=350, workers=7,
#               strategy='rand1bin',
    )
    p = result.x
    report = REPORT_FMT % (strip, p[0], p[1])
    print('Result: \n', report, result)
    return p

def check_energy_scale(
      result, strip, energies, 
      eliminate_background=True, pict_folder=None):
#    print "CHECK RESULTS: ", result
    energies = sorted(energies)
    e_min, e_max = energies[0], energies[-1]
    # prepare coefficients
    side_coefs_ = side_coefs
    side_coefs_[0, strip], side_coefs_[1, strip] = result
    coefs = record()
    coefs.front_coefs = front_coefs
    coefs.side_coefs = side_coefs_
    
    # build spectrum
    en_spectrs, xsample, _ = sp.get_combined_spectrum(
        frame,
        event_type='front-side', scale='alpha', tof=False, 
        energy_scale=True, coefs_folder='.', coefs=coefs, 
        bins=np.arange(4000, 8500, 15),
        dlt_t=DLT_T,
    )
        
    # plot a figure
    en_spectr = en_spectrs[strip - 1] #according to en_spectrs has shape [0 .. 47][..]
    back = background(en_spectr, niter=5, parameters='BACK1_ORDER8')
    en_spectr = en_spectr - back
    en_spectr[en_spectr < 0] = 0.
    xsample = xsample[1:]
    fig, ax = plt.subplots()
    ax.set_title("Focal-side calibration energy spectrum, strip %s" 
                 % (strip,))
    ax.set_xlabel("Energy, KeV")
    ax.set_ylabel("Counts")
    ax.plot(xsample, en_spectr, linestyle='steps')
    ax.set_xlim(e_min, e_max)
    ax.set_xticks(
        np.arange(round(e_min - 250, -2), round(e_max + 250, -2),  100)
    )
    ax.grid()
    if pict_folder:
        fig.savefig(
            os.path.join(pict_folder, 
              'side_calibration_strip{}.png'.format(str(strip))
            )
        )
    else:
        plt.show()
    return xsample, en_spectr

def write_coefs_to_file(coefs_dict, strips_in_detector=8):
     with open('side_alpha.txt', 'w') as f:
         for strip in sorted(coefs_dict.keys()):
             a, b = coefs_dict[strip]
             f.write(REPORT_FMT % (strip, a, b))
    
if __name__ == '__main__':
    STRIPS = list(range(1, 49))
#    ENERGIES = [6899, 7137, 7922, 8699, 9261] # YtCa
#    WEIGHTS = [1., 0.3, 1., 1., 6.]
#    ENERGIES = [5304., 7200., 7862., 8404.] # PbCa
    DLT_T = 700 # dlt time between focal and side events
    ENERGIES = [6263, 6419, 6650, 7008, 7133] # ArYb enriched
#[6135, 6265, 6904, 7014] # ArYb
    WEIGHTS = [1,] * len(ENERGIES) #[1., 1., 1., 1., 1.]
    PICT_FOLDER = '/home/eastwood/Изображения/work/Mar_2020/side_clbr'
    
    #### MANUAL CALIBRATION OF FOCAL-SIDE ENERGY SPECTRS IN APLHA SCALE ########
    side_coefs = sp.get_calibration_coefficients(
      COEFS_FOLDER + '/' + 'side_alpha.txt')
    front_coefs = sp.get_calibration_coefficients(
      COEFS_FOLDER + '/' + 'front_alpha.txt')
    energies = sp.apply_all_calibration_coefficients(frame, COEFS_FOLDER)
    frame = frame[(energies >= 500) & (energies <= 6000)]
    res = search_focal_side(
        frame, energies, 'alpha', 'front', 
        tof=False, dlt_t=DLT_T
    )
    (front_events, front_energies), (side_events, side_energies) = res
            
    parameters_bounds = [(2.5, 3.5), (-200, 200)]
    results = {}
    for strip in STRIPS:
        ind = side_events['strip'] == strip
        side_events_ = side_events[ind]
        front_energies_ = front_energies[ind]
        result = optimize(parameters_bounds, strip, 
                          front_energies_, side_events_, WEIGHTS)
        results[strip] = result
        check_energy_scale(result, strip, ENERGIES, pict_folder=PICT_FOLDER)
        
    print("Show results: ", results)
    write_coefs_to_file(results)