"""
Recalculate calibration coefficients on fission scale using
alpha calibration coefficients and channels.

Prepare data frame generator and alpha calibration coefficients to start 
using scripts from <project_folder>/data_processing/calibration. 
! note: check if alpha scale is already calibrated so the coefficients are 
required to perform calibration in fission scale.

15.01.2020 eastwood
"""
import os

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

import data_processingGNS2021.tools.spectrum as sp
from data_processingGNS2021.tools.read_file_2019 import read_parameters

"""TODO
1. upload alpha coefficients
2. choose events for the calibration
3. calc energies
4. send fission channels and alpha enegies to calibration
"""
def get_data(filename, front_coefs, back_coefs, number=400000):
    print("START reading: ", filename)
    data = read_parameters(filename, ['event_type', 'coord', 'alpha_channel', 
            'back_alpha_channel', 'fission_channel', 'back_fission_channel'])
    
    front_strips, back_strips = data['coord']
    front_channels = data['alpha_channel']
    back_channels = data['back_alpha_channel']
    front_fission_channels = data['fission_channel']
    back_fission_channels = data['back_fission_channel']   
    
    # get front channels, energies
    front_energies = front_coefs[0][front_strips] * front_channels + \
      front_coefs[1][front_strips]
      
     # get back channels, energies
    back_energies = back_coefs[0][back_strips] * back_channels + \
      back_coefs[1][back_strips]
    
    # delete zeros
    ind = (front_energies > 0) & (front_energies < 10000) & \
          (front_fission_channels != 0)
    front_energies = front_energies[ind][:number]
    front_fission_channels = front_fission_channels[ind][:number]
    
#    ind = (back_energies != 0) & (back_fission_channels != 0)
#    back_energies = back_energies[ind][:number]
    back_energies = front_energies
    back_fission_channels = back_fission_channels[ind][:number]
    
    front_strips = front_strips[ind][:number]
    back_strips = back_strips[ind][:number]
    print("CHECK BACK 128: ", 128 in set(back_strips))
    print("res: ", front_fission_channels, back_fission_channels)
    
    print("FINISH reading: ", filename)
    return front_strips, front_fission_channels, front_energies, \
           back_strips, back_fission_channels, back_energies
      
#### fitting functions 
def line(x, *p):
    a, b =  p
    return a * x + b

def get_fitted(channels, energies):
    """Fit data by line 
    input:
        channels - fission channels to calibrate
        energies - alpha events energies
    return coefs, coefficients_deviations, fit line
    """
    x, y = channels, energies
    p0 = [1., 0.]
    coef, corr = curve_fit(line, x, y, p0=p0)
    
    std_deviations = np.sqrt(np.diag(corr))
    curve = line(x, *coef)
    return coef, std_deviations, curve

# fit data by line and calculate fission coefficients
def write_report(output_filename, strip, coefs):
    report = '\n%d    A = %2.5f ; B = %2.1f\n' % (strip, coefs[0], coefs[1])
    with open(output_filename, 'a') as f:
        f.write(report)
    
def process_data(data, show=False, output_front='front_fission.txt',
                 output_back='back_fission.txt'):  
    """Fit data by line and calculate front fission and back fission
    coefficients."""
    print("START processing: ")
    front_strips, front_fission_channels, front_energies, \
      back_strips, back_fission_channels, back_energies = data
    
    print("START fitting")
    # calculate
    
    for front_strip in range(1, 49):
        ind = front_strips == front_strip
        
        if np.sum(ind) < 10:
            print("\nFRONT STRIP {} IS MISSED\n".format(front_strip))
            continue
        
        front_fission_channels_ = front_fission_channels[ind] 
        front_energies_ = front_energies[ind]
        
        Ffront_coefs, front_dev, F_fit = \
            get_fitted(front_fission_channels_, 
                       front_energies_)
       
        if show:
            fig, ax = plt.subplots()
            ax.set_title("Front fission calibration")
            ax.set_xlabel("fission channel")
            ax.set_ylabel("energy, KeV")
            
            ax.plot(front_fission_channels_, front_energies_, 'o',
                    front_fission_channels_, F_fit, '--')            
            plt.show()
        
        write_report(output_front, front_strip, Ffront_coefs)
        print("""Result:
            front strip: {}
            front coefs: {}
            """.format(front_strip, Ffront_coefs))
                
    for back_strip in range(1, 129):
        ind = back_strips == back_strip
        
        if np.sum(ind) < 10:
            print("\nBACK STRIP {} IS MISSED\n".format(back_strip))
            print(back_strips[ind], back_fission_channels[ind])
            continue
        
        back_fission_channels_ = back_fission_channels[ind] 
        back_energies_ = back_energies[ind]
        
        Fback_coefs, back_dev, F_fit = \
            get_fitted(back_fission_channels_, 
                       back_energies_)
       
        if show:
            fig, ax = plt.subplots()
            ax.set_title("Back fission calibration")
            ax.set_xlabel("fission channel")
            ax.set_ylabel("energy, KeV")
            
            ax.plot(back_fission_channels_, back_energies_, 'o',
                    back_fission_channels_, F_fit, '--')            
            plt.show()
        
        write_report(output_back, back_strip, Fback_coefs)
        print("""Result:
            back strip: {}
            back coefs: {}
            """.format(back_strip, Fback_coefs))
        
    print("FINISH")
    return Ffront_coefs#, Fback_coefs
            
if __name__ == '__main__':
    COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Jan_2020_digit'
    DATA_PATH = '/home/eastwood/gitlab_projects/data/analogue/Jan_2020_digit'
    
    # FILENAMES list
    PREFIX = os.path.join(DATA_PATH, 'PbCa.')
    FILENAMES = [PREFIX + str(file_) for file_ in range(33, 33 + 1)]
    FILENAMES = [filename for filename in FILENAMES 
                 if os.path.isfile(filename)]
    
    front_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + 
                                               '/front_alpha.txt')
    back_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + 
                                               '/back_alpha.txt')
    
    # processing
    process_func = lambda filename: \
        process_data(get_data(filename, front_coefs, back_coefs, number=4000000),
             show=False, 
             output_front=os.path.join(COEFS_FOLDER, 'front_fission.txt'),
             output_back=os.path.join(COEFS_FOLDER, 'back_fission.txt'))
        
    process_func(FILENAMES[0])