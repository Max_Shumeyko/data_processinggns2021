#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 13:15:03 2020
Fit curve with parabola.
F(x, *p) = -|a|*x^2 + b * x + c; p = [a, b, c]

@author: eastwood
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.odr import Model, Data, ODR

def parabola(P, x):
    return -abs(P[0]) * x**2 + P[1] * x + P[2]

def calc_dev(x, y, coef):
    return [((y_ - parabola(coef, x_)) / y_)**2 for x_, y_ in zip(x, y)]

def calc_weights(x, y, coef):
    deviations_y = calc_dev(x, y, coef)
    weights = [1. / dev_  for dev_ in deviations_y]
    weights = [w_ / max(weights) for w_ in weights]
    return [w_**4 / sum(weights)**4 for w_ in weights]
    

def parabola_fit(x, y, weights=None, p0=None):
    """Fit data by parabola and 
    return fitting coefficients, std_deviations on parameters
    """
    if p0 is None:
        p0 = [1., 0., 0.]
    #coef, corr = curve_fit(parabola, x, y, p0=p0) 
    parabola_m = Model(parabola)
    if np.any(weights):
        mydata = Data(x, y, wd=weights)
    else:
        mydata = Data(x, y)
    myodr = ODR(mydata, parabola_m, beta0=p0)
#    try:
#        std_deviations = np.sqrt(np.diag(corr))
#    except NameError:
#        std_deviations = np.zeros(len(coef))
#    curve = parabola(x, *coef)
#    return curve, coef, std_deviations
    result = myodr.run()
    coef = result.beta
    std_deviations = result.sd_beta    
    return coef, std_deviations

def robust_parabola_fit(x, y, iterations=2):
    """Fit data by parabola using robust weighted coefs and 2 parabola_fit
    iterations.
    return fitting coefficients, std_deviations on parameters
    """
    coef, std_ = parabola_fit(x, y)
    for i in range(iterations):
        weights = calc_weights(x, y, coef)
        coef, std_ = parabola_fit(x, y, weights=weights, p0=coef)
    return coef, std_
        
if __name__ == '__main__':
    sample_y = np.asarray([0.0142, 0.0174, 0.0182, 0.0183, 0.0123, 0.0155])
    sample_x = np.asarray([165.0, 195.0, 224.9, 255., 285.0, 315.0])
    
    #curve, coef, std_ 
    coef, std_ = parabola_fit(sample_x, sample_y)
    print("COEFS SIMPLE FIT COEFS / DEVIATIONS: ", coef, std_)
    # plot simple
    plt.plot(sample_x, sample_y, 'o', label='simple fit')  
    x = np.linspace(sample_x[0], sample_x[-1], 100)
    plt.plot(x, parabola(coef, x), '--')
    
    coef, std_ = robust_parabola_fit(sample_x, sample_y)
    print("COEFS ROBUST FIT COEFS / DEVIATIONS: ", coef, std_)# plot robust
    plt.plot(sample_x, sample_y, 'o', label='robust fit')  
    x = np.linspace(sample_x[0], sample_x[-1], 100)
    plt.plot(x, parabola(coef, x), '--')
    plt.legend()
    plt.show()


