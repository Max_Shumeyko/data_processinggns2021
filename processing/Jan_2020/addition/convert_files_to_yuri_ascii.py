#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 23 18:26:49 2020

@author: eastwood
"""
import os 

import pandas as pd
import numpy as np

from data_processingGNS2021.tools.array_types import analog_block

DATA_FOLDER = '/home/eastwood/gitlab_projects/data/analogue/23Feb_probe'

if __name__ == '__main__':
    os.chdir(DATA_FOLDER)
    files = os.listdir('.')

    for file_ in files:
        frame = np.fromfile(file_, dtype=analog_block)
        frame = pd.DataFrame(frame)[['w6', 'w8']]
        frame = frame[(frame['w6'] > 0) & (frame['w8'] > 0)]
        output_file_ = os.path.join(DATA_FOLDER, 
                                    file_.replace('.', '_') + '.txt')
        #frame.to_csv(output_file_, sep=' ')
        with open(output_file_, 'w') as f:
            f.write("{:<11s}{:<11s} {:<6s}\n".format("ind", "word_7", "word_9"))
            for ind, (word7, word9) in frame.iterrows():
                f.write("{:<11d}{:<11d} {:<11d}\n".format(ind, word7, word9))
    