#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  7 13:49:58 2019

@author: eastwood

FOR GENERATORS CONT
Build and spectrum of DGFRS' experimental data distributions using different 
parameters and visualisation options from several files 
using data generator! 
(see <prj>/processing_examples/read_data/generator_analogue_2019.py)

Main parameters (options):
*    energy_scale: specify if the spectrum is in energy scale (True) or raw channel. 
        Values: True / False
*    tof: specify if events for spectrum have time-of-flight mark.
        Values: True/False/'all' (spectrum contains all events)
*    scale: specify if spectrum contains events in alpha scale (< 25 MeV) or
        in fission scale ( 50 - 250 MeV). Values: 'alpha'/'fission'.
*    threshold: visualisation option. A parameter for summary 2D spectrum figure
        which specify the threshold as a multiplier for the maximum value of the
        spectrum.
        Values: float 0.0 - 1.0
*    bins: specify bins as np.ndarray for the distribution histogram. 
        Values: None or np.ndarray
*    coefs_folder: specify a folder with calibration coefficients to build the
        distribution in energy scale. Usually it's already specified in 
        <project_folder>/processing_examples/read_data/read_<format>.py scripts
        and ready-to-use after the frame was read
        
        
See <project_folder>/tools/spectrum.py for details. 

"""

import os 
import copy
from collections import defaultdict
from tkinter.filedialog import askdirectory

import numpy as np

import data_processingGNS2021.tools.spectrum as sp

SPECTRUM_DICT = defaultdict(lambda: dict())

def collect_by_types(distr_type):
    """
    Collect spectrum functions names according to it types:
    ('FOCAL', 'FOCAL-SIDE', 'POSITION', 'MWPC', etc.
    
    get access to functions using
    SPECTRUM_DICT[distr_type] -> dict(func_name, function)
    """
    def my_decorator(func):
        name = func.__name__
        SPECTRUM_DICT[distr_type].update({name: func})
        return func
    
    return my_decorator

OPTIONS = dict(coefs_folder = \
'/home/eastwood/gitlab_projects/data/calibration_coefficients/YtCa_Feb2020_digit', # according to generator_<>.py file
               energy_scale = True, # True or False
               tof = False, #True False 'all'
               scale = 'alpha', # 'alpha'|'fission'
               threshold = 1.,
               bins = None,)

#### FOCAL channel/energy distributions
# front 
@collect_by_types('FOCAL')
def front(frame, **options):
    return sp.Spectrum(frame, event_type='front', **options)

# back
@collect_by_types('FOCAL')
def back(frame, **options):
    return sp.Spectrum(frame, event_type='back', **options)

# side
@collect_by_types('FOCAL')
def side(frame, **options):
    return sp.Spectrum(frame, event_type='side', **options)

#### TIME CORRELATED FOCAL-SIDE channel/energy distributions (E = Ef + Es, dt <= 20 mks)
# front_side
@collect_by_types('FOCAL-SIDE')
def front_side(frame, **options):
    return sp.CombinedSpectrum(frame, event_type='front-side', **options)

# back_side
@collect_by_types('FOCAL-SIDE')
def back_side(frame, **options):
    return sp.CombinedSpectrum(frame, event_type='back-side', **options)

### POSITION DISTRIBUTIONS
# 2D
@collect_by_types('POSITION')
def pos_2D(frame, **options):
    return sp.PositionSpectrum(frame, **options)

# front
@collect_by_types('POSITION')
def pos_front(frame, **options):
    return sp.PositionSpectrum(frame, **options).get_front_distribution()

# back
@collect_by_types('POSITION')
def pos_back(frame, **options):
    return sp.PositionSpectrum(frame, **options).get_back_distribution()

#### MWPC - multiwire proportional chamber
# deltaE1 - 1st Proportional chamber
@collect_by_types('MWPC')
def dE1(frame, **options):
    return sp.TofSpectrum(frame, tof_counter='tofD1', **options)

@collect_by_types('MWPC')
def dE2(frame, **options):
    return sp.TofSpectrum(frame, tof_counter='tofD2', **options)

#### SHOW DISTRIBUTIONS
#spectr_front.show(threshold=threshold, supertitle = u'Energy distribution for the 48 front focal detector\'s strips')#Спектр переднего детектора в автоматической калибровке')
#spectr_back.show(threshold=threshold, supertitle = u'Спектр заднего детектора')
#spectr_side.show(supertitle=u'Спектр бокового детектора')
#spectr_front_side.show(threshold=threshold, supertitle=u'Спектр корреляций переднего и бокового детекторов')
#spectr_back_side.show(threshold=threshold, supertitle=u'Спектр  корреляций заднего и бокового детекторов')
#
#deltaE1.show()
#deltaE2.show()
def dialog(question, answers, presentation=None):
    """
    Ask until quit or choose one of answers list[str/int].
    Return answers_list/None.
    
    answer: [answer1, answer2, .., answerN] -> [answer: answer in answers] 
    
    Example:
    In [126]: dialog('How much is the fish?', map(str, list('12345')))
    ! Type: ' '/q/quit/exit to quit
    Answers:  ['1', '2', '3', '4', '5']
    How much is the fish?
    ->8
    ' '/q/quit/exit to quit
    Answers:  ['1', '2', '3', '4', '5']
    How much is the fish?
    ->1, 2, 8, 5
    Out[126]: ['1', '2', '5']


    """ 
    conv_f = lambda s: str(s).lower().strip() # ' AbC ' -> 'abc'
    
    presentation = answers if presentation is None else presentation
    answers = list(map(conv_f, answers))
    quit_arr = (' ', 'q', 'quit', 'exit')
    
    while True:
        print("! Type: ' '/q/quit/exit to quit") 
        print("Answers: ", presentation)
        inp = input(question + '\n->')
        
        if inp in quit_arr: # quit the loop by quit symbol
            break
        
        if ',' in inp: # parse input 
            inp = list(map(conv_f, inp.split(',')))
            return [s for s in inp if s in answers]
        elif len(inp) > 0:
            s = conv_f(inp) 
            if s in answers:
                return s
        else:
            pass
 
class DataCollection(object): 
    """Construct data container for the dictionary of spectrum building functions.
    Collected data can be summarized and extracted by key name.

    collection = DataCollection(spectr_func_dict)
    collection[func_name] -> Spectrum<func_name> obj
    collection1 + collection -> collection_sum
    
    collection.show() - show all collected spectums 
    """
    def __init__(self, func_dict, container=None):
        self._func_dict = func_dict
        if container:
            self._container = container
        else:
            self._container = dict()

    def has_same_type(self, other):
        try:
            result = (self._func_dict == other._func_dict) 
            for key in self._container:
                result = result and (key in other._container)
                if not result:
                    return False 
        except AttributeError:
            return False

        return True

    def __bool__(self):
        return bool(self._container)
    
    def __add__(self, other):
        if not self.has_same_type(other):
            msg = "Data collections {0} {1} have different types"
            raise TypeError(msg.format(self.__name__, other.__name__))

        container = copy.deepcopy(self._container)
        for key in self._container:
            container[key] += other[key]
        return type(self)(self._func_dict, container=container)

    def __getitem__(self, name):
        return self._container[name]

    def __setitem__(self, key, value):
        self._container[key] = value

    def show(self):
        for key, item in list(self._container.items()):
            if hasattr(item, "show"):
                item.show()
    
    def show_info(self):
        for key, item in list(self._container.items()):
            if hasattr(item, "show"):
                print("Key: '{0}'; configure:".format(key))
                print("Spectrum class: ", item.__class__)
                for k, i in list(item.configure.items()):
                    print("\t{0} -> {1}".format(k, str(i)))

class Worker(object): #☭ ☭ ☭ 
    """Construct dict of spectrum building functions using dialog, apply them
    to data and collect in DataCollection structure. 
    
    Example:
    worker = Worker()
    worker(frame) -> DataCollection obj # apply to get data
    Use to collect spectrums from data frame generator:
    np.sum(map(worker, frame_generator)) -> data_collection_sum
    data_collection: DataCollection 
    data_collection[spectrum_func_name] -> spectrum class # all processed data inside
    """   
    def __init__(self, set_options=False):
        self.functions = Worker._initial_dialog()
        if self.functions is None:
            raise IOError("Worker object hasn't been initialized!")
        if set_options:
            self.options = Worker._options_dialog()
        else:
            global OPTIONS
            self.options = OPTIONS 

    def __call__(self, frame): 
        data_collection = DataCollection(self.functions)
        for key in self.functions:
            data_collection[key] = self.functions[key](frame, **self.options)
        return data_collection

    @classmethod
    def _options_dialog(cls):
        options = dict()
        OPTIONS_TEMP = dict(coefs_folder=None,
                            energy_scale = (False, True), # True or False
                            tof = (False, True, 'all'),
                            scale = ('alpha', 'fission'),
                            threshold = (1.0, 0.5, 0.25, 0.1, 0.05, 0.01))
        global OPTIONS
        print("\n<<OPTIONS DIALOG>>\nInitial options: ")
        for key, val in list(OPTIONS.items()):
            print("\t{0}: {1}".format(key, val))

        decision_dict = dict(enumerate(OPTIONS_TEMP.keys())) # num -> options names
        question = "Choose options (0, 1, ..):" 
        answers = list(decision_dict.keys()) # num
        option_index = dialog(question, answers, list(decision_dict.items()))

        # choose options variants
        try:
            for type_key in option_index:
                option = decision_dict[int(type_key)] # choose option name
                msg = '\n\nOption: {0}, \nvariants: {1}'
                print(msg.format(option, OPTIONS_TEMP[option]))
                
                if option == 'coefs_folder':
                    folder = askdirectory()
                    options.update({'coefs_folder': folder})
                    print('Coefs_folder: ', folder)
                    continue
                
                # choose options
                decision_dict_vars = dict(enumerate(OPTIONS_TEMP[option]))
                question = "Choose options for spectrum building (0, 1, ..)"
                answers = list(decision_dict_vars.keys())
                answer = dialog(question, answers, list(decision_dict_vars.items()))
                
                value = decision_dict_vars[int(answer)]
                options.update({option: value}) 
                    
        except TypeError as e:
            if e.message == "'NoneType' object is not iterable":
                print(">> Empty set selected. Return base OPTIONS!")
            else:
                return OPTIONS
                    
        outer_keys = set(OPTIONS.keys()) - set(options.keys())
        options.update({key: OPTIONS[key] for key in outer_keys})
        print()
        print(">> Final parameters: ", options)
        print()
        return options
            
    @classmethod
    def _initial_dialog(cls):
        """Construct dict of spectrum constructing functions.
        
        FOCAL -> Front/Back/Side
        TIME CORRELATED -> front-side/back-side
        POSITION -> 2D/front/back
        MWPC -> dE1/dE2
        
        see functions above marked with @collect_by_types
        return func_dict: 'str' name -> function spectrum_func
        
        """
        print("\n<<CHOOSE SPECTRUM FUNCTIONS DIALOG>>")
        functions = defaultdict() # functions[n](dataframe) -> spectrum_data
                
        # choose type
        decision_dict = dict(enumerate(SPECTRUM_DICT.keys())) # num -> spectrum functions names
        question = "Choose types of distribution (0, 1, ..):" 
        answers = list(decision_dict.keys()) # num
        type_keys = dialog(question, answers, list(decision_dict.items()))
        
        # choose functions
        try:
            for type_key in type_keys:
                type_ = decision_dict[int(type_key)]
    
                msg = '\n\nFunction type: {0}, \nFunctions inside: {1}'
                print(msg.format(type_, list(SPECTRUM_DICT[type_].keys())))
                
                # choose functions
                decision_dict_funcs = dict(enumerate(SPECTRUM_DICT[type_]))
                question = "Choose functions for spectrum building (0, 1, ..)"
                answers = list(decision_dict_funcs.keys())
                func_keys = dialog(question, answers, list(decision_dict_funcs.items()))
                
                # add to functions list
                for func_key in func_keys:               
                    func_name = decision_dict_funcs[int(func_key)]
                    functions.update({func_name: 
                                      SPECTRUM_DICT[type_][func_name]})
                    
        except TypeError as e:
            if e.message == "'NoneType' object is not iterable":
                print("Empty set selected. Bye!")
            else:
                raise e
                    
        print('>> Output: ', list(functions.keys()))
        return functions
            
def go(data_generator, set_options=False):
    """
    Choose spectrum building functions using dialog (see Worker), apply them
    to data from data_generator and collect in DataCollection object 
    (see Data Collection).

    return data_collection 

    Examples:
    data = go(frame_generator, set_options=True) # set_options=False to use default o
    <function choosing dialog> <option choosing dialog>
    data.show_info() # see info about spectrum collections
    data.show() # plot all distributions
    data['keyname'] - get Spectrum<keyname> in collection
    """
    worker = Worker(set_options=set_options)
    sample = []
    for data in data_generator:
        try:
            item = worker(data)
        except AssertionError as e:
            print("Error occured while processing file!", e)
        else:
            sample.append(item)
    return np.sum(sample)

if __name__ == '__main__':
    collection = go(frame_generator, set_options=False)