# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 19:28:19 2018

Search for recoil-alpha correlations.

Prepare data table and alpha calibration coefficients to start using scripts from 
<project_folder>/data_processing/read_data_table<forma>.py 

Instruction:
    1. Get data table from <project_folder>/data_processing/read_data_table<forma>.py
    2. Specify parameters for search:
        * time_dlt: time interval between consequent events in microseconds (1000 mcs = 1 ms
        * chain_length_min, chain_length_max: min and max number of 
            time-correlated events in the chain 
        * recoil_energy_min, recoil_energy_max: energy diaposon of Recoil for search in KeV
        * energy_min, energy_max: energy diaposon of alpha for search in KeV 
        * recoil_first: if recoil can be included as the first event in each chain.  bool
        * fission_flag: if fission events could be included as the member of each chain.  bool
        * only_fission: if search only for fission fragments. bool
        * limit: number of total events to search. If -1: search the whole data_table. bool
    3. Run this script to perform search
    
@author: eastwood

"""
import matplotlib.pyplot as plt
import data_processingGNS2021.tools.quick_methods as qm

#from data_processingGNS2021.processing_examples import read_files as rf

class record:
    pass

search_properties = record()

# specify energy interval 
ind = ((data_table['energy_front'] > 6245) & \
       (data_table['energy_back'] < 6270)) | \
      ((data_table['energy_front'] > 6990) & \
       (data_table['energy_front'] < 7015))
#### search R-a (recoil-alpha pairs)
search_properties.time_dlt = 10000000
search_properties.chain_length_min = 2
search_properties.chain_length_max = 4
search_properties.recoil_energy_min, search_properties.recoil_energy_max = 6990, 7015#3000, 19000
search_properties.energy_min, search_properties.energy_max = 6000, 7270#6200, 11000
search_properties.recoil_first = False
search_properties.fission_flag = False
search_properties.only_fission = False
search_properties.limit = -1

chains = qm.find_chains(data_table, search_properties)
for i in chains[: 5]:
    print(data_table[i])
    
recoil_events = [data_table[i[0]] for i in chains]
alpha_events = [data_table[i[1]] for i in chains]
# get Recoil - alpha energies
R_energies = np.asarray([i['energy_front'] for i in recoil_events])
a_energies = np.asarray([i['energy_front'] for i in alpha_events])

fig, ax = plt.subplots()
ax.set_title('Recoil-alpha correlations')
ax.set_xlabel('Energies of recoil particles, KeV', fontsize='large')
ax.set_ylabel('Energies of alpha particles, KeV', fontsize='large')
ax.scatter(R_energies, a_energies, alpha=0.3)
plt.show()
