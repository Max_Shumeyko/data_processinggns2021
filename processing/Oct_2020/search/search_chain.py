# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 20:07:20 2018

Search for full chains (Recoil-alpha-alpha-...-fission fragment) correlations.

Prepare data table and alpha calibration coefficients to start using scripts from 
<project_folder>/data_processing/read_data_table<forma>.py 

Instruction:
    1. Get data table as dt_generator from 
       <project_folder>/data_processing/read_data/generator_<analogue|digital>_2019.py 
    2. Specify parameters for search:
        * time_dlt: time interval between consequent events in microseconds (1000 mcs = 1 ms
        * chain_length_min, chain_length_max: min and max number of 
            time-correlated events in the chain 
        * recoil_energy_min, recoil_energy_max: energy diaposon of Recoil for search in KeV
        * energy_min, energy_max: energy diaposon of alpha for search in KeV 
        * recoil_first: if recoil can be included as the first event in each chain.  bool
        * fission_flag: if fission events could be included as the member of each chain.  bool
        * only_fission: if search only for fission fragments. bool
        * limit: number of total events to search. If -1: search the whole data_table. bool
    3. Run this script to perform search
@author: eastwood

"""
import numpy as np

import matplotlib.pyplot as plt
import data_processingGNS2021.tools.quick_methods as qm

#from data_processingGNS2021.processing_examples import read_files as rf

class record:
    pass

SEARCH_PROPERTIES = record()

#### search for chains
SEARCH_PROPERTIES.time_dlt = 26000000 # microsecons
SEARCH_PROPERTIES.chain_length_min = 2
SEARCH_PROPERTIES.chain_length_max = -1
SEARCH_PROPERTIES.recoil_energy_min =  6500 # KeV
SEARCH_PROPERTIES.recoil_energy_max = 19000 # KeV
SEARCH_PROPERTIES.energy_min, SEARCH_PROPERTIES.energy_max = 6000, 9300
SEARCH_PROPERTIES.recoil_first = False
SEARCH_PROPERTIES.fission_flag = False
SEARCH_PROPERTIES.only_fission = False
SEARCH_PROPERTIES.fission_time_dlt = 200 * 10 ** 6 # 200 seconds
SEARCH_PROPERTIES.limit =  -1

def filter_by_energy(data_table, energies_bundle):
    """Filter events from data table according to limits of 
    energies bundle: [(energy1_min, energy1_max), ...], both ends included.
    
    Return data_table.
    """
    
    ind = np.zeros(len(data_table), dtype=np.bool)
    for en_min, en_max in energies_bundle:
        ind = ind | ((data_table['energy_front'] >= en_min) & \
                     (data_table['energy_front'] <= en_max))
    return data_table[ind]

def output(r_events, a_events):
    pass

if __name__ == "__main__":
    energy_bundle = [(6620, 6740), (9200, 9340), ]
    
    for data_table in dt_generator:
        # filter
        data_table = filter_by_energy(data_table, energy_bundle)
        data_table = data_table[data_table['event_type'] == 1]
        # search
        chains = qm.find_chains(data_table, SEARCH_PROPERTIES)
        for i in chains[: 5]:
            print(data_table[i])
            
        # get Recoil - alpha 
        recoil_events = [data_table[i[0]] for i in chains]
        alpha_events = [data_table[i[1]] for i in chains]
        R_energies = np.asarray([i['energy_front'] for i in recoil_events])
        a_energies = np.asarray([i['energy_front'] for i in alpha_events])
        print('Len R, a: ', len(R_energies), len(a_energies))
        
        # output
        output(recoil_events, alpha_events)
        
        # show
        fig, ax = plt.subplots()
        ax.set_title('Recoil-alpha correlations')
        ax.set_xlabel('Energies of recoil particles, KeV', fontsize='large')
        ax.set_ylabel('Energies of alpha particles, KeV', fontsize='large')
        ax.scatter(R_energies, a_energies, alpha=0.3)
        plt.show()
