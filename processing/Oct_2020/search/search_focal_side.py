#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 15:04:16 2020

@author: eastwood
"""
from data_processingGNS2021.tools.quick_methods import find_sync_pairs

def search_focal_side(event_table, 
                      energies,
                      scale='alpha', event_type='front',
                      tof=False, dlt_t=20):
    """
    Find syncronized pairs of events like front-side or back-side in 
    different energy scales.
    Input:
        event_table: np.ndarray[DATA_EVENT] events data frame
        energies: [float] events energies array
        scale: str energy scale: 'alpha', 'fission'
        event_type: str type of event: 'front', 'back'
        tof: bool time-of-flight flag: True, False
        dlt_t: float time window between two consequent events, microseconds
    Return:
        (focal_events, focal_energies), 
        (side_events, side_energies)
    Example:
        (front_events, front_energies), (side_events, side_energies) =\ 
          find_sync_pairs(event_table, 'alpha', 'front')       
    """
    if scale == 'alpha':
        scale_ = 0 # corresponds to alpha scale
    elif scale == 'fission':
        scale_ = 1
        
    if event_type == 'front':
        event_type_ = 1
    elif event_type == 'back':
        event_type_ = 2
    
    # find_sync_pairs return [ind_focal: int, ind_side: int]
    ind = find_sync_pairs(event_table, scale_, event_type_, tof,
                           dlt_t=dlt_t)
    
    # throw away zero energy events
    focal_events = event_table[ind[:, 0]]
    focal_energies = energies[ind[:, 0]]    
    side_events = event_table[ind[:, 1]]
    side_energies = energies[ind[:, 1]]
    
    ind = (focal_energies > 0) & (side_energies > 0)
    focal_events = focal_events[ind]
    focal_energies = focal_energies[ind]
    side_events = side_events[ind]
    side_energies = side_energies[ind]
    return (focal_events, focal_energies), (side_events, side_energies)