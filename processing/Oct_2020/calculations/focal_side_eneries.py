#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 16:29:59 2020
Calculate focal-side energies distribution.

Use generator_<>_2019.py data generator to start calculations.
Script requires event frame generator: frame_generator and COEFS_FOLDER.
@author: eastwood
"""
import numpy as np
import matplotlib.pyplot as plt

from data_processingGNS2021.processing_examples.search.search_focal_side \
    import search_focal_side
 
def plot_results(front_energies, side_energies):
    fig, (ax_f, ax_s) = plt.subplots(2, 1, sharex=True)
    
    ax_f.set_title(
        "Front energies distribution for focal-side correlated events"
    )
    ax_f.set_xlabel("Energy, KeV")
    ax_f.set_ylabel("Number of counts")
    ax_f.hist(front_energies, bins=100)
    
    ax_s.set_title(
        "Side energies distribution for focal-side correlated events"
    )
    ax_s.set_xlabel("Energy, KeV")
    ax_s.set_ylabel("Number of counts")
    ax_s.hist(side_energies, bins=100)
    
    plt.show()
    
if __name__ == '__main__':
    # processing data files and get focal-side correlated samples
    front_energies = np.ndarray(0, dtype=float)
    side_energies = np.ndarray(0, dtype=float)
    count_all = 0
    count_focal_side = 0
    
    for frame in frame_generator:
        energies = sp.apply_all_calibration_coefficients(frame, COEFS_FOLDER)
        
        (front_events, front_energies_), (side_events, side_energies_) =\
            search_focal_side(frame, energies, 'alpha', 'front', 
                              tof=False, dlt_t=10)
               
#        count_focal_side += len(front_events)
        
        # collect nonzero f-s events
        total_energy = front_energies_ + side_energies_
        ind = (total_energy > 9200) & (total_energy < 9300)
        front_energies_ = front_energies_[ind]
        side_energies_ = side_energies_[ind]
        
        # count number of events and f-s events
        count_all += len(frame)
        count_focal_side += len(front_energies)
        
        front_energies = np.r_[front_energies, front_energies_]
        side_energies = np.r_[side_energies, side_energies_]
    
    print("REPORT: \nCount all events: %d;\nCount focal-side events: %d"\
        % (count_all, count_focal_side))
    print("Rate (f-s events / all): ", float(count_focal_side) / count_all)
    # calc and show energy distributions
    plot_results(front_energies, side_energies)