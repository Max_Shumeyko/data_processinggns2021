# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 14:17:13 2018
Perform manual calibration of focal-side channel spectr for
selected STRIP in alpha scale.
Build compined time correlated spectr of paired front and side events
and make line calibration of the spectrum.

Prepare data frame to start using scripts from 
<project_folder>/data_processing/readata.
@author: eastwood

"""
from sys import exit

import numpy as np
import matplotlib.pyplot as plt
 
import data_processingGNS2021.tools.spectrum as sp
from data_processingGNS2021.tools.calibrator import calibrator, line

class record: pass

# INITIAL DATA:
#ENERGIES = [7922, 8699, 9261] # KeV Yb+48Ca reaction products
#ENERGIES =  [6039, 6624, 6731, 6899.2, 7137, 7922, 8699, 9261]  # [6039, 6624, 6731, 6899.2, 7137, 7922, 8699, 9261] 
#ENERGIES =  [7137, 7922, 8699, 9261] # [6040,6143,6264,6899.2,7137,7922,8699,9261] # [7922, 8699, 9261]
ENERGIES = [6263, 6419, 7008, 7133] #[6.135, 6.265, 6.904, 7.014] # ArYb
STRIP = 1
    
#### MANUAL CALIBRATION OF FOCAL-SIDE ENERGY SPECTRS IN APLHA SCALE ########
side_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + '/' + 'side_alpha.txt')
front_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + '/' + 'front_alpha.txt')
energies = sp.apply_all_calibration_coefficients(frame, COEFS_FOLDER)
#frame = frame[(energies >= 1100) & (energies <= 6000)]
# Set initial coefficients
def initial_dialog():    
    s = input('Set initial coefficients or type STOP:\n a, b -> ')
    if s.upper() == 'STOP':
        exit(0)
    a, b = [float(i) for i in s.split(',')]
    return a, b

print("""
Instruction:
1. Input side strip number and initial coefficients like this:
>>> a, b -> 2.5, 0.0
2. Highlight peaks according to calibration energies.
3. Perform calibration by typing:
>>> result = Clbr.calibrate()
OR
>>> result = Clbr.calibrate(filename='probe_calibration.txt', ind=strip) 
    -> to write calibration coefficients, peaks and deviations to the file.
4. Check if calibration is good on energy scale. Type:
>>> check_energy_scale()
    
""")
#side_coefs[0, strip], side_coefs[1, strip] = 2.538, -36. 
side_coefs[0, STRIP], side_coefs[1, STRIP] = initial_dialog()
print('Side strip: ', STRIP)
print('Initial coefficients: ', side_coefs[0, STRIP], side_coefs[1, STRIP])
print('S/n = |sum(Ei - Etable)| / Npeaks; i = 0 .. Npeaks-1 \n')
        
#calculate calibration boundaries of spectrums
a, b = side_coefs[0, STRIP], side_coefs[1, STRIP]
xmin = 0 # int((2500 - b) / a) # a == side_coefs[0][i] 
xmax = 10000# int((18600 - b) / a)  # b == side_coefs[1][i] 

# add coefficients to data structure
coefs = record()
coefs.side_coefs = side_coefs
coefs.front_coefs = front_coefs  
  
# build combined time-correlated front-side events spectrum 
# in channel scale
def get_data(coefs, strip=STRIP):
    ch_spectrs, xsample, _ = sp.get_combined_spectrum(frame,\
        event_type='front-side', scale='alpha', tof=False, energy_scale=False, \
        coefs_folder=COEFS_FOLDER, coefs=coefs, 
        bins=np.arange(0, 10000, 20),
        dlt_t=20000)
        
    # apply automatic peak recognition and calibrate founded peaks by line
    # in channel-energy plot
    ch_spectrs = ch_spectrs[STRIP - 1] #according to en_spectrs has shape [0 .. 47][..]
    xsample = xsample[1:]
    return xsample, ch_spectrs
 
xsample, ch_spectrs = get_data(coefs, STRIP)
print(('SAMPLE LENGTH: ', np.sum(ch_spectrs)))

Clbr = calibrator(energies=ENERGIES, calibration_function=line, fit_type='default')
Clbr.read(xsample, ch_spectrs)
#Clbr.dl(xmin, xmax)
print('Energies of the peaks for the calibration:', ENERGIES)
print('''Highlight the peaks on the chart and type:
>>> result = Clbr.calibrate(ind=STRIP)
>>> result = Clbr.calibrate(filename='probe_calibration.txt', ind=STRIP)

Then type:
>>> check_energy_scale()

''')

## show energy distribution to check the calibration quality
def check_energy_scale(result):
    # prepare coefficients
    side_coefs[0, STRIP], side_coefs[1, STRIP] = result
    coefs.side_coefs = side_coefs
    
    # build spectrum
    en_spectrs, xsample, _ = sp.get_combined_spectrum(
        frame, 
        event_type='front-side', scale='alpha',
        tof=False, energy_scale=True, coefs_folder='.', coefs=coefs, 
        bins=np.arange(5000, 10000, 20), dlt_t=500
    )
        
    # plot a figure
    en_spectrs = en_spectrs[STRIP - 1] #according to en_spectrs has shape [0 .. 47][..]
    xsample = xsample[1:]
    fig, ax = plt.subplots()
    ax.plot(xsample, en_spectrs, linestyle='steps')
    ax.set_xlim(4000, 15000)
    plt.show()
    return xsample, en_spectrs
