#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  6 14:43:28 2019

Calculate yields (Counts of R-a events / Integral(I)dt) for each sector of
the irradiated target.

@author: eastwood
"""
import os
import sys
from collections import defaultdict, OrderedDict
from contextlib import contextmanager

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from . import magnets_correlations_optimum as mco
#from data_processingGNS2021.tools.correlations_functions import find_chains

class record:
    pass

# sector number -> region (t_start, t_stop); t = 0 .. 65536
separate_table = OrderedDict({
  "0.210": lambda x: (x >= 3370) & (x <= 10110),
  "1.000": lambda x: (x >= 10110) & (x <= 16865),
  "0.800": lambda x: (x >= 16865) & (x <= 23611),
  "0.600": lambda x: (x >= 23611) & (x <= 30330),
  "0.500": lambda x: (x >= 30330) & (x <= 37070),
  "0.429": lambda x: (x >= 37070) | (x <= 3370),                                      
})

def belong_to_sector(rotation_times_array, sector='s1'): 
    return separate_table[sector](rotation_times_array)  

def get_data_generator(filenames, log_frame, coefs_folder):
    """Yields data_table[DATA_EVENT], filename, time_start, 
    time_stop {timestamps}"""
    read_file_dt = lambda filename, **argv: mco.read_file_(
        filename, mco.read_dt_a,
        coefs_folder=coefs_folder,                                
        serialize=False,
        **argv
    ) # read binary function

    mapper_dt_a = lambda filenames: mco.mapper_(
        filenames, read_file_dt,
        prefix='gns. ',
        data_type='data_table'
    ) # mapper - read function for set of files
    data_tables_gen = mco.data_generator(filenames, mapper_dt_a) # generator
    
    for filename, data_table in data_tables_gen:
        try:
            time_start = log_frame\
              .loc[log_frame['Filename'] == filename, 'Time_start']\
              .iloc[0]
        except KeyError as e:
            print("Not such file %s in the log frame" % (filename,))
            print(e)
            continue
        dt_time = data_table['time_micro'][-1] - data_table['time_micro'][0]
        print("File time length (hours): ", \
            round(dt_time // 10**6 / 3600., 1))
        time_stop = time_start + pd.offsets.Second(dt_time // 10**6)
        yield data_table, filename, time_start, time_stop
#    return mco.generate_time_borders(data_gen, log_frame)

def get_count_Ra(data_table):
    """Count recoil-alpha correlations for Th217 isotopes."""
    search_properties = record()

    #### FIND R-a (recoil-alpha pairs)
    search_properties.time_dlt = 1250 # microseconds
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_first = True
    search_properties.search_fission = False
    
    energy_bundle = [(7000, 25000, 'R'), (9160, 9400, 'a'), ]
    
    chains = mco.find_chains(data_table, energy_bundle, search_properties)
    return mco.calc_len_chains(chains)

@contextmanager
def change_output(output_file):
    try:
        holder = None
        if output_file:
            fobj = open(output_file, 'a')
            holder = sys.stdout
            sys.stdout = fobj
            yield fobj
        else:
            yield None
    finally:
        if holder:
            sys.stdout = holder
            fobj.close()

# good final output       
#def print_result(result, result_a, 
#                 time_start=None, time_stop=None,
#                 header="Report: ", output_file=None):
#    with change_output(output_file):
#        if header:
#            print "\n" + header
#        if time_start is not None and time_stop is not None:
#            print "Time interval: %s -> %s\n" % (time_start, time_stop)
#        print "Thickness Yield_Ra Yield_a"
#        for key, value in result.items():
#    #        isotope, thickness = key.split()
#            s = "{:9s} {:<8.3f} {:4.3f}"\
#              .format(key, round(value, 3), round(result_a[key], 3))
#            print s
#        print '-' * 40
#        print '\n'
            
def print_result(result, result_a, 
                 time_start=None, time_stop=None,
                 header="Report: ", output_file=None):
    with change_output(output_file):
        s = s1 = "{:s},".format(str(time_stop))
        for key, value in list(result.items()):
            s += "{:f},".format(round(value, 3))
            s1 += "{:f},".format(round(result_a[key], 3))
        
        s1 += '\n'
        print(",".join(['time',] + list(result.keys())))
        print(s)
        print(s1)
        
def show_yield_distribution(result_sector_dict):   
    fig, ax = plt.subplots(2, 1, sharex=True)
    title_Ra = """Yields R-a sector distribution for Recoil-alpha events, 
analog data"""
    ax[0].set_title(title_Ra)
    ax[0].set_xlabel("Time")
    ax[0].set_ylabel("Yield, 1 / [microA * second]")
    title_a = """Yields alpha sector distribution for Recoil-alpha events, 
analog data"""
    ax[1].set_title(title_a)
    ax[1].set_xlabel("Time")
    ax[1].set_ylabel("Yield, 1 / [microA * second]")
    
    times = result_sector_dict['times']
    for sector, yields in list(result_sector_dict['yields_Ra'].items()):
        ax[0].plot(times, yields, label=sector)
    for sector, yields in list(result_sector_dict['yields_a'].items()):
        ax[1].plot(times, yields, label=sector)
    ax[0].legend()
    ax[1].legend()
    plt.show()        

def process(filenames, log_frame, protocol_frame,
            coefs_folder,
            OUTPUT_FILENAME):
    data_generator = get_data_generator(filenames, log_frame, coefs_folder)
    sectors = list(separate_table.keys())
    output_dict = {'Ra': defaultdict(list),
                   'a': defaultdict(list)}
    result_sector_dict = {'times': [], 
                          'yields_Ra': defaultdict(list),
                          'yields_a': defaultdict(list)}
    
    start = None
    for data_frame, filename, time_start, time_stop in data_generator:
        if not start:
            start = time_start
        print('Time: ', time_start, ' -> ', time_stop)
        data_frame = data_frame[data_frame['event_type'] == 1]
        d_sec = (time_stop - time_start).total_seconds()
        rotation_times = data_frame['rotation_time']
        tof = data_frame['tof']
        result = OrderedDict()
        result_a = {}
        
        # calc beam integral
        try:
            integral = mco.get_beam_integral(
              protocol_frame, time_start, time_stop
            )
        except mco.Error as e:
            print("! ERROR OCCURED ! Report: {};\nPass file: {}\n".format(
              str(e), filename
            ))
            continue
        
        result_sector_dict['times'].append(time_stop)
        print("Integral / second: %f" % (integral / d_sec, ))
        
        # collect sector's distributions
        for sector in sectors:
            # get sector data sample
            ind = belong_to_sector(rotation_times, sector)            
            # Recoils belong only to chosen sector, alphas belong to all
            data_frame_ = data_frame[(tof & ind) | (~tof)] 
            
            # count R-a correlations
            count_Ra = get_count_Ra(data_frame_)
            # count alphas
            df = data_frame
            count_a = np.sum(
                (~df['tof']) & ind & 
                (df['energy_front'] > 9160) & (df['energy_front'] < 9400)
            )
            print("sector: %s; count_a / second: %f" % \
              (sector, count_a / d_sec, ))
            output_dict['Ra'][sector].append((count_Ra, integral))
            output_dict['a'][sector].append((count_a, integral))
            # calc yields   
            yield_ = count_Ra / integral
            yield_a = count_a / integral
            result[sector] = yield_
            result_a[sector] = yield_a
            result_sector_dict['yields_Ra'][sector].append(yield_)
            result_sector_dict['yields_a'][sector].append(yield_a)
        print_result(result, result_a, header="Report " + filename + ":")
    
    # calculate sectors distributions    
    result = {}
    result_a = {}
    for sector in sectors:
        sample = output_dict['Ra'][sector]
        Ra_yield = sum(ra_count for ra_count, _ in sample) / \
            sum(integral for _, integral in sample)
        result[sector] = Ra_yield
        
        sample = output_dict['a'][sector]
        a_yield = sum(ra_count for ra_count, _ in sample) / \
            sum(integral for _, integral in sample)
        result_a[sector] = a_yield
    print(",".join(list(result.keys())))
    
    # show results
    print_result(result, result_a, start, time_stop, 
                 header="Report for all files: ",
                 output_file="sectors_report.txt")
    show_yield_distribution(result_sector_dict)
    return result

if __name__ == '__main__':  
    COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/23Feb_2020'
    DATA_PATH = '/home/eastwood/gitlab_projects/data/analogue/Feb23_2020'
    # FILENAMES list
    PREFIX = os.path.join(DATA_PATH, 'PbCa.')
    FILENAMES = [PREFIX + str(file_) for file_ in range(458, 468 + 1)]
    
    OUTPUT_FILENAME = 'sectors_yields_report.txt'
    log_frame = mco.get_log_file('log_analog.txt')     
    protocol_frame = mco.read_protocol('.')
#    # processing
    result = process(FILENAMES, log_frame, protocol_frame, 
                     COEFS_FOLDER, OUTPUT_FILENAME)
