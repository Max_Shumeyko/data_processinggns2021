#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 15:59:17 2019

Поиск оптимальных параметров токов магнитов, которым
соответствует максимальный удельный выход коррелированных событий типа
Recoil-alpha и Recoil-fission для аналоговой и цифровой электроники.

Входные данные:
    * файлы с данными: PbCa.33, ...
      см. DATA_PATH_analog, DATA_PATH_digital
    * файлы с калибровочными коэффициентам для передних стрипов (см. <>/tools/spectrum.py)
      см. COEFS_FOLDER_analog и COEFS_FOLDER_digital
    '/home/eastwood/gitlab_projects/data/calibration_coefficients/Oct_2019'
    * файлы-таблицы files_Q1_Q2.txt и files_Q1_Q2_digital.txt
      |Filename:str|D1:float(ток магнита)|D2:float (ток магнита)|Time_start(время
      начала записи файла в формате "Year-month-day Hour-minute-second")|
      |adc_fails:bool (если файл был остановлен при вылете adc)
      ~/gitlab_projects/data_processingGNS2021/processing/tables
    * файл-протокол с данными по токам: 23.11.19.xlsx, ...
        "~/gitlab_projects/data_processingGNS2021/processing/Jan_2020/calculations/protocol.txt"
        
Выходные данные:
    * графики распределения удельного выхода particle_yield("Counts/I_beam", time[min]) 
    * функция и график гиперплоскости удельного выхода частиц от токов 
        магнитов: F_o(Mean("Counts/I_beam"), Magnet1[A], Magnet2[A])
        
@author: eastwood

"""
import os


from datetime import datetime as dtt

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.interpolate import interp1d, splrep, splev 
from scipy.integrate import quad

from data_processingGNS2021.processing.Jan_2020.calculations.parabola_fit\
    import robust_parabola_fit, parabola

from data_processingGNS2021.processing_examples.read_data.read_protocol \
    import read_protocol
# read functions data table
from data_processingGNS2021.processing_examples.read_data.read_functions \
    import read_file_, mapper_ 
from data_processingGNS2021.tools.read_file_2019 import get_data_table as read_dt_a
from data_processingGNS2021.tools.read_digital_2019 import read_data_table as read_dt_d
from data_processingGNS2021.tools.correlations_functions import find_chains

class record:
    pass

class Error(Exception):
    pass

#### PREPARE DATA    
def get_log_file(filename):
    """filename: str -> pd.DataFrame
    
    Read files_{Magnet_1}_{Magnet_2}.txt and build data frame with columns
     |Filename:str|D1:float(ток магнита)|D2:float(ток магнита)|
     |Time_start(время начала записи файла в формате 
     "Year-month-day Hour-minute-second")|adc_fall:fall (если файл был
     остановлен при вылете adc)
    """
    to_datetime = lambda line: dtt.strptime(line, "%Y-%m-%d %H:%M:%S")
    
    df = pd.read_csv(filename)
    df['Time_start'] = list(map(to_datetime, df['Time_start']))
    return df

def data_generator(filenames, mapper):
    for ind, data_table in enumerate(mapper(filenames)):
        if data_table is not None:
            yield os.path.split(filenames[ind])[1], data_table
    
def data_generator_analog(filenames):
    """[str] -> generator (filename, data_table)
    
    Read data tables from given analog filenames list.
    Yields (filename, data_table)
    """
    read_file_dt = lambda filename, **argv: read_file_(filename, read_dt_a,\
                       coefs_folder=COEFS_FOLDER_analog,                                
                       serialize=False,
                       **argv) # return frame, filesize

    mapper_dt_a = lambda filenames: mapper_(filenames, read_file_dt,\
                                            prefix='gns. ',
                                            data_type='data_table')
    return data_generator(filenames, mapper_dt_a)

def data_generator_digital(filenames):
    """[str], pd.DataFrame -> [(pd.DataFrame, int, int)]
    
    Read data tables from given digital filenames list and calculate time interval
    for each using start times from log_frame (see read_protocol).
    
    Yields (filename, data_table)
    """
    read_file_dt = lambda filename, **argv: read_file_(filename, read_dt_d,\
                coefs_folder=COEFS_FOLDER_digital, 
                serialize=False,
                **argv) # return frame, filesize

    mapper_dt_d = lambda filenames: mapper_(filenames, read_file_dt,\
                                            data_type='data_table')
    return data_generator(filenames, mapper_dt_d)

def generate_time_borders(data_tables_generator, log_frame):
    """Calculate time interval
    for each using start times from log_frame (see read_protocol).
    
    Yields (data_table, time_start, time_stop)
    """
    for filename, data_table in data_tables_generator:
        try:
            time_start = log_frame\
              .loc[log_frame['Filename'] == filename, 'Time_start']\
              .iloc[0]
        except KeyError as e:
            print("Not such file %s in the log frame" % (filename,))
            print(e)
            continue
        dt_time = data_table['time_micro'][-1] - data_table['time_micro'][0]
        print("File time length (hours): ", \
            round(dt_time // 10**6 / 3600., 1))
        time_stop = time_start + pd.offsets.Second(dt_time // 10**6)
        yield data_table, time_start, time_stop

#### CALCULATIONS
#def get_beam_integral(protocol_frame, time_start, time_stop):
#    """
#    pd.DataFrame, pd.Timestamp, pd.Timestamp -> float
#    
#    Calculate beam current integral for given time interval.
#    BC_integral = sum(delta_times * beam_currents); delta_times in seconds!
#    
#    """
#    pf = protocol_frame
#    t1, t2 = time_start, time_stop
#    subframe = pf.loc[(pf['time'] >= t1) & (pf['time'] <= t2)]
#    time_pre = subframe['time'].iloc[0]
#    delta_t = time_pre - t1
#    integral = delta_t.total_seconds() * subframe['beam_current'].iloc[0]
#    
#    for _, line in subframe.iloc[1:].iterrows():
#        delta_t = line['time'] - time_pre
#        time_pre = line['time']
#        integral += delta_t.total_seconds() * line['beam_current']
#        
#    delta_t = t2 - line['time']
#    integral += delta_t.total_seconds() * line['beam_current']
#    return integral

def line_function(x0, x1, y0, y1):
    return lambda x: y0 + (y1 - y0) * (x - x0) / (x1 - x0)

def get_bc_array(seconds_array, beam_current_pre, line):
    """Calculates beam current array for given time interval (seconds_array)
    by linear approximation of two points:
        (t_start, beam_current_start), (t_stop, beam_current_stop),
          where N = len(seconds_array)
        
    [int], float, pd.Series -> [float] 
    
    """
    t_start, t_stop = seconds_array[0], seconds_array[-1]
    bc_start, bc_stop = beam_current_pre, line['beam_current']
    
    line = line_function(t_start, t_stop, bc_start, bc_stop)
    return list(map(line, seconds_array))

def get_beam_integral(protocol_frame, time_start, time_stop):
    """
    pd.DataFrame, pd.Timestamp, pd.Timestamp -> float
    
    Calculate beam current integral for given time interval.
    BC_integral = integral(beam_current)dt; 
        delta_times in seconds!
    
    """
    pf = protocol_frame
    t1, t2 = time_start, time_stop
    subframe = pf.loc[(pf['time'] >= t1) & (pf['time'] <= t2)]
    try:
        time_pre = subframe['time'].iloc[0]
        beam_current_pre = subframe['beam_current'].iloc[0]
        delta_t = time_pre - t1
        integral = delta_t.total_seconds() * subframe['beam_current'].iloc[0]
    except IndexError as e:
        raise Error("Protocol doesn't contain beam data for given time")    
    
    for _, line in subframe.iloc[1:].iterrows():
        delta_t = line['time'] - time_pre        
        
        # calculate beam current (bc) integral on time interval using 
        # quadratic intergration of
        # linear interpolation function on given interval bc_start, bc_stop        
        seconds_array = np.arange(int(delta_t.total_seconds())) # 0 .. total_sec, dlt_t = 1 sec
        bc_array = get_bc_array(seconds_array, 
                                beam_current_pre, line)
        interpolation_line = interp1d(seconds_array, bc_array)
        integral += quad(interpolation_line,
                         seconds_array[0], seconds_array[-1])[0]
        time_pre = line['time']
        beam_current_pre = line['beam_current']
        
    delta_t = t2 - line['time']
    integral += delta_t.total_seconds() * line['beam_current']
    return integral
        
def calc_len_chains(chains):
    """Calculate number of founded correlations.
    See search_chains functions.
    
    """
    len_ = 0
    for chain in list(chains.values()):
        len_ += len(chain)
    return len_

def search_Ra(data_table):
    """Count recoil-alpha correlations for No252 isotopes with energies
    in range [8300, 8680] KeV.
    
    """
    search_properties = record()

    #### FIND R-a (recoil-alpha pairs)
    search_properties.time_dlt = 7200000 # microseconds -> 6 seconds
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_first = True
    search_properties.search_fission = False
    
    energy_bundle = [(2000, 25000, 'R'), (8300, 8680, 'a'), ]
    
    chains = find_chains(data_table, energy_bundle, search_properties)
    return calc_len_chains(chains)

def search_RF(data_table):
    """Count recoil-fission correlations for No252 isotopes."""
    search_properties = record()

    search_properties.time_dlt = 8400000 # microseconds -> 6 seconds
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_first = True
    search_properties.search_fission = True
    
    energy_bundle = [(5000, 25000, 'R'), (None, None, 'F'),]
    chains = find_chains(data_table, energy_bundle, search_properties)
    return calc_len_chains(chains)
    
def search_correlations(data_table):
    """
    pd.DataFrame -> int, int, int, int
    
    Calculate number of R-alpha, R-fission, alpha particles and fission 
    decays in given data table.
    
    """
    Ra_count = search_Ra(data_table)
    RF_count = search_RF(data_table)
    
    dt = data_table
    a_count = np.sum((dt['energy_front'] >= 8300) & 
                (dt['energy_front'] <= 8680) & (~dt['tof']))
    F_count = np.sum(dt['energy_front'] > 50000)
    return Ra_count, RF_count, a_count, F_count

def calculate_events_and_integral(data_generator, protocol_frame):
    """Calculate recoil-alpha, recoil-fission, alpha and fission event 
    numbers provided by No252 decay modes and beam current integral
    (Ra_count, RF_count, a_count, F_count, integral) and using
    protocol_frame with experimental data. Then shows result.
    
    """
    Ra_count, RF_count, a_count, F_count = 0, 0, 0, 0
    integral = 0.
    # processing
    for data_table, time_start, time_stop in data_generator:
        try:
            integral += get_beam_integral(protocol_frame, 
                                          time_start, time_stop)
            Ra_, RF_, a_, F_ = \
                search_correlations(data_table)
        except Error as e:
            print("Error occured inside {} - {}; \nReport: {}".format(
                time_start, time_stop, str(e)
            ))
        Ra_count += Ra_
        RF_count += RF_
        a_count += a_
        F_count += F_
       
    return Ra_count, RF_count, a_count, F_count, integral  

### REPORT
FMT_COUNTS = \
"""

DATA TYPE: {0:<10s}; 
Integral = integral(I(time))d(time); I - beam current [A] per second
Number recoil-alphas: {1:<6d}; 
Number recoil-fissions: {2:<6d}; 
Number alphas: {3:<6d}; 
Number fissions: {4:<6d};
integral = {5:<8.2f}

"""

FMT_YIELDS = \
"""DATA TYPE: {0:<10s};
YIELD = sum(events) / integral(I(time))d(time); I - beam current [A] per second
Yield recoil-alphas: {1:<6.5f}; 
Yield recoil-fissions: {2:<6.5f}; 
Yield alphas: {3:<6.5f}; 
Yield fissions: {4:<6.5f}

"""

def make_report(data_type, magnets_group, magnets, Ra_count, RF_count, a_count, 
         F_count, integral, output_filename):
    """Make report output, plot, print and write to file."""
    print("MAGNETS GROUP: ", magnets_group)
    print(FMT_COUNTS.format(data_type.upper(), Ra_count, RF_count,
                            a_count, F_count, integral))
    assert integral > 0, "Empty samples: Integral = 0!"
    yield_Ra = Ra_count / integral
    yield_RF = RF_count / integral
    yield_a = a_count / integral
    yield_F = F_count / integral
    print(FMT_YIELDS.format(data_type.upper(), yield_Ra, yield_RF, 
                            yield_a, yield_F))
    
    FMT_HEADER = "data_type,{0},{1},yield_Ra,yield_Rf,yield_a,yield_F\n"\
                 .format(*magnets)
                
    first_open = not os.path.isfile(output_filename) 
    with open(output_filename, 'a') as f:
        if first_open:
            f.write(FMT_HEADER)
        magnet_current1, magnet_current2 = magnets_group
        parameters = (data_type, magnet_current1, magnet_current2, 
                      yield_Ra, yield_RF, yield_a, yield_F)
        
        line = ','.join(str(item) for item in parameters)
        f.write(line + '\n')

def read_magnets_report(folder='.', filename='magnets_report.txt'):
     if os.path.isfile(filename):
         frame = pd.read_csv(filename)
         frame = frame.drop_duplicates()
         return frame
     raise IOError("No such file: %s" % filename)     

def plot_magnet_currents(report_frame, magnets, data_type, yield_types):
    """Plots several yield lines (Ra, RF, a, F) for each magnet type.
    Plot: "Magnet current, A" / "Yield, (A*s)^-1"
    
    """   
    for magnet in magnets:
        fig, ax = plt.subplots()
        ax.set_title("Yields on %s magnet;\nData sourse: %s" % 
                     (magnet, data_type.upper())
                     )
        ax.set_xlabel("Magnet current, A")
        ax.set_ylabel("Yield, events/ (A * second)")
        for yield_name in list(yield_types.keys()):                        
            currents = report_frame[magnet]
            yields = report_frame[yield_name]
            ax.plot(currents, yields, 'x', label=yield_types[yield_name])
            

#            # plot spline interpolation
#            #spline = interp1d(currents, yields, kind='cubic')#'quadratic')#splrep(currents, yields)
#            spline = splrep(currents, yields, k=4, s=0)
#            x_array = np.linspace(min(currents), max(currents), 100)
#            #s_yields = spline(x_array)
#            s_yields = splev(x_array, spline)
#            ax.plot(x_array, s_yields, '--', 
#                    alpha=0.6,
#                    label=yield_types[yield_name] + "_spline")
            
            # plot parabola fit
            coef, std_ = robust_parabola_fit(currents, yields)
            x_array = np.linspace(min(currents), max(currents), 100)
            s_yields = parabola(coef, x_array)
            ax.plot(x_array, s_yields, '--', 
                    alpha=0.6,
                    label=yield_types[yield_name] + " robust porabola fit")
            
            # find max
            ind_max = np.argmax(s_yields)
            x_max, y_max = x_array[ind_max], s_yields[ind_max]
            ax.plot([x_max, ], [y_max, ], 'D',)
            
        ax.legend()
 
def plot_3d(report_frame, magnets, yield_types):
    """Show 3d yields - currents distributions."""
    if len(magnets) != 2:
        raise ValueError("wrong magnets dimensions!")
        
    magn1, magn2 = magnets
    for yield_type in list(yield_types.keys()):
     
        ind_a = report_frame['data_type'] == 'analog'
        Z_a = report_frame[ind_a][yield_type].values
        X_a = report_frame[ind_a][magn1].values
        Y_a = report_frame[ind_a][magn2].values
        
        ind_d = report_frame['data_type'] == 'digital'
        Z_d = report_frame[ind_d][yield_type].values
        X_d = report_frame[ind_d][magn1].values
        Y_d = report_frame[ind_d][magn2].values

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_title("Yields on magnets: %s;\nYield type: %s" % 
                         (",".join([str(m) for m in magnets]), 
                          str(yield_type))
                    )
        ax.set_xlabel("Magnet current %s, A" % magn1)
        ax.set_xlabel("Magnet current %s, A" % magn2)
        ax.set_zlabel("Yield, events/ (A * second)")
        ax.scatter(X_a, Y_a, Z_a, label="analog")
        ax.scatter(X_d, Y_d, Z_d, label="digital")
        ax.legend()
    
def show_results():
    report_frame = read_magnets_report()
    rf = report_frame
    
    magnet_names = set(['D1', 'D2', 'Q1', 'Q2', 'Q3'])
    columns = set(rf.columns)
    
    magnets = list(columns & magnet_names)
    data_types = ['analog', 'digital']
    yield_types = {'yield_Ra': 'Yield recoil-alpha correlations', 
                  'yield_Rf': 'Yield recoil-fission correlations', 
                  'yield_a': 'Yield alpha-particles', 
                  'yield_F': 'Yield fission decays'}
    
    for data_type in data_types:
        frame_a = rf[rf['data_type'] == data_type]
        plot_magnet_currents(frame_a, magnets, data_type, yield_types)
    plot_3d(report_frame, magnets, yield_types)
    
#### MAIN PROCESSING FUNCTION
def process(protocol_frame, log_frames_analog, log_frame_digital,
            output_filename,
            magnets=['Q1', 'Q2']):
    
    data_source = {
       'analog': (data_generator_analog, log_frame_analog, DATA_PATH_analog), 
#                   filenames_analog), 
       'digital': (None, log_frame_digital,
                   DATA_PATH_digital)}#, 
#                    filenames_digital),}data_generator_digital
    
    for data_type, (data_gen, log_frame, data_path) in list(data_source.items()):
        if data_gen is None:
            continue
        # group by magnets current values, like (Q1, Q2) = (224.9, 52.3) [A]
        for magnets_group, data_frame in log_frame.groupby(magnets):
            print('MAGNETS: %s - CURRENTS: %s' % (magnets, magnets_group))
            # init  
            get_full_path=lambda filename: os.path.join(data_path, filename)
            filenames = list(map(get_full_path,
                            data_frame['Filename']))
            data_generator = generate_time_borders(data_gen(filenames), 
                                                   data_frame)
            # processing
            Ra_count, RF_count, a_count, F_count, integral =\
                calculate_events_and_integral(data_generator, protocol_frame)
    
            # show results
            make_report(data_type, magnets_group, magnets, 
                 Ra_count, RF_count, a_count, 
                 F_count, integral, output_filename) 
        
if "__main__" in __name__:   
    # get data filenames     
#    DATA_PATH_analog = '/home/eastwood/gitlab_projects/data/analogue/Jan_2020'
    DATA_PATH_analog = '/home/eastwood/gitlab_projects/data/analogue/March_2020'
    DATA_PATH_digital = '/home/eastwood/gitlab_projects/data/digital/Jan_2020'
#    # get calibration coeficients
    COEFS_FOLDER_analog = \
        '/home/eastwood/gitlab_projects/data/calibration_coefficients/Jan_2020'
    COEFS_FOLDER_digital = \
        '/home/eastwood/gitlab_projects/data/calibration_coefficients/Jan_2020_digit'
    
#    # analog filenames
#    PREFIX = os.path.join(DATA_PATH_analog, 'PbCa.')
#    FILENAMES_ANALOG = [PREFIX + str(file_) for file_ in range(33, 38 + 1)]   
#    
#    # digital filenames
#    filenames = ['24jan5.shdat',]
#    #['24jan4.shdat', '24jan5.shdat', '26jan1.shdat', '26jan2.shdat']
#    f = lambda file_: os.path.join(DATA_PATH_digital, file_)
#    FILENAMES_DIGITAL = map(f, filenames)
    
    # get log filename
    # лог файл с токами, именами файлов и временем старта каждого
    LOG_FILE_analog = "/home/eastwood/gitlab_projects/data_processingGNS2021/" +\
               "processing/Jan_2020/calculations/files_Q1_Q2.txt" 
               
    LOG_FILE_digital = "/home/eastwood/gitlab_projects/data_processingGNS2021/" +\
               "processing/Jan_2020/calculations/files_Q1_Q2_digital.txt"
    OUTPUT_FILENAME = 'magnets_report.txt'
    # read data
    log_frame_analog = get_log_file(LOG_FILE_analog)
    log_frame_digital = get_log_file(LOG_FILE_digital)
    protocol_frame = read_protocol('.')

    # NOTE! data reader functions get calibration coefficients from 
    # processing/Jan_2020/read_show/generator_... functions - 
    # see "import mapper_dt" above
    process(protocol_frame, log_frame_analog, log_frame_digital, 
            OUTPUT_FILENAME,
            magnets=['Q1', 'Q2'])
#    
    # results
    show_results()
        
    
    