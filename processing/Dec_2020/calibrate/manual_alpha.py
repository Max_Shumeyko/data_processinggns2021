# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 14:34:23 2018

Perform manual calibration of focal (or back) channel spectr in alpha scale.

Prepare data frame to start using scripts from 
<project_folder>/data_processing/read_data.

Then use <project_folder>/data_processing/get_distributions 
to build front spect (spectr_front) or back spectr (spectr_back)
to start calibration.
@author: eastwood
"""
from data_processingGNS2021.tools.calibrator import calibrator, line
import data_processingGNS2021.tools.spectrum as sp
#### MANUAL CALIBRATION METHOD

print("""
    Instruction:
    1. Input strip number like this:
    >>> strip >> 1
    2. Highlight peaks according to calibration energies.
    3. Perform calibration by typing:
    result = Clbr.calibrate()
    result = Clbr.calibrate(filename='probe_calibration.txt', ind=strip) ->
        -> to write calibration coefficients, peaks and deviations to the file.
    4. Move to 1. until perform calibration for all 48 front strips or 128 back
    strips.
""")
    
# Set initial coefficients
def initial_dialog():
    from sys import exit
    s = input('Set strip or type STOP:\n strip >>')
    if s.upper() == 'STOP':
        exit(0)
    return int(s)
    
# GET SPECTRUM for calibration
# EXAMPLE:
hist = spectr_back
#hist = sp.Spectrum.fromfile('Dec_2017_Yuri.txt')
#
energies = [ 7137, 7922, 8699, 9261] #[6263, 6419, 7008, 7133]
#[6040, 6143, 6264, 7922, 8699, 9261] #  6899.2, 7137,
#energies = [7922,8699,9261]
#[6030, 6258.8, 6625., 6732., 6899.2, 7922, 8699, 9261] # KeV Yb+48Ca reaction products
#energies = [6732., 6899.2, 7922, 9261]
#energies = [5115.2, 5223.3, 5304.3, 7200.3, 7862., 8404.3]#[6899.2,7137,7922,8699,9261]#[6135, 6265, 6904, 7014, 7132] # , 6419

Clbr = calibrator(energies=energies, calibration_function=line, fit_type='default')
strip = initial_dialog()
Clbr.read(hist.data.index, hist[strip])
#Clbr.dl(2400, 4500)
#Clbr.dl(2300, 4200)
#Clbr.dl(1450, 2600)
#Clbr.dl(1270, 2700)
# --> START CALIBRATION. Type in console:
# >>> Clbr.calibrate()
# >>> Clbr.calibrate(filename='probe_calibration.txt', ind=strip)