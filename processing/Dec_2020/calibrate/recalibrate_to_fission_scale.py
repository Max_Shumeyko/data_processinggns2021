"""
Recalculate calibration coefficients on fission scale using
alpha calibration coefficients and channels.

Prepare data frame generator and alpha calibration coefficients to start 
using scripts from <project_folder>/data_processing/calibration. 
! note: check if alpha scale is already calibrated so the coefficients are 
required to perform calibration in fission scale.

15.01.2020 eastwood
"""

import os

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

import data_processingGNS2021.tools.spectrum as sp
from data_processingGNS2021.tools.read_file_2020_nov import read_parameters

"""TODO
1. make a conveer with frame_generator
"""
def get_data(filename, front_coefs, back_coefs, side_coefs, number=400000):
    print("START reading: ", filename)
    data = read_parameters(filename, ['event_type', 'coord', 'alpha_channel', 
            'back_alpha_channel', 'fission_channel', 'back_fission_channel',
            'side_strip'])
    
    event_type = data['event_type']
    front_strips, back_strips = data['coord']
    front_channels = data['alpha_channel']
    front_fission_channels = data['fission_channel']
    
    back_channels = data['back_alpha_channel']
    back_fission_channels = data['back_fission_channel']  
    
    side_strips = data['side_strip']
    side_channels = front_channels.copy()
    side_fission_channels = front_fission_channels.copy()
    
    # get front channels, energies
    ind = (event_type == 1) & (front_channels > 0) & (front_strips > 0) \
        & (front_fission_channels > 50) & (front_fission_channels < 200) 
    ind = ind.nonzero()[0][:number]
    front_channels = front_channels[ind]
    front_fission_channels = front_fission_channels[ind]
    front_strips = front_strips[ind]
    front_energies = front_coefs[0][front_strips] * front_channels + \
      front_coefs[1][front_strips]
      
     # get back channels, energies
    ind = (event_type == 1) & (back_channels > 0) & (back_strips > 0) \
        & (back_fission_channels > 50) & (back_fission_channels < 200)  
    ind = ind.nonzero()[0][:number]
    back_channels = back_channels[ind]
    back_fission_channels = back_fission_channels[ind]
    back_strips = back_strips[ind]
    back_energies = back_coefs[0][back_strips] * back_channels + \
      back_coefs[1][back_strips]
          
    # get side channels, energies
    ind = (event_type == 3) & (side_channels > 0) & (side_strips > 0) \
        & (side_fission_channels > 50) & (side_fission_channels < 200) 
    ind = ind.nonzero()[0][:number]
    side_channels = side_channels[ind]
    side_fission_channels = side_fission_channels[ind]
    side_strips = side_strips[ind]
    side_energies = side_coefs[0][side_strips] * side_channels + \
      side_coefs[1][side_strips]
    
    print("FINISH reading: ", filename)
    return front_strips, front_fission_channels, front_energies, \
           back_strips, back_fission_channels, back_energies, \
           side_strips, side_fission_channels, side_energies
      
#### fitting functions 
def line(x, *p):
    a, b =  p
    return a * x + b

def get_fitted(channels, energies):
    """Fit data by line 
    input:
        channels - fission channels to calibrate
        energies - alpha events energies
    return coefs, coefficients_deviations, fit line
    """
    x, y = channels, energies
    p0 = [1., 0.]
    coef, corr = curve_fit(line, x, y, p0=p0)
    
    std_deviations = np.sqrt(np.diag(corr))
    curve = line(x, *coef)
    return coef, std_deviations, curve

# fit data by line and calculate fission coefficients
def write_report(output_filename, strip, coefs):
    report = '\n%d    A = %2.5f ; B = %2.1f\n' % (strip, coefs[0], coefs[1])
    with open(output_filename, 'a') as f:
        f.write(report)
    
def calculate_coefs(strip_range=None, input_strips=None, 
        input_channels=None, input_energies=None, event_type='Front', 
        output_filename='front_fission.txt', show=False):
    
    for strip in range(*strip_range):
        ind = input_strips == strip
        
        if np.sum(ind) < 10:
            print("\n{} STRIP {} IS MISSED\n".format(event_type, strip))
            continue
        
        input_channels_ = input_channels[ind] 
        input_energies_ = input_energies[ind]
        
        coefs, dev, F_fit = get_fitted(input_channels_, input_energies_)
       
        if show:
            fig, ax = plt.subplots()
            ax.set_title("%s fission calibration" % (event_type, ))
            ax.set_xlabel("fission channel")
            ax.set_ylabel("energy, KeV")
            
            ax.plot(input_channels_, input_energies_, 'o',
                    input_channels_, F_fit, '--')            
            plt.show()
        
        write_report(output_filename, strip, coefs)
        print(f"""Result:
            {event_type} strip: {strip}
            {event_type} coefs: {coefs}""")    
    
def collect_data(filenames, front_coefs, back_coefs, side_coefs, 
                 number=10000):
    collection = [get_data(filename, front_coefs, back_coefs, side_coefs, 
                           number=number) for filename in filenames]
    print(collection)
    print()
    assert len(collection) > 0, "Did not collect enough data"
    size = len(collection[-1])
    print(collection[-1])
    print()
    output = []
    for i in range(size):
        collected = np.concatenate([row[i] for row in collection])
        output.append(collected)
    print(output)
    print()
    return output
                
def process_data(data, show=False):  
    """Fit data by line and calculate front fission and back fission
    coefficients."""
    print("START processing: ")
    front_strips, front_fission_channels, front_energies, \
      back_strips, back_fission_channels, back_energies, \
      side_strips, side_fission_channels, side_energies = data
    
    front_output = os.path.join(COEFS_FOLDER, 'front_fission.txt')
    back_output = os.path.join(COEFS_FOLDER, 'back_fission.txt')
    side_output = os.path.join(COEFS_FOLDER, 'side_fission.txt')
    
    data_dict = {
                'front': {'strip_range': (1, 48), 
                            'input_strips': front_strips,
                            'input_channels': front_fission_channels,
                            'input_energies': front_energies,
                            'event_type': 'Front',
                            'output_filename': front_output,
                            'show': False,
                  },
                
                 'back': {'strip_range': (1, 128), 
                           'input_strips': back_strips,
                           'input_channels': back_fission_channels,
                           'input_energies': back_energies,
                           'event_type': 'Back',
                           'output_filename': back_output,
                           'show': False,
                  },
                 
                  'side': {'strip_range': (1, 64), 
                            'input_strips': side_strips,
                            'input_channels': side_fission_channels,
                            'input_energies': side_energies,
                            'event_type': 'Side',
                            'output_filename': side_output,
                            'show': False,
                   },
    }
    print("START fitting")
    # calculate
    for detector_type in data_dict.keys():
        calculate_coefs(**data_dict[detector_type])
        
    print("FINISH")
    return True
            
if __name__ == '__main__':
    COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Dec_2020'
    DATA_PATH = '/home/eastwood/gitlab_projects/data/analogue/Nov_2020'
    
    # FILENAMES list
    PREFIX = os.path.join(DATA_PATH, 'YbCa.')
    FILENAMES = [PREFIX + str(file_) for file_ in range(520, 600 + 1)] 
    FILENAMES = [filename for filename in FILENAMES 
                 if os.path.isfile(filename)]
    
    front_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + 
                                               '/front_alpha.txt')
    back_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + 
                                               '/back_alpha.txt')
    side_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + 
                                               '/side_alpha.txt')
    
    # processing
    process_func = lambda filenames: \
      process_data(
        collect_data(filenames, front_coefs, back_coefs, side_coefs, 
                 number=4000),
        show=False, 
      )
        
    process_func(FILENAMES)