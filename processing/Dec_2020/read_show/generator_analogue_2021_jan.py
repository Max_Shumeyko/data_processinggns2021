#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 13:31:25 2019

GET DATA ANALOGUE 2019
Read analog experimental data from raw binary to data frame and
calibration coefficients to coefs.
The data format is actual for Oct 2019.

RETURN generator which yields data frames.

Valid filename's patterns:
    <prefix.>xxx
    <prefix.>xxx, <prefix.>yyy, <prefix.>zzz, ...
    <prefix.>xxx - <prefix.>yyy
    <prefix.>xxx-<prefix.>yyy and etc.
    
Output format description (np.ndarray) FRAME:
    event_type: '1'-front, '2'-back, '3'-side, '4'-veto
    strip: front: 1-48 back: 1-128, side: 1-6
    channel: front: 4096 back: 8192, side: 8192
    scale: 0-alpha, 1-fission
    time_macro: int64 seconds
    time_micro: int64 microseconds
    beam_mark: bool
    tof: bool
    tofD1: bool
    tofD2: bool
    rotation_time: uint16
    synchronization_time: uint16
    
Output format description (np.ndarray) DATA_TABLE:
    event_type: 1 - focal-back, 3 - side
    energy_front: double 0 .. 250000 [KeV]
    energy_back: double 0 .. 250000 [KeV]
    cell_x: x coordinate 0 .. 48
    cell_y: y coordinate 0 .. 128
    time_micro: int microseconds 
    time_macro: int seconds 
    beam_mark: bool beam-on mark
    tof: bool time-of-flight mark   
    
@author: eastwood
"""

import os

import numpy as np

from data_processingGNS2021.processing_examples.read_data.read_functions \
    import read_file_, mapper_

from data_processingGNS2021.tools.spectrum \
    import apply_all_calibration_coefficients
    
# read functions frame 
from data_processingGNS2021.tools.read_file_2020_dec \
    import read_file as read_analog  
    
read_file = lambda filename, **argv: read_file_(
    bytes(filename, encoding='utf-8'), 
    read_analog, 
    strip_convert=True,
    count_blocks=0, 
    serialize=False, # save output in .npy file
    **argv
) # return frame, filesize

mapper_frame = lambda filenames: mapper_(filenames, read_file, 
                                         prefix='gns. ', 
                                         data_type='frame') # return frame gns.

# read functions data table
from data_processingGNS2021.tools.read_file_2020_dec import get_data_table as read_dt
read_file_dt = lambda filename, **argv: read_file_(
    bytes(filename, encoding='utf-8'), read_dt,\
    coefs_folder=COEFS_FOLDER, 
    serialize=False,
    **argv
) # return frame, filesize

mapper_dt = lambda filenames: mapper_(filenames, read_file_dt,\
                                      prefix='gns. ',
                                      data_type='data_table')

# folder with calibration coefficients
#COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Oct_2019'

# COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Nov_2020_dima'#Oct_2020'#Mar_2020'
COEFS_FOLDER = 'D:\\work\\data\\calibration_coefficients\\Jan_2021'
DATA_PATH = 'D:\\work\\data\\analogue\\YbCa_2021'
 
# FILENAMES list
# PREFIX = os.path.join(DATA_PATH, 'YbCa.')

PREFIX = os.path.join(DATA_PATH, 'DEC20. ')
FILENAMES = [PREFIX + str(file_) for file_ in range(362, 431 + 1)] 

#FILENAMES = [DATA_PATH + '/probe.bin', ] # generated sample
# read
frame_generator = mapper_frame(FILENAMES)
dt_generator = mapper_dt(FILENAMES)

### 
# frame = np.concatenate([frame for frame in frame_generator if frame is not None])
#energies = apply_all_calibration_coefficients(frame, COEFS_FOLDER)
#data_table = np.concatenate([dt for dt in dt_generator if dt is not None])