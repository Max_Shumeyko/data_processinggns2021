# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 17:16:04 2018

GET GENERATOR DIGITAL 2019
Read digital experimental data from raw binary to data frame and
calibration coefficients to coefs.
The data format is actual for OCT 2019.  
    
Output format description (np.ndarray): 
    event_type: '1'-front, '2'-back, '3'-side, '4'-veto
    strip: front: 1-48 back: 1-128, side: 1-6
    channel: front: 4096 back: 8192, side: 8192
    scale: 0-alpha, 1-fission
    time_macro: int64 seconds
    time_micro: int64 microseconds
    beam_mark: bool
    tof: bool
    tofD1: bool
    tofD2: bool
    synchronization_time: uint16
    

@author: eastwood
"""
import os 

import numpy as np

import data_processingGNS2021.tools.spectrum as sp
from data_processingGNS2021.processing_examples.read_data.read_functions \
    import read_file_, mapper_

from data_processingGNS2021.tools.read_digital_2019 import read_file as read_digital

# read functions  frame   
read_file = lambda filename, **argv: read_file_(filename, read_digital, 
                                                serialize=False, **argv) # return frame, filesize
mapper = lambda filenames: mapper_(filenames,
                                   read_file, prefix='',
                                   data_type='frame') # return frame

# read functions data table
from data_processingGNS2021.tools.read_digital_2019 import read_data_table as read_dt

read_file_dt = lambda filename, **argv: read_file_(filename, read_dt,\
                coefs_folder=COEFS_FOLDER, 
                serialize=False,
                **argv) # return frame, filesize

mapper_dt = lambda filenames: mapper_(filenames, read_file_dt,\
                                      data_type='data_table')

# specify calibration and data folders
COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Mar_2020_digit'
#'/home/eastwood/gitlab_projects/data/calibration_coefficients/YtCa_Feb2020_digit'
#COEFS_FOLDER = \
#'/home/eastwood/gitlab_projects/data/calibration_coefficients/PbCa_Feb2020_after_23jan_digit'
#'/home/eastwood/codes/C++/DAQ_GNS2019/exe/SIMULATION_DATABASE'
#DATA_PATH = '/home/eastwood/gitlab_projects/data/digital/Jan_2020'
DATA_PATH = '/home/eastwood/gitlab_projects/data/digital/Mar_2020'
#'/home/eastwood/gitlab_projects/data/digital/YtCa_Feb2020'
#DATA_PATH = \
#'/home/eastwood/gitlab_projects/data/digital/PbCa_Jan_Feb2020_after23jan'
# READ
os.chdir(DATA_PATH)
filenames = ['30mart6.shdat', ]#'30mart7.shdat', '30mart8.shdat',  '30mart9.shdat']
#filenames = sorted(os.listdir('.'))
#filenames = [fname for fname in filenames if os.path.getsize(fname) > 0]
#banned = ['28mart13off.shdat', '29mart7.shdat']
#filenames = [i for i in filenames if i not in banned]

f = lambda file_: os.path.join(DATA_PATH, file_)
FILENAMES = list(map(f, filenames))


frame_generator = mapper(FILENAMES)
dt_generator = mapper_dt(FILENAMES)

coefs = sp.get_all_calibration_coefficients(coefs_folder=COEFS_FOLDER)
coefs_folder = os.path.abspath(COEFS_FOLDER) 

frame = np.concatenate(list(frame_generator))
energies = sp.apply_all_calibration_coefficients(frame, COEFS_FOLDER)
#data_table = np.concatenate(list(dt_generator))

