# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 15:28:35 2018

Perform automatic calibration of focal (or back) channel spectr in alpha scale.

Prepare data frame to start using scripts from 
<project_folder>/data_processing/read_data.

Then use <project_folder>/data_processing/get_distributions 
to build front spect (spectr_front) or back spectr (spectr_back) in alpha scale
to start calibration.
Instruction:
    1. Specify calibration channel region: xmin, xmax.
    2. Specify strips (number) of spectrs to calibrate: start, stop.
       Calibration will be performed for strips from start to stop.
    3. Specify output_filename for output.
    4. Run this script to perform calibration.
    5. Variate calibration properties for better peak recognition.
        Most important parameters are:
        * calibration_properties.energies
        * filter_properties.threshold 
        * filter_properties.window_smooth 
    6. Repeat until you have all strips calibrated
        48 front strip or 128 back strips.
        
@author: eastwood
"""

import numpy as np

import data_processingGNS2021.tools.calibration as clbr
   
#### AUTO CALIBRATE FRONT OR BACK STRIPS IN ALPHA-SCALE ######
    
# SPECIFY PARAMETERS:
try:
    hist = spectr_side # spectr_back
except NameError:
    hist = collection['side']
    
xmin, xmax = 1250, 1900
start, stop = 1, 64 # choose strips 

output_filename = None
output_filename = os.path.join(COEFS_FOLDER, 'side_alpha.txt')

class record: pass
    
def get_calibration_properties():#set calibration parameters
    
    calibration_properties = record()
    calibration_properties.visualize = False
    calibration_properties.weighted_average_sigma = False
    
    # параметр для определения области соответствия расчетного положения пиков 
    # (соотв. пропорции калибровочных энергий) и реальных найденных пиков 
    # (метод spectrum_tools.search_peaks), если реальные пики не находятся вблизи расчетных 
    # в области dlt, то они заменяются на расчетные, в противном случае выбирается пик
    # наиболее близкий к расчетному. see calibration.calibrate_area
    calibration_properties.dlt = 17 
    
    # SPECIFY calibration energies
    # EXAMPLE: calibration_properties.energies = [6040,6143,6264,6899.2,7137,7922,8699,9261] 
    calibration_properties.energies = [4818.7, 5147.9, 5486.9]
            
    search_properties = record() #for noisy spectrum
    search_properties.widths = np.arange(1, 7)
    search_properties.wavelet = 'blackman'
    search_properties.min_length = 1.
    search_properties.min_snr = 0.04
    search_properties.noise_perc = 0.3 
    
    filter_properties = record()
    filter_properties.window_smooth = 5
    filter_properties.smooth_wavelet = 'blackman'
    filter_properties.background_options = 'BACK1_ORDER8' #,BACK1_INCLUDE_COMPTON' 
    filter_properties.threshold = 220.0
    return calibration_properties, search_properties, filter_properties
    
calibration_properties, search_properties, filter_properties = get_calibration_properties()
#
### --> START CALIBRATION: 
xpeaks, coefs, good_results = clbr.calibrate_spectr(start, stop, xmin, xmax,\
    hist.data, calibration_properties, filter_properties, \
    search_properties, output_filename=output_filename)
