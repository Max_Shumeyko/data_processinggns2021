"""
Recalculate calibration coefficients on fission scale using
alpha calibration coefficients and channels.

Prepare data frame generator and alpha calibration coefficients to start 
using scripts from <project_folder>/data_processing/calibration. 
! note: check if alpha scale is already calibrated so the coefficients are 
required to perform calibration in fission scale.

15.01.2020 eastwood
"""
import os

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

import data_processingGNS2021.tools.spectrum as sp
from data_processingGNS2021.tools.read_file_2020_dec import read_parameters

"""TODO
1. upload alpha coefficients
2. choose events for the calibration
3. calc energies
4. send fission channels and alpha enegies to calibration
"""
def get_data(filename, front_coefs, back_coefs, side_coefs, number=400000):
    
    print("START reading: ", filename)
    data = read_parameters(
        filename, 
        [
            'event_type', 
            'coord', 'side_strip', 
            'alpha_channel', 'back_alpha_channel', 
            'fission_channel', 'back_fission_channel'
         ]
    )
    
    event_type = data['event_type']
    front_strips, back_strips = data['coord']
    side_strips = data['side_strip']
    front_channels = data['alpha_channel']
    back_channels = data['back_alpha_channel']
    front_fission_channels = data['fission_channel']
    back_fission_channels = data['back_fission_channel']   
    
    # get front channels, energies
    # ! <name>_coefs[0] == [0., 0.], 
    # so all input zero strips will return 0 energy
    front_energies = front_coefs[0][front_strips] * front_channels + \
      front_coefs[1][front_strips]
      
     # get back channels, energies
    back_energies = back_coefs[0][back_strips] * back_channels + \
      back_coefs[1][back_strips]
    
    # delete zeros
    ind = (front_energies > 0) & (front_energies < 10000) & \
          (front_fission_channels != 0) & (event_type == 1)
    front_strips = front_strips[ind][:number]
    front_energies = front_energies[ind][:number]
    front_fission_channels_ = front_fission_channels[ind][:number]
    
    #
    back_strips = back_strips[ind][:number]
    back_energies = front_energies
    back_fission_channels = back_fission_channels[ind][:number]
    
      
    # get side channels, energies
    side_energies = side_coefs[0][side_strips] * front_channels + \
      side_coefs[1][side_strips]
      
    # print("CHECK SIDE ENERGIES", side_energies)
    
    # delete zeros
    ind = (side_energies > 0) & (side_energies < 10000) & \
          (front_fission_channels != 0) & (event_type == 3)
    side_strips = side_strips[ind][:number]
    side_energies = side_energies[ind][:number]
    side_fission_channels = front_fission_channels[ind][:number]
    
    print("FINISH reading: ", filename)
    return front_strips, front_fission_channels_, front_energies, \
           back_strips, back_fission_channels, back_energies,\
           side_strips, side_fission_channels, side_energies
      
#### fitting functions 
def line(x, *p):
    a, b =  p
    return a * x + b

def get_fitted(channels, energies):
    """Fit data by line 
    input:
        channels - fission channels to calibrate
        energies - alpha events energies
    return coefs, coefficients_deviations, fit line
    """
    x, y = channels, energies
    p0 = [1., 0.]
    coef, corr = curve_fit(line, x, y, p0=p0)
    
    std_deviations = np.sqrt(np.diag(corr))
    curve = line(x, *coef)
    return coef, std_deviations, curve

# fit data by line and calculate fission coefficients
def write_report(output_filename, strip, coefs):
    report = '\n%d    A = %2.5f ; B = %2.1f\n' % (strip, coefs[0], coefs[1])
    with open(output_filename, 'a') as f:
        f.write(report)
    
def process_detector(process_dict, detector, show):
        
    for strip in process_dict[detector]['strip_range']:
        strips = process_dict[detector]['strips']
        ind = strips == strip
        
        if np.sum(ind) < 10:
            print(f"\n{detector.upper()} STRIP {strip} IS MISSED\n")
            continue
        
        fission_channels = process_dict[detector]['fission_channels'] 
        fission_channels_ = fission_channels[ind]
        energies = process_dict[detector]['energies']
        energies_ = energies[ind]
        
        Fcoefs, dev, F_fit = \
            get_fitted(fission_channels_, 
                       energies_)
       
        if show:
            fig, ax = plt.subplots()
            ax.set_title(f"{detector.capitalize()} fission calibration")
            ax.set_xlabel("fission channel")
            ax.set_ylabel("energy, KeV")
            
            ax.plot(fission_channels_, energies_, 'o',
                    fission_channels_, F_fit, '--')            
            plt.show()
        
        output_fname = process_dict[detector]['output_fname']
        write_report(output_fname, strip, Fcoefs)
        print(f"""Result:
            {detector} strip: {strip}
            {detector} coefs: {Fcoefs}
            """)
            
def process_data(data, show=False):  
    """Fit data by line and calculate front fission and back fission
    coefficients."""
    print("START processing: ")
    front_strips, front_fission_channels, front_energies, \
      back_strips, back_fission_channels, back_energies,\
      side_strips, side_fission_channels, side_energies = data
    
    print("START fitting")
    # calculate
    
    process_dict = {
        'front': {
            'strip_range': range(1, 48 + 1),
            'strips': front_strips,
            'fission_channels': front_fission_channels,
            'energies': front_energies,
            'output_fname': os.path.join(COEFS_FOLDER, 'front_fission.txt'),
        },
        
        'back': {
            'strip_range': range(1, 128 + 1),
            'strips': back_strips,
            'fission_channels': back_fission_channels,
            'energies': back_energies,
            'output_fname': os.path.join(COEFS_FOLDER, 'back_fission.txt'),
        },
        
        'side': {
            'strip_range': range(1, 64 + 1),
            'strips': side_strips,
            'fission_channels': side_fission_channels,
            'energies': side_energies,
            'output_fname': os.path.join(COEFS_FOLDER, 'side_fission.txt'),
        },
        
    }
    
    process_detector(process_dict, 'front', show)
    process_detector(process_dict, 'back', show)
    process_detector(process_dict, 'side', show)
        
    print("FINISH")
            
if __name__ == '__main__':
    # COEFS_FOLDER = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Jan_2020'
    # DATA_PATH = '/home/eastwood/gitlab_projects/data/analogue/Jan_2020'
    
    # FILENAMES list
    # PREFIX = os.path.join(DATA_PATH, 'PbCa.')
    # FILENAMES = [PREFIX + str(file_) for file_ in range(33, 33 + 1)]
    # FILENAMES = [filename for filename in FILENAMES 
    #              if os.path.isfile(filename)]
    
    front_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + 
                                               '/front_alpha.txt')
    back_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + 
                                               '/back_alpha.txt')
    side_coefs = sp.get_calibration_coefficients(COEFS_FOLDER + 
                                               '/side_alpha.txt')
    filename = 'D:\\work\\data\\analogue\\YbCa_2021\\DEC20. 402'
    # processing
    process_func = lambda filename: \
        process_data(
            get_data(filename, front_coefs, back_coefs, side_coefs, 
                     number=400000),
            show=False
        )
        
    process_func(filename)