# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 15:28:35 2018

Perform automatic calibration of focal (or back) channel spectr in alpha scale.

Prepare data frame to start using scripts from 
<project_folder>/data_processing/read_data.

Then use <project_folder>/data_processing/get_distributions 
to build front spect (spectr_front) or back spectr (spectr_back) in alpha scale
to start calibration.
Instruction:
    1. Specify calibration channel region: xmin, xmax.
    2. Specify strips (number) of spectrs to calibrate: start, stop.
       Calibration will be performed for strips from start to stop.
    3. Specify output_filename for output.
    4. Run this script to perform calibration.
    5. Variate calibration properties for better peak recognition.
        Most important parameters are:
        * calibration_properties.energies
        * filter_properties.threshold 
        * filter_properties.window_smooth 
    6. Repeat until you have all strips calibrated
        48 front strip or 128 back strips.
        
@author: eastwood
"""

import os

import numpy as np

import data_processingGNS2021.tools.calibration as clbr
   
#### AUTO CALIBRATE FRONT OR BACK STRIPS IN ALPHA-SCALE ######
    
# SPECIFY PARAMETERS:
hist = spectr_front # spectr_back
xmin, xmax = 1300, 2100
start, stop = 1, 48 # choose strips 
#output_filename = '/home/eastwood/gitlab_projects/data/digital/Oct_2019/Coefs/back_alpha.txt'
#output_filename = '/home/eastwood/gitlab_projects/data/analogue/Oct_2019/YtAr40/Coef1/back_alpha.txt'
#output_filename = False # specify filename like 'back_clbr_Dec28016.txt' or None 
### EXAMPLE: output_filename = 'data/calibration_coefficients/Dec_2017_Yuri_analog/front_alpha.txt'
#output_filename = '/home/eastwood/gitlab_projects/data/calibration_coefficients/Sep_2019_Sim_digital/back_alpha.txt'

output_filename = os.path.join(COEFS_FOLDER, 'front_alpha.txt')
#'/home/eastwood/gitlab_projects/data/calibration_coefficients/YtCa_Feb2020_digit/front_alpha.txt'
#output_filename = None

class record: pass
    
def get_calibration_properties():#set calibration parameters
    
    calibration_properties = record()
    calibration_properties.visualize = False
    calibration_properties.weighted_average_sigma = False
    
    # параметр для определения области соответствия расчетного положения пиков 
    # (соотв. пропорции калибровочных энергий) и реальных найденных пиков 
    # (метод spectrum_tools.search_peaks), если реальные пики не находятся вблизи расчетных 
    # в области dlt, то они заменяются на расчетные, в противном случае выбирается пик
    # наиболее близкий к расчетному. see calibration.calibrate_area
    calibration_properties.dlt = 27 
    
    # SPECIFY calibration energies
    # EXAMPLE: calibration_properties.energies = [6040,6143,6264,6899.2,7137,7922,8699,9261] 
    calibration_properties.energies = [6263, 6419, 7008, 7133] # ArYb
    #[6040,6143,6264,6899.2,7137,7922,8699,9261]
    # [5115.2, 5223.3, 5304.3, 7200.3, 7862., 8404.3] # PbCa
        #[6899.2,7137,7922,8699,9261] YtCa
        #[6039, 6624, 6731, 6899.2, 7137, 7922, 8699, 9261] # simulated analogue spectrum YbCa
    # [6030, 6143, 6258.8, 6625, 6732,6899.2, 7922, 9261]
            
    search_properties = record() #for noisy spectrum
    search_properties.widths = np.arange(1, 5)
    search_properties.wavelet = 'blackman'
    search_properties.min_length = 1.
    search_properties.min_snr = 0.6
    search_properties.noise_perc = 0.1 
    
    filter_properties = record()
    filter_properties.window_smooth = 7
    filter_properties.smooth_wavelet = 'blackman'
    filter_properties.background_options = 'BACK1_ORDER8' #,BACK1_INCLUDE_COMPTON' 
    filter_properties.threshold = 1.0
    return calibration_properties, search_properties, filter_properties
    
calibration_properties, search_properties, filter_properties = get_calibration_properties()
#
### --> START CALIBRATION: 
xpeaks, coefs, good_results = clbr.calibrate_spectr(start, stop, xmin, xmax,\
    hist.data, calibration_properties, filter_properties, \
    search_properties, output_filename=output_filename)
