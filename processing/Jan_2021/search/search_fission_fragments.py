# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 01:02:24 2021

Search for fission fragments in given data table generator consuquence
using data of existing chains.

Instruction:
    1. Get data table as dt_generator from 
       <project_folder>/data_processing/read_data/generator_....py 
    2. Get FILENAMES list from the generator_....py
    3. Specify parameters for search as 
        list of cells for search: 
        [(cell_x, cell_y, filename_start, time_start)]
        , where
            * cell_x - int, front strip position
            * cell_y - int, back strip position
            * filename_start - str, file where the chain was found
            * time - time in the file where the chain was found
    4. Run this script to perform search
"""

import os 

from data_processingGNS2021.tools.correlations_functions import find_fission, \
    NoneInputError, ZERO_INPUT_MSG
    
    
REPORT_STR = \
  """FISSION[{energy:<6.0f} KeV]({time}) pos: {pos}; \
     f_start: {fstart}; f_stop: {fstop}"""
    
    
def file_cmp(filename1, filename2):
    key = lambda fname: int(fname.split('.')[1])
    return key(filename1) > key(filename2)

def get_time(data_table):
     # events are ordered by time and starts from 0
    return data_table['time_micro'][-1]

def calc_time(time):
    """
    Calculates the time elapsed from the start chain event
    and return result in pretty looking h:m:s str format.

    """
    time_sec = time // 10**6
    h = time_sec // 3600
    m = (time_sec % 3600) // 60
    s = time_sec % 60
    
    if not h:
        h = '00'
    if not m:
        m = '00'
    if not s:
        s = '00'
        
    return "{0} h:{1} min:{2} s".format(h, m, s)
    
if __name__ == "__main__":
    
    LIST_TO_SEARCH = [
        (42, 42, "Am+Ca.91", 8665944589),
        (10, 92, "Am+Ca.106", 14604127863),
        (30, 27, "Am+Ca.106", 4112047111),
        (11, 61, "Am+Ca.116", 15983185795),
        (23, 48, "Am+Ca.117", 26254868451),
        (29, 108, "Am+Ca.125", 19956998473),
        (4, 57, "Am+Ca.141", 23631323415),
        (44, 48, "Am+Ca.141", 26428098240),
        (29, 30, "Am+Ca.147", 12991347551),
        (13, 60, "Am+Ca.150", 17935976247),
        (31, 73, "Am+Ca.150", 4810612358),
        (42, 71, "Am+Ca.153", 5036686629),
        (32, 51, "Am+Ca.155", 5536688131),
        (48, 51, "Am+Ca.156", 22251331050),
        (39, 16, "Am+Ca.156", 7888043089),
        (25, 69, "Am+Ca.161", 18416644881),
        (26, 10, "Am+Ca.171", 31385154114),  
        (22, 35, "Am+Ca.189", 14698770487),
        (8, 34, "Am+Ca.236", 13061337976),
    ]
    
    time_sum = 0
    results = []
    filenames = [os.path.basename(fname) for fname in FILENAMES]
    start_times = {item[2]: item[3] for item in LIST_TO_SEARCH}
    
    for filename, dt in zip(filenames, dt_generator):
        if dt is None:
            continue
        
        # calculate the start time of the first chain event
        if filename in start_times:
            start_times[filename] += time_sum
        
        # search for each fission in the search list
        for cell_x, cell_y, filename_chain, time_start in LIST_TO_SEARCH:
            if file_cmp(filename_chain, filename):
                continue
            
            result = find_fission(dt, cell_x, cell_y, time=time_start)
            if len(result) > 0:
                
                for event in result:
                    # energy of the fission fragment is the largest energy
                    if event['energy_front'] >= event['energy_back']:
                        energy = event['energy_front']
                    else:
                        energy = event['energy_back']
                    
                    time = calc_time(event['time_micro'] + time_sum 
                                     - start_times[filename_chain])
                    pos = f"({cell_x}; {cell_y})"
                    results.append(
                      (event['energy_front'], time, pos, filename_chain, filename)
                    )
        
        time_sum += get_time(dt)
        
    # print the report
    for energy_front, time, pos, file_start, file_stop in results:
        print('-' * 50, '\n', 'REPORT: ')
        print(REPORT_STR.format(
            energy=energy_front,
            time=time,
            pos=pos,
            fstart=file_start,
            fstop=file_stop,
          )
        )
                
        
        