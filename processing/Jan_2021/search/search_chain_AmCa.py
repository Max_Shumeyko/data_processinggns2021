# -*- coding: utf-8 -*-
"""
Search for full chains (Recoil-alpha-alpha-...-fission fragment) correlations.

Instruction:
    1. Get data table as dt_generator from 
       <project_folder>/data_processing/read_data/generator_....py 
    2. Specify parameters for search:
        * time_dlt : <int> time in microseconds between two consecutive events;
        * chain_length_min : <int> minimal length of chains;
        * chain_length_max : <int> maximal length of chains; 
                           If has default value -1 -> the upper limit will set
                           to 50;
        * recoil_first : <c_bool> check if first event in chain is recoil;
    3. Run this script to perform search
    
Output:
    dict with lists of chains:
    dict: [(cell_x, cell_y)] -> [np.ndarray[DATA_EVENT]]
    
------------------------------------------------------------------------------
Main searh function:
    find_chains(data_table, energy_bundle, search_properties)
    
Search time correlated decay chains using search_properties and
    collect them into cell dict.
    
    [DATA_EVENT], [(float, float, str)], obj -> 
    -> defaultdict{[cell_x, cell_y): [np.ndarray[DATA_EVENT]]}
    
    Input: 
        
    data_table (np.ndarray, dtype=DATA_EVENT)
    
    energy_bundle - list of energy limits in KeV, like  [(6620, 6740, 'a'), (9200, 9340, 'a'), ]
       energy_min, energy_max - energy limits in KeV
       event_type - recoil or alpha event type: 'a': alpha particle, 
       'R': recoil nuclei, 'F': fission fragment, 'S': side only events
       
    search_properties - search structure with parameters:
        time_dlt : <int> time in microseconds between two consecutive events;
        chain_length_min : <int> minimal length of chains;
        chain_length_max : <int> maximal length of chains; 
                           If has default value -1 -> the upper limit will set
                           to 50;
        recoil_first : <c_bool> check if first event in chain is recoil;
        search_side: <c_bool> check if to include side-only events to the 
                     chain
        search_fission : <c_bool> check if search includes fission decay 
                         events (energies > 40000.)
                
    Return: cell dict with list of chains
    
    ***    
    Example:
    --------    
    class record:
        pass

    search_properties = record()
    
    #### FIND R-a (recoil-alpha pairs)
    search_properties.time_dlt = 26000000
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_first = True
    search_properties.search_fission = False
    energy_bundle = [(6620, 6740, 'a'), (9200, 9340, 'a'), ]
        
    dt = next(dt_generator)
    chains = find_chains(dt, energy_bundle, search_properties)
    
    # get data table indexes from given cell
    cell_x = 24
    cell_y = 48
    print chains[(cell_x, cell_y)]
    
@author: eastwood

"""
import os
import datetime

import numpy as np

import matplotlib.pyplot as plt

from data_processingGNS2021.tools.correlations_functions import find_chains, \
    NoneInputError, ZERO_INPUT_MSG


class record:
    pass


def get_chain_lengthes(chain):
    res = []
    for i, j in chain.items():
        res.append(len(j[0]))
    return res

def get_time_start(filename):
    """
    str -> datetime.datetime obj

    Get full-path filename and return it's creation time as datetime.

    """
    creation_time = os.stat(filename).st_mtime
    return datetime.datetime.fromtimestamp(creation_time)

def stamp_to_str(timestamp):
    return timestamp.strftime("%H:%M:%S %d-%m-%Y")

def get_event_type(event):
    if (event['energy_front'] > 50000) or (event['energy_back'] > 50000):
        return "Fission"
    elif (event['tof']):
        return "Recoil"
    else:
        if event['event_type'] == 3:
            return "Side Alpha"
        return "Alpha"
    
def format_dlt_t(t):
    """
    int -> str

    Convert int microseconds to str formatted time.

    """
    l = len(str(t))
    if l <= 3:
        return f"{t} μs"
    elif l <= 6:
        return f"{t // 10**3} ms"
    elif l <= 8:
        return f"{round(t / 10**6, 1)} s"
    else:
        return f"{t // 10**6} s"

def print_chain(fname, time_start, pos, chain):
    print('-' * 50, '\nFILENAME: ', os.path.basename(fname), '\n')
    print('POSITION: ', *pos)
    
    # calc the start time of the chain
    chain_start = time_start + \
      datetime.timedelta(seconds=
                         int(chain[0]['time_micro'] // 10**6))
    chain_start = stamp_to_str(chain_start)
    print(f"Time start :: {chain_start}")
    
    t0 = chain[0]['time_micro']
    print(f"Time in microseconds from the beginning: {t0}")
    N = len(chain)
    for i in range(N)[1:]:
        dlt_t = chain['time_micro'][i] - t0
        dlt_t = format_dlt_t(dlt_t)
        t0 = chain['time_micro'][i]
        event_type = get_event_type(chain[i-1])
        energy = int(chain[i-1]['energy_front'])
        print(f"{event_type}[{energy} KeV]({dlt_t})  -->")
    event_type = get_event_type(chain[i])
    energy = int(chain[i]['energy_front'])
    print(f"{event_type}[{energy} KeV]")   
    
def print_report(result_dict):
    print('-' * 50, '\n', 'CHAINS FOUND REPORT: \n')
    for fname, sub_dict in result_dict.items(): # iterate over files and reslts     
        time_start = get_time_start(fname)
        
        for pos, arr in sub_dict.items(): # iterate over cells
            for chain in arr: # iterate over chains in each cell
               print_chain(fname, time_start, pos, chain) 
    
if __name__ == "__main__":
    
    search_properties = record()

    #### FIND R-a (recoil-alpha pairs)
    search_properties.time_dlt = 85 * 10**6 # microseconds
    search_properties.chain_length_min = 3
    search_properties.chain_length_max = 15
    search_properties.recoil_first = True
    search_properties.search_side = True
    search_properties.search_fission = True
    
    # energy_bundle = [(4000, 20000, 'R'), (9300, 11500, 'a'), ]
    energy_bundle = [
        (6000, 25000, 'R'), (7000, 14600, 'a'), 
        (8000, 13000, 'S'), (40000, 300000, 'F')
    ] #  
    
    bs = os.path.basename
    result_dict = {}

    for fname, dt in zip(FILENAMES, dt_generator):
        if dt is None:
            continue
        
        try:
            res = find_chains(dt, energy_bundle, search_properties)
        except NoneInputError as e:
            print(e)
            continue
        except AssertionError as e:
            if str(e) == ZERO_INPUT_MSG:
                print(e)
                continue
            else:
                raise(e)
        
        if not res:
            continue
        # flag = False
        # if max(get_chain_lengthes(res)) > 2:
        #     flag = True
        # if not flag:
        #     continue      
        
        result_dict[fname] = res
        print(f"CHAINS FOUND IN {bs(fname)}")
     
    print_report(result_dict)
